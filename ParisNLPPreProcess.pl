#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

$| = 1;

my $udpipedsource = shift || die "Please provide absolute path to UDPipe'd input corpus";
my $tagmodeldir = shift || "/media/data/tagmodels";
my $tokmodeldir = shift || "/media/data/tokmodels";
my $aldir = shift || "/home/ParisNLP/alanalyser";

die unless -d $tagmodeldir;
die unless -d $tokmodeldir;
die unless -d $aldir;

my $corpus = $udpipedsource;
$corpus =~ s/^.*\///;
$corpus =~ s/-.*//;
my $lang = $corpus;
$lang =~ s/_.*//;

my ($tokeniser, $tokmodel, $tagger, $tagmodel);

$tokeniser = "cat";

for (<$tagmodeldir/*>) {
  s/^.*\///;
  if (/^$corpus/) {
    $tagmodel = $_;
    last;
  } elsif (/^$lang-/) {
    $tagmodel = $_;
  }
}
if ($tagmodel ne "") {
  $tagger = "perl $aldir/alVWTagger.pl -m $tagmodeldir/$tagmodel -f conll";
} else {
  $tagger = "cat";
}

open PIPE, "| $tokeniser | $tagger" || die $!;
binmode PIPE, ":utf8";
while (<>) {
  print PIPE $_;
}
