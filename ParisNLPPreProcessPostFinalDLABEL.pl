#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

$| = 1;

my $udpipedsource = shift || die "Please provide absolute path to UDPipe'd input corpus";
my $tagmodeldir = shift || "/media/data/tagmodels";
my $tokmodeldir = shift || "/media/data/tokmodels";
my $aldir = shift || "/home/ParisNLP/alanalyser";
my $forcemode = shift || "";
$forcemode = lc($forcemode);

die unless -d $tagmodeldir;
die unless -d $tokmodeldir;
die unless -d $aldir;

my $corpus = $udpipedsource;
$corpus =~ s/^.*\///;
$corpus =~ s/-.*//;
my $lang = $corpus;
$lang =~ s/_.*//;

my ($tokeniser, $tokmodel, $tagger, $tagmodel);

# TOKENISATION
$tokeniser = "cat";# Défaut (modes PP1 et UDPipe): on garde la tokenisation et la segmentation en phrases de UDPipe
$tokmodel = "$tokmodeldir/alTokModel.$corpus";
if ($corpus =~ /^(et|fi|it|it_pud|la_ittb|fi_ftb|cu|fa|got|ar_pud|ar|hu)$/) {
  # mode PP2, sous-cas où BS tokenise et segmente en phrases
  if (-d $tokmodel) {
    $tokeniser = "perl $aldir/alCoNLLU2Tok.pl | perl $aldir/alTok2Txt.pl | perl $aldir/alVWTokeniser.pl -m $tokmodel | perl $aldir/alTok2CoNLLU.pl | perl $aldir/alAddFakeHeadsInCoNLL.pl";
    $tokeniser .= " | perl $aldir/alCoNLLReinjectWordsAndLemmas.pl $udpipedsource" if $udpipedsource =~ /^\//;
  }
} elsif ($corpus =~ /^(da|lv|ja_pud|ja|vi)$/) {
  # mode PP2, sous-cas où BS tokenise mais où on récupère la segmentation en phrases d'UDPipe
  if (-d $tokmodel) {
    $tokeniser = "perl $aldir/alCoNLLU2Tok.pl | perl $aldir/alTok2Txt.pl | perl $aldir/alVWTokeniser.pl -m $tokmodel -no_s | perl $aldir/alTok2CoNLLU.pl | perl $aldir/alAddFakeHeadsInCoNLL.pl";
    $tokeniser .= " | perl $aldir/alCoNLLReinjectWordsAndLemmas.pl $udpipedsource" if $udpipedsource =~ /^\//;
  }
}

# TAGGING
for (<$tagmodeldir/*>) {
  s/^.*\///;
  if (/^$corpus/) {
    $tagmodel = $_;
    last;
  } elsif (/^$lang-/) {
    $tagmodel = $_;
  }
}
#if ($tagmodel eq "ru" && $corpus ne "ru") {
#  $tagmodel = "ru_syntagrus";
#}
if ($corpus =~ /^(cs|cs_cac|cs_pud|pt|it_partut|zh|grc|grc_proiel|id|gl_treegal|la|sl_sst|kmr|bxr|sme|hsb)$/) {
  # Mode UDPipe: on garde les tags UDPipe
  $tagger = "cat";
} elsif ($tagmodel ne "") {
  # Modes PP1 et PP2: BS re-taggue (UPOS + MSTAG) - les lemmes et XPOS de UDPipe sont donc conservés
  $tagger = "perl $aldir/alVWTagger.pl -m $tagmodeldir/$tagmodel -f conll";
} else {
  # défaut: si pas de modèle de tagging trouvé, on garde les tags UDPipe
  $tagger = "cat";
}

open PIPE, "| $tokeniser | $tagger" || die $!;
binmode PIPE, ":utf8";
while (<>) {
  print PIPE $_;
}

