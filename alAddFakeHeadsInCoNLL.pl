#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

while (<>) {
  chomp;
  if (/^(\d+)(\t[^\t]+\t[^\t]+\t[^\t]+\t[^\t]+\t[^\t]+\t)_(\t.*)$/) {
    if ($1 == 1) {
      print $1.$2."0".$3."\n";
    } else {
      print $1.$2."1".$3."\n";
    }
  } else {
    print $_."\n";
  }
}
