#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

my (%flt, %labels);
my $l;
while (<>) {
  print STDERR "\r".(++$l);
  chomp;
  if (/^([^\t]+)\t([^\t]+)\t([^\t]+)$/) {
    $flt{$1}{$3}{$2} = 1;
    $labels{$2} = 1;
  } elsif (/^([^\t]+)\t([^\t]+)$/) {
    my ($w, $ct) = ($1,lc($2));
    next if $w =~ /^[0-9\.\-\$–]+$/;
    $ct =~ s/^\+/$w\+/;
    $ct =~ s/\+[^\+]+\/[^\+]+//g;
    $ct =~ s/^\+*([^\+]+)(?:\++(.*))?$/$2/ || die "|$ct|$w";
    my $l = $1;
    $ct = "x" if $ct eq "";
    $ct =~ s/^([^\+]+)(?:\+(.*))?$/$1#$2/ || die "||$ct||$w";
    $flt{$w}{$l}{$ct} = 1;
    $labels{$ct} = 1;
  } else {
    next;
  }
}
print STDERR "\n";

my ($origt, $t, $cat, %tags, $udcat, %udtags, %t2udt);
for $origt (keys %labels) {
  $origt =~ /^([^#]+)(?:#(.*))?$/ || die $t;
  $cat = $1;
  $t = $2;
  %tags = ();
  %udtags = ();
  $udcat = "";
  for (split /[\.\+]/, $t) {
    $tags{$_} = 1;
  }
  if ($cat =~ /^(sent|rpar|lpar|guio|cm|quot|apos|rquot|lquot|punct)$/) {
    $udcat = "PUNCT";
  } elsif ($cat =~ /^(title|n)$/) {
    $udcat = "NOUN";
  } elsif ($cat =~ /^(num)$/) {
    $udcat = "NUM";
  } elsif ($cat =~ /^(adj|a)$/) {
    $udcat = "ADJ";
  } elsif ($cat =~ /^(np)$/) {
    $udcat = "PROPN";
  } elsif ($cat =~ /^(adv|cnjadv|preverb|postverb|postadv|preadv|pred|predet|pres|pref)$/) {
    $udcat = "ADV";
  } elsif ($cat =~ /^(cnjcoo|cc)$/) {
    $udcat = "CCONJ";
  } elsif ($cat =~ /^(cnjsub|cs)$/) {
    $udcat = "SCONJ";
  } elsif ($cat =~ /^(ij|interj)$/) {
    $udcat = "INTJ";
  } elsif ($cat =~ /^(v\d*|vbser|vbhaver|vblex|vbmod|vpart|vbloc|vrbnull|vbcop|vbsint|vbavea|vblexestar)$/) {
    if ($t =~ /(^|\.)(subst?)(\.|$)/) {
      $udcat = "NOUN";
    #    } elsif ($t =~ /(^|\.)(px[123](sg|pl)|nom|acc|gen|dat|ins|loc)(\.|$)/) {
    #      $udcat = "NOUN";
    } else {
      $udcat = "VERB";
    }
  } elsif ($cat =~ /^(part|pcle)$/) {
    $udcat = "PART";
  } elsif ($cat =~ /^(post|pr|atp|pprep)$/) {
    $udcat = "ADP";
  } elsif ($cat =~ /^(qst|vaux)$/) {
    $udcat = "AUX";
  } elsif ($cat =~ /^(pro?n|rel|clit)$/) {
    $udcat = "PRON";
    $udtags{"PronType=Rel"} = 1 if $cat eq "rel";
  } elsif ($cat =~ /^(det|detnt|dem)$/) {
    $udcat = "DET";
  } elsif ($cat =~ /^(abbr)$/) {
    $udcat = "X";
  } elsif ($cat =~ /^(gen|fut|postnum|past|pros|x)$/) {
    $udcat = "X";
  } else {
    print STDERR "New input cat '$cat'\n";
    next;
  }
  #$udtags{""} = 1 if defined $tags{"adj"};
  #$udtags{""} = 1 if defined $tags{"cmp"};
  #$udtags{""} = 1 if defined $tags{"mix"};
  #$udtags{""} = 1 if defined $tags{"st"};
  #$udtags{""} = 1 if defined $tags{"sw"};
  #$udtags{""} = 1 if defined $tags{"top"};
  $udtags{"Animacy=Anim"} = 1 if defined $tags{"aa"};
  $udtags{"Animacy=Anim"} = 1 if defined $tags{"ma"};
  $udtags{"Animacy=Hum"} = 1 if defined $tags{"mp"};
  $udtags{"Animacy=Inan"} = 1 if defined $tags{"mi"};
  $udtags{"Animacy=Inan"} = 1 if defined $tags{"nn"};
  $udtags{"Aspect=Perf"} = 1 if defined $tags{"perf"};
  $udtags{"Case=Abe"} = 1 if defined $tags{"abe"};
  $udtags{"Case=Abl"} = 1 if defined $tags{"abl"};
  $udtags{"Case=Acc"} = 1 if defined $tags{"acc"};
  $udtags{"Case=Acc"} = 1 if defined $tags{"obl"};
  $udtags{"Case=Ade"} = 1 if defined $tags{"ade"};
  $udtags{"Case=All"} = 1 if defined $tags{"all"};
  $udtags{"Case=Com"} = 1 if defined $tags{"com"};
  $udtags{"Case=Dat"} = 1 if defined $tags{"dat"};
  $udtags{"Case=Dis"} = 1 if defined $tags{"dis"};
  $udtags{"Case=Ess"} = 1 if defined $tags{"ess"};
  $udtags{"Case=Ess"} = 1 if defined $tags{"prl"};
  $udtags{"Case=Gen"} = 1 if defined $tags{"gen"};
  $udtags{"Case=Ill"} = 1 if defined $tags{"ill"};
  $udtags{"Case=Ine"} = 1 if defined $tags{"ine"};
  $udtags{"Case=Ins"} = 1 if defined $tags{"ins"};
  $udtags{"Case=Loc"} = 1 if defined $tags{"loc"};
  $udtags{"Case=Nom"} = 1 if defined $tags{"nom"};
  $udtags{"Case=Par"} = 1 if defined $tags{"par"};
  $udtags{"Case=Tra"} = 1 if defined $tags{"tra"};
  $udtags{"Case=Voc"} = 1 if defined $tags{"voc"};
  $udtags{"Definite=Def"} = 1 if defined $tags{"def"};
  $udtags{"Definite=Ind"} = 1 if defined $tags{"ind"};
  $udtags{"Degree=Cmp"} = 1 if defined $tags{"comp"};
  $udtags{"Degree=Sup"} = 1 if defined $tags{"sup"};
  $udtags{"Gender=Com"} = 1 if defined $tags{"ut"};
  $udtags{"Gender=Fem"} = 1 if defined $tags{"f"};
  $udtags{"Gender=Masc"} = 1 if defined $tags{"m"};
  $udtags{"Gender=Masc"} = 1 if defined $tags{"ma"};
  $udtags{"Gender=Masc"} = 1 if defined $tags{"mi"};
  $udtags{"Gender=Masc"} = 1 if defined $tags{"mp"};
  $udtags{"Gender=Neut"} = 1 if defined $tags{"nt"};
  $udtags{"Mood=Cnd"} = 1 if defined $tags{"cni"};
  $udtags{"Mood=Imp"} = 1 if defined $tags{"imp"};
  $udtags{"Mood=Ind"} = 1 if defined $tags{"fti"};
  $udtags{"Mood=Ind"} = 1 if defined $tags{"ifi"};
  $udtags{"Mood=Ind"} = 1 if defined $tags{"pii"};
  $udtags{"Mood=Ind"} = 1 if defined $tags{"plu"};
  $udtags{"Mood=Ind"} = 1 if defined $tags{"pmp"};
  $udtags{"Mood=Ind"} = 1 if defined $tags{"pri"};
  $udtags{"Mood=Sub"} = 1 if defined $tags{"fts"};
  $udtags{"Mood=Sub"} = 1 if defined $tags{"pis"};
  $udtags{"Mood=Sub"} = 1 if defined $tags{"prs"};
  $udtags{"Number=Coll"} = 1 if defined $tags{"coll"};
  $udtags{"Number=Dual"} = 1 if defined $tags{"dual"};
  $udtags{"Number=Plur"} = 1 if defined $tags{"pl"};
  $udtags{"Number=Sing"} = 1 if defined $tags{"sg"};
  $udtags{"Person=1"} = 1 if defined $tags{"p1"};
  $udtags{"Person=2"} = 1 if defined $tags{"p2"};
  $udtags{"Person=3"} = 1 if defined $tags{"p3"};
  $udtags{"Polarity=Neg"} = 1 if defined $tags{"neg"};
  $udtags{"Polarity=Pos"} = 1 if defined $tags{"aff"};
  $udtags{"Tense=Fut"} = 1 if defined $tags{"fti"};
  $udtags{"Tense=Fut"} = 1 if defined $tags{"fts"};
  $udtags{"Tense=Imp"} = 1 if defined $tags{"impf"};
  $udtags{"Tense=Imp"} = 1 if defined $tags{"pii"};
  $udtags{"Tense=Imp"} = 1 if defined $tags{"pis"};
  $udtags{"Tense=Past"} = 1 if defined $tags{"ifi"};
  $udtags{"Tense=Past"} = 1 if defined $tags{"past"};
  $udtags{"Tense=Past"} = 1 if defined $tags{"pp"};
  $udtags{"Tense=Past"} = 1 if defined $tags{"pret"};
  $udtags{"Tense=Pqp"} = 1 if defined $tags{"plu"};
  $udtags{"Tense=Pqp"} = 1 if defined $tags{"pmp"};
  $udtags{"Tense=Pres"} = 1 if defined $tags{"cni"};
  $udtags{"Tense=Pres"} = 1 if defined $tags{"pprs"};
  $udtags{"Tense=Pres"} = 1 if defined $tags{"pres"};
  $udtags{"Tense=Pres"} = 1 if defined $tags{"pri"};
  $udtags{"Tense=Pres"} = 1 if defined $tags{"prs"};
  $udtags{"VerbForm=Ger"} = 1 if defined $tags{"ger"};
  $udtags{"VerbForm=Inf"} = 1 if defined $tags{"inf"};
  $udtags{"VerbForm=Part"} = 1 if defined $tags{"pp"};
  $udtags{"VerbForm=Part"} = 1 if defined $tags{"pprs"};
  $udtags{"VerbForm=Sup"} = 1 if defined $tags{"supn"};
  $udtags{"Voice=Act"} = 1 if defined $tags{"actv"};
  $udtags{"Voice=Cau"} = 1 if defined $tags{"caus"};
  $udtags{"Voice=Mid"} = 1 if defined $tags{"midv"};
  $udtags{"Voice=Pass"} = 1 if defined $tags{"pass"};
  $udtags{"Voice=Pass"} = 1 if defined $tags{"pasv"};
  for (1,2,3) {
    if (defined $tags{"px".$_."sg"}) {
      $udtags{"Number[psor]=Sing"} = 1;
      $udtags{"Person[psor]=$_"} = 1;
    }
    if (defined $tags{"px".$_."pl"}) {
      $udtags{"Number[psor]=Plur"} = 1;
      $udtags{"Person[psor]=$_"} = 1;
    }
  }
  die if $udcat eq "";
  $t2udt{$origt} = $udcat."#".(join "|", sort {$a cmp $b} keys %udtags);
}

for my $f (keys %flt) {
  for my $l (keys %{$flt{$f}}) {
    for my $t (keys %{$flt{$f}{$l}}) {
      print "$f\t$t2udt{$t}\t$l\n";
    }
  }
}
