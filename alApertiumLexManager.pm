package alCPunct;
use utf8;
use strict;
use alVariables;
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(%apertiumlangcode2langcode &apertium_detranslitterate);

our %apertiumlangcode2langcode = (
			      "afr" => "af",
			      "ara" => "ar",
			      "arg" => "an",
			      "ast" => "ast",
			      "bel" => "be",
			      "ben" => "bn",
			      "bre" => "br",
			      "bul" => "bg",
			      "cat" => "ca",
			      "ces" => "cs",
			      "cos" => "co",
			      "cym" => "cy",
			      "dan" => "da",
			      "deu" => "de",
			      "ell" => "el",
			      "eng" => "en",
			      "eus" => "eu",
			      "fao" => "fo",
			      "fra" => "fr",
			      "gla" => "gd",
			      "glg" => "gl",
			      "glv" => "gv",
			      "heb" => "he",
			      "hin" => "hi",
			      "hye" => "hy",
			      "ind" => "id",
			      "isl" => "is",
			      "ita" => "it",
			      "kmr" => "kmr",
			      "ltz" => "lb",
			      "lvs" => "lv",
			      "mar" => "mr",
			      "mkd" => "mk",
			      "mlt" => "mt",
			      "nld" => "nl",
			      "nno" => "no",
			      "nob" => "nb",
			      "pol" => "pl",
			      "por" => "pt",
			      "ron" => "ro",
			      "rus" => "ru",
			      "san" => "sa",
			      "scn" => "scn",
			      "slv" => "si",
			      "spa" => "es",
			      "sqi" => "sq",
			      "srd" => "srd",
			      "swe" => "sv",
			      "tet" => "tet",
			      "ukr" => "uk",
			      "urd" => "ur",
			      "zho" => "zh",
			      "zlm" => "ms",
			     );


sub apertium_detranslitterate {
  my $s = shift;
  my $lang = shift || "";
  return $s unless $lang eq "sa";
  $s =~ s/aM/अं/g;
  $s =~ s/AM/आं/g;
  $s =~ s/iM/इं/g;
  $s =~ s/IM/ईं/g;
  $s =~ s/uM/उं/g;
  $s =~ s/UM/ऊं/g;
  $s =~ s/qM/ऋं/g;
  $s =~ s/QM/अं/g;
  $s =~ s/LM/ीं/g;
  $s =~ s/lM/ऌं/g;
  $s =~ s/eM/एं/g;
  $s =~ s/EM/ऐं/g;
  $s =~ s/oM/ओं/g;
  $s =~ s/OM/औं/g;
  $s =~ s/aH/अः/g;
  $s =~ s/AH/आः/g;
  $s =~ s/iH/इः/g;
  $s =~ s/IH/ईः/g;
  $s =~ s/uH/उः/g;
  $s =~ s/UH/ऊः/g;
  $s =~ s/qH/ऋः/g;
  $s =~ s/QH/अः/g;
  $s =~ s/LH/ीः/g;
  $s =~ s/lH/ऌः/g;
  $s =~ s/eH/एः/g;
  $s =~ s/EH/ऐः/g;
  $s =~ s/oH/ओः/g;
  $s =~ s/OH/औः/g;
  $s =~ s/az/अँ/g;
  $s =~ s/Az/आँ/g;
  $s =~ s/iz/इँ/g;
  $s =~ s/Iz/ईँ/g;
  $s =~ s/uz/उँ/g;
  $s =~ s/Uz/ऊँ/g;
  $s =~ s/qz/ऋँ/g;
  $s =~ s/Qz/अँ/g;
  $s =~ s/Lz/ीँ/g;
  $s =~ s/lz/ऌँ/g;
  $s =~ s/ez/एँ/g;
  $s =~ s/Ez/ऐँ/g;
  $s =~ s/oz/ओँ/g;
  $s =~ s/Oz/औँ/g;
  $s =~ s/ka/क/g;
  $s =~ s/kA/का/g;
  $s =~ s/ki/कि/g;
  $s =~ s/kI/की/g;
  $s =~ s/ku/कु/g;
  $s =~ s/kU/कू/g;
  $s =~ s/kq/कृ/g;
  $s =~ s/kQ/कॄ/g;
  $s =~ s/kL/कॢ/g;
  $s =~ s/ke/के/g;
  $s =~ s/kE/कै/g;
  $s =~ s/ko/को/g;
  $s =~ s/kO/कौ/g;
  $s =~ s/Ka/ख/g;
  $s =~ s/KA/खा/g;
  $s =~ s/Ki/खि/g;
  $s =~ s/KI/खी/g;
  $s =~ s/Ku/खु/g;
  $s =~ s/KU/खू/g;
  $s =~ s/Kq/खृ/g;
  $s =~ s/KQ/खॄ/g;
  $s =~ s/KL/खॢ/g;
  $s =~ s/Ke/खे/g;
  $s =~ s/KE/खै/g;
  $s =~ s/Ko/खो/g;
  $s =~ s/KO/खौ/g;
  $s =~ s/ga/ग/g;
  $s =~ s/gA/गा/g;
  $s =~ s/gi/गि/g;
  $s =~ s/gI/गी/g;
  $s =~ s/gu/गु/g;
  $s =~ s/gU/गू/g;
  $s =~ s/gq/गृ/g;
  $s =~ s/gQ/गॄ/g;
  $s =~ s/gL/गॢ/g;
  $s =~ s/ge/गे/g;
  $s =~ s/gE/गै/g;
  $s =~ s/go/गो/g;
  $s =~ s/gO/गौ/g;
  $s =~ s/Ga/घ/g;
  $s =~ s/GA/घा/g;
  $s =~ s/Gi/घि/g;
  $s =~ s/GI/घी/g;
  $s =~ s/Gu/घु/g;
  $s =~ s/GU/घू/g;
  $s =~ s/Gq/घृ/g;
  $s =~ s/GQ/घॄ/g;
  $s =~ s/GL/घॢ/g;
  $s =~ s/Ge/घे/g;
  $s =~ s/GE/घै/g;
  $s =~ s/Go/घो/g;
  $s =~ s/GO/घौ/g;
  $s =~ s/fa/ङ/g;
  $s =~ s/fA/ङा/g;
  $s =~ s/fi/ङि/g;
  $s =~ s/fI/ङी/g;
  $s =~ s/fu/ङु/g;
  $s =~ s/fU/ङू/g;
  $s =~ s/fq/ङृ/g;
  $s =~ s/fQ/ङॄ/g;
  $s =~ s/fL/ङॢ/g;
  $s =~ s/fe/ङे/g;
  $s =~ s/fE/ङै/g;
  $s =~ s/fo/ङो/g;
  $s =~ s/fO/ङौ/g;
  $s =~ s/ca/च/g;
  $s =~ s/cA/चा/g;
  $s =~ s/ci/चि/g;
  $s =~ s/cI/ची/g;
  $s =~ s/cu/चु/g;
  $s =~ s/cU/चू/g;
  $s =~ s/cq/चृ/g;
  $s =~ s/cQ/चॄ/g;
  $s =~ s/cL/चॢ/g;
  $s =~ s/ce/चे/g;
  $s =~ s/cE/चै/g;
  $s =~ s/co/चो/g;
  $s =~ s/cO/चौ/g;
  $s =~ s/Ca/छ/g;
  $s =~ s/CA/छा/g;
  $s =~ s/Ci/छि/g;
  $s =~ s/CI/छी/g;
  $s =~ s/Cu/छु/g;
  $s =~ s/CU/छू/g;
  $s =~ s/Cq/छृ/g;
  $s =~ s/CQ/छॄ/g;
  $s =~ s/CL/छॢ/g;
  $s =~ s/Ce/छे/g;
  $s =~ s/CE/छै/g;
  $s =~ s/Co/छो/g;
  $s =~ s/CO/छौ/g;
  $s =~ s/ja/ज/g;
  $s =~ s/jA/जा/g;
  $s =~ s/ji/जि/g;
  $s =~ s/jI/जी/g;
  $s =~ s/ju/जु/g;
  $s =~ s/jU/जू/g;
  $s =~ s/jq/जृ/g;
  $s =~ s/jQ/जॄ/g;
  $s =~ s/jL/जॢ/g;
  $s =~ s/je/जे/g;
  $s =~ s/jE/जै/g;
  $s =~ s/jo/जो/g;
  $s =~ s/jO/जौ/g;
  $s =~ s/Ja/झ/g;
  $s =~ s/JA/झा/g;
  $s =~ s/Ji/झि/g;
  $s =~ s/JI/झी/g;
  $s =~ s/Ju/झु/g;
  $s =~ s/JU/झू/g;
  $s =~ s/Jq/झृ/g;
  $s =~ s/JQ/झॄ/g;
  $s =~ s/JL/झॢ/g;
  $s =~ s/Je/झे/g;
  $s =~ s/JE/झै/g;
  $s =~ s/Jo/झो/g;
  $s =~ s/JO/झौ/g;
  $s =~ s/Fa/ञ/g;
  $s =~ s/FA/ञा/g;
  $s =~ s/Fi/ञि/g;
  $s =~ s/FI/ञी/g;
  $s =~ s/Fu/ञु/g;
  $s =~ s/FU/ञू/g;
  $s =~ s/Fq/ञृ/g;
  $s =~ s/FQ/ञॄ/g;
  $s =~ s/FL/ञॢ/g;
  $s =~ s/Fe/ञे/g;
  $s =~ s/FE/ञै/g;
  $s =~ s/Fo/ञो/g;
  $s =~ s/FO/ञौ/g;
  $s =~ s/ta/ट/g;
  $s =~ s/tA/टा/g;
  $s =~ s/ti/टि/g;
  $s =~ s/tI/टी/g;
  $s =~ s/tu/टु/g;
  $s =~ s/tU/टू/g;
  $s =~ s/tq/टृ/g;
  $s =~ s/tQ/टॄ/g;
  $s =~ s/tL/टॢ/g;
  $s =~ s/te/टे/g;
  $s =~ s/tE/टै/g;
  $s =~ s/to/टो/g;
  $s =~ s/tO/टौ/g;
  $s =~ s/Ta/ठ/g;
  $s =~ s/TA/ठा/g;
  $s =~ s/Ti/ठि/g;
  $s =~ s/TI/ठी/g;
  $s =~ s/Tu/ठु/g;
  $s =~ s/TU/ठू/g;
  $s =~ s/Tq/ठृ/g;
  $s =~ s/TQ/ठॄ/g;
  $s =~ s/TL/ठॢ/g;
  $s =~ s/Te/ठे/g;
  $s =~ s/TE/ठै/g;
  $s =~ s/To/ठो/g;
  $s =~ s/TO/ठौ/g;
  $s =~ s/da/ड/g;
  $s =~ s/dA/डा/g;
  $s =~ s/di/डि/g;
  $s =~ s/dI/डी/g;
  $s =~ s/du/डु/g;
  $s =~ s/dU/डू/g;
  $s =~ s/dq/डृ/g;
  $s =~ s/dQ/डॄ/g;
  $s =~ s/dL/डॢ/g;
  $s =~ s/de/डे/g;
  $s =~ s/dE/डै/g;
  $s =~ s/do/डो/g;
  $s =~ s/dO/डौ/g;
  $s =~ s/Da/ढ/g;
  $s =~ s/DA/ढा/g;
  $s =~ s/Di/ढि/g;
  $s =~ s/DI/ढी/g;
  $s =~ s/Du/ढु/g;
  $s =~ s/DU/ढू/g;
  $s =~ s/Dq/ढृ/g;
  $s =~ s/DQ/ढॄ/g;
  $s =~ s/DL/ढॢ/g;
  $s =~ s/De/ढे/g;
  $s =~ s/DE/ढै/g;
  $s =~ s/Do/ढो/g;
  $s =~ s/DO/ढौ/g;
  $s =~ s/Na/ण/g;
  $s =~ s/NA/णा/g;
  $s =~ s/Ni/णि/g;
  $s =~ s/NI/णी/g;
  $s =~ s/Nu/णु/g;
  $s =~ s/NU/णू/g;
  $s =~ s/Nq/णृ/g;
  $s =~ s/NQ/णॄ/g;
  $s =~ s/NL/णॢ/g;
  $s =~ s/Ne/णे/g;
  $s =~ s/NE/णै/g;
  $s =~ s/No/णो/g;
  $s =~ s/NO/णौ/g;
  $s =~ s/wa/त/g;
  $s =~ s/wA/ता/g;
  $s =~ s/wi/ति/g;
  $s =~ s/wI/ती/g;
  $s =~ s/wu/तु/g;
  $s =~ s/wU/तू/g;
  $s =~ s/wq/तृ/g;
  $s =~ s/wQ/तॄ/g;
  $s =~ s/wL/तॢ/g;
  $s =~ s/we/ते/g;
  $s =~ s/wE/तै/g;
  $s =~ s/wo/तो/g;
  $s =~ s/wO/तौ/g;
  $s =~ s/Wa/थ/g;
  $s =~ s/WA/था/g;
  $s =~ s/Wi/थि/g;
  $s =~ s/WI/थी/g;
  $s =~ s/Wu/थु/g;
  $s =~ s/WU/थू/g;
  $s =~ s/Wq/थृ/g;
  $s =~ s/WQ/थॄ/g;
  $s =~ s/WL/थॢ/g;
  $s =~ s/We/थे/g;
  $s =~ s/WE/थै/g;
  $s =~ s/Wo/थो/g;
  $s =~ s/WO/थौ/g;
  $s =~ s/xa/द/g;
  $s =~ s/xA/दा/g;
  $s =~ s/xi/दि/g;
  $s =~ s/xI/दी/g;
  $s =~ s/xu/दु/g;
  $s =~ s/xU/दू/g;
  $s =~ s/xq/दृ/g;
  $s =~ s/xQ/दॄ/g;
  $s =~ s/xL/दॢ/g;
  $s =~ s/xe/दे/g;
  $s =~ s/xE/दै/g;
  $s =~ s/xo/दो/g;
  $s =~ s/xO/दौ/g;
  $s =~ s/Xa/ध/g;
  $s =~ s/XA/धा/g;
  $s =~ s/Xi/धि/g;
  $s =~ s/XI/धी/g;
  $s =~ s/Xu/धु/g;
  $s =~ s/XU/धू/g;
  $s =~ s/Xq/धृ/g;
  $s =~ s/XQ/धॄ/g;
  $s =~ s/XL/धॢ/g;
  $s =~ s/Xe/धे/g;
  $s =~ s/XE/धै/g;
  $s =~ s/Xo/धो/g;
  $s =~ s/XO/धौ/g;
  $s =~ s/na/न/g;
  $s =~ s/nA/ना/g;
  $s =~ s/ni/नि/g;
  $s =~ s/nI/नी/g;
  $s =~ s/nu/नु/g;
  $s =~ s/nU/नू/g;
  $s =~ s/nq/नृ/g;
  $s =~ s/nQ/नॄ/g;
  $s =~ s/nL/नॢ/g;
  $s =~ s/ne/ने/g;
  $s =~ s/nE/नै/g;
  $s =~ s/no/नो/g;
  $s =~ s/nO/नौ/g;
  $s =~ s/pa/प/g;
  $s =~ s/pA/पा/g;
  $s =~ s/pi/पि/g;
  $s =~ s/pI/पी/g;
  $s =~ s/pu/पु/g;
  $s =~ s/pU/पू/g;
  $s =~ s/pq/पृ/g;
  $s =~ s/pQ/पॄ/g;
  $s =~ s/pL/पॢ/g;
  $s =~ s/pe/पे/g;
  $s =~ s/pE/पै/g;
  $s =~ s/po/पो/g;
  $s =~ s/pO/पौ/g;
  $s =~ s/Pa/फ/g;
  $s =~ s/PA/फा/g;
  $s =~ s/Pi/फि/g;
  $s =~ s/PI/फी/g;
  $s =~ s/Pu/फु/g;
  $s =~ s/PU/फू/g;
  $s =~ s/Pq/फृ/g;
  $s =~ s/PQ/फॄ/g;
  $s =~ s/PL/फॢ/g;
  $s =~ s/Pe/फे/g;
  $s =~ s/PE/फै/g;
  $s =~ s/Po/फो/g;
  $s =~ s/PO/फौ/g;
  $s =~ s/ba/ब/g;
  $s =~ s/bA/बा/g;
  $s =~ s/bi/बि/g;
  $s =~ s/bI/बी/g;
  $s =~ s/bu/बु/g;
  $s =~ s/bU/बू/g;
  $s =~ s/bq/बृ/g;
  $s =~ s/bQ/बॄ/g;
  $s =~ s/bL/बॢ/g;
  $s =~ s/be/बे/g;
  $s =~ s/bE/बै/g;
  $s =~ s/bo/बो/g;
  $s =~ s/bO/बौ/g;
  $s =~ s/Ba/भ/g;
  $s =~ s/BA/भा/g;
  $s =~ s/Bi/भि/g;
  $s =~ s/BI/भी/g;
  $s =~ s/Bu/भु/g;
  $s =~ s/BU/भू/g;
  $s =~ s/Bq/भृ/g;
  $s =~ s/BQ/भॄ/g;
  $s =~ s/BL/भॢ/g;
  $s =~ s/Be/भे/g;
  $s =~ s/BE/भै/g;
  $s =~ s/Bo/भो/g;
  $s =~ s/BO/भौ/g;
  $s =~ s/ma/म/g;
  $s =~ s/mA/मा/g;
  $s =~ s/mi/मि/g;
  $s =~ s/mI/मी/g;
  $s =~ s/mu/मु/g;
  $s =~ s/mU/मू/g;
  $s =~ s/mq/मृ/g;
  $s =~ s/mQ/मॄ/g;
  $s =~ s/mL/मॢ/g;
  $s =~ s/me/मे/g;
  $s =~ s/mE/मै/g;
  $s =~ s/mo/मो/g;
  $s =~ s/mO/मौ/g;
  $s =~ s/ya/य/g;
  $s =~ s/yA/या/g;
  $s =~ s/yi/यि/g;
  $s =~ s/yI/यी/g;
  $s =~ s/yu/यु/g;
  $s =~ s/yU/यू/g;
  $s =~ s/yq/यृ/g;
  $s =~ s/yQ/यॄ/g;
  $s =~ s/yL/यॢ/g;
  $s =~ s/ye/ये/g;
  $s =~ s/yE/यै/g;
  $s =~ s/yo/यो/g;
  $s =~ s/yO/यौ/g;
  $s =~ s/ra/र/g;
  $s =~ s/rA/रा/g;
  $s =~ s/ri/रि/g;
  $s =~ s/rI/री/g;
  $s =~ s/ru/रु/g;
  $s =~ s/rU/रू/g;
  $s =~ s/rq/रृ/g;
  $s =~ s/rQ/रॄ/g;
  $s =~ s/rL/रॢ/g;
  $s =~ s/re/रे/g;
  $s =~ s/rE/रै/g;
  $s =~ s/ro/रो/g;
  $s =~ s/rO/रौ/g;
  $s =~ s/la/ल/g;
  $s =~ s/lA/ला/g;
  $s =~ s/li/लि/g;
  $s =~ s/lI/ली/g;
  $s =~ s/lu/लु/g;
  $s =~ s/lU/लू/g;
  $s =~ s/lq/लृ/g;
  $s =~ s/lQ/लॄ/g;
  $s =~ s/lL/लॢ/g;
  $s =~ s/le/ले/g;
  $s =~ s/lE/लै/g;
  $s =~ s/lo/लो/g;
  $s =~ s/lO/लौ/g;
  $s =~ s/va/व/g;
  $s =~ s/vA/वा/g;
  $s =~ s/vi/वि/g;
  $s =~ s/vI/वी/g;
  $s =~ s/vu/वु/g;
  $s =~ s/vU/वू/g;
  $s =~ s/vq/वृ/g;
  $s =~ s/vQ/वॄ/g;
  $s =~ s/vL/वॢ/g;
  $s =~ s/ve/वे/g;
  $s =~ s/vE/वै/g;
  $s =~ s/vo/वो/g;
  $s =~ s/vO/वौ/g;
  $s =~ s/Sa/श/g;
  $s =~ s/SA/शा/g;
  $s =~ s/Si/शि/g;
  $s =~ s/SI/शी/g;
  $s =~ s/Su/शु/g;
  $s =~ s/SU/शू/g;
  $s =~ s/Sq/शृ/g;
  $s =~ s/SQ/शॄ/g;
  $s =~ s/SL/शॢ/g;
  $s =~ s/Se/शे/g;
  $s =~ s/SE/शै/g;
  $s =~ s/So/शो/g;
  $s =~ s/SO/शौ/g;
  $s =~ s/Ra/ष/g;
  $s =~ s/RA/षा/g;
  $s =~ s/Ri/षि/g;
  $s =~ s/RI/षी/g;
  $s =~ s/Ru/षु/g;
  $s =~ s/RU/षू/g;
  $s =~ s/Rq/षृ/g;
  $s =~ s/RQ/षॄ/g;
  $s =~ s/RL/षॢ/g;
  $s =~ s/Re/षे/g;
  $s =~ s/RE/षै/g;
  $s =~ s/Ro/षो/g;
  $s =~ s/RO/षौ/g;
  $s =~ s/sa/स/g;
  $s =~ s/sA/सा/g;
  $s =~ s/si/सि/g;
  $s =~ s/sI/सी/g;
  $s =~ s/su/सु/g;
  $s =~ s/sU/सू/g;
  $s =~ s/sq/सृ/g;
  $s =~ s/sQ/सॄ/g;
  $s =~ s/sL/सॢ/g;
  $s =~ s/se/से/g;
  $s =~ s/sE/सै/g;
  $s =~ s/so/सो/g;
  $s =~ s/sO/सौ/g;
  $s =~ s/ha/ह/g;
  $s =~ s/hA/हा/g;
  $s =~ s/hi/हि/g;
  $s =~ s/hI/ही/g;
  $s =~ s/hu/हु/g;
  $s =~ s/hU/हू/g;
  $s =~ s/hq/हृ/g;
  $s =~ s/hQ/हॄ/g;
  $s =~ s/hL/हॢ/g;
  $s =~ s/he/हे/g;
  $s =~ s/hE/है/g;
  $s =~ s/ho/हो/g;
  $s =~ s/hO/हौ/g;

  $s =~ s/k/क्/g;
  $s =~ s/K/ख्/g;
  $s =~ s/g/ग्/g;
  $s =~ s/G/घ्/g;
  $s =~ s/f/ङ्/g;
  $s =~ s/c/च्/g;
  $s =~ s/C/छ्/g;
  $s =~ s/j/ज्/g;
  $s =~ s/J/झ्/g;
  $s =~ s/F/ञ्/g;
  $s =~ s/t/ट्/g;
  $s =~ s/T/ठ्/g;
  $s =~ s/d/ड्/g;
  $s =~ s/D/ढ्/g;
  $s =~ s/N/ण्/g;
  $s =~ s/w/त्/g;
  $s =~ s/W/थ्/g;
  $s =~ s/x/द्/g;
  $s =~ s/X/ध्/g;
  $s =~ s/n/न्/g;
  $s =~ s/p/प्/g;
  $s =~ s/P/फ्/g;
  $s =~ s/b/ब्/g;
  $s =~ s/B/भ्/g;
  $s =~ s/m/म्/g;
  $s =~ s/y/य्/g;
  $s =~ s/r/र्/g;
  $s =~ s/l/ल्/g;
  $s =~ s/v/व्/g;
  $s =~ s/S/श्/g;
  $s =~ s/R/ष्/g;
  $s =~ s/s/स्/g;
  $s =~ s/h/ह्/g;
  $s =~ s/a/अ/g;
  $s =~ s/A/आ/g;
  $s =~ s/i/इ/g;
  $s =~ s/I/ई/g;
  $s =~ s/u/उ/g;
  $s =~ s/U/ऊ/g;
  $s =~ s/q/ऋ/g;
  $s =~ s/Q/ॠ/g;
  $s =~ s/l/ॡ/g;
  $s =~ s/L/ऌ/g;
  $s =~ s/e/ए/g;
  $s =~ s/E/ऐ/g;
  $s =~ s/o/ओ/g;
  $s =~ s/O/औ/g;
  $s =~ s/0/०/g;
  $s =~ s/1/१/g;
  $s =~ s/2/२/g;
  $s =~ s/3/३/g;
  $s =~ s/4/४/g;
  $s =~ s/5/५/g;
  $s =~ s/6/६/g;
  $s =~ s/7/७/g;
  $s =~ s/8/८/g;
  $s =~ s/9/९/g;

  return $s;
}

1;

