#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use alRawTextCleaning;
use alURL;
use alEmail;
use alTokenisation;
use alEditDistance;
use strict;

# Usage: provide a TMX file as an input (e.g. downloaded on the OPUS web site)
#        option -i means "invert languages" (treat first language as target language, not source language)

my $invert_languages;
my $keep_all;
my $factor = 1; # 10 = keep as much as possible ; 1 = default
my $steps = 3; # 3 = default

while (1) {
  $_ = shift;
  if (/^$/) {last}
  elsif (/^-i$/) {$invert_languages = 1}
  elsif (/^-a$/) {$keep_all = 1}
  elsif (/^-f$/) {$factor = shift || die}
  elsif (/^-s$/) {$steps = shift || die}
  else {die "Unknown option '$_'"}
}

my @seg;
my ($lang, $seg);
my (%toks1, %toks2);
my (%count1, %count2);
my @langs;
my $l;
my $i=-1;
my $real_i;
my (%fwd, %bwd);

while (<>) {
  print STDERR "\rReading TMX file: ".(++$l);
  chomp;
  if (m|<tuv xml:lang="([^"]+)"><seg>(.*)</seg></tuv>|) {
    $i++;
    $lang = $1;
    $seg = $2;
    $real_i = $invert_languages ? 1-$i : $i;
    if (defined($langs[$real_i])) {
      die unless $1 eq $langs[$real_i];
    } else {
      $langs[$real_i] = $1;
    }
    die if $i >= 2;
    $seg = altok_interpret_entities($seg);
    $seg = altok_escape_xml($seg);
    $seg = altok_escape_metacharacters($seg);
    $seg = altok_tokenise($seg, $lang);
    $seg =~ s/  +/ /g;
    $seg =~ s/^ //;
    $seg =~ s/ $//;
    $seg[$real_i] = $seg;
  } elsif (m|</tu>|) {
    for my $w1 (split / /, $seg[0]) {
      $toks1{$w1} = 1;
    }
    for my $w2 (split / /, $seg[1]) {
      $toks2{$w2} = 1;
    }
    for my $w1 (keys %toks1) {
      $count1{$w1}++;
      for my $w2 (keys %toks2) {
	$fwd{$w1}{$w2}++;
	$bwd{$w2}{$w1}++;
      }
    }
    for my $w2 (keys %toks2) {
      $count2{$w2}++;
    }
    @seg = ();
    %toks1 = ();
    %toks2 = ();
    $i = -1;
  }
  unless ($keep_all) {
    if ($l % 1000000 == 0) {
      for my $w1 (keys %count1) {
	if ($count1{$w1} == 1) {
	  delete ($count1{$w1});
	  for my $w2 (keys %{$fwd{$w1}}) {
	    delete ($bwd{$w2}{$w1});
	  }
	  delete ($fwd{$w1});
	} elsif ($l % 5000000 == 0) {
	  for my $w2 (keys %{$fwd{$w1}}) {
	    if ($fwd{$w1}{$w2} == 1) {
	      delete ($fwd{$w1}{$w2});
	      delete ($bwd{$w2}{$w1});
	    }
	  }
	}
      }
      for my $w2 (keys %count2) {
	if ($count2{$w2} == 1) {
	  delete ($count2{$w2});
	  for my $w1 (keys %{$bwd{$w2}}) {
	    delete ($fwd{$w1}{$w2});
	  }
	  delete ($bwd{$w2});
	}
      }
      print STDERR "\n\t".(scalar keys %count1)." | ".(scalar keys %count2)."\n";
      last if scalar keys %count1 >= 1000000 && scalar keys %count2 >= 1000000;
    }
  }
}
print STDERR "\n";

unless ($keep_all) {
  print STDERR "\rRemoving hapaxes...";
  for my $w1 (keys %count1) {
    if ($count1{$w1} == 1) {
      delete ($count1{$w1});
      for my $w2 (keys %{$fwd{$w1}}) {
	delete ($bwd{$w2}{$w1});
      }
      delete ($fwd{$w1});
    } else {
      for my $w2 (keys %{$fwd{$w1}}) {
	if ($fwd{$w1}{$w2} == 1) {
	  delete ($fwd{$w1}{$w2});
	  delete ($bwd{$w2}{$w1});
	}
      }
    }
  }
  for my $w2 (keys %count2) {
    if ($count2{$w2} == 1) {
      delete ($count2{$w2});
      for my $w1 (keys %{$bwd{$w2}}) {
	delete ($fwd{$w1}{$w2});
      }
      delete ($bwd{$w2});
    }
  }
}
print STDERR "\n\tFinal: ".(scalar keys %count1)." | ".(scalar keys %count2)."\n";

print STDERR "\rKeeping only most adequately distributed counterparts for input words...";
for my $w1 (keys %fwd) {
  my $n = 0;
  for my $w2 (sort {2/($count1{$w1}/$fwd{$w1}{$b}+$count2{$b}/$fwd{$w1}{$b}) <=> 2/($count1{$w1}/$fwd{$w1}{$a}+$count2{$a}/$fwd{$w1}{$a})} keys %{$fwd{$w1}}) {
    $n++;
    delete $fwd{$w1}{$w2} if $n > 10;
  }
}
print STDERR "done\n";

my $maxiter = $steps - 1;
my ($metrics, $n);
my ($ed_transd, $ed, $transd);
my %metrics_cache;
my @sorted_fwd_keys = sort {$count1{$b} <=> $count1{$a}} keys %fwd;
for my $iter (0..$maxiter) {
  $n = 0;
  my %weights = ();
  %metrics_cache = ();
  my $k;
  for my $w1 (@sorted_fwd_keys) {
    $k++;
    print STDERR "\rAnalysing word pairs: $k input words/".($#sorted_fwd_keys+1);
#    next if $count1{$w1} <= 2;
    for my $w2 (sort {metrics($w1,$b) <=> metrics($w1,$a)} keys %{$fwd{$w1}}) {
#      next if $count2{$w2} <= 2;
      $metrics = metrics($w1,$w2);
      if (($metrics > 0 && $fwd{$w1}{$w2} >= 10/$factor)
	  || ($metrics > 0 && $metrics > 1-$factor*$fwd{$w1}{$w2}/10)
	 ) {
	$ed_transd = edit_distance($w1,$w2,1);
	$ed_transd =~ /^([\d\.e\+\-]*)\t(.*)$/ || die $ed_transd;
	$ed = $1;
	$transd = $2;
	if ($iter == 0 || $ed < length($w1.$w2)/4) {
	  for (split / /, $transd) {
	    /^\((.*)\|(.*)\)$/ || die $transd;
	    $weights{$1}{__ALL__}++;
	    $weights{$1}{$2}++;
	  }
	}
	$n++;
	print "$w1\t$w2\t$metrics\t$ed\n" if $iter == $maxiter;
	last;
      }
    }
  }
  print STDERR "<$n>\n";
  unless ($iter == $maxiter) {
    for my $c1 (sort {$weights{$b}{__ALL__} <=> $weights{$a}{__ALL__}} keys %weights) {
      for my $c2 (sort {$weights{$c1}{$b} <=> $weights{$c1}{$a}} keys %{$weights{$c1}}) {
	next if $c2 eq "__ALL__";
	update_edit_distance_weight($c1,$c2,1-sqrt($weights{$c1}{$c2}/$weights{$c1}{__ALL__}));
      }
    }
  }
}
 

sub metrics {
  my ($w1,$w2) = @_;
  my $edit_d;
  if (defined($metrics_cache{$w1}) && defined($metrics_cache{$w1}{$w2})) {
    return $metrics_cache{$w1}{$w2};
  }
  my $distrib_d = 2/($count1{$w1}/$fwd{$w1}{$w2}+$count2{$w2}/$fwd{$w1}{$w2});
  $distrib_d /= 1+2/$fwd{$w1}{$w2};
  if ($distrib_d >= 0.5) {
    $metrics_cache{$w1}{$w2} = $distrib_d-$edit_d*$edit_d/100;
  } else {
    $edit_d = edit_distance($w1,$w2,0);
    $metrics_cache{$w1}{$w2} = $distrib_d-(0.5-$distrib_d)*$edit_d*$edit_d/10;
  }
  return $metrics_cache{$w1}{$w2};
}
