#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use strict;

my ($w,$t);

while (<>) {
  chomp;
  s/\s+$//;
  s/^\s+//;
  my $o = "";
  for (split / +/, $_) {
    $o .= "  " unless $o eq "";
    /^(.+)\/([^\/ ]+)$/ || die;
    $w = $1;
    $t = $2;
    $w =~ s/(?<=.)/\/INWORD /g;
    $w =~ s/\/INWORD $//;
    $o .= $w."/".$t;
  }
  print "$o\n";
}
