package alCPunct;
use utf8;
use strict;
use alVariables;
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(&altok_detokenise_multiple_punctuations &altok_retokenise_lexical_punctuations);

sub altok_detokenise_multiple_punctuations {
  my $s = shift;

  $s =~ s/(^|$alAlphanum| ) (\. \. +\/ +\. \.) ([ \"]|$alAlphanum|$)/$1 {$2} _CPUNCT $4/g;
  $s =~ s/(^|$alAlphanum| ) (($alPunctuation)(?: \3)+) ([ \"]|$alAlphanum|$)/$1 {$2} _CPUNCT $4/g;
  $s =~ s/(?<= )(&lt; [=-](?: [=-])*)(?= )/_remove_single_spaces($1)/ge;	# cas particulier: flèches
  $s =~ s/(?<= )([=-] (?:[=-] )*&gt;)(?= )/_remove_single_spaces($1)/ge;	# cas particulier: flèches

  
  return $s;
}

sub altok_retokenise_lexical_punctuations {
  my $s = shift;
  my $lang = shift;
  
  if ($lang eq "fr") {
    $s =~ s/ et \/ ou / et\/ou /g;
    $s =~ s/(?<= )(- (?:t - )?(?:ce|elles?|ils?|en|on|je|la|les?|leur|lui|[mt]oi|[vn]ous|tu|y))(?= )/_remove_single_spaces($1)/gei;
    # donne-m'en => donne -m'en
    $s =~ s/(?<= )(- [mlt] ['’´])(?= )/_remove_single_spaces($1)/gei;
    $s =~ s/(?<= )(- (?:née?s?|cl(?:é|ef)s?|ci|là))(?= )/_remove_single_spaces($1)/gei;
    # hyphenated prefixes
    $s =~ s/(?<= )((?:qu|lorsqu|puisqu|quelqu|quoiqu|[dlnmtcjs]) ['’´])(?= )/_remove_single_spaces($1)/gei;
    # specific cases (should be dealt with by a lexicon)
    $s =~ s/(?<= )(aujourd ' hui|peut - être|(?:là|ici) - bas|t(?:ee)? - shirt|hors - jeu|mi - temps|après - midi|week - end|expert - comptable|soi - disant|peut - être)(?= )/_remove_single_spaces($1)/gei;
    $s =~ s/(?<= )((?:nous|vous|eux|moi|toi|soi|lui|elles?) - mêmes?)(?= )/_remove_single_spaces($1)/gei;
  } elsif ($lang =~ /^roh/) {
    $s =~ s/(?<! ) ' (?! )/' /g;
  } elsif ($lang =~ /^kmr/) {
    $s =~ s/(?<= )([0-9]+) ' (?! )/$1'/g;
  } elsif ($lang eq "it") {
    $s = " $s ";
    $s =~ s/( |$alPunctuation) e \' (?= |$alPunctuation)/$1 {e'} é /g;
    $s =~ s/( |$alPunctuation) ([A-Za]?[a-z]+(?:[ielsrtn]t|[eirdavtp]r|ci|al|ap|of|ol|ag))a\' (?= |$alPunctuation)/$1 {$2a'} $2à /g;
    $s =~ s/( |$alPunctuation) ([A-Za]?[a-z]+(?:pi|n[dt]|bl|iv|u[ld]|gi|ab))u\' (?= |$alPunctuation)/$1 {$2u'} $2ù /g;
    $s =~ s/( |$alPunctuation) ([A-Za]?[a-z]+(?:[eubfvpgmsdczlntir]))o\' (?= |$alPunctuation)/$1 {$2o'} $2ò /g;
    $s =~ s/^ (.*) $/$1/ || die;
  } elsif ($lang eq "en") {
    $s =~ s/ and \/ or / and\/or /g;
    $s =~ s/ or \/ and / or\/and /g;
    $s =~ s/(?<= )(\w{3,} - free|(?:short|medium|long) - term|(?:half|first|second) - mixed|(?:lop|blind) - sided|(?:square|ill|odd|round) - shaped|good - looking|[a-zA-Z']+ - the - [a-zA-Z']+)(?= )/_remove_single_spaces($1)/gei;
    $s =~ s/(?<= )([a-z]{3,}ty - (?:one|two|three|four|five|six|seven|eight|nine))(?= )/_remove_single_spaces($1)/gei;
    $s =~ s/(?<= )((?:\d+|one|two|three|four|five|six|seven|eight|nine|ten|eleven|twelve|[a-z]+teen|[a-z]+ty(?:-(?:one|two|three|four|five|six|seven|eight|nine))?) - (?:(?:week|year|day)s? - old|week|year|minute|hour|second|day|sided))(?= )/_remove_single_spaces($1)/gei;
    $s =~ s/(?<= )((?:pay-by-the|last|next) - (?:week|year|minute|hour|second|day))(?= )/_remove_single_spaces($1)/gei;
    $s =~ s/(?<= )((?:lop|blind) - (?:sided))(?= )/_remove_single_spaces($1)/gei;
    $s =~ s/(?<= )((?:square|ill|odd|round) - (?:shaped))(?= )/_remove_single_spaces($1)/gei;
    $s =~ s/(?<= )((?:good|better|best) - (?:looking))(?= )/_remove_single_spaces($1)/gei;    
#    s/ (-)(l)\b/ {$1$2} - _UNSPLIT_I/gi;
#    s/ (-)(lt)\b/ {$1$2} - _UNSPLIT_It/gi;
#    s/ (-)(lf)\b/ {$1$2} - _UNSPLIT_If/gi;
#    s/ (-)(ls)\b/ {$1$2} - _UNSPLIT_Is/gi;
#    s/ (-)(l'm)\b/ {$1$2} - _UNSPLIT_I _UNSPLIT_'m/gi;

###	$s =~ s/(?<! ) (['’´]) (?! )/$1/g;  
  }

  
  return $s;
}

sub _remove_single_spaces {
  my $s = shift;
  $s =~ s/(?<! ) (?! )//g;
  return $s;
}

1;
