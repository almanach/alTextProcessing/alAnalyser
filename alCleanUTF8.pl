#!/usr/bin/perl

binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use alUTF8;
use strict;

while (<>) {
  chomp;
  print altok_clean_utf8($_)."\n";
}
