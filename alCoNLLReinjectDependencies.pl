#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

$|=1;

my $udpipefile = shift || die "Please provide the UDPipe file (source for dependency information) as an argument";
die "UDPipe file '$udpipefile' not found" unless -r $udpipefile;
open UDPIPE, "<$udpipefile" || die $!;
binmode UDPIPE, ":utf8";

my $cur_word_start;
my $cur_word_end;
my ($tok, $gov, $deplabel);
my (@chars, @govminchars, @govmaxchars, @deplabels);
my (@id2min_char, @id2max_char);
my @cur_sentence;
my $id;

while (<UDPIPE>) {
  chomp;
  if (/^$/) {
    for $id (1..$#cur_sentence) {
      for (split //, $cur_sentence[$id]{tok}) {
	next if / /;
	push @govminchars, "";
	push @govmaxchars, "";
	push @deplabels, "";
      }
      if ($cur_sentence[$id]{gov} == 0) {
	$govminchars[$#govminchars] = -1;
	$govmaxchars[$#govmaxchars] = -1;
      } else {
	$govminchars[$#govminchars] = $id2min_char[$cur_sentence[$id]{gov}];
	$govmaxchars[$#govmaxchars] = $id2max_char[$cur_sentence[$id]{gov}];
      }
      $deplabels[$#deplabels] = $cur_sentence[$id]{deplabel};
    }
    die "$#deplabels ≠ $#chars" unless $#deplabels == $#chars;
    @cur_sentence = ();
  } elsif (/^(\d+)\t([^\t]+)\t[^\t]+\t[^\t]+\t[^\t]+\t[^\t]+\t([^\t]+)\t([^\t]+)\t/) {
    $id = $1;
    $tok = $2;
    $gov = $3;
    $deplabel = $4;
    die $gov unless $gov =~ /^\d+$/;
    die $deplabel if $deplabel eq "_" || $deplabel eq "";
    $cur_sentence[$id]{tok} = $tok;
    $cur_sentence[$id]{gov} = $gov;
    $cur_sentence[$id]{deplabel} = $deplabel;
    $id2min_char[$id] = $#chars+1;
    for (split //, $tok) {
      next if / /;
      push @chars, $_;
    }
    $id2max_char[$id] = $#chars;
  }
}
close UDPIPE;

my $offset;
my $remainder;
my @char2id;
my @id2min_char;
my @id2max_char;
my $sentence_min_char;
my $sentence_max_char;
$id = 0;
while (<>) {
  chomp;
  if (/^$/) {
    my $root = 0;
    $sentence_max_char = $offset;
    for my $i (1..$#cur_sentence) {
      $cur_sentence[$i]{remainder} =~ /^([^\t]+\t[^\t]+\t[^\t]+\t[^\t]+\t)[^\t]+\t[^\t]+(\t[^\t]+\t[^\t]+)$/
	|| die "$i/$cur_sentence[$i]{tok}:<$cur_sentence[$i]{remainder}>";
      my $before = $1;
      my $after = $2;
      my $allc;
      my $allgovs;
      my %possible_govs;
      for my $c ($id2min_char[$i]..$id2max_char[$i]) {
	unless ($govminchars[$c] eq "") {
	  for ($govminchars[$c]..$govmaxchars[$c]) {
	    if ($_ == -1) {
	      $possible_govs{0} = $deplabels[$c];
	    } else {
	      if ($_ < $sentence_min_char || $_ > $sentence_max_char) {
		$possible_govs{0} = "root";
	      } else {
		unless ($char2id[$_] == $i) {
		  $possible_govs{$char2id[$_]} = $deplabels[$c];
		}
	      }
	    }
	  }
	}
	$allc .= $chars[$c];
      }
      print $cur_sentence[$i]{introlines};
      if (scalar keys %possible_govs == 0) {
	print $i."\t".$cur_sentence[$i]{tok}."\t".$before.($i+1)."\tgoeswith$after\n";
      } else {
	my $best_dep = 100000000;
	for (keys %possible_govs) {
	  if ($root == 0 && $_ == 0) {
	    $best_dep = 0;
	    last;
	  } else {
	    if (abs($best_dep-$i) > abs($_-$i)) {
	      $best_dep = $_;
	    } elsif (abs($best_dep-$i) == abs($_-$i)) {
	      $best_dep = $_ if $_ < $i;
	    }
	  }
	}
	die if $best_dep == 100000000;
	if ($root > 0 && $best_dep == 0) {
	  $best_dep = $root;
	  $possible_govs{$best_dep} = "dep";
	}
	if ($best_dep == 0) {
	  $root = $i;
	}
	print $i."\t".$cur_sentence[$i]{tok}."\t".$before.$best_dep."\t".$possible_govs{$best_dep}."$after\n";
      }
    }
    print "\n";
    die if $root == 0;
    $id = 0;
    @cur_sentence = ();
    $sentence_min_char = $offset+1;
  } elsif (/^(\d+)\t([^\t]+)\t(.*)/) {
    $id = $1;
    $tok = $2;
    $remainder = $3;
    $cur_sentence[$id]{tok} = $tok;
    $cur_sentence[$id]{remainder} = $remainder;
    $id2min_char[$id] = $offset;
    for (split //, $tok) {
      next if / /;
      die "$chars[$offset] ≠ $_" unless $chars[$offset] eq $_;
      $char2id[$offset] = $id;
      $offset++;
    }
    $id2max_char[$id] = $offset-1;
  } else {
    $cur_sentence[$id+1]{introlines} .= $_."\n";
  }
}
