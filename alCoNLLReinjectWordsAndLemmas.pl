#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

$|=1;

my $udpipefile = shift || die "Please provide the UDPipe file (source for Words and Lemma information) as an argument";
die "UDPipe file '$udpipefile' not found" unless -r $udpipefile;
open UDPIPE, "<$udpipefile" || die $!;
binmode UDPIPE, ":utf8";

my $cur_word_start;
my $cur_word_end;
my ($tok, $lemma, $word_sequence, $lemma_sequence, $pos);
my (@chars, @words, @lemmas);

while (<UDPIPE>) {
  chomp;
  if (/^$/) {
    $cur_word_start = 0;
    $cur_word_end = 0;
  } elsif (/^(\d+)-(\d+)\t([^\t]+)\t/) {
    $cur_word_start = $1;
    $cur_word_end = $2;
    $tok = $3;
    $word_sequence = "";
    $lemma_sequence = "";
    for (split //, $tok) {
      next if / /;
      push @chars, $_;
      push @words, "";
      push @lemmas, "";
    }
  } elsif ($cur_word_start == 0 && /^\d+\t([^\t]+)\t([^\t]+)\t/) {
    $tok = $1;
    $lemma = $2;
    for (split //, $tok) {
      next if / /;
      push @chars, $_;
      push @words, "";
      push @lemmas, "";
    }
    $lemmas[$#lemmas] = $lemma unless $lemma eq "_";
  } elsif ($cur_word_start > 0 && /^(\d+)\t([^\t]+)\t([^\t]+)\t/) {
    $pos = $1;
    $word_sequence .= "\t" unless $word_sequence eq "";
    $lemma_sequence .= "\t" unless $lemma_sequence eq "";
    $word_sequence .= $2;
    $lemma_sequence .= $3;
    if ($pos == $cur_word_end) {
      $words[$#words] = $word_sequence;
      $lemmas[$#lemmas] = $lemma_sequence;
      $cur_word_start = 0;
      $cur_word_end = 0;
    }
  }
}

my $id;
my $offset;
my $remainder;
while (<>) {
  chomp;
  if (/^$/) {
    $id = 0;
    print "$_\n";
  } elsif (/^\d+\t([^\t]+)\t[^\t]*\t(.*)/) {
    $tok = $1;
    $remainder = $2;
    for (split //, $tok) {
      next if / /;
      die "$chars[$offset] ≠ $_" unless $chars[$offset] eq $_;
      $offset++;
    }
    if ($words[$offset-1] ne "") {
      my @w = split /\t/, $words[$offset-1];
      my @l = split /\t/, $lemmas[$offset-1];
      print $id+1;
      print "-";
      print $id+1+$#w;
      print "\t".$tok."\t_\t_\t_\t_\t_\t_\t_\t_\n";
      for (0..$#w) {
	$id++;
	print "$id\t$w[$_]\t".($l[$_] || "_")."\t_\t_\t_\t".($id == 1 ? "0" : "1")."\t_\t_\t_\n";
      }
    } elsif ($lemmas[$offset-1] ne "") {
      $id++;
      print "$id\t$tok\t".($lemmas[$offset-1] || "_")."\t$remainder\n";
    } else {
      $id++;
      print "$id\t$tok\t_\t$remainder\n";
    }
  } elsif (/^\d+-\d+\t/) {
    die "The input file should not contain Words";
  } else {
    print "$_\n";
  }
}
