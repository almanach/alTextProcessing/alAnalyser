#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

print "\\r\\r\\t\n";
$sentid = 1;

while (<>) {
  chomp;
  if (/^$/) {
    if ($bool == 1) {
      $skipminpos = 0;
      $skipmaxpos = 0;
      $bool = 0;
      $sentid++;
    } else { # second empty line in a row = new paragraph
      print "\\r\\t\n";
    }
  } elsif (/^(\d+)-(\d+)\t([^\t]+)/) {
    $skipminpos = $1;
    $skipmaxpos = $2;
    $t = $3;
    $bool = 1;
    $t =~ s/ / /g;
    $upos = "";
    $lemma = "";
    #    print " " unless /SpaceAfter=No/;
  } elsif (/^(\d+)\t([^\t]+)\t([^\t]+)\t([^\t]+)\t/) {
    $pos = $1;
    if ($skipminpos == 0 || $pos == $skipmaxpos + 1) {
      if ($pos == $skipmaxpos + 1) {
	$skipminpos = 0;
	$skipmaxpos = 0;
      }
      $t = $2;
      $lemma = $3;
      $upos = $4;
      $bool = 1;
      $t =~ s/ / /g;
      print "1\t".$sentid."\t".$t."\t".$lemma."\t".$upos."\t-\n";
      #      print " " unless /SpaceAfter=No/;
    } elsif ($skipminpos <= $pos && $skipmaxpos >= $pos) {
      if ($skipmaxpos > $pos) {
	$lemma .= " ";
	$upos .= " ";
      }
      $lemma .= $3;
      $upos .= $4;
      if ($skipmaxpos == $pos) {
	print "1\t".$sentid."\t".$t."\t".$lemma."\t".$upos."\t-\n";
      }
    } else {
      die "$pos / $skipminpos / $skipmaxpos";
    }
  }
}
print "\\r\n";
