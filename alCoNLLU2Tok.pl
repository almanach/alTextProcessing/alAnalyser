#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

$|=1;

my $bool;
my $t;
my $skipminpos;
my $skipmaxpos;
my $pos;

my $empty_line_at_sentence_boundaries = 0;

while (1) {
  $_ = shift;
  if (/^$/) {last}
  elsif (/^-?-sb$/) {$empty_line_at_sentence_boundaries = 1}
  else {die "Option '$_' unknown"}
}

while (<>) {
  chomp;
  if (/^$/) {
    print "\n";
    print "\n" if $empty_line_at_sentence_boundaries;
    $skipminpos = 0;
    $skipmaxpos = 0;
    $bool = 0;
  } elsif (/^(\d+)-(\d+)\t([^\t]+)/) {
    $skipminpos = $1;
    $skipmaxpos = $2;
    $t = $3;
    print " " if $bool;
    $bool = 1;
    $t =~ s/ / /g;
    print $t;
    print " " unless /SpaceAfter=No/;
  } elsif (/^(\d+)\t([^\t]+)/) {
    $pos = $1;
    if ($skipminpos == 0 || $pos == $skipmaxpos + 1) {
      if ($pos == $skipmaxpos + 1) {
	$skipminpos = 0;
	$skipmaxpos = 0;
      }
      $t = $2;
      print " " if $bool;
      $bool = 1;
      $t =~ s/ / /g;
      print $t;
      print " " unless /SpaceAfter=No/;
    } elsif ($skipminpos <= $pos && $skipmaxpos >= $pos) {
    } else {
      die "$pos / $skipminpos / $skipmaxpos";
    }
  }
}
