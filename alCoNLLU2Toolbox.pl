#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use strict;

my %hash;
my ($in_sentence, $id, $sep);

while (<>) {
  chomp;
  s/^# sent_id/# u_id/;
  if (/^# (_[A-Za-z]+|ELANMedia)(.*)$/) {
    print "\\$1$2\n";
  } elsif (/^# (u_id|text|lemma_en|ger_u|com|problems|ELANParticipant|ELANBegin|ELANEnd|mmarker) = (.*)$/) {
    $hash{$1} = $2;
    $in_sentence = 1;
  } elsif (/^(\d+)\t([^\t]+)\t([^\t]+)\t([^\t]+)\t([^\t]+)\t([^\t]+)/) {
    $in_sentence = 1;
    $id = $1;
    $sep = ($id == 1 ? "" : " ");
    $hash{norm} .= $sep.$2;
    $hash{lemma} .= $sep.$3;
    $hash{pos} .= $sep.$4;
    $hash{cpos} .= $sep.$5;
    $hash{morphosyn} .= $sep.$6;
  } elsif (/^$/) {
    print_cur_sentence() if $in_sentence;
    $in_sentence = 0;
    %hash = ();
  } else {
    die "ERROR: invalid line: $_";
  }
}
print_cur_sentence() if $in_sentence;

sub print_cur_sentence {
  for ("u_id", "text", "norm", "pos", "cpos", "morphosyn", "lemma", "lemma_en", "ger_u", "com", "problems", "ELANParticipant", "ELANBegin", "ELANEnd", "mmarker") {
    print "\\$_ $hash{$_}\n";
  }
  print "\n";
}
