#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

$|=1;

my $features = shift || die;
die "Invalid features list" unless $features =~ /^[A-Z][A-Za-z\[\]]*(?:,[A-Z][A-Za-z\[\]]*)*$/;
$features =~ s/,/\|/g;

my @l;
while (<>) {
  chomp;
  if (/^\d+\t/) {
    @l = split /\t/, $_;
    $l[3] .= "#";
    $l[5] = join "|", sort {$a cmp $b} split /\|/, $l[5];
    for (split /\|/, $l[5]) {
      if (/^$features=/) {
	$l[3] .= "|".$_;
      }
    }
    $l[3] =~ s/#\|/#/;
    print join "\t", @l;
    print "\n";
  } else {
    print "$_\n";
  }
}

