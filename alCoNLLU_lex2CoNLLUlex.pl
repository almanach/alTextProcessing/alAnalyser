#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

#Usage: cat conllu-treebank(s) | perl alCoNLLU_lex2CoNLLUlex.pl lexicon.ftlm > mapping

# cat ~/Documents/ud-treebanks-v2.0/UD_Ancient_Greek*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl -d /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/greek_diogenes-0.0.1.mlex > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-vmappings/greek_diogenes.vmapping
# cat ~/Documents/ud-treebanks-v2.0/UD_Latin*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/latin_diogenes-0.0.1.mlex -d > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-vmappings/latin_diogenes.vmapping
# cat ~/Documents/ud-treebanks-v2.0/UD_Polish*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/pollex.mlex -d > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-vmappings/pollex.vmapping
# cat ~/Documents/ud-treebanks-v2.0/UD_Slovenian*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl -s /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/sloleks.mlex > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-vmappings/sloleks.vmapping
# cat ~/Documents/ud-treebanks-v2.0/UD_Croatian*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/hml_50_metashare_v2-0.0.1.mlex -m > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-vmappings/hml.vmapping
# cat ~/Documents/ud-treebanks-v2.0/UD_Irish*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/inmdb-0.0.1.mlex -u > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-vmappings/inmdb.vmapping

# cat ~/Documents/ud-treebanks-v2.0/UD_French*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/lefff.mlex > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-mappings/lefff.mapping
# cat ~/Documents/ud-treebanks-v2.0/UD_Spanish*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/leffe.mlex > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-mappings/leffe.mapping
# cat ~/Documents/ud-treebanks-v2.0/UD_English*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/enlex-0.1.mlex > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-mappings/enlex.mapping
# cat ~/Documents/ud-treebanks-v2.0/UD_Norwegian-Bokmaal/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/ordbank_bm-0.0.1.mlex > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-mappings/ordbank_bm.mapping

# cat ~/Documents/ud-treebanks-v2.0/UD_Persian*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/perlex.mlex > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-mappings/perlex.mapping
# cat ~/Documents/ud-treebanks-v2.0/UD_Swedish*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/saldo.mlex > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-mappings/saldo.mapping

# cat ~/Documents/ud-treebanks-v2.0/UD_Italian*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/morph_it.mlex > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-mappings/morph_it.mapping
# cat ~/Documents/ud-treebanks-v2.0/UD_Galician*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/leffga-0.1.1.mlex > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-mappings/leffga.mapping 
# cat ~/Documents/ud-treebanks-v2.0/UD_Danish*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/sto-0.0.1.mlex > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-mappings/sto.mapping
# cat ~/Documents/ud-treebanks-v2.0/UD_Dutch*/*.conllu | perl ~/Documents/al/alanalyser/trunk/alCoNLLU_lex2CoNLLUlex.pl /Users/sagot/Documents/alparticles/lrec18udlexicons/other-sources/alpino_lex.mlex > /Users/sagot/Documents/alparticles/lrec18udlexicons/other-mappings/alpino_lex.mapping


my $lex;
my $decompose = 0;
my $multext = 0;
my $sloleks = 0;
my $udlex = 0;
while (1) {
  $_ = shift;
  last if /^$/;
  if (/^-d$/) {$decompose = 1}
  elsif (/^-m$/) {$decompose = 1; $multext = 1;}
  elsif (/^-s$/) {$decompose = 1; $multext = 1; $sloleks = 1;}
  elsif (/^-u$/) {$decompose = 1; $udlex = 1;}
  else {$lex = $_}
}
die "Please provide the lexicon to be converted (format: form <tab> cat <tab> lemma <tab> msfeatures)" if $lex eq "";

my (%UDform_lemma_cat_ms2occ, %LEXform_lemma_cat_ms, %cat_ms, %UDcat_UDms, %UDform_cat2lemma, %UDform_lemma_cat2occ);
my %UDms; # working hash

#8       le      le      DET     _       Definite=Def|Gender=Masc|Number=Sing|PronType=Art       9       det     _       _

my ($id, $pos, $form, $lemma, $cat, $ms, $all_ms, @ms);
my ($cur_multiword_token, $mwt_start, $mwt_end);
while (<>) {
  chomp;
  if (/^$/ || /^#.*/) {
  } elsif (/^(\d+)-(\d+)\t([^\t]+)/) {
    $mwt_start = $1;
    $mwt_end = $2;
    $cur_multiword_token = $3;
  } elsif (/^(\d+)\t([^\t]+)\t([^\t]+)\t([^\t]+)\t[^\t]*\t([^\t]+)\t/) {
    ($id, $form, $lemma, $cat, $ms) = ($1, $2, $3, $4, $5);
    $UDform_lemma_cat2occ{$form}{$lemma}{$cat}++;
    $UDform_lemma_cat_ms2occ{$form}{$lemma}{$cat}{$ms}++;
    if (!defined($UDform_cat2lemma{$form})
	|| !defined($UDform_cat2lemma{$form}{$cat})) {
      $UDform_cat2lemma{$form}{$cat} = $lemma;
    } elsif (defined($UDform_cat2lemma{$form})
	     && defined($UDform_cat2lemma{$form}{$cat})
	     && $UDform_cat2lemma{$form}{$cat} ne $lemma
	    ) {
      $UDform_cat2lemma{$form}{$cat} = "__MULTIPLE_LEMMAS__";
    }
    $UDcat_UDms{$cat}{$ms} = 1;
  }
}

open LEX, "<$lex" || die $!;
binmode LEX, ":utf8";
while (<LEX>) {
  chomp;
  if (/^$/ || /^#/ || /^_/) {
  } elsif (/^([^\t]+)\t([^\t]+)\t([^\t]+)\t([^\t]*)$/) {
    ($form, $lemma, $cat, $all_ms) = ($1, $3, $2, $4);
    next if $lemma =~ /^_/ || $lemma =~ /_$/;
    next if $all_ms eq "_HIDDEN_LEMMA_";
    $all_ms =~ s/^$cat://;
    $all_ms = "_" if $all_ms eq "";
    if ($all_ms =~ /\|/ && !$udlex) {
      @ms = split /\|/, $all_ms;
    } elsif ($lex =~ /lefff/) {
      if ($all_ms eq "PFIJTSC") {
	@ms = ("");
      } elsif ($all_ms =~ /^([A-Z])([0-9])([0-9])([sp])$/) {
	@ms = ("$1$2$4", "$1$3$4");
      } elsif ($all_ms =~ /^([A-Z])([A-Z])([0-9][sp])$/) {
	@ms = ("$1$3", "$2$3");
      } elsif ($all_ms =~ /^([A-Z])([A-Z])([0-9])([0-9])([sp])$/) {
	@ms = ("$1$3$5", "$2$3$5", "$1$4$5", "$2$4$5");
      } elsif ($all_ms eq "K") {
	@ms = ("Kms", "Kmp", "Kfs", "Kfp");
      } elsif ($all_ms =~ /^K([fm])$/) {
	@ms = ("K$1s", "K$1p");
      } elsif ($all_ms =~ /^([fm])$/) {
	@ms = ("$1s", "$1p");
      } elsif ($all_ms =~ /^([sp])$/) {
	@ms = ("m$1", "f$1");
      } elsif ($cat =~ /^(?:adj|nc|np)$/ && $all_ms eq "") {
	@ms = ("ms", "fs", "mp", "fp");
      } else {
	@ms = ($all_ms);
      }
    } else {
      @ms = ($all_ms);
    }
    for $ms (@ms) {
      $LEXform_lemma_cat_ms{$form}{$lemma}{$cat}{$ms} = 1;
      $cat_ms{$cat}{$ms} = 1;
    }
  } else {
    print STDERR "WARNING: ignoring lex line '$_'\n";
  }
}

my $UDcat;
my %postponed_form_lemmas;
my %tmp_LEXcat_LEXms_UDcat;
for $form (keys %LEXform_lemma_cat_ms) {
  next unless defined $UDform_lemma_cat_ms2occ{$form};
  for $lemma (keys %{$LEXform_lemma_cat_ms{$form}}) {
    if (defined $UDform_lemma_cat_ms2occ{$form}{$lemma}) {
      for $cat (keys %{$LEXform_lemma_cat_ms{$form}{$lemma}}) {
	for $ms (keys %{$LEXform_lemma_cat_ms{$form}{$lemma}{$cat}}) {
	  for $UDcat (keys %{$UDform_lemma_cat_ms2occ{$form}{$lemma}}) {
	    $tmp_LEXcat_LEXms_UDcat{$cat}{$ms}{$UDcat}++;
	  }
	}
      }
    } else {
      $postponed_form_lemmas{$form}{$lemma} = 1;
    }
  }
}

my (%LEXcat_LEXms_UDcat);
# temporary, in order to process postponed data; we will recompute a final LEXcat_LEXms_UDcat afterwards;
if ($decompose) {
  for $cat (sort keys %tmp_LEXcat_LEXms_UDcat) {
    for $ms (sort keys %{$tmp_LEXcat_LEXms_UDcat{$cat}}) {
      for (split /\t/, decompose_and_convert_cat($cat,$ms)) {
	next if /^UNK$/;
	$LEXcat_LEXms_UDcat{$cat}{$ms}{$_}++;
      }
    }
  }
} else {
  for $cat (sort keys %tmp_LEXcat_LEXms_UDcat) {
    for $ms (sort keys %{$tmp_LEXcat_LEXms_UDcat{$cat}}) {
      $UDcat = (sort {$tmp_LEXcat_LEXms_UDcat{$cat}{$ms}{$b} <=> $tmp_LEXcat_LEXms_UDcat{$cat}{$ms}{$a}} keys %{$tmp_LEXcat_LEXms_UDcat{$cat}{$ms}})[0];
      unless ($cat =~ /^aux/ && $UDcat ne "AUX") {
	$LEXcat_LEXms_UDcat{$cat}{$ms}{$UDcat}++;
      }
      if ($UDcat eq "AUX" && defined($tmp_LEXcat_LEXms_UDcat{$cat}{$ms}{VERB})) {
	$LEXcat_LEXms_UDcat{$cat}{$ms}{VERB}++ unless $cat =~ /^aux/;
      }
      if ($UDcat eq "VERB" && defined($tmp_LEXcat_LEXms_UDcat{$cat}{$ms}{AUX})) {
	$LEXcat_LEXms_UDcat{$cat}{$ms}{AUX}++;
      }
    }
  }
}

my %form_lemma_cat_ms2UDlemma;
my $UDlemma;
for $form (keys %postponed_form_lemmas) {
  for $lemma (keys %{$postponed_form_lemmas{$form}}) {
    if (scalar keys %{$LEXform_lemma_cat_ms{$form}{$lemma}} == 1) {
      for $cat (keys %{$LEXform_lemma_cat_ms{$form}{$lemma}}) { # only one
	for $ms (keys %{$LEXform_lemma_cat_ms{$form}{$lemma}{$cat}}) {
	  if (defined($LEXcat_LEXms_UDcat{$cat}) && defined($LEXcat_LEXms_UDcat{$cat}{$ms})) {
	    for $UDcat (keys %{$LEXcat_LEXms_UDcat{$cat}{$ms}}) {
	      if (defined($LEXcat_LEXms_UDcat{$cat}{$ms})
		  && defined($UDform_cat2lemma{$form}{$UDcat})
		  && $UDform_cat2lemma{$form}{$UDcat} ne "__MULTIPLE_LEMMAS__"
		  && !defined($LEXform_lemma_cat_ms{$form}{$UDform_cat2lemma{$form}{$UDcat}})) {
		#	      print STDERR "\$tmp_LEXcat_LEXms_UDcat{$cat}{$ms}{$UDcat}++;\t/\t$lemma > $UDform_cat2lemma{$form}{$UDcat} ($form)\n" if $form eq "le"
	      }
	    }
	  } elsif (defined($UDform_lemma_cat_ms2occ{$form})) {
	    my %tmp_hash;
	    for $UDlemma (sort keys %{$UDform_lemma_cat_ms2occ{$form}}) {
	      for $UDcat (sort keys %{$UDform_lemma_cat_ms2occ{$form}{$UDlemma}}) {
		my $bool = 0;
	      try:
		for my $other_lemma (sort keys %{$LEXform_lemma_cat_ms{$form}}) {
		  next if $other_lemma eq $lemma;
		  for my $other_cat (sort keys %{$LEXform_lemma_cat_ms{$form}{$other_lemma}}) {
		    for my $other_ms (sort keys %{$LEXform_lemma_cat_ms{$form}{$other_lemma}{$other_cat}}) {
		      if (defined($LEXcat_LEXms_UDcat{$other_cat})
			  && defined($LEXcat_LEXms_UDcat{$other_cat}{$other_ms})
			  && defined($LEXcat_LEXms_UDcat{$other_cat}{$other_ms}{$UDcat})) {
			$bool = 1;
			last try;
		      }
		    }
		  }
		}
		unless ($bool) {
		  $tmp_hash{$UDlemma."\t".$UDcat} = $UDform_lemma_cat2occ{$form}{$UDlemma}{$UDcat};
		}
	      }
	    }
	    if (scalar keys %tmp_hash > 0) {
	      my @tmp = sort {$tmp_hash{$b} <=> $tmp_hash{$a}} keys %tmp_hash;
	      $tmp[0] =~ /^(.+)\t(.+)$/ || die;
	      $tmp_LEXcat_LEXms_UDcat{$cat}{$ms}{$2}++;
	      $form_lemma_cat_ms2UDlemma{$form}{$lemma}{$cat}{$ms} = $1;
	    }
	  }
	}
      }
    }
  }
}
%LEXcat_LEXms_UDcat = ();
if ($decompose) {
  for $cat (sort keys %tmp_LEXcat_LEXms_UDcat) {
    for $ms (sort keys %{$tmp_LEXcat_LEXms_UDcat{$cat}}) {
      for (split /\t/, decompose_and_convert_cat($cat,$ms)) {
	next if /^UNK$/;
	$LEXcat_LEXms_UDcat{$cat}{$ms}{$_}++;
      }
    }
  }
} else {
  for $cat (sort keys %tmp_LEXcat_LEXms_UDcat) {
    for $ms (sort keys %{$tmp_LEXcat_LEXms_UDcat{$cat}}) {
      $UDcat = (sort {$tmp_LEXcat_LEXms_UDcat{$cat}{$ms}{$b} <=> $tmp_LEXcat_LEXms_UDcat{$cat}{$ms}{$a}} keys %{$tmp_LEXcat_LEXms_UDcat{$cat}{$ms}})[0];
      unless ($cat =~ /^aux/ && $UDcat ne "AUX") {
	$LEXcat_LEXms_UDcat{$cat}{$ms}{$UDcat}++;
      }
      if ($UDcat eq "AUX" && defined($tmp_LEXcat_LEXms_UDcat{$cat}{$ms}{VERB})) {
	$LEXcat_LEXms_UDcat{$cat}{$ms}{VERB}++ unless $cat =~ /^aux/;
      }
      if ($UDcat eq "VERB" && defined($tmp_LEXcat_LEXms_UDcat{$cat}{$ms}{AUX})) {
	$LEXcat_LEXms_UDcat{$cat}{$ms}{AUX}++;
      }
    }
  }
}

my $UDms;
my %tmp_LEXcat_LEXms_UDcat_UDms;
for $form (keys %LEXform_lemma_cat_ms) {
  next unless defined $UDform_lemma_cat_ms2occ{$form} || $decompose;
  for $lemma (keys %{$LEXform_lemma_cat_ms{$form}}) {
    for $cat (keys %{$LEXform_lemma_cat_ms{$form}{$lemma}}) {
      for $ms (keys %{$LEXform_lemma_cat_ms{$form}{$lemma}{$cat}}) {
	for $UDcat (keys %{$LEXcat_LEXms_UDcat{$cat}{$ms}}) {
	  if (defined($form_lemma_cat_ms2UDlemma{$form})
	      && defined($form_lemma_cat_ms2UDlemma{$form}{$lemma})
	      && defined($form_lemma_cat_ms2UDlemma{$form}{$lemma}{$cat})
	      && defined($form_lemma_cat_ms2UDlemma{$form}{$lemma}{$cat}{$ms})) {
	    $UDlemma = $form_lemma_cat_ms2UDlemma{$form}{$lemma}{$cat}{$ms};
	  } else {
	    $UDlemma = $lemma;
	  }
	  if ($decompose) {
	    $UDms = decompose_and_convert_ms($UDcat,$cat,$ms);
	    next if $UDms eq "UNK";
	    $tmp_LEXcat_LEXms_UDcat_UDms{$cat}{$ms}{$UDcat}{$UDms}++;
	  } else {
	    next unless defined $UDform_lemma_cat_ms2occ{$form}{$UDlemma};
	    next unless defined $UDform_lemma_cat_ms2occ{$form}{$UDlemma}{$UDcat};
	    for $UDms (keys %{$UDform_lemma_cat_ms2occ{$form}{$UDlemma}{$UDcat}}) {
	      $tmp_LEXcat_LEXms_UDcat_UDms{$cat}{$ms}{$UDcat}{$UDms}++;
	    }
	  }
	}
      }
    }
  }
}

my %LEXcat_LEXms_UDcat_UDms;
for $cat (sort keys %tmp_LEXcat_LEXms_UDcat_UDms) {
  for $ms (sort keys %{$tmp_LEXcat_LEXms_UDcat_UDms{$cat}}) {
    for $UDcat (sort keys %{$tmp_LEXcat_LEXms_UDcat_UDms{$cat}{$ms}}) {
      if ($decompose) {
	for (keys %{$tmp_LEXcat_LEXms_UDcat_UDms{$cat}{$ms}{$UDcat}}) {
	  $LEXcat_LEXms_UDcat_UDms{$cat}{$ms}{$UDcat}{$_} = 1;
	}
      } else {
	$UDms = (sort {$tmp_LEXcat_LEXms_UDcat_UDms{$cat}{$ms}{$UDcat}{$b} <=> $tmp_LEXcat_LEXms_UDcat_UDms{$cat}{$ms}{$UDcat}{$a}} keys %{$tmp_LEXcat_LEXms_UDcat_UDms{$cat}{$ms}{$UDcat}})[0];
	$LEXcat_LEXms_UDcat_UDms{$cat}{$ms}{$UDcat}{$UDms} = 1;
      }
    }
  }
}

my (%printed_cat_ms, %printed_UDcat_UDms);
for $cat (sort keys %LEXcat_LEXms_UDcat_UDms) {
  for $ms (sort keys %{$LEXcat_LEXms_UDcat_UDms{$cat}}) {
    for $UDcat (sort keys %{$LEXcat_LEXms_UDcat_UDms{$cat}{$ms}}) {
      for $UDms (sort keys %{$LEXcat_LEXms_UDcat_UDms{$cat}{$ms}{$UDcat}}) {
	print "$cat\t$ms\t$UDcat\t$UDms\n";
	$printed_cat_ms{$cat}{$ms} = 1;
	$printed_UDcat_UDms{$UDcat}{$UDms} = 1;
      }
    }
  }
}

for $cat (sort keys %cat_ms) {
  for $ms (sort keys %{$cat_ms{$cat}}) {
    next if defined $printed_cat_ms{$cat}{$ms};
    print "$cat\t$ms\t\t\n";
  }
}
for $UDcat (sort keys %UDcat_UDms) {
  for $UDms (sort keys %{$UDcat_UDms{$UDcat}}) {
    next if defined $printed_UDcat_UDms{$UDcat}{$UDms};
    print "\t\t$UDcat\t$UDms\n";
  }
}

for $form (sort keys %form_lemma_cat_ms2UDlemma) {
  for $lemma (sort keys %{$form_lemma_cat_ms2UDlemma{$form}}) {
    for $cat (sort keys %{$form_lemma_cat_ms2UDlemma{$form}{$lemma}}) {
      for $ms (sort keys %{$form_lemma_cat_ms2UDlemma{$form}{$lemma}{$cat}}) {
	print "\t\t\t$form\t$lemma\t$cat\t$ms\t$form_lemma_cat_ms2UDlemma{$form}{$lemma}{$cat}{$ms}\n";
      }
    }
  }
}

sub decompose_and_convert_cat {
  my $cat = shift;
  my $ms = shift;
  if ($cat =~ /^ponct[sw]?$/) {return "PUNCT"}
  elsif ($cat =~ /^parent[of]?$/) {return "PUNCT"}
  elsif ($cat =~ /^epsilon$/) {return "UNK"}
  elsif ($multext) {
    if ($cat eq "N") {
      if ($ms =~ /^c/) {return "NOUN"}
      elsif ($ms =~ /^p/) {return "PROPN"}
      elsif ($ms =~ /^g/) {return "NOUN"}
    }
    elsif ($cat eq "V") {
      if ($ms =~ /^a/) {return "AUX"}
      elsif ($ms =~ /^o/) {return "AUX"} # modal
      else {return "VERB"}
    }
    elsif ($cat eq "A") {return "ADJ"}
    elsif ($cat eq "P") {return "PRON"}
    elsif ($cat eq "D") {return "DET"}
    elsif ($cat eq "T") {return "DET"}
    elsif ($cat eq "R") {return "ADV"}
    elsif ($cat eq "S") {return "ADP"}
    elsif ($cat eq "C") {
      if ($ms eq "c") {return "CCONJ"}
      else {return "SCONJ"}
    }
    elsif ($cat eq "M") {
      if ($ms =~ /^o/) {return "ADJ"} # ordinal
      else {return "NUM"}
    }
    elsif ($cat eq "Q") {return "PART"}
    elsif ($cat eq "I") {return "INTJ"}
    elsif ($cat eq "Y") {return "X"}
    elsif ($cat eq "X") {return "X"}
    else {die "Unknown cat '$cat'"}
  } elsif ($udlex) { # just in case (ex.: inmdb)
    if ($cat eq "A") {return "ADJ"}
    if ($cat eq "P") {return "ADP"}
    if ($cat eq "N") {return "NOUN"}
    if ($cat eq "V") {return "VERB"}
  } else {
    if ($cat eq "NorA") {return "NOUN\tADJ"}
    elsif ($cat =~ /^A[crs]?$/) {
      if ($ms =~ /adj(sg|pl)/) {
	return "DET";
      } else {
	return "ADJ"
      }
    }
    elsif ($cat =~ /^V[aimgs]?p?$/) {return "VERB"}
    elsif ($cat =~ /^bedzie$/) {return "AUX"}
    elsif ($cat eq "D") {return "ADP"}#!!! (pollex)
    elsif ($cat eq "N") {return "NOUN"}
    elsif ($cat eq "C") {return "SCONJ"}
    elsif ($cat eq "I") {return "INTJ"}
    elsif ($cat eq "NUM") {return "NUM"}
    elsif ($cat eq "num") {return "NUM"}
    elsif ($cat eq "PART") {return "PART"}
    elsif ($cat eq "PREP") {return "ADP"}
    elsif ($cat eq "PROCL") {return "ADV"}
    elsif ($cat eq "adv") {return "ADV"}
    elsif ($cat eq "depr") {return "NOUN"}
    elsif ($cat eq "R") {return "ADV"}
    elsif ($cat eq "P") {return "PRON"}
    elsif ($cat eq "W") {return "ADJ"} # winien (pollex)
    elsif ($cat eq "siebie") {return "PRON"}
    elsif ($cat =~ /^punct$/) {return "PUNCT"}
    elsif ($cat =~ /^pred$/) {return "VERB"}
    elsif ($cat eq "UNKNOWN") {return "UNK"}
    else {die "Unknown cat '$cat'"}
  }
}

sub decompose_and_convert_ms {
  my $UDcat = shift;
  my $cat = shift;
  my $ms = shift;
  my $UDms;
  %UDms = ();
  if ($multext) {
    $ms .= "------------------";
    my @codes = split //, (" ".$ms);
    if ($cat eq "N" || $cat eq "Y") {
      read_multext_gender($cat,$codes[2]);
      read_multext_number($cat,$codes[3]);
      read_multext_case($cat,$codes[4]);
      read_multext_definite($cat,$codes[5]);
      read_multext_animacy($cat,$codes[7]);
      read_multext_animacy_human($cat,$codes[12]);
    } elsif ($cat eq "V") {
      if ($sloleks) {
	$ms =~ s/^(.)(.)(.)(...)(.)(.)(....)/\1\3-\4\6\5\7\2/;
	@codes = split //, (" ".$ms);
      }
      if ($codes[2] eq "i") {$UDms{"Mood=Ind"}=1; $UDms{"VerbForm=Fin"}=1}
      elsif ($codes[2] eq "s") {$UDms{"Mood=Sub"}=1; $UDms{"VerbForm=Fin"}=1}
      elsif ($codes[2] eq "m") {$UDms{"Mood=Imp"}=1; $UDms{"VerbForm=Fin"}=1}
      elsif ($codes[2] eq "c") {$UDms{"VerbForm=Com"} = 1}
      elsif ($codes[2] eq "n") {$UDms{"VerbForm=Inf"} = 1}
      elsif ($codes[2] eq "p") {$UDms{"VerbForm=Part"} = 1}
      elsif ($codes[2] eq "g") {$UDms{"VerbForm=Ger"} = 1}
      elsif ($codes[2] eq "u") {$UDms{"VerbForm=Sup"} = 1}
      elsif ($codes[2] eq "t") {$UDms{"VerbForm=Conv"} = 1}
      elsif ($codes[2] eq "q") {$UDms{"Mood=Qot"}=1; $UDms{"VerbForm=Fin"}=1}
      elsif ($codes[2] eq "o") {die}
      elsif ($codes[2] eq "r") {$UDms{"Mood=Ind"}=1; $UDms{"Tense=Pres"}=1; $UDms{"VerbForm=Fin"} = 1}
      elsif ($codes[2] eq "f") {$UDms{"Mood=Ind"}=1; $UDms{"Tense=Fut"}=1; $UDms{"VerbForm=Fin"} = 1}
      elsif ($#codes < 2 || $codes[2] eq "-") {}
      else {die "Unknown code '$codes[2]' in position 2, category $cat"}
      if ($codes[3] eq "p") {$UDms{"Tense=Pres"}=1;}
      elsif ($codes[3] eq "i") {$UDms{"Tense=Imp"}=1;}
      elsif ($codes[3] eq "f") {$UDms{"Tense=Fut"}=1;}
      elsif ($codes[3] eq "s") {$UDms{"Tense=Past"} = 1}
      elsif ($codes[3] eq "l") {$UDms{"Tense=Pqp"} = 1}
      elsif ($codes[3] eq "a") {$UDms{"Tense=Past"} = 1;}# add  $UDms{"Aspect=Perf"} = 1?
      elsif ($#codes < 3 || $codes[3] eq "-") {}
      else {die "Unknown code '$codes[3]' in position 3, category $cat"}
      read_multext_pers($cat,$codes[4]);
      read_multext_number($cat,$codes[5]);
      read_multext_gender($cat,$codes[6]);
      if ($codes[7] eq "a") {$UDms{"Voice=Act"} = 1}
      elsif ($codes[7] eq "p") {$UDms{"Voice=Pass"} = 1}
      elsif ($codes[7] eq "m") {$UDms{"Voice=Mid"} = 1}
      elsif ($#codes < 7 || $codes[7] eq "-") {}
      else {die "Unknown code '$codes[7]' in position 7, category $cat"}
      read_multext_polarity($cat,$codes[8]);
    } elsif ($cat eq "A") {
      read_multext_degree($cat,$codes[2]);
      read_multext_gender($cat,$codes[3]);
      read_multext_number($cat,$codes[4]);
      read_multext_case($cat,$codes[5]);
      read_multext_definite($cat,$codes[6]);
      read_multext_animacy($cat,$codes[8]);
      read_multext_animacy_human($cat,$codes[16]);
      read_multext_polarity($cat,$codes[17]);
    } elsif ($cat eq "P") {
      if ($codes[1] eq "p") {$UDms{"PronType=Prs"}=1;}
      elsif ($codes[1] eq "d") {$UDms{"PronType=Dem"}=1;}
      elsif ($codes[1] eq "i") {$UDms{"PronType=Ind"}=1;}
      elsif ($codes[1] eq "s") {$UDms{"Poss=Yes"} = 1}
      elsif ($codes[1] eq "q") {$UDms{"PronType=Int"} = 1}
      elsif ($codes[1] eq "r") {$UDms{"PronType=Rel"}=1;}
      elsif ($codes[1] eq "e") {$UDms{"PronType=Exc"}=1;}
      elsif ($codes[1] eq "x") {$UDms{"PronType=Prs"}=1; $UDms{"Reflex=Yes"} = 1}
      elsif ($codes[1] eq "y") {$UDms{"PronType=Rcp"} = 1}
      elsif ($codes[1] eq "z") {$UDms{"PronType=Neg"}=1;}
      elsif ($codes[1] eq "g") {}
      elsif ($codes[1] eq "w") {$UDms{"PronType=Int"} = 1}# int-rel = ?
      elsif ($codes[1] eq "m") {} # determinal = ?
      elsif ($codes[1] eq "t") {} # ex-there = ?
      elsif ($codes[1] eq "n") {} # non-specific = ?
      elsif ($codes[1] eq "h") {$UDms{"PronType=Emp"} = 1}
      elsif ($#codes < 1 || $codes[1] eq "-") {}
      else {die "Unknown code '$codes[3]' in position 3, category $cat"}
      read_multext_pers($cat,$codes[2]);
      read_multext_gender($cat,$codes[3]);
      read_multext_number($cat,$codes[4]);
      read_multext_case($cat,$codes[5]);
      read_multext_definite($cat,$codes[11]);
      read_multext_animacy($cat,$codes[12]);
      read_multext_animacy_human($cat,$codes[19]);
    } elsif ($cat eq "D") {
      read_multext_prontype($cat,$codes[1]);
      read_multext_pers($cat,$codes[2]);
      read_multext_gender($cat,$codes[3]);
      read_multext_number($cat,$codes[4]);
      read_multext_case($cat,$codes[5]);
    } elsif ($cat eq "T") {
      if ($codes[1] eq "f") {$UDms{"Definite=Def"}=1;}
      elsif ($codes[1] eq "i") {$UDms{"Definite=Ind"}=1;}
      elsif ($codes[1] eq "s") {$UDms{"Poss=Yes"} = 1}
      elsif ($codes[1] eq "d") {$UDms{"PronType=Dem"} = 1}
      elsif ($#codes < 1 || $codes[1] eq "-") {}
      else {die "Unknown code '$codes[3]' in position 3, category $cat"}
      read_multext_pers($cat,$codes[2]);
      read_multext_gender($cat,$codes[3]);
      read_multext_number($cat,$codes[4]);
      read_multext_case($cat,$codes[5]);
    } elsif ($cat eq "R") {
      read_multext_degree($cat,$codes[2]);
      read_multext_number($cat,$codes[4]);
      read_multext_pers($cat,$codes[5]);
      read_multext_prontype($cat,$codes[6]);
      read_multext_case($cat,$codes[7]);
    } elsif ($cat eq "S") {
      if ($sloleks) {
	$ms = "--".$ms;
	@codes = split //, (" ".$ms);
      }
      if ($codes[1] eq "p") {$UDms{"AdpType=Prep"}=1;}
      elsif ($codes[1] eq "t") {$UDms{"AdpType=Post"}=1;}
      elsif ($#codes < 1 || $codes[1] eq "-") {}
      else {die "Unknown code '$codes[1]' in position 3, category $cat"}
      read_multext_case($cat,$codes[3]);
    } elsif ($cat eq "C") {
      read_multext_number($cat,$codes[6]);
      read_multext_pers($cat,$codes[7]);
    } elsif ($cat eq "M") {
      if ($sloleks) {
	$ms =~ s/^(.)(....)/\2\1/;
	@codes = split //, (" ".$ms);
      }
      read_multext_gender($cat,$codes[2]);
      read_multext_number($cat,$codes[3]);
      read_multext_case($cat,$codes[4]);
      read_multext_definite($cat,$codes[6]);
      read_multext_animacy($cat,$codes[10]);
      read_multext_animacy_human($cat,$codes[13]);
    } elsif ($cat eq "Q") {
    } elsif ($cat eq "I") {
    } elsif ($cat eq "X") {
    }
  } elsif ($udlex) {
    return "_" if $ms eq "";
    return $ms;
  } else {
    if ($cat eq "D") {$UDms{"AdpType=Prep"}}
    #  elsif ($cat eq "depr") {$UDms{"???Depreciative???"}}
    for (split /[:\.]/, $ms) {
      if ($_ eq $cat) {next}
      elsif (/^(?:1st|pri)$/) {$UDms{"Pers=1"} = 1}
      elsif (/^(?:2nd|sec)$/) {$UDms{"Pers=2"} = 1}
      elsif (/^(?:3rd|ter)$/) {$UDms{"Pers=3"} = 1}
      elsif (/^advpos$/) {$UDms{"Degree=Pos"}}
      elsif (/^advcomp$/) {$UDms{"Degree=Comp"}}
      elsif (/^advsup$/) {$UDms{"Degree=Sup"}}
      elsif (/^acc$/) {$UDms{"Case=Acc"} = 1}
      elsif (/^abl$/) {$UDms{"Case=Abl"} = 1}
      elsif (/^act$/) {$UDms{"Voice=Act"} = 1}
      elsif (/^aff$/) {$UDms{"Polarity=Pos"} = 1}
      elsif (/^aor$/) {$UDms{"Aspect=Perf"} = 1; $UDms{"Tense=Past"} = 1}
      elsif (/^comp$/) {$UDms{"Degree=Cmp"} = 1}
      elsif (/^dat$/) {$UDms{"Case=Dat"} = 1}
      elsif (/^dual$/) {$UDms{"Number=Dual"} = 1}
      elsif (/^f(?:em)?$/) {$UDms{"Gender=Fem"} = 1}
      elsif (/^fut$/) {$UDms{"Tense=Fut"} = 1}
      elsif (/^futperf$/) {$UDms{"Tense=Fut"} = 1}
      elsif (/^gen$/) {$UDms{"Case=Gen"} = 1}
      elsif (/^imperat$/) {$UDms{"Mood=Imp"} = 1; $UDms{"VerbForm=Fin"} = 1}
      elsif (/^imperf$/) {$UDms{"Aspect=Imperf"} = 1}
      elsif (/^ind$/) {$UDms{"Mood=Ind"}; $UDms{"VerbForm=Fin"} = 1}
      elsif (/^inf$/) {$UDms{"VerbForm=Inf"} = 1}
      elsif (/^interrog$/) {$UDms{"PronType=Int"} = 1}
      elsif (/^irreg_comp$/) {$UDms{"Degree=Cmp"} = 1}
      elsif (/^irreg_superl$/) {$UDms{"Degree=Sup"} = 1}
      elsif (/^m(?:asc|[123])$/) {$UDms{"Gender=Masc"} = 1}
      elsif (/^mid$/) {$UDms{"Voice=Mid"} = 1}
      elsif (/^mp$/) {$UDms{"Voice=Mid,Pass"} = 1}
      elsif (/^n(?:eut)?$/) {$UDms{"Gender=Neut"} = 1}
      elsif (/^neg$/) {$UDms{"Polarity=Neg"} = 1}
      elsif (/^nom$/) {$UDms{"Case=Nom"} = 1}
      elsif (/^opt$/) {$UDms{"Mood=Opt"} = 1; $UDms{"VerbForm=Fin"} = 1}
      elsif (/^part$/) {$UDms{"VerbForm=Part"} = 1}
      elsif (/^pass$/) {$UDms{"Voice=Pass"} = 1}
      elsif (/^perf$/) {$UDms{"Aspect=Perf"} = 1}
      elsif (/^(?:adj)?pl$/) {$UDms{"Number=Plur"} = 1}
      elsif (/^plup$/) {$UDms{"Tense=Pqp"} = 1}
      elsif (/^prep$/) {$UDms{"AdpType=Prep"} = 1}
      elsif (/^pres$/) {$UDms{"Tense=Pres"} = 1}
      elsif (/^(?:adj)?sg$/) {$UDms{"Number=Sing"} = 1}
      elsif (/^subj$/) {$UDms{"Mood=Sub"} = 1; $UDms{"VerbForm=Fin"} = 1}
      elsif (/^sup(?:erl)?$/) {$UDms{"Degree=Sup"} = 1}
      elsif (/^voc$/) {$UDms{"Case=Voc"} = 1}
      elsif (/^gerundive$/) {$UDms{"VerbForm=Gdv"} = 1}
      elsif (/^supine$/) {$UDms{"VerbForm=Sup"} = 1}
      elsif (/^loc$/) {$UDms{"Case=Loc"} = 1}
      elsif (/^inst$/) {$UDms{"Case=Inst"} = 1}
      elsif (/^pos$/) {$UDms{"Degree=Pos"} = 1}
      elsif (/^nwok$/) {$UDms{"Variant=Short"} = 1}
      elsif (/^wok$/) {$UDms{"Variant=Long"} = 1}
      elsif (/^npraep$/) {$UDms{"Variant=Short"} = 1}
      elsif (/^praep$/) {$UDms{"Variant=Long"} = 1}
      elsif (/^_HIDDEN_LEMMA_$/) {$UDms{"UNK"} = 1}
      elsif (/^(adverb|adverbial|aeolic|alphabetic|attic|conj|contr|doric|enclitic|epic|exclam|expletive|geog_name|homeric|indecl|indeclform|ionic|iota_intens|nodiacritics|nu_movable|numeral|parad_form|particle|poetic|proclitic|prose|n?agl|n?akc|rec|congr|\?)$/) {}
      else {die "Unknown MS: '$_'"}
    }
  }
  if (scalar keys %UDms > 0) {
    return "UNK" if defined($UDms{UNK});
    $UDms = join ("|", sort {$a cmp $b} keys %UDms);
  } else {
    $UDms = "_";
  }
  return $UDms;
}

sub read_multext_case {
  my $cat = shift;
  my $c = shift;
  if ($c eq "n") {$UDms{"Case=Nom"} = 1}
  elsif ($c eq "g") {$UDms{"Case=Gen"} = 1}
  elsif ($c eq "d") {$UDms{"Case=Dat"} = 1}
  elsif ($c eq "a") {$UDms{"Case=Acc"} = 1}
  elsif ($c eq "v") {$UDms{"Case=Voc"} = 1}
  elsif ($c eq "l") {$UDms{"Case=Loc"} = 1}
  elsif ($c eq "i") {$UDms{"Case=Ins"} = 1}
  elsif ($c eq "r") {$UDms{"Case=Nom"} = 1}
  elsif ($c eq "o") {$UDms{"Case=Acc"} = 1}
  elsif ($c eq "1") {$UDms{"Case=Par"} = 1}
  elsif ($c eq "x") {$UDms{"Case=Ill"} = 1}
  elsif ($c eq "2") {$UDms{"Case=Ine"} = 1}
  elsif ($c eq "e") {$UDms{"Case=Ela"} = 1}
  elsif ($c eq "t") {$UDms{"Case=All"} = 1}
  elsif ($c eq "3") {$UDms{"Case=Ade"} = 1}
  elsif ($c eq "b") {$UDms{"Case=Abl"} = 1}
  elsif ($c eq "4") {$UDms{"Case=Tra"} = 1}
  elsif ($c eq "9") {$UDms{"Case=Ter"} = 1}
  elsif ($c eq "w") {$UDms{"Case=Ess"} = 1}
  elsif ($c eq "5") {$UDms{"Case=Abe"} = 1}
  elsif ($c eq "k") {$UDms{"Case=Com"} = 1}
  elsif ($c eq "7") {$UDms{"Case=Add"} = 1}
  elsif ($c eq "m") {$UDms{"Case=Tem"} = 1}
  elsif ($c eq "c") {$UDms{"Case=Cau"} = 1}
  elsif ($c eq "s") {$UDms{"Case=Sub"} = 1}
  elsif ($c eq "h") {$UDms{"Case=Del"} = 1}
  elsif ($c eq "q") {$UDms{"Case=Com"} = 1}
  elsif ($c eq "y") {$UDms{"Case=Tra"} = 1}
  elsif ($c eq "p") {$UDms{"Case=Sup"} = 1}
  elsif ($c eq "u") {$UDms{"Case=Dis"} = 1}
  elsif ($c eq "f") {$UDms{"Case=Ess"} = 1;$UDms{"Polite=Form"} = 1}
  elsif ($c eq "-") {}
  else {die "Unknown case '$c', category $cat"}
}

sub read_multext_definite {
  my $cat = shift;
  my $c = shift;
  if ($c eq "n") {$UDms{"Definite=Ind"} = 1}
  elsif ($c eq "y") {$UDms{"Definite=Def"} = 1}
  elsif ($c eq "s") {$UDms{"Definite=Def"} = 1}#?
  elsif ($c eq "f") {$UDms{"Definite=Def"} = 1}#?
  elsif ($c eq "p") {$UDms{"Definite=Def"} = 1}#?
  elsif ($c eq "d") {$UDms{"Definite=Def"} = 1}#?
  elsif ($c eq "-") {}
  else {die "Unknown definiteness '$c', category $cat"}
}

sub read_multext_animacy {
  my $cat = shift;
  my $c = shift;
  if ($c eq "n") {$UDms{"Animacy=Anim"} = 1}
  elsif ($c eq "y") {$UDms{"Animacy=Inan"} = 1}
  elsif ($c eq "-") {}
  else {die "Unknown animacy '$c', category $cat"}
}

sub read_multext_animacy_human {
  my $cat = shift;
  my $c = shift;
  if ($c eq "n") {$UDms{"Animacy=Nhum"} = 1}
  elsif ($c eq "y") {$UDms{"Animacy=Hum"} = 1}
  elsif ($c eq "-") {}
  else {die "Unknown humanness '$c', category $cat"}
}

sub read_multext_polarity {
  my $cat = shift;
  my $c = shift;
  if ($c eq "n") {$UDms{"Polarity=Pos"} = 1}
  elsif ($c eq "y") {$UDms{"Polarity=Neg"} = 1}
  elsif ($c eq "-") {}
  else {die "Unknown polarity '$c', category $cat"}
}

sub read_multext_degree {
  my $cat = shift;
  my $c = shift;
  if ($c eq "p") {$UDms{"Degree=Pos"}=1;}
  elsif ($c eq "c") {$UDms{"Degree=Cmp"}=1;}
  elsif ($c eq "s") {$UDms{"Degree=Sup"}=1;}
  elsif ($c eq "e") {}
  elsif ($c eq "d") {}
  elsif ($c eq "m") {} #??? found in HML5
  elsif ($c eq "-") {}
  else {die "Unknown degree '$c', category $cat"}
}

sub read_multext_gender {
  my $cat = shift;
  my $c = shift;
  if ($c eq "m") {$UDms{"Gender=Masc"} = 1}
  elsif ($c eq "f") {$UDms{"Gender=Fem"} = 1}
  elsif ($c eq "n") {$UDms{"Gender=Neut"} = 1}
  elsif ($c eq "c") {$UDms{"Gender=Com"} = 1}
  elsif ($c eq "-") {}
  else {die "Unknown gender '$c', category $cat"}
}

sub read_multext_number {
  my $cat = shift;
  my $c = shift;
  if ($c eq "s") {$UDms{"Number=Sing"}=1;}
  elsif ($c eq "p") {$UDms{"Number=Plur"}=1;}
  elsif ($c eq "d") {$UDms{"Number=Dual"}=1;}
  elsif ($c eq "l") {$UDms{"Number=Coll"}=1;}
  elsif ($c eq "-") {}
  else {die "Unknown number '$c', category $cat"}
}

sub read_multext_pers {
  my $cat = shift;
  my $c = shift;
  if ($c eq "1") {$UDms{"Pers=1"}=1;}
  elsif ($c eq "2") {$UDms{"Pers=2"}=1;}
  elsif ($c eq "3") {$UDms{"Pers=3"}=1;}
  elsif ($c eq "-") {}
  else {die "Unknown person '$c', category $cat"}
}

sub read_multext_prontype {
  my $cat = shift;
  my $c = shift;
  if ($c eq "p") {$UDms{"PronType=Prs"}=1;}
  elsif ($c eq "d") {$UDms{"PronType=Dem"}=1;}
  elsif ($c eq "i") {$UDms{"PronType=Ind"}=1;}
  elsif ($c eq "s") {$UDms{"Poss=Yes"} = 1}
  elsif ($c eq "q") {$UDms{"PronType=Int"} = 1}
  elsif ($c eq "r") {$UDms{"PronType=Rel"}=1;}
  elsif ($c eq "e") {$UDms{"PronType=Exc"}=1;}
  elsif ($c eq "x") {$UDms{"PronType=Prs"}=1; $UDms{"Reflex=Yes"} = 1}
  elsif ($c eq "y") {$UDms{"PronType=Rcp"} = 1}
  elsif ($c eq "z") {$UDms{"PronType=Neg"}=1;}
  elsif ($c eq "g") {}
  elsif ($c eq "w") {$UDms{"PronType=Int"} = 1}# int-rel = ?
  elsif ($c eq "m") {} # determinal = ?
  elsif ($c eq "t") {} # ex-there = ?
  elsif ($c eq "n") {} # non-specific = ?
  elsif ($c eq "h") {$UDms{"PronType=Emp"} = 1}
  elsif ($c eq "-") {}
  else {die "Unknown pron type '$c', category $cat"}
}
