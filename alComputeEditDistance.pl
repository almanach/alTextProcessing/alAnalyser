#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use alEditDistance;
use strict;

while (<>) {
  chomp;
  /^(\S+)\t(\S+)$/ || die $_;
  print "$1\t$2\t".edit_distance($1,$2,1)."\n";
}
