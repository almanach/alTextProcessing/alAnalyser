#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;
use alLanguageNameConverter;

my $mode = shift || 0;

while (<>) {
  chomp;
  if ($mode == 0) {
    print $_."\t".abbr2name($_)."\t".abbr2wikicode($_)."\n";
  } elsif ($mode == 1) {
    print name2abbr($_)."\t".$_."\t".name2wikicode($_)."\n";
  }
}
