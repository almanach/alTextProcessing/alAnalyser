#!/usr/bin/perl

use utf8;
use strict;
use Lingua::ZH::HanConvert;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";


while (<>) {
  chomp;
  print Lingua::ZH::HanConvert::simple($_);
  print "\n";
}
