#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use alRawTextCleaning;
use alURL;
use alEmail;
use alTokenisation;
use alEditDistance;
use strict;

my $sourcelanglex = shift || die;

my %lex;

open LEX, "<$sourcelanglex" || die $!;
binmode LEX, ":utf8";
while (<LEX>) {
  chomp;
  /^([^\t]+)\t([^\t]+)/ || next;
  $lex{$1}{$2} = 1;
}

while (<>) {
  chomp;
  /^([^\t]+)\t([^\t]+)/ || next;
  if (defined($lex{$2})) {
    for (keys %{$lex{$2}}) {
      print "$1\t$_\t*\n";
    }
  }
}
