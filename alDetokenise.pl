#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use FindBin;
use lib $FindBin::Bin;
use utf8;
use alTokenisation;
use alRawTextCleaning;
use strict;

$| = 1;

while (<>) {
  chomp;
  print altok_deescape_xml(altok_detokenise($_))."\n";
}
