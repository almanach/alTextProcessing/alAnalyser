package alEditDistance;
use utf8;
use strict;
use alVariables;
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(&edit_distance &edit_distance_fast &update_edit_distance_weight);

my %weights;

sub _min3 {
  my ($a,$b,$c)=@_;
  return _min($a,_min($b,$c));
}

sub _min {
  my ($a,$b)=@_;
  return $a if $a < $b;
  return $b;
}

sub _max {
  my $a = shift;
  my $b = shift;
  return $a if $a > $b;
  return $b;
}

sub update_edit_distance_weight {
  my ($c1, $c2, $w) = @_;
  $c1 = "\t" if $c1 eq "";
  $c2 = "\t" if $c2 eq "";
  $weights{$c1}{$c2} = $w;
}


sub _weight {
  my ($x,$y,$i,$j,$n,$m,$verbose)=@_;
  my $sim;
  if (defined($weights{$x}) && defined($weights{$x}{$y})) {
    $sim = $weights{$x}{$y};
  } elsif ($x eq $y) {
    $sim = 0;
  } else {
    $sim = 1;
  }
  return $sim;
}

sub edit_distance {

  my ($s,$t,$verbose,$max)=@_;

  my ($weight1, $weight2, $weight3);

  my $n=length($s);
  my $m=length($t);

  my @d;

  $d[0][0]=0;

  foreach my $i (1 .. $n) {
    $d[$i][0] = $d[$i-1][0]+_weight(substr($s,$i-1,1),"\t",$i,0,$n,$m);
  }
  $d[$n+1][0] = $d[$n][0];
  foreach my $j (1 .. $m) {
    $d[0][$j] = $d[0][$j-1]+_weight(substr($t,$j-1,1),"\t",0,$j,$n,$m);
  }
  $d[0][$m+1] = $d[0][$m];

  my (%back_i, %back_j);
  
  foreach my $i (1 .. $n+1) {
    my $s_i;
    if ($i == 0) {
      $s_i = "\t";
    } elsif ($i == $n+1) {
      $s_i = "\t";
    } else {
      $s_i=substr($s,$i-1,1);
    }
    foreach my $j (1 .. $m+1) {

      my $t_j;
      if ($j == 0) {
	$t_j = "\t";
      } elsif ($j == $m+1) {
	$t_j = "\t";
      } else {
	$t_j=substr($t,$j-1,1);
      }

      $weight1 = $d[$i-1][$j]+_weight($s_i,"\t",$i,$j,$n,$m,$verbose);
      $weight2 = $d[$i][$j-1]+_weight("\t",$t_j,$i,$j,$n,$m,$verbose);
      $weight3 = $d[$i-1][$j-1]+_weight($s_i,$t_j,$i,$j,$n,$m,$verbose);

      $d[$i][$j]=_min3($weight1, $weight2, $weight3);
      if ($d[$i][$j] == $weight3) {
	$back_i{$i}{$j} = $i-1;
	$back_j{$i}{$j} = $j-1;
      } elsif ($d[$i][$j] == $weight1) {
	$back_i{$i}{$j} = $i-1;
	$back_j{$i}{$j} = $j;
      } elsif ($d[$i][$j] == $weight2) {
	$back_i{$i}{$j} = $i;
	$back_j{$i}{$j} = $j-1;
      }

    }

    if ($max) {
      my $bool = 1;
      for (1..$#{$d[$i]}) {
	if ($d[$i][$_] < $max) {
	  $bool = 0;
	  last;
	}
      }
      return (9000+$i).($verbose ? "\t" : "") if $bool;
    }
  }

  return "9999".($verbose ? "\t" : "") if $max && $d[$n+1][$m+1] > $max;
  
  if ($verbose) {
    my $cur_i = $n+1;
    my $cur_j = $m+1;
    my $prev_i;
    my $prev_j;
    my $one_of_the_best_paths;
    while ($cur_i > 0 || $cur_j > 0) {
      $prev_i = $cur_i;
      $prev_j = $cur_j;
      ($cur_i,$cur_j) = ($back_i{$prev_i}{$cur_j},$back_j{$prev_i}{$cur_j});
      if ($prev_i == $cur_i) {
	$one_of_the_best_paths = "(|".substr($t,$cur_j,1).") ".$one_of_the_best_paths;
      } elsif ($prev_j == $cur_j) {
	$one_of_the_best_paths = "(".substr($s,$cur_i,1)."|) ".$one_of_the_best_paths;
      } else {
	$one_of_the_best_paths = "(".substr($s,$cur_i,1)."|".substr($t,$cur_j,1).") ".$one_of_the_best_paths;
      }
    }
    $one_of_the_best_paths =~ s/ \(\|\) $//;
#    while ($one_of_the_best_paths =~ s/ \(\|([^)])\) \(\|([^)]+)\)$/ (|$1$2)/) {}
#    while ($one_of_the_best_paths =~ s/ \(([^)])\|\) \(([^)]+)\|\)$/ (|$1$2)/) {}
#    while ($one_of_the_best_paths =~ s/\|\) \(([^\|()]+)\|\) \(/$1|) (/) {}
    return $d[$n+1][$m+1]."\t".$one_of_the_best_paths;#/sqrt(_min(_plength($s),_plength($t)));
  } else {
    return $d[$n+1][$m+1];#/sqrt(_min(_plength($s),_plength($t)));
  }
}

sub _plength {
  my $s = shift;
  my $l = 0;
  my $pos = 0;
  my $posfactor;
  my $length = length($s);
  while ($s =~ s/^(.)//) {
    $pos++;
    if ($pos > 3) {
      $posfactor = ($length-$pos+1)/($length-2);
    } else {
      $posfactor = 1;
    }
    if ($1 =~ /^[aeiouǫęôêy]$/) {
      $l += 0.1*$posfactor;
    } else {
      $l += $posfactor;
    }
  }
  return $l;
}

sub edit_distance_fast {
  my ($s1, $s2, $mode, $max) = @_;
  # mode: default = 0; add 1 for 'remove_diacritics', add 2 for taking '%weights' into account
  my @ar1;
  my @ar2;
  if ($mode == 1) {
    @ar1 = split //, remove_diacritics($s1);
    @ar2 = split //, remove_diacritics($s2);
  } else {
    @ar1 = split //, $s1;
    @ar2 = split //, $s2;
  }
  
  my @d;
  $d[$_][0] = $_ foreach (0 .. @ar1);
  $d[0][$_] = $_ foreach (0 .. @ar2);
  
  my $weight_del = 1;
  my $weight_ins = 1;
  for my $i (1 .. @ar1){
    for my $j (1 .. @ar2){
      my $weight_subst = $ar1[$i-1] eq $ar2[$j-1] ? 0 : 1;
      if ($mode >= 2) {
	$weight_subst = $weights{$ar1[$i-1]}{$ar2[$j-1]} if $weight_subst == 1 && defined($weights{$ar1[$i-1]}{$ar2[$j-1]});
	$weight_del = $weights{$ar1[$i]}{"_"} if defined($weights{$ar1[$i]}{"_"});
	$weight_ins = $weights{"_"}{$ar2[$j]} if defined($weights{"_"}{$ar2[$j]});
      }
      $d[$i][$j] = _min3(
		       $d[$i-1][$j] + $weight_ins, 
		       $d[$i][$j-1] + $weight_del, 
		       $d[$i-1][$j-1] + $weight_subst );
    }
    if ($max) {
      my $bool = 1;
      for (1..$#{$d[$i]}) {
	if ($d[$i][$_] < $max) {
	  $bool = 0;
	  last;
	}
      }
      return (9000+$i) if $bool;
    }
  }
  return 9999 if $max > 0 && $d[@ar1][@ar2] > $max;
  
  return $d[@ar1][@ar2];
}

sub remove_diacritics {
  my ($s) = @_;
  $s =~ tr/ǽǣáàâäąãăåćčçďéèêëęěğìíîĩĭıïĺľłńñňòóôõöøŕřśšşťţùúûũüǔỳýŷÿźẑżžÁÀÂÄĄÃĂÅĆČÇĎÉÈÊËĘĚĞÌÍÎĨĬİÏĹĽŁŃÑŇÒÓÔÕÖØŔŘŚŠŞŤŢÙÚÛŨÜǓỲÝŶŸŹẐŻŽ/ææaaaaaaaacccdeeeeeegiiiiiiilllnnnoooooorrsssttuuuuuuyyyyzzzzAAAAAAAACCCDEEEEEEGIIIIIIILLLNNNOOOOOORRSSSTTUUUUUUYYYYZZZZ/;
  $s =~ s/œ/oe/g;
  $s =~ s/æ/ae/g;
  $s =~ s/ƣ/oi/g;
  $s =~ s/ĳ/ij/g;
  $s =~ s/ȣ/ou/g;
  $s =~ s/Œ/OE/g;
  $s =~ s/Æ/AE/g;
  $s =~ s/Ƣ/OI/g;
  $s =~ s/Ĳ/IJ/g;
  $s =~ s/Ȣ/OU/g;
  return $s;
}

1;
