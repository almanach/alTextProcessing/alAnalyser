package alEmail;
use utf8;
use strict;
use alVariables;
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(&altok_email);

sub altok_email {
  my $s = shift;
  $s =~ s/ ((?:mailto +: +)?(?:\w| [\.．\%-] ?)+ *[＠\@] *(?:(?:$alAlphanum| - ?)+ +[\.．] +)+${alAlphanum}{2,4}) / {$1} _EMAIL /go;
  $s =~ s/ (\&lt; +)\{($comment_content)} _EMAIL( +\&gt;) / \{$1$2$3\} _EMAIL /go;
  return $s;
}

1;
