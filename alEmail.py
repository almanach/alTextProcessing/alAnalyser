#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import alVariables

import regex as re

def altok_email(s):
	#en perl, il y avait des modifiers O aussi sur ces regex, en python pas forcément nécessaire de les compile car on les utilise pas souvent ?
	s = re.sub(rf" ((?:mailto +: +)?(?:\w| [\.．\%-] ?)+ *[＠\@] *(?:(?:{alVariables.alAlphanum.pattern}| - ?)+ +[\.．] +)+{alVariables.alAlphanum.pattern}{{2,4}}) ", r" {\1} _EMAIL ", s)
	s = re.sub(rf" (\&lt; +){{({alVariables.comment_content.pattern})}} _EMAIL( +\&gt;) ", r" {\1\2\3} _EMAIL ", s)
	return s