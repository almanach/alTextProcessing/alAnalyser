#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;
use Encode;

my $embedfileprefix = shift || die;
my $minocc = shift || 30;

my %lex;
my %newlex;
while (<>) {
  chomp;
  s/ / /g;
  /^([^\t]+)\t([^\t]+)/ || next;
  $lex{$1}{$2} = 1;
}

my (%ewords, %nonrarewords, @ewords, @embeddings, $word, $occ, @e, $prod, @prods, @sorted_indices);
open EMBED, "<$embedfileprefix.vocab" || die $!;
binmode EMBED, ":utf8";
my $n = 0;
my $nembeds = 0;
while (<EMBED>) {
  chomp;
  print STDERR "\r  Loading vocab...".(++$n);
  next unless is_clean_utf8($_);
#  next if / /;
  s/^([^ ]+) (\d+)$// || die;
  $word = $1;
  $occ = $2;
  next if $occ < $minocc;
  next if length($word) > 16;
  next if $word =~ /^[\d\p{Punct}]+$/;
  next if $word =~ /^\p{Punct}+\w+\p{Punct}*$/;
  if (defined($lex{$word})) {
    if ($nembeds <= 10000) {
      $ewords[$nembeds] = $word;
      $ewords{$word} = $nembeds;
      $nembeds++;
    }
  } else {
    $nonrarewords{$word} = $occ;
  }
}
close EMBED;
print STDERR " -> $nembeds reference embeddings\n";
open EMBED, "<$embedfileprefix.vectors" || die $!;
binmode EMBED, ":utf8";
$n = 0;
my $veclen;
while (<EMBED>) {
  chomp;
  print STDERR "\r  Loading reference embeddings...".(++$n);
  next unless is_clean_utf8($_);
#  next if / /;
  s/^([^ ]+) // || die;
  $word = $1;
  next unless defined($ewords{$word});
  $nembeds = $ewords{$word};
  @{$embeddings[$nembeds]} = split / /, $_;
  $veclen = 0;
  for (0..$#{$embeddings[$nembeds]}) {
    $veclen += $embeddings[$nembeds][$_]**2;
  }
  $veclen = sqrt($veclen);
  for (0..$#{$embeddings[$nembeds]}) {
    $embeddings[$nembeds][$_] /= $veclen;
  }
  die "<$word><$#{$embeddings[$nembeds]}><$_>" unless $#{$embeddings[$nembeds]} == 99;
}
close EMBED;
print STDERR "\n";

open EMBED, "<$embedfileprefix.vectors" || die $!;
binmode EMBED, ":utf8";
my ($best_index, $best_score);
chomp(my $header = <EMBED>);
die "|$header|" unless $header =~ s/ 100$//;
$n = 0;
my $nb_nonrarewords = scalar keys %nonrarewords;
while (<EMBED>) {
  chomp;
  next unless is_clean_utf8($_);
  next if / /;
  s/^([^ ]+) // || die;
  $word = $1;
  next unless defined($nonrarewords{$word});
  print STDERR "\r  Studying embeddings for OOL words...".(++$n)."/$nb_nonrarewords ($word)                                               ";
  @e = split / /, $_;
  $veclen = 0;
  for (0..$#e) {
    $veclen += $e[$_]**2;
  }
  $veclen = sqrt($veclen);
  for (0..$#e) {
    $e[$_] /= $veclen;
  }
  $best_index = -1;
  $best_score = -1000000;
 loop:
  for my $i (0..$#embeddings) {
    $prod = 0;
    for my $j (0..$#{$embeddings[$i]}) {
      $prod += $e[$j]*$embeddings[$i][$j];
    }
    if ($prod > $best_score) {
      $best_score = $prod;
      $best_index = $i;
    }
  }
  if ($best_score > 0.8) {
    for (keys %{$lex{$ewords[$best_index]}}) {
      $newlex{$word}{$_} = "$best_score $nonrarewords{$word}";
    }
  }
}
close EMBED;
print STDERR "\r  Loading embeddings...done                       \n";

for $word (sort keys %lex) {
  for (sort keys %{$lex{$word}}) {
    print nbsp2sp($word)."\t$_\t*\n";
  }
}
for $word (sort keys %newlex) {
  for (sort keys %{$newlex{$word}}) {
    print nbsp2sp($word)."\t$_\t$newlex{$word}{$_}\n";
  }
}


sub clean_utf8 {
  my $s = shift;
  # takes any UTF-8 encoded string as an input (including strings with malformed UTF-8) and discards any malformed character, thus returning valid UTF-8, although resulting in possible information loss
  my ($r, $c);
  eval {
    $r = Encode::encode('UTF-8', $s, Encode::FB_CROAK);
  };
  if ($@) {
    $r = "";
    for my $char (split(//, $s)) {
      $c = eval { Encode::encode('UTF-8', $char, Encode::FB_CROAK) } or next;
      $r .= $c;
    }
  }
  return Encode::decode('UTF-8',$r);
}

sub is_clean_utf8 {
  my $s = shift;
  # takes any UTF-8 encoded string as an input (including strings with malformed UTF-8) and returns 1 if it is valid
  eval {
    $s = Encode::encode('UTF-8', $s, Encode::FB_CROAK);
  };
  if ($@) {
    return 0;
  }
  return 1;
}

sub nbsp2sp {
  my $s = shift;
  $s =~ s/ / /g;
  return $s;
}
