#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

my @l;
my (%olex_ctfl, %olex_cfl, %olex_fcl, %olex_fctl, %ilex_ctfl, %ilex_cfl, %ilex_fcl, %ilex_fctl, %olex_cfttparts, %ilex_ct_xc, $ilex_type);

my $otherlexfile = shift || die;
open OLEX, "<$otherlexfile" || die $!;
binmode OLEX, ":utf8";
while (<OLEX>) {
  chomp;
  @l = split /\t/, $_;
  if ($#l == 2) {
    if ($l[1] =~ /^([^#]+)#(.*)/) {
      my ($c,$t) = ($1,$2);
      $olex_cfl{$c}{$l[0]}{$l[2]} = 1;
      $olex_fcl{$l[0]}{$c}{$l[2]} = 1;
      $olex_ctfl{$c}{$t}{$l[0]}{$l[2]} = 1;
      $olex_fctl{$l[0]}{$c}{$t}{$l[2]} = 1;
      for (split /[#\.\t\|]/, $t) {
	$olex_cfttparts{$c}{$l[0]}{$t}{$_} = 1;
      }
    } else {
      $olex_cfl{$l[1]}{$l[0]}{$l[2]} = 1;
      $olex_fcl{$l[0]}{$l[1]}{$l[2]} = 1;
      $olex_ctfl{$l[1]}{""}{$l[0]}{$l[2]} = 1;
      $olex_fctl{$l[0]}{$l[1]}{""}{$l[2]} = 1;
    }
  } elsif ($#l == 5 && /^\d+\t/) {
    $olex_cfl{$l[3]}{$l[1]}{$l[2]} = $l[0];
    $olex_fcl{$l[1]}{$l[3]}{$l[2]} = $l[0];
    $olex_ctfl{$l[3]}{$l[5]}{$l[1]}{$l[2]} = $l[0];
    $olex_fctl{$l[1]}{$l[3]}{$l[5]}{$l[2]} = $l[0];
    for (split /[#\.\t\|]/, $l[5]) {
      $olex_cfttparts{$l[2]}{$l[0]}{$l[5]}{$_} = 1;
    }
  }
}
close OLEX;

while (<>) {
  chomp;
  @l = split /\t/, $_;
  if ($#l == 2) {
    if ($l[1] =~ /^([^#]+)#(.*)/) {
      my ($c,$t) = ($1,$2);
      next if $c eq "_";
      $ilex_cfl{$c}{$l[0]}{$l[2]} = 1;
      $ilex_fcl{$l[0]}{$c}{$l[2]} = 1;
      $ilex_ctfl{$c}{$t}{$l[0]}{$l[2]} = 1;
      $ilex_fctl{$l[0]}{$c}{$t}{$l[2]} = 1;
    } else {
      $ilex_cfl{$l[1]}{$l[0]}{$l[2]} = 1;
      $ilex_fcl{$l[0]}{$l[1]}{$l[2]} = 1;
      $ilex_ctfl{$l[1]}{""}{$l[0]}{$l[2]} = 1;
      $ilex_fctl{$l[0]}{$l[1]}{""}{$l[2]} = 1;
    }
    $ilex_type = "ftl";
  } elsif ($#l == 5 && /^\d+\t/) {
    next if $l[3] eq "_";
    $ilex_cfl{$l[3]}{$l[1]}{$l[2]} = $l[0];
    $ilex_fcl{$l[1]}{$l[3]}{$l[2]} = $l[0];
    $ilex_ctfl{$l[3]}{$l[5]}{$l[1]}{$l[2]} = $l[0];
    $ilex_fctl{$l[1]}{$l[3]}{$l[5]}{$l[2]} = $l[0];
    $ilex_ct_xc{$l[3]}{$l[5]}{$l[4]}++;
    $ilex_type = "conllish";
  }
}


# finding mappings from other cats and tags to input cats (stored in decision tree)
# 1. simple cases
my %tmp;
for my $f (keys %olex_fctl) {
  for my $oc (keys %{$olex_fctl{$f}}) {
    next if scalar keys %{$olex_fctl{$f}{$oc}} > 1;
    next unless defined $ilex_fctl{$f};
    next if scalar keys %{$ilex_fctl{$f}} > 1;
    my $locc;
    for my $ot (keys %{$olex_fctl{$f}{$oc}}) {
      for my $ic (keys %{$ilex_fctl{$f}}) {
	next if scalar keys %{$ilex_fctl{$f}{$ic}} > 1;
	for my $it (keys %{$ilex_fctl{$f}{$ic}}) {
	  for my $l (keys %{$olex_fctl{$f}{$oc}{$ot}}) {
	    $tmp{$oc}{$ot}{$ic}{$it}++;
	  }
	}
      }
    }
  }
}
my (%oc_ot2nonamb_ic, %oc_ot2nonamb_it);
for my $oc (keys %tmp) {
  for my $ot (keys %{$tmp{$oc}}) {
    next if scalar keys %{$tmp{$oc}{$ot}} > 1;
    for my $ic (keys %{$tmp{$oc}{$ot}}) {
      next if scalar keys %{$tmp{$oc}{$ot}{$ic}} > 1;
      for my $it (keys %{$tmp{$oc}{$ot}{$ic}}) {
	$oc_ot2nonamb_ic{$oc}{$ot} = $ic;
	$oc_ot2nonamb_it{$oc}{$ot} = $it;
      }
    }
  }
}
# 2. remaining cases: we do something complicated
my (%oc_otpart_ic, $denominator);
my ($best_otpart, $best_otpart_score, $ic_for_best_otpart);
my ($best_otpart2, $best_otpart_score2, $ic_for_best_otpart2);
my %chosen_otparts;
my (%decision_tree_input, %decision_tree_output, %otpart_ics);
for my $oc (keys %olex_ctfl) {
  # trying to be clever: finding tag parts that allow for chosing the right category
  %chosen_otparts = ();
  my $didsomething = 1;
  while ($didsomething) {
    $didsomething = 0;
    %oc_otpart_ic = ();

    for my $ot (keys %{$olex_ctfl{$oc}}) {
    floop:
      for my $f (keys %{$olex_ctfl{$oc}{$ot}}) {
	next unless defined $ilex_fcl{$f};
	for (keys %{$olex_cfttparts{$oc}{$f}{$ot}}) {
	  next floop if defined $chosen_otparts{$_};
	}
	$denominator = (scalar keys %{$ilex_fcl{$f}});
	$denominator *= $denominator;
	for my $otpart (split /[#\.\t\|]/, $ot) {
	  for my $ic (keys %{$ilex_fcl{$f}}) {
	    $oc_otpart_ic{$oc}{$otpart}{$ic} += 1/$denominator;
	    $oc_otpart_ic{$oc}{$otpart}{__ALL__} += 1/$denominator;
	  }
	}
      }
    }

    %otpart_ics = ();
    $best_otpart_score = 0;
    for my $otpart (keys %{$oc_otpart_ic{$oc}}) {
      for my $ic (sort {$oc_otpart_ic{$oc}{$otpart}{$b}/$oc_otpart_ic{$oc}{$otpart}{__ALL__} <=> $oc_otpart_ic{$oc}{$otpart}{$a}/$oc_otpart_ic{$oc}{$otpart}{__ALL__}} keys %{$oc_otpart_ic{$oc}{$otpart}}) {
	next if $ic eq "__ALL__";
	next if $oc_otpart_ic{$oc}{$otpart}{$ic} < 5;
	next if defined $chosen_otparts{$otpart};
	push @{$otpart_ics{$otpart}}, $ic;
      }
    }
    for my $otpart (keys %{$oc_otpart_ic{$oc}}) {
      if ($oc_otpart_ic{$oc}{$otpart}{$otpart_ics{$otpart}[0]}/$oc_otpart_ic{$oc}{$otpart}{__ALL__} > $best_otpart_score) {
	$best_otpart = $otpart;
	$best_otpart_score = $oc_otpart_ic{$oc}{$otpart}{$otpart_ics{$otpart}[0]}/$oc_otpart_ic{$oc}{$otpart}{__ALL__};
	$ic_for_best_otpart = $otpart_ics{$otpart}[0];
      }
    }
    if ($best_otpart_score > 0 && $best_otpart_score < 0.8) { #retrying with 2 (ambiguous) input cats
      $best_otpart_score2 = 0;
      for my $otpart (keys %{$oc_otpart_ic{$oc}}) {
	if (defined($otpart_ics{$otpart}[1])) {
	  if (($oc_otpart_ic{$oc}{$otpart}{$otpart_ics{$otpart}[0]}+$oc_otpart_ic{$oc}{$otpart}{$otpart_ics{$otpart}[1]})/$oc_otpart_ic{$oc}{$otpart}{__ALL__} > $best_otpart_score2) {
	    $best_otpart2 = $otpart;
	    $best_otpart_score2 = ($oc_otpart_ic{$oc}{$otpart}{$otpart_ics{$otpart}[0]}+$oc_otpart_ic{$oc}{$otpart}{$otpart_ics{$otpart}[1]})/$oc_otpart_ic{$oc}{$otpart}{__ALL__};
	    $ic_for_best_otpart2 = $otpart_ics{$otpart}[0]."|".$otpart_ics{$otpart}[1];
	  }
	} else {
	  if ($oc_otpart_ic{$oc}{$otpart}{$otpart_ics{$otpart}[0]}/$oc_otpart_ic{$oc}{$otpart}{__ALL__} > $best_otpart_score2) {
	    $best_otpart2 = $otpart;
	    $best_otpart_score2 = $oc_otpart_ic{$oc}{$otpart}{$otpart_ics{$otpart}[0]}/$oc_otpart_ic{$oc}{$otpart}{__ALL__};
	    $ic_for_best_otpart2 = $otpart_ics{$otpart}[0];
	  }
	}
      }
      if ($best_otpart_score2-$best_otpart_score > 0.3) {
	$best_otpart = $best_otpart2;
	$best_otpart_score = $best_otpart_score2;
	$ic_for_best_otpart = $ic_for_best_otpart2;
      }
    }
    if ($best_otpart_score > 0.8) {
      $didsomething = 1;
      $chosen_otparts{$best_otpart} = 1;
      push @{$decision_tree_input{$oc}},$best_otpart;
      push @{$decision_tree_output{$oc}}, $ic_for_best_otpart;
      print STDERR "$oc\t$best_otpart\t$ic_for_best_otpart\t$best_otpart_score\n";
    }
  }

  # default: direct correspondence cat->cat (otpart = "")
  %oc_otpart_ic = ();
  for my $ot (keys %{$olex_ctfl{$oc}}) {
  floop:
    for my $f (keys %{$olex_ctfl{$oc}{$ot}}) {
      next unless defined $ilex_fcl{$f};
      for (keys %{$olex_cfttparts{$oc}{$f}{$ot}}) {
	next floop if defined $chosen_otparts{$_};
      }
      $denominator = (scalar keys %{$ilex_fcl{$f}});
      $denominator *= $denominator;
      for my $ic (keys %{$ilex_fcl{$f}}) {
	$oc_otpart_ic{$oc}{""}{$ic} += 1/$denominator;
	$oc_otpart_ic{$oc}{""}{__ALL__} += 1/$denominator;
      }
    }
  }
  
  if ($oc_otpart_ic{$oc}{""}{__ALL__} > 0) {
    for my $ic (sort {$oc_otpart_ic{$oc}{""}{$b}/$oc_otpart_ic{$oc}{""}{__ALL__} <=> $oc_otpart_ic{$oc}{""}{$a}/$oc_otpart_ic{$oc}{""}{__ALL__}} keys %{$oc_otpart_ic{$oc}{""}}) {
      next if $ic eq "__ALL__";
      push @{$otpart_ics{""}}, $ic;
    }
    $best_otpart_score = $oc_otpart_ic{$oc}{""}{$otpart_ics{""}[0]}/$oc_otpart_ic{$oc}{""}{__ALL__};
    $ic_for_best_otpart = $otpart_ics{""}[0];
    if ($best_otpart_score > 0 && $best_otpart_score <= 0.8 && defined($otpart_ics{""}[1]) && $oc_otpart_ic{$oc}{""}{$otpart_ics{""}[1]} > 0.3) { #retrying with 2 (ambiguous) input cats
      $best_otpart_score = ($oc_otpart_ic{$oc}{""}{$otpart_ics{""}[0]}+$oc_otpart_ic{$oc}{""}{$otpart_ics{""}[1]})/$oc_otpart_ic{$oc}{""}{__ALL__};
      $ic_for_best_otpart = $otpart_ics{""}[0]."|".$otpart_ics{""}[1];
    }
    $chosen_otparts{$best_otpart} = 1;
    push @{$decision_tree_input{$oc}},"";
    push @{$decision_tree_output{$oc}}, $ic_for_best_otpart;
    print STDERR "$oc\t\t$ic_for_best_otpart\t$best_otpart_score\n";
  }
}

my $c;
my %olex_cl_ic;
#trying lemma-level disamb of amb forms
for my $oc (sort keys %olex_cfttparts) {
  for my $f (sort keys %{$olex_cfttparts{$oc}}) {
    for my $ot (sort keys %{$olex_cfttparts{$oc}{$f}}) {
      $c = "";
      unless (defined($oc_ot2nonamb_ic{$oc}) && defined($oc_ot2nonamb_ic{$oc}{$ot})) {
	for (0..$#{$decision_tree_input{$oc}}) {
	  if ($decision_tree_input{$oc}[$_] eq "" || defined($olex_cfttparts{$oc}{$f}{$ot}{$decision_tree_input{$oc}[$_]})) {
	    $c = $decision_tree_output{$oc}[$_];
	    for my $l (keys %{$olex_ctfl{$oc}{$ot}{$f}}) {
	      for (split /\|/, $c) {
		$olex_cl_ic{$oc}{$l}{$_}+=1/(1+$c=~s/\|/\|/g);
	      }
	    }
	    last;
	  }
	}
      }
    }
  }
}

# finalising icat association with olex
my %olex_ic_ot_f_l;
my %oc_ot_f2guessedic;
for my $oc (sort keys %olex_cfttparts) {
  for my $f (sort keys %{$olex_cfttparts{$oc}}) {
    for my $ot (sort keys %{$olex_cfttparts{$oc}{$f}}) {
      if (defined($oc_ot2nonamb_ic{$oc}) && defined($oc_ot2nonamb_ic{$oc}{$ot})) {
	for my $l (keys %{$olex_ctfl{$oc}{$ot}{$f}}) {
	  $olex_ic_ot_f_l{$oc_ot2nonamb_ic{$oc}{$ot}}{$ot}{$f}{$l} = $olex_ctfl{$oc}{$ot}{$f}{$l};
	  $olex_ic_ot_f_l{$oc_ot2nonamb_ic{$oc}{$ot}}{$ot}{$f}{__ALL__} += $olex_ctfl{$oc}{$ot}{$f}{$l};
	}
      } else {
	$c = "-UNK-";
	for (0..$#{$decision_tree_input{$oc}}) {
	  if ($decision_tree_input{$oc}[$_] eq "" || defined($olex_cfttparts{$oc}{$f}{$ot}{$decision_tree_input{$oc}[$_]})) {
	    $c = $decision_tree_output{$oc}[$_];
	    last;
	  }
	}
	for my $l (keys %{$olex_ctfl{$oc}{$ot}{$f}}) {
	  if ($c =~ /\|/) {
	    my ($best_score, $best_cat);
	    for (split /\|/, $c) {
	      if ($olex_cl_ic{$oc}{$l}{$_} > $best_score) {
		$best_score = $olex_cl_ic{$oc}{$l}{$_};
		$best_cat = $_;
	      }
	    }
	    die "$oc\t$f\t$ot\t$c\t$l\n" if $best_score == 0;
	    $c = $best_cat;
	  }
	  die "$oc\t$f\t$ot\t$c\t$l\n" if $c eq "";
	  $olex_ic_ot_f_l{$c}{$ot}{$f}{$l} = $olex_ctfl{$oc}{$ot}{$f}{$l};
	  $olex_ic_ot_f_l{$c}{$ot}{$f}{__ALL__} += $olex_ctfl{$oc}{$ot}{$f}{$l};
	  $oc_ot_f2guessedic{$oc}{$ot}{$f} = $c;
	}
      }
    }
  }
}

# perparing association between old tags and input tags
my %ic_ot_it;
my (%ic_otpart_count, %ic_itpart_count, %ic_otpart_itpart_count);
for my $ic (keys %olex_ic_ot_f_l) {
  for my $ot (keys %{$olex_ic_ot_f_l{$ic}}) {
    for my $f (keys %{$olex_ic_ot_f_l{$ic}{$ot}}) {
      next unless defined $ilex_fctl{$f}{$ic};
      for my $it (keys %{$ilex_fctl{$f}{$ic}}) {
	$ic_ot_it{$ic}{$ot}{$it} += 1/(scalar keys %{$ilex_fctl{$f}{$ic}});
	for my $otpart (split /[#\.\t\|]/, $ot) {
	  $ic_otpart_count{$ic}{$otpart}++;
	  for my $itpart (split /[#\.\t\|]/, $it) {
	    $ic_otpart_itpart_count{$ic}{$otpart}{$itpart} += $olex_ic_ot_f_l{$ic}{$ot}{$f}{__ALL__};#++;
	  }
	}
	for my $itpart (split /[#\.\t\|]/, $it) {
	  $ic_itpart_count{$ic}{$itpart} += $olex_ic_ot_f_l{$ic}{$ot}{$f}{__ALL__};#++;
	}
      }
    }
  }
}
my ($best_score, $score, $best_it, %ic_otpart_itpart2score);
for my $ic (sort keys %ic_otpart_itpart_count) {
  for my $otpart (sort keys %{$ic_otpart_itpart_count{$ic}}) {
    $best_score = 0;
    for my $itpart (sort {$ic_otpart_itpart_count{$ic}{$otpart}{$b} <=> $ic_otpart_itpart_count{$ic}{$otpart}{$a}} keys %{$ic_otpart_itpart_count{$ic}{$otpart}}) {
      $score = 4/(3*$ic_otpart_count{$ic}{$otpart}/$ic_otpart_itpart_count{$ic}{$otpart}{$itpart}+$ic_itpart_count{$ic}{$itpart}/$ic_otpart_itpart_count{$ic}{$otpart}{$itpart});
      $score *= 2 if $itpart =~ /=$otpart$/i;
      $score *= 2 if $itpart =~ /^$otpart.*=Yes$/i;
      $ic_otpart_itpart2score{$ic}{$otpart}{$itpart} = $score;
    }
  }
}

# computing association between old tags and input tags
my (%ic_ot_it2score,%ic_ot_it2count);
for my $ic (keys %olex_ic_ot_f_l) {
  for my $ot (keys %{$olex_ic_ot_f_l{$ic}}) {
    for my $f (keys %{$olex_ic_ot_f_l{$ic}{$ot}}) {
      next unless defined $ilex_fctl{$f}{$ic};
      for my $it (keys %{$ilex_fctl{$f}{$ic}}) {
	$denominator = scalar keys %{$ilex_fctl{$f}{$ic}};
	$denominator *= $denominator;
	$ic_ot_it2count{$ic}{$ot}{$it} += 1/$denominator;
      }
    }
  }
}
for my $ic (keys %ic_ot_it2count) {
  for my $ot (keys %{$ic_ot_it2count{$ic}}) {
    for my $it (keys %{$ic_ot_it2count{$ic}{$ot}}) {
      for my $otpart (split /[#\.\t\|]/, $ot) {
	for my $itpart (split /[#\.\t\|]/, $it) {
	  $ic_ot_it2score{$ic}{$ot}{$it} += $ic_otpart_itpart2score{$ic}{$otpart}{$itpart};
	}
      }
      $ic_ot_it2score{$ic}{$ot}{$it} /= sqrt(1+$it=~s/\|/\|/g);
    }
  }
}
my %ic_ot2best_it;
for my $ic (keys %ic_ot_it2count) {
  for my $ot (keys %{$ic_ot_it2count{$ic}}) {
    next unless scalar keys %{$ic_ot_it2count{$ic}{$ot}} > 1;
    for my $it (sort {$ic_ot_it2score{$ic}{$ot}{$b} <=> $ic_ot_it2score{$ic}{$ot}{$a}} keys %{$ic_ot_it2count{$ic}{$ot}}) {
      $ic_ot2best_it{$ic}{$ot} = $it;
      last;
    }
  }
}
my %lex;
for my $ic (keys %ilex_ctfl) {
  for my $it (keys %{$ilex_ctfl{$ic}}) {
    for my $f (keys %{$ilex_ctfl{$ic}{$it}}) {
      for my $l (keys %{$ilex_ctfl{$ic}{$it}{$f}}) {
	$lex{$ic}{$f}{$it}{$l} = 2;
      }
    }
  }
}
for my $oc (keys %olex_ctfl) {
  for my $ot (keys %{$olex_ctfl{$oc}}) {
    for my $f (keys %{$olex_ctfl{$oc}{$ot}}) {
      for my $l (keys %{$olex_ctfl{$oc}{$ot}{$f}}) {
	if (defined($oc_ot2nonamb_ic{$oc}) && defined($oc_ot2nonamb_ic{$oc}{$ot})) {
	  $lex{$oc_ot2nonamb_ic{$oc}{$ot}}{$f}{$oc_ot2nonamb_it{$oc}{$ot}}{$l} = 1;
	} elsif (defined($oc_ot_f2guessedic{$oc}{$ot}{$f})) {
	  next if $oc_ot_f2guessedic{$oc}{$ot}{$f} eq "-UNK-";
	  $lex{$oc_ot_f2guessedic{$oc}{$ot}{$f}}{$f}{$ic_ot2best_it{$oc_ot_f2guessedic{$oc}{$ot}{$f}}{$ot}}{$l} = 0;
	}
      }
    }
  }
}
for my $ic (sort keys %lex) {
  for my $f (sort keys %{$lex{$ic}}) {
    for my $it (sort keys %{$lex{$ic}{$f}}) {
      for my $l (sort keys %{$lex{$ic}{$f}{$it}}) {
	print "$lex{$ic}{$f}{$it}{$l}\t$f\t$l\t$ic\t$it\n";
      }
    }
  }
}
