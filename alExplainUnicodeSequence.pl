#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

use alUnicode;

my $lang;
my $reverse; # default = 0, i.e. in direction from other script to latin script. Use option '-r' to get the other direction

while (1) {
  $_ = shift;
  if (/^$/) {last}
  else {die "Unknown option '$_'"}
}

while (<>) {
  chomp;
  print al_explainUnicodeSequence($_)."\n\n";
}
