#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use alRawTextCleaning;
use alURL;
use alEmail;
use alTokenisation;
use strict;

my $keepall = shift || 0;

my (%lex, %f2count, %l2count);
my ($n,$k);
my ($f,$l,$t);
while (<>) {
  chomp;
  print STDERR "\r".(++$n);
  /^[^\t]*\t([^\t]*)\t([^\t]*)\t([^\t]*)\t[^\t]*\t([^\t]*)/ || next;
  ($f, $l, $t) = ($1, $2, $3."#".$4);
  unless ($keepall) {
    next if length($l) > 16;
  }
  $f =~ s/ / /g;
  $l =~ s/ / /g;
  unless ($keepall) {
    unless ($l eq "_") {
      next if $l =~ /^["\!\$\%\&\(\)\*\+,\.;:\/\|]-?\P{Punct}/;
      next if $l =~ /^\p{Punct}+$/ && $f !~ /^\p{Punct}+$/;
    }
    next if $f =~ /^(?:\p{Punct}+\d+|\d+\p{Punct}+)[\p{Punct}\d]*$/;
  }
  $lex{$l}{$f}{$t}++;
  $f2count{$f}++;
  $l2count{$l}++;
  $k++;
  if ($k % 10000000 == 0) {
    my $c;
    for (keys %f2count) {
      $c++ if $f2count{$_} >= 3;
    }
    print STDERR "\n\t$c\n";
    last if $c >= 1000000;
  }
}
print STDERR "\n";

for my $l (sort {$a cmp $b} keys %lex) {
  for my $f (sort {$a cmp $b} keys %{$lex{$l}}) {
    unless ($keepall) {
      next if $f2count{$f} <= 5 && $l2count{$l} == $f2count{$f};
      next if $f2count{$f} <= 2 && $l2count{$l} < 2*$f2count{$f};
      next if $l2count{$l} <= 2;
      next if $f2count{$f} <= $n/40000 && $l2count{$l} == $f2count{$f};
      next if $f2count{$f} <= $n/100000 && $l2count{$l} < 2*$f2count{$f};
      next if $l2count{$l} <= $n/100000;
    }
    for (sort {$a cmp $b} keys %{$lex{$l}{$f}}) {
      print "$f\t$_\t$l\n";
    }
  }
}
