#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use alRawTextCleaning;
use alURL;
use alEmail;
use alTokenisation;
use Encode;
use strict;

my %tokens;
my $lang = shift || die;

my $l;
while (<>) {
  print STDERR "\rReading raw text: ".(++$l);
  chomp;
  next if /^</;
  next if /<\?xml/;
  next if /\/>/;
  next if /<\//;
  next unless is_clean_utf8($_);
  $_ = altok_interpret_entities($_);
  $_ = altok_escape_metacharacters($_);
  $_ = altok_escape_xml($_);
  $_ = altok_tokenise($_, $lang);
  s/  +/ /g;
  s/^ //;
  s/ $//;
  for (split / /, $_) {
    $tokens{$_}++;
  }
}

for (sort {$a cmp $b} keys %tokens) {
  print $_."\t".$tokens{$_}."\n";
}

sub is_clean_utf8 {
  my $s = shift;
  # takes any UTF-8 encoded string as an input (including strings with malformed UTF-8) and returns 1 if it is valid
  eval {
    $s = Encode::encode('UTF-8', $s, Encode::FB_CROAK);
  };
  if ($@) {
    return 0;
  }
  return 1;
}
