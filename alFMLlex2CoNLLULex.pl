#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

# Usage: cat mylex.xxlex | perl al CoNLLUlex2CoNLLUL.pl > mylex.conll-ul

# zaituztete	VERB#Mood=Ind|Tense=Pres	ukan
# >
# 0	1	encodent	encoder	VERB	_	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	_

while (<>) {
  chomp;
  if (/^([^\t]+)\t([^\t]+)#([^\t#]*)\t([^\t]+)$/) {
    print "0\t1\t$1\t".($4 eq "*" && $1 ne "*" ? "_" : $4)."\t$2\t_\t".($3 eq "" ? "_" : $3)."\t_\n";
  } else {
    print STDERR "### ERROR: Incorrect line, ignored: '$_'\n";
  }
}
