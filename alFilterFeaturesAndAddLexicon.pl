#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

my $other_lexicon_to_be_featurefiltered_and_merged = shift || die;

my ($f, $l, $t, $c, $a, $newt);
my (%fctl, %ca);
while (<>) {
  chomp;
  /^([^\t]+)\t([^\t#]+)(?:#([^\t]*))?\t([^\t]+)$/ || next;
  ($f, $c, $t, $l) = ($1, $2, $3, $4);
  for $a (split /\|/, $t) {
    $ca{$c}{$a} = 1;
  }
  $t = join "|", sort {$a cmp $b} split /\|/, $t;
  $fctl{$f}{$c}{$t}{$l} = 1;
}

open ELEX, "<$other_lexicon_to_be_featurefiltered_and_merged" || die $!;
binmode ELEX, ":utf8";
while (<ELEX>) {
  chomp;
  /^([^\t]+)\t([^\t#]+)(?:#([^\t]*))?\t([^\t]+)$/ || next;
  ($f, $c, $t, $l) = ($1, $2, $3, $4);
  $newt = "";
  for $a (split /\|/, $t) {
    if (defined($ca{$c}) && defined($ca{$c}{$a})) {
      $newt .= "|" unless $newt eq "";
      $newt .= $a;
    }
  }
  $newt = join "|", sort {$a cmp $b} split /\|/, $newt;
  $fctl{$f}{$c}{$newt}{$l} = 1;
}
close ELEX;

for $f (sort keys %fctl) {
  for $c (sort keys %{$fctl{$f}}) {
    for $t (sort keys %{$fctl{$f}{$c}}) {
      for $l (sort keys %{$fctl{$f}{$c}{$t}}) {
	print "$f\t$c#$t\t$l\n";
      }
    }
  }
}
