#!/usr/bin/perl

binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use alUTF8;
use strict;

# Converts a text whose encoding is unknown, but known to be either <8-bit-encoding> (default: L1), UTF-8 or UTF-8 reencoded in UTF-8 as if it were in <8-bit-encoding>, into UTF-8 text
# Examples with L1:
#$ echo "ä â à á é è ê ë î ï í ì ô ö ò ó ú ù û ü ÿ ç Ä Â À Á É È Ê Ë Î Ï Í Ì Ô Ö Ò Ó Ú Ù Û Ü Ç" | perl /Users/sagot/Documents/alAnalyser/alForceUTF8.pl 
#ä â à á é è ê ë î ï í ì ô ö ò ó ú ù û ü ÿ ç Ä Â À Á É È Ê Ë Î Ï Í Ì Ô Ö Ò Ó Ú Ù Û Ü Ç
#$ echo "ä â à á é è ê ë î ï í ì ô ö ò ó ú ù û ü ÿ ç Ä Â À Á É È Ê Ë Î Ï Í Ì Ô Ö Ò Ó Ú Ù Û Ü Ç" | recode u8..l1 | perl /Users/sagot/Documents/alAnalyser/alForceUTF8.pl 
#ä â à á é è ê ë î ï í ì ô ö ò ó ú ù û ü ÿ ç Ä Â À Á É È Ê Ë Î Ï Í Ì Ô Ö Ò Ó Ú Ù Û Ü Ç
#$ echo "ä â à á é è ê ë î ï í ì ô ö ò ó ú ù û ü ÿ ç Ä Â À Á É È Ê Ë Î Ï Í Ì Ô Ö Ò Ó Ú Ù Û Ü Ç" | recode l1..u8 | perl /Users/sagot/Documents/alAnalyser/alForceUTF8.pl 
#ä â à á é è ê ë î ï í ì ô ö ò ó ú ù û ü ÿ ç Ä Â À Á É È Ê Ë Î Ï Í Ì Ô Ö Ò Ó Ú Ù Û Ü Ç
# works (for now) on latin-[12]-compatible characters

my $enc8bits = "l1";

while (1) {
  $_ = shift;
  if (/^$/) {last}
  elsif (/^-e$/) {$enc8bits = shift || die "Option '$_' must be followed by an encoding name (l1)"}
  else {die "Unknown option '$_'"}
}
die "Unknown encoding '$enc8bits'" unless $enc8bits =~ s/^l(?:atin)?-?([12])$/iso-8859-$1/i;
binmode STDIN, $enc8bits;

#my ($recoded, $encoding);

while (<>) {
  chomp;
  print altok_force_utf8($_,$enc8bits)."\n";
}

# while (<>) {
#   $encoding = $enc8bits;
#   $recoded = $_;
#   $recoded =~ s/(\P{ASCII})/"&#".ord($1).";"/ge;
# #  print STDERR "$recoded\n";
#   if ($recoded =~ /&#195;&#13[1-5];&#194;&#1(?:2[89]|[3-9][0-9]);|&#195;&#162;&#194;&#128;&#194;&#15[2-9]|&#195;&#130;&#194;&#1[78][0-9];/) {
#     $_ = decode("UTF-8", $_, Encode::FB_CROAK);
#     $_ = encode($enc8bits, $_, Encode::FB_CROAK);
#   }
#   if ($recoded =~ /&#19[5-7];&#1(?:2[89]|[3-9][0-9]);|&#226;&#128;&#15[2-9];/) {
#     $encoding = "UTF-8";
#   }  
#   print decode($encoding, $_, Encode::FB_CROAK);
# }
