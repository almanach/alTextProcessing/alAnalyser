#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

my (%flt, %labels);
my $l;
while (<>) {
  print STDERR "\r".(++$l);
  chomp;
  /^([^\t]+)\t([^\t]+)\t([^\t]+)$/ || next;
  $flt{$1}{$3}{$2} = 1;
  $labels{$2} = 1;
}
print STDERR "\n";

my ($origt, $t, $cat, %tags, $udcat, %udtags, %t2udt);
for $origt (keys %labels) {
  $origt =~ /^([^#]+)(?:#(.*))?$/ || die $t;
  $cat = $1;
  $t = $2;
  %tags = ();
  %udtags = ();
  $udcat = "";
  for (split /\+/, $t) {
    $tags{$_} = 1;
  }
  if ($cat =~ /^(\+?PUNCT|Punct)$/) {
    $udcat = "PUNCT";
  } elsif ($cat =~ /^(Ne?t?i?a?)$/) {
    $udcat = "NOUN";
  } elsif ($cat =~ /^(Num)$/) {
    $udcat = "NUM";
  } elsif ($cat =~ /^(Ae?)$/) {
    $udcat = "ADJ";
  } elsif ($cat =~ /^(CC)$/) {
    $udcat = "CCONJ";
  } elsif ($cat =~ /^(CS)$/) {
    $udcat = "SCONJ";
  } elsif ($cat =~ /^(Interj)$/) {
    $udcat = "INTJ";
  } elsif ($cat =~ /^(Adp|Po|Pr)$/) {
    $udcat = "ADP";
  } elsif ($cat =~ /^(Pron)$/) {
    $udcat = "PRON";
  } elsif ($cat =~ /^(Adv)$/) {
    $udcat = "ADV";
  } elsif ($cat =~ /^(V)$/) {
    $udcat = "VERB";
  } elsif ($cat =~ /^(Pcle)$/) {
    $udcat = "PART";
  } elsif ($cat =~ /^(CLB)$/) {
    $udcat = "X";
  } else {
    print STDERR "New input cat '$cat'\n";
    next;
  }
  $udtags{"Number=Plur"} = 1 if defined $tags{"Pl"};
  $udtags{"Number=Sing"} = 1 if defined $tags{"Sg"};
  $udtags{"Case=Abe"} = 1 if defined $tags{"Abe"};
  $udtags{"Case=Abl"} = 1 if defined $tags{"Abl"};
  $udtags{"Case=Acc"} = 1 if defined $tags{"Acc"};
  $udtags{"Case=Acc"} = 1 if defined $tags{"Obl"};
  $udtags{"Case=Ade"} = 1 if defined $tags{"Ade"};
  $udtags{"Case=All"} = 1 if defined $tags{"All"};
  $udtags{"Case=Com"} = 1 if defined $tags{"Com"};
  $udtags{"Case=Dat"} = 1 if defined $tags{"Dat"};
  $udtags{"Case=Dis"} = 1 if defined $tags{"Dis"};
  $udtags{"Case=Ela"} = 1 if defined $tags{"Ela"};
  $udtags{"Case=Ess"} = 1 if defined $tags{"Ess"};
  $udtags{"Case=Ess"} = 1 if defined $tags{"Prl"};
  $udtags{"Case=Gen"} = 1 if defined $tags{"Gen"};
  $udtags{"Case=Ill"} = 1 if defined $tags{"Ill"};
  $udtags{"Case=Ine"} = 1 if defined $tags{"Ine"};
  $udtags{"Case=Ins"} = 1 if defined $tags{"Ins"};
  $udtags{"Case=Loc"} = 1 if defined $tags{"Loc"};
  $udtags{"Case=Nom"} = 1 if defined $tags{"Nom"};
  $udtags{"Case=Par"} = 1 if defined $tags{"Par"};
  $udtags{"Case=Tra"} = 1 if defined $tags{"Tra"};
  $udtags{"Case=Voc"} = 1 if defined $tags{"Voc"};
  $udtags{"PronType=Prs"} = 1 if defined $tags{"Prs"};
  $udtags{"VerbForm=Part"} = 1 if defined $tags{"Prc"};
  $udtags{"Mood=Qot"} = 1 if defined $tags{"Quot"};
  $udtags{"Mood=Ind"} = 1 if defined $tags{"Ind"};
  $udtags{"Mood=Cnd"} = 1 if defined $tags{"Cond"};
  $udtags{"Mood=Imp"} = 1 if defined $tags{"Quot"};
  $udtags{"Tense=Past"} = 1 if defined $tags{"Prt"};
  $udtags{"Tense=Pres"} = 1 if defined $tags{"Prs"};
  $udtags{"VerbForm=Part"} = 1 if (defined $tags{"A"} || defined $tags{"N"}) && $udcat eq "VERB";
  for (1,2,3) {
    if (defined $tags{"Sg".$_}) {
      $udtags{"Number=Sing"} = 1;
      $udtags{"Person=$_"} = 1;
    }
    if (defined $tags{"Pl".$_}) {
      $udtags{"Number=Plur"} = 1;
      $udtags{"Person=$_"} = 1;
    }
  }
  
  $t2udt{$origt} = $udcat."#".(join "|", sort {$a cmp $b} keys %udtags);
}

for my $f (keys %flt) {
  for my $l (keys %{$flt{$f}}) {
    for my $t (keys %{$flt{$f}{$l}}) {
      print "$f\t$t2udt{$t}\t$l\n";
    }
  }
}
