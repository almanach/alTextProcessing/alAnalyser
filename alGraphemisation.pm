package alGraphemisation;
use utf8;
use strict;
use alVariables;
use Exporter;
use Unicode::Normalize;
our @ISA = 'Exporter';
our @EXPORT = qw(&graphemise);

sub graphemise {
  my $s = shift;
  my $lang = shift;
  $s = Unicode::Normalize::NFC($s);

  if ($lang eq "xlc") { # Lycian
    $s =~ s/'//g;
    
    $s =~ s/dð/tθ/g;
    $s =~ s/ð/θ/g;
    $s =~ s/β/φ/g;
    $s =~ s/ʁ/χ/g;
    $s =~ s/d/t/g;
    $s =~ s/g/k/g;
    $s =~ s/dᶻ/tˢ/g;
    $s =~ s/z/s/g;
    $s =~ s/ʝ/ç/g;
    $s =~ s/b/p/g;
    $s =~ s/d/t/g;
    $s =~ s/ɟ/c/g;

    $s =~ s/æ̃ⁿ?/ẽ/g;    
    $s =~ s/ɑ̃ⁿ?/ã/g;

    $s =~ s/æ/e/g;
    $s =~ s/ʊ/u/g;
    $s =~ s/ɪ/i/g;
    $s =~ s/ɑ/a/g;
    
    $s =~ s/(?<=[aiueoẽãnmrl])n(?![aiueoẽãnmrl])/ñ/g;
    $s =~ s/ᵊn/ñ/g;
    
    $s =~ s/(?<=[aiueoẽãnmrl])m(?![aiueoẽãnmrl])/m̃/g;
    $s =~ s/ᵊm/m̃/g;


    $s =~ s/χ/g/g;
    $s =~ s/k/χ/g;
    $s =~ s/#d/ñt/g;
    $s =~ s/kʷ/q/g;
    $s =~ s/c/k/g;
    $s =~ s/ç/K/g;
    $s =~ s/tʷ/τ/g;
    $s =~ s/H/◇/g;
    $s =~ s/tˢ/z/g;
    $s =~ s/φ/b/g;
    $s =~ s/θ/d/g;
    $s =~ s/th/ϑ/g;
    $s =~ s/tθ/dd/g;
    $s =~ s/y/j/g;

    $s =~ s/([^aiueoẽãnñmrlbᵊ])ᵊ?([^aiueoẽãᵊrlmnñ\-])/$1$2$2/g;

    $s =~ s/ᵊ//g;

  } elsif ($lang =~ /^grc/) {
    $s =~ s/[sz]d/ζ/g;
    $s =~ s/kʰ?s/ξ/g;
    $s =~ s/ps/ψ/g;
    $s =~ s/tʰ/θ/g;
    $s =~ s/pʰ/φ/g;
    $s =~ s/kʰ/χ/g;
    $s =~ tr/abgdeēiklmnoprstuō/αβγδεηικλμνοπρστυω/;

    $s =~ s/ν([γκχξ])/$1$1/g;

    $s =~ s/(^| )([αεηιουω])/$1$2̓/;
    $s =~ s/(^| )h([αεηιουω])/$1$2̔/;

    $s = Unicode::Normalize::NFC($s);
  }
  
  return $s;
}
1;

