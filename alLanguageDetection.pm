package alLanguageDetection;
use utf8;
use strict;
use locale;
use Lingua::ZH::HanConvert;
use Lingua::Identify qw(:language_identification);
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(&altok_detect_language);


my %re2lang = (
	       qr/ß/ => "de",
	       qr/[řů]/ => "cs",
	       qr/[ņāēīģķļņ]/ => "lv",
	       qr/[įųė]/ => "lt",
	       qr/\p{Hebrew}/ => "he",
	       qr/\p{Hangul}/ => "ko",
	       qr/\p{Gujarati}/ => "gu",
	       qr/\p{Armenian}/ => "hy",
	       qr/\p{Georgian}/ => "ka",
	       qr/\p{Thai}/ => "th",
	       qr/\p{Tamil}/ => "ta",
	       qr/\p{Greek}\p{Greek}/ => "el",
	       qr/[\p{InHiragana}\p{InKatakana}]/ => "ja",
	       qr/[ƠỜỚỞỠỢƯỪỨỬỮỰẶẴẲẮẰỀỂỄẾỆơờớởỡợưừứửữựặẵẳắằềểễếệ]/ => "vi",
		);

my %two_first_words2lang = (
			    "Stärker in" => "de",
			    "условия труда" => "ru",
			    "Creo que" => "es",
			    "We gaan" => "nl",
			    "We need" => "en",
			    "There is" => "en",
			    "I believe" => "en",
			    "I have" => "en",
			    "At the" => "en",
			    "Améliorer la" => "fr",
			    "UNA MAGGIORE" => "it",
			    "I find" => "en",
			    "Acho que" => "pt",
			    "The new" => "en",
			    "Me siento" => "es",
			    "Il faut" => "fr",
			    "I work" => "en",
			    "Improve the" => "en",
			    "Je suis" => "fr",
			    "La politique" => "fr",
			    "Ensure that" => "en",
			    "We should" => "en",
			    "Better communication" => "en",
			    "The way" => "en",
			    "CREO QUE" => "es",
			    "Il est" => "fr",
			    "Хотелось бы" => "ru",
			    "In the" => "en",
			    "i would" => "en",
			    "Would like" => "en",
			    "i think" => "en",
			    "Lack of" => "en",
			    "Need to" => "en",
			    "I think" => "en",
			    "There should" => "en",
			    "To be" => "en",
			    "Plus de" => "fr",
			    "I feel" => "en",
			    "Too many" => "en",
			    "Acredito que" => "pt",
			    "i feel" => "en",
			    "Je pense" => "fr",
			    "We have" => "en",
			    "We are" => "en",
			    "It is" => "en",
			    "The company" => "en",
			    "There are" => "en",
			    "It would" => "en",
			    "I do" => "en",
			    "As a" => "en",
			    "Es wäre" => "de",
			    "A more" => "en",
			    "I FEEL" => "en",
			    "I enjoy" => "en",
			    "Listen to" => "en",
			    "Ritengo che" => "it",
			    "I beleive" => "en",
			    "The management" => "en",
			    "Le management" => "fr",
			    "Uważam, że" => "pl",
			    "AXA is" => "en",
			    "Get rid" => "en",
			    "More training" => "en",
			    "Повышение заработной" => "ru",
			    "Підвищення заробітної" => "uk",
			    "Ich bin" => "de",
			    "A better" => "en",
			    "Avon is" => "en",
			    "I dont" => "en",
			    "Pay and" => "en",
			    "Me gustaría" => "es",
			    "Il faudrait" => "fr",
			    "Une meilleure" => "fr",
			    "The proposed" => "en",
			    "Il serait" => "fr",
			    "maggiore formazione" => "it",
			    "A empresa" => "pt",
			    "Я считаю," => "ru",
			    "Ich würde" => "de",
			    "Due to" => "en",
			    "More opportunities" => "en",
			    "En mi" => "es",
			    "Estoy muy" => "es",
			    "Je trouve" => "fr",
			    "Na minha" => "pt",
			    "Change the" => "en",
			    "I love" => "en",
			    "I understand" => "en",
			    "Credo che" => "it",
			    "Not enough" => "en",
			    "The current" => "en",
			    "There needs" => "en",
			    "This is" => "en",
			    "To have" => "en",
			    "With the" => "en",
			    "Una maggiore" => "it",
			    "System building" => "en",
			    "Long term" => "en",
			    "Minder complexiteit" => "nl",
			    "Nog meer" => "nl",
			    "Effectievere" => "nl",
			    "Minder processen" => "nl",
			    "Minder projecten" => "nl",
			   );
my %one_word2lang = (
		     "communicatie" => "nl",
		     "transparantie" => "nl",
		     "complexiteit" => "nl",
		     "aannemen" => "nl",
		     "betere" => "nl",
		     "vanwege" => "nl",
		     "fördern" => "de",
		     "Anerkennung" => "de",
		     );
my %two_words2lang = (
		      "en cuenta" => "es",
		     );

my %first_word2lang = (
		       "график" => "ru",
		       "ГРАФИК" => "ru",
		       "Extend" => "en",
		       "Great" => "en",
		       "It's" => "en",
		       "Bessere" => "de",
		       "Das" => "de",
		       "Die" => "de",
		       "Eine" => "de",
		       "Ich" => "de",
		       "Mehr" => "de",
		       "Wenn" => "de",
		       "mehr" => "de",
		       "Allow" => "en",
		       "Although" => "en",
		       "An" => "en",
		       "Being" => "en",
		       "Ensure" => "en",
		       "First" => "en",
		       "Get" => "en",
		       "Give" => "en",
		       "Greater" => "en",
		       "Have" => "en",
		       "Having" => "en",
		       "If" => "en",
		       "Improve" => "en",
		       "Increase" => "en",
		       "Lack" => "en",
		       "Need" => "en",
		       "New" => "en",
		       "Our" => "en",
		       "People" => "en",
		       "Poor" => "en",
		       "Please" => "en",
		       "Provide" => "en",
		       "Reduce" => "en",
		       "Some" => "en",
		       "THE" => "en",
		       "There" => "en",
		       "Very" => "en",
		       "When" => "en",
		       "Whilst" => "en",
		       "With" => "en",
		       "Would" => "en",
		       "You" => "en",
		       "improve" => "en",
		       "keep" => "en",
		       "more" => "en",
		       "the" => "en",
		       "there" => "en",
		       "CREO" => "es",
		       "Creo" => "es",
		       "El" => "es",
		       "Estoy" => "es",
		       "Mayor" => "es",
		       "Mejorar" => "es",
		       "Améliorer" => "fr",
		       "Avoir" => "fr",
		       "Dans" => "fr",
		       "Donner" => "fr",
		       "JE" => "fr",
		       "Nous" => "fr",
		       "Pour" => "fr",
		       "Une" => "fr",
		       "je" => "fr",
		       "les" => "fr",
		       "AVERE" => "it",
		       "Avere" => "it",
		       "MAGGIOR" => "it",
		       "MAGGIORE" => "it",
		       "Maggiore" => "it",
		       "maggior" => "it",
		       "maggiore" => "it",
		       "Meer" => "nl",
		       "Brak" => "pl",
		       "Wynagrodzenie" => "pl",
		       "Zbyt" => "pl",
		       "wynagrodzenie" => "pl",
		       "Acho" => "pt",
		       "Acredito" => "pt",
		       "Gostaria" => "pt",
		       "Mudaria" => "pt",
		       "Хотелось" => "ru",
		       "Повышение" => "ru",
		       "Çalışanların" => "tr",
		       "Ücret" => "tr",
		       "Підвищення" => "uk",
		       "Actief" => "nl",
		       "Befähigung" => "de",
		       "Eigenentwicklung" => "de",
		       "Expertenkarrieren" => "de",
		       "Funktionalität" => "de",
		       "Förderung" => "de",
		       "Grössere" => "de",
		       "Höhere" => "de",
		       "Höherer" => "de",
		       "Höheres" => "de",
		       "Mitarbeiter" => "de",
		       "Motivationsförderung" => "de",
		       "Qualität" => "de",
		       "Qualitätsgerechtes" => "de",
		       "Qualitätsverbesserung" => "de",
		       "Stabilität" => "de",
		       "Strukturierte" => "de",
		       "Teamfähigkeit" => "de",
		       "Weiterbildung" => "de",
		       "Übertarifliche" => "de",
		       "Дисциплину" => "ru",
		      );

my %single_word2lang = (
		       "HUMAN" => "en",
		       "имидж" => "ru",
		       );

sub altok_detect_language {
  my $s = shift;
  my $lang = shift;
  return $lang unless $lang eq "";
  $s =~ s/^\s+//;
  $s =~ s/\s+$//;
  for my $re (keys %re2lang) {
    return $re2lang{$re} if $s =~ /($re)/;
  }
  if ($s =~ /^([^ ]+ [^ ]+)( |$)/) {
    return $two_first_words2lang{$1} if defined($two_first_words2lang{$1});
  }
  if ($s =~ /^([^ ]+)( |$)/) {
    return $first_word2lang{$1} if defined($first_word2lang{$1});
  }
  if ($s =~ /^([^ ]+)$/) {
    return $single_word2lang{$1} if defined($single_word2lang{$1});
  }
  my @tokens = split / /, $s;
  for (0..$#tokens) {
    return $one_word2lang{lc($tokens[$_])} if defined($one_word2lang{lc($tokens[$_])});
    return $two_words2lang{$tokens[$_]." ".$tokens[$_+1]} if defined($two_words2lang{$tokens[$_]." ".$tokens[$_+1]});
  }
  return "zh_TW" if ($s ne Lingua::ZH::HanConvert::simple($s));
  return "zh_CN" if ($s ne Lingua::ZH::HanConvert::trad($s));
  $lang = lc(Lingua::Identify::langof($s));
  return $lang unless $lang eq "";
  $lang = lc(Lingua::Identify::langof(lc($s)));
  return $lang unless $lang eq "";
  print STDERR "WARNING [altokLanguageDetection]: could not guess language of input '$s' (lowercase: '".(lc($s))."'), returning 'en'\n";
  return "en";
}
