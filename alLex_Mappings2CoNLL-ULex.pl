#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

my $mappings_file = shift || die;
my $amlgm_file = shift || "";

my (%mapping_LEXcat_LEXms_UDcat_UDms, %mapping_LEXform_LEXlemma_LEXcat_LEXms_UDlemma, %LEXform_lemma_cat_ms); 

open MAPPINGS, "<$mappings_file" || die $!;
binmode MAPPINGS, ":utf8";
while (<MAPPINGS>) {
  chomp;
  if (/^([^\t]+)\t([^\t]*)\t([^\t]+)\t([^\t]*)/) {
    $mapping_LEXcat_LEXms_UDcat_UDms{$1}{$2}{$3}{$4} = 1;
  } elsif (/^\t\t\t([^\t]+)\t([^\t]+)\t([^\t]+)\t([^\t]*)\t([^\t]+)/) {
    $mapping_LEXform_LEXlemma_LEXcat_LEXms_UDlemma{$1}{$2}{$3}{$4}{$5} = 1;
  }
}

while (<>) {
  chomp;
  if (/^$/ || /^#/ || /^_/) {
  } elsif (/^([^\t]+)\t([^\t]+)\t([^\t]+)\t([^\t]*)$/) {
    my ($form, $lemma, $cat, $all_ms) = ($1, $3, $2, $4);
    next if $lemma =~ /^_/ || $lemma =~ /_$/;
    next if $all_ms eq "_HIDDEN_LEMMA_";
    $all_ms =~ s/^$cat://;
    $all_ms = "_" if $all_ms eq "";
    my @ms;
    if ($all_ms =~ /\|/) {
      @ms = split /\|/, $all_ms;
    } elsif ($mappings_file =~ /lefff/) {
      if ($all_ms eq "PFIJTSC") {
	@ms = ("");
      } elsif ($all_ms =~ /^([A-Z])([0-9])([0-9])([sp])$/) {
	@ms = ("$1$2$4", "$1$3$4");
      } elsif ($all_ms =~ /^([A-Z])([A-Z])([0-9][sp])$/) {
	@ms = ("$1$3", "$2$3");
      } elsif ($all_ms =~ /^([A-Z])([A-Z])([0-9])([0-9])([sp])$/) {
	@ms = ("$1$3$5", "$2$3$5", "$1$4$5", "$2$4$5");
      } elsif ($all_ms eq "K") {
	@ms = ("Kms", "Kmp", "Kfs", "Kfp");
      } elsif ($all_ms =~ /^K([fm])$/) {
	@ms = ("K$1s", "K$1p");
      } elsif ($all_ms =~ /^([fm])$/) {
	@ms = ("$1s", "$1p");
      } elsif ($all_ms =~ /^([sp])$/) {
	@ms = ("m$1", "f$1");
      } elsif ($cat =~ /^(?:adj|nc|np)$/ && $all_ms eq "") {
	@ms = ("ms", "fs", "mp", "fp");
      } else {
	@ms = ($all_ms);
      }
    } else {
      @ms = ($all_ms);
    }
    for (@ms) {
      $LEXform_lemma_cat_ms{$form}{$lemma}{$cat}{$_} = 1;
    }
  }
}

my %dirty_hash_for_amlgms;
for my $form (sort keys %LEXform_lemma_cat_ms) {
  for my $lemma (sort keys %{$LEXform_lemma_cat_ms{$form}}) {
    for my $cat (sort keys %{$LEXform_lemma_cat_ms{$form}{$lemma}}) {
      next unless defined $mapping_LEXcat_LEXms_UDcat_UDms{$cat};
      for my $ms (sort keys %{$LEXform_lemma_cat_ms{$form}{$lemma}{$cat}}) {
	next unless defined $mapping_LEXcat_LEXms_UDcat_UDms{$cat}{$ms};
	for my $UDcat (sort keys %{$mapping_LEXcat_LEXms_UDcat_UDms{$cat}{$ms}}) {
	  for my $UDms (sort keys %{$mapping_LEXcat_LEXms_UDcat_UDms{$cat}{$ms}{$UDcat}}) {
	    if (defined($mapping_LEXform_LEXlemma_LEXcat_LEXms_UDlemma{$form}{$lemma}{$cat}{$ms})) {
	      for my $UDlemma (sort keys %{$mapping_LEXform_LEXlemma_LEXcat_LEXms_UDlemma{$form}{$lemma}{$cat}{$ms}}) {
		print "0\t1\t$form\t$UDlemma\t$UDcat\t$cat#$ms\t$UDms\t_\n";
		$dirty_hash_for_amlgms{$form}{$cat}{"$form\t$UDlemma\t$UDcat\t$cat#$ms\t$UDms\t_"} = 1;
	      }
	    } else {
	      # 0	1	encodent	encoder	VERB	_	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	_
	      print "0\t1\t$form\t$lemma\t$UDcat\t$cat#$ms\t$UDms\t_\n";
	      $dirty_hash_for_amlgms{$form}{$cat}{"$form\t$lemma\t$UDcat\t$cat#$ms\t$UDms\t_"} = 1;
	    }
	  }
	}
      }
    }
  }	
}

if ($amlgm_file) {
  open AMLGM, "<$amlgm_file" || die $!;
  binmode AMLGM, ":utf8";
  while (<AMLGM>) {
    chomp;
    next if /^#/;
    next if /^\s*$/;
    /^([^ ]+)\t(.+ .+)$/ || print STDERR "# WARNING: ignored amlgm definition '$_'\n";
    my $amlgm = $1;
    my $expansion = $2;
    my $n = ($expansion =~ s/ / /g)+1;
    # 18-19   des     _       _       _       _       _       _       _       _
    # 18      de      de      ADP     _       _       20      case    _       _
    # 19      les     le      DET     _       Definite=Def|Gender=Masc|Number=Plur|PronType=Art       20      det     _       _
    my $i;
    my @paths;
    for my $token (split / /, $expansion) {
      die if $token eq "";
      if ($token =~ s/__(.+)$//) {
	my $cat = $1;
	$token =~ s/_/ /g;
	die "Unknown token '$token' with cat '$cat'" unless defined $dirty_hash_for_amlgms{$token} && $dirty_hash_for_amlgms{$token}{$cat};
	for my $line (keys %{$dirty_hash_for_amlgms{$token}{$cat}}) {
	  $line = ($i || "0")."\t".($i+1)."\t".$line."\n";
	  if ($i == 0) {
	    $paths[$i]{$line} = 1;
	  } else {
	    for (keys %{$paths[$i-1]}) {
	      $paths[$i]{$_.$line} = 1;
	    }
	  }
	}
      } else {
	$token =~ s/_/ /g;
	die "Unknown token '$token'" unless defined $dirty_hash_for_amlgms{$token};
	for my $cat (keys %{$dirty_hash_for_amlgms{$token}}) {
	  for my $line (keys %{$dirty_hash_for_amlgms{$token}{$cat}}) {
	    $line = ($i || "0")."\t".($i+1)."\t".$line."\n";
	    if ($i == 0) {
	      $paths[$i]{$line} = 1;
	    } else {
	      for (keys %{$paths[$i-1]}) {
		$paths[$i]{$_.$line} = 1;
	      }
	    }
	  }
	}
      }
      $i++;
    }
    if ($i > 1) {
      print "0-$n\t$amlgm\t_\t_\t_\t_\t_\t_\t_\t_\n";
      print join "0-$n\t$amlgm\t_\t_\t_\t_\t_\t_\t_\t_\n", sort keys %{$paths[$i-1]};
    }
  }
}
