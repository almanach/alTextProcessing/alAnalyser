#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use strict;

# Converts a UTF-8 text into a <target_encoding> (default: L1) text, without loss: non-convertible characters are encoded using entities (&#nnn;)

my $targetencoding = "l1";

while (1) {
  $_ = shift;
  if (/^$/) {last}
  elsif (/^-e$/) {$targetencoding = shift || die "Option '$_' must be followed by an encoding name (ascii or l1 [default = l1])"}
  else {die "Unknown option '$_'"}
}

if ($targetencoding =~ /^ascii$/i) {
  while (<>) {
    s/(\P{ASCII})/"&#".ord($1).";"/ge;  
    print $_;
  }
} elsif ($targetencoding =~ /^l(?:atin)?-?1$/i) {
  while (<>) {
    s/([^\0-~ -ÿ])/"&#".ord($1).";"/ge;
    print $_;
  }
} else {die "Unsupported encoding '$targetencoding'"}

