#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

my $verbose;
while (1) {
  $_ = shift;
  last if /^$/;
  if (/^-v$/) {$verbose = 1}
  else {die}
}


my ($mwt_start, $mwt_end, $rem, $cur_multiword_token, $cur_multiword_lines, $nb_distinct_forms, $nb_distinct_lemma_cat_pairs);
my ($nb_amlgm_forms, $nb_simple_forms);
my (%amlgm_form_lines, %simple_form_lines, %forms, %lemma_cat_pairs);
$mwt_start = -1;
while (<>) {
  chomp;
  if (/^$/ || /^#.*/) {
  } elsif (/^(\d+)-(\d+)\t([^\t]+)\t(.*)/) {
    $mwt_start = $1;
    $mwt_end = $2;
    $cur_multiword_token = $3;
    $rem = $4;
    die $rem unless $rem =~ /^_(\t_)*$/;
  } elsif (/^(\d+)\t(\d+)\t([^\t]+)\t([^\t]*)\t([^\t]*)\t[^\t]*\t([^\t]*)/) {
    my ($id_start, $id_end, $form, $lemma, $cat, $ms) = ($1, $2, $3, $4, $5, $6);
    $forms{$form}++;
    $lemma_cat_pairs{$lemma."\t".$cat}++;
    if ($mwt_start >= 0) {
      $nb_amlgm_forms++ if $mwt_start == $id_start;
      $mwt_start = -1 if $mwt_end == $id_end;
    } else {
      $nb_simple_forms++;
    }
  } else {die $_}
}
$nb_distinct_forms = scalar keys %forms;
$nb_distinct_lemma_cat_pairs = scalar keys %lemma_cat_pairs;

if ($verbose) {
  print "Nb simple entries:\t$nb_simple_forms\n";
  print "Nb amlgm entries:\t$nb_amlgm_forms\n";
  print "Nb distinct forms:\t$nb_distinct_forms\n";
  print "Nb distinct <lemma,cat> pairs:\t$nb_distinct_lemma_cat_pairs\n";
} else {
  print "$nb_simple_forms\t$nb_amlgm_forms\t$nb_distinct_forms\t$nb_distinct_lemma_cat_pairs\n";
}
