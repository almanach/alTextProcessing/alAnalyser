package alNormalisation;
use utf8;
use strict;
use alVariables;
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(&altok_normalise);

sub altok_normalise {
  #pour le grec ancien, grec moderne, lycien et chinois utilisant des caractères latins du codemap chinois
  my $s = shift;
  my $lang = shift;

  return $s if $s =~ /_XML$/;

  die "### ERROR: the input of altok_normalise must have been XML-escaped beforehand; in particular, there should not remain any '<' and '>' characters unless within {...} _XML markup: $s|" if $s =~ /[<>]/;
  my ($r,$t);
  my @s = split / /, $s;
  my $is_greek = ($lang =~ /^el(-|$)/);
  my $is_ancient_greek = ($lang =~ /^(?:grc|grk)(-|$)/);
  my $is_lycian = ($lang eq "xlc");
  my $has_chinese_latin_characters = ($s =~ /[ａ-ｚ０-９Ａ-Ｚ]/);
  for my $i (0..$#s) {
    $r .= " " unless $i == 0;
    $t = $s[$i];
    if ($t ne "") {
      $_ = $t;
      if ($is_greek) {
	if (/ϲ$/ &&
	    ($s[$i+1] =~ /^$alPunctuation/
	     || $s[$i+1] eq "") # real whitespace
	   ) {
	  s/ϲ$/ς/;
	}
	tr/ϲ/σ/;
	s/΄//g;
	s/΅//g;
	s/ͺ//g;
	tr/ϒϓϔ/ΥΎΫ/;
	tr/Ϲ/Σ/;
	tr/ϐϑϰϱϕ/βθκρφ/;
	tr/ΆΈΉΊΌΎΏάέήίόύώ/ΆΈΉΊΌΎΏάέήίόύώ/; # Ancient Greek acute accent > Modern Greek accent
      } elsif ($is_ancient_greek) {
	if (/ϲ$/ &&
	    ($s[$i+1] =~ /^$alPunctuation/
	     || $s[$i+1] eq "") # real whitespace
	   ) {
	  s/ϲ$/ς/;
	}
	tr/ϲ/σ/;
	tr/Ϲ/Σ/;
	tr/ϐϑϰϱϕ/βθκρφ/;
	tr/ΆΈΉΊΌΎΏάέήίόύώ/ΆΈΉΊΌΎΏάέήίόύώ/; # Modern Greek accent > Ancient Greek acute accent
      } elsif ($is_lycian) { # Lycian
	tr/θ/ϑ/;
      }
      s/^([ａ-ｚ０-９Ａ-Ｚ]+)$/_altok_normalise_latin($1)/ge if $has_chinese_latin_characters;
      if ($_ ne $t) {
	$r .= "{$t} ";
      }
      $r .= $_;
    }
  }
  $r .= " " unless $r eq "";
  return $r;
}

sub _altok_normalise_latin {
  my $s = shift;
  #unicode chinois 
  $s =~ tr/[ａ-ｚ０-９Ａ-Ｚ]/[a-z0-9A-Z]/;
  return $s;
}


1;
