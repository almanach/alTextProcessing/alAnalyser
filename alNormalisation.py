#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import regex as re
import alVariables

def _altok_normalise_latin(s):
	#unicode chinois
	#transliteration avec tout de marqué, en perl c'est des classes de caractères mais ici on ne peut pas
	s = s.translate(s.maketrans("ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ０１２３４５６７８９ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ", "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"))
	return s

def altok_normalise(s, lang):
	#pour le grec ancien, grec moderne, lycien et chinois utilisant des caractères latins du codemap chinois
	if re.search(r"_XML$", s):
		return s
	assert(re.search(r"[<>]", s) is None), f"### ERROR: the input of altok_normalise must have been XML-escaped beforehand; in particular, there should not remain any '<' and '>' characters unless within {{...}} _XML markup: {s}|"
	s_list = s.split(" ")
	is_greek, is_ancient_greek, is_lycian, has_chinese_latin_characters = 0, 0, 0, 0
	if re.search(r"^el(-|$)", lang):
		is_greek = 1
	if re.search(r"^(?:grc|grk)(-|$)", lang):
		is_ancient_greek = 1
	if lang == "xlc":
		is_lycian = 1
	if re.search(r"[ａ-ｚ０-９Ａ-Ｚ]", s):
		has_chinese_latin_characters = 1

	r, t = "", ""
	for i, item in enumerate(s_list):
		if i != 0:
			r += " "
		t = item
		if t != "":
			if is_greek:
				#ajout d'un test sinon on aura une erreur d'index
				if (i+1) <= (len(s_list)-1):
					if ((re.search(r"ϲ$", t) and re.search(rf"^{alVariables.alPunctuation.pattern}", s_list[i+1])) or (s_list[i+1] == "")): #real whitespace
						t = re.sub(r"ϲ$", r"ς", t)
				t = t.translate(t.maketrans("ϲ", "σ"))
				t = re.sub(r"΄", r"", t)
				t = re.sub(r"΅", r"", t)
				t = re.sub(r"ͺ", r"", t)
				t = t.translate(t.maketrans("ϒϓϔ", "ΥΎΫ"))
				t = t.translate(t.maketrans("Ϲ", "Σ"))
				t = t.translate(t.maketrans("ϐϑϰϱϕ", "βθκρφ"))
				t = t.translate(t.maketrans("ΆΈΉΊΌΎΏάέήίόύώ", "ΆΈΉΊΌΎΏάέήίόύώ")) # Ancient Greek acute accent > Modern Greek accent

			elif is_ancient_greek:
				if (i+1) <= (len(s_list)-1):
					if ((re.search(r"ϲ$", t) and re.search(rf"^{alVariables.alPunctuation.pattern}", s_list[i+1])) or (s_list[i+1] == "")): #real whitespace
						t = re.sub(r"ϲ$", r"ς", t)
				t = t.translate(t.maketrans("ϲ", "σ"))
				t = t.translate(t.maketrans("Ϲ", "Σ"))
				t = t.translate(t.maketrans("ϐϑϰϱϕ", "βθκρφ"))
				t = t.translate(t.maketrans("ΆΈΉΊΌΎΏάέήίόύώ", "ΆΈΉΊΌΎΏάέήίόύώ")) # Modern Greek accent > Ancient Greek acute accent

			elif is_lycian: # Lycian
				t = t.translate(t.maketrans("θ", "ϑ"))

			if has_chinese_latin_characters:
				s = re.sub(r"^([ａ-ｚ０-９Ａ-Ｚ]+)$", lambda x : _altok_normalise_latin(x.group(1)), s)
				#pass

			if t != item:
				r += f"{{{item}}} "
			r+= t
	
	#ICI ajout de la regex pour ajouter un espace final QUE si on en a pas déjà de base...car le split Python n'est pas le même que Perl (un élément vide à la fin en plus)
	if r != "" and not re.search(r" $", r):
		r += " "
	return r

