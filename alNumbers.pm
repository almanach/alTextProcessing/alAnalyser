package alNumbers;
use utf8;
use strict;
use alVariables;
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(&altok_numbers);

my ($l, $maj, $numFR1, $numFR2, $numNOSEP, $num_ext, $numUS, $rom1_10, $rom2_10, $rom10_maxrom, $numROM, $numROMnotI, $num, $num_ext, $multinum, $multinum_ext, $multinumL, $multinumROMnotI, $multinumROMI, $fraction, $num_left_ctxt, $num_right_ctxt, $numsuff, $romnum_lctxt, $numsuff_space);
my ($listnumid, $listnumidB, $listnumprefix, $listnumprefix2, $listnumprefix2b, $listnumprefix3);
my ($digit, $order, $zhnumber, $point, $mixednumber, $number);


sub altok_numbers {
  my $s = shift;		# must be already tokenised
  my $lang = shift;
  my $less_lists = shift || 0;
  my $less_splits = shift || 0;

  return $s if $s =~ /_XML$/;
  if ($lang =~/^zh-eleve/) {
    $digit = qr/(?:[〇零一二三四五六七八九○0-9０-９])/o;
    $order = qr/(?:[十百千萬億兆万亿])/o;
    $point = qr/(?:[.．点點])/o;
    $zhnumber = qr/(?:[－-]?(?:十$digit|$digit$order)(?:$digit?$order?)*(?:$point$digit+)?(?:％|$order)?)/o;
    $mixednumber = qr/(?:[－-]?(?:(?:十$digit|$digit$order)(?:$digit?$order?)*|[0-9０-９]+)(?:$point$digit+)?(?:％|%|$order)?)/o;
    $number = qr/(?:[0-9０-９]+(?:$point[0-9０-９]+)?$order?$zhnumber?)/o;
  } elsif ($lang eq "th") {
    $digit = qr/(?:[๐๑๒๓๔๕๖๗๘๙0-9])/o;
  } else {
    $l        = qr/[^a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźžżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŽŻÑ\.}]/o;
    $maj      =qr/[^ A-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŽŻÑ]/o;
    $numFR1   = qr/(?:\d{1,3}(?:(?:  | _UNDERSCORE )+\d{3})*(?:(?:  | _UNDERSCORE )* (?:,| , ) (?:  | _UNDERSCORE )*\d+)?)/o;
    $numFR2   = qr/(?:\d{1,3}(?: (?:\.| \. ) \d{3})*(?:(?:  | _UNDERSCORE )*,(?:  | _UNDERSCORE )*\d+)?)/o;
    $numNOSEP = qr/(?:\d+(?:(?:  | _UNDERSCORE )*(?: [,\.] |[,\.])(?:  | _UNDERSCORE )*\d+)?)/o;
    $num_ext = qr/(?:\d*(?:[IlO]*\d+|\d+[IlO])(?: (?: [,\.] |[,\.]) [\dIlO]+)?)/o;
    $numUS    = qr/(?:\d{1,3}(?: (?: , |,) \d{3})*(?: (?: \. |\.) (?:[12] (?: \/ |\/) [234]|\d+))?)/o;
    $rom1_10 = qr/(?:III?|I?V|VIII?|VI|I?X|I)/o;
    $rom2_10 = qr/(?:III?|I?V|VIII?|VI|I?X)/o;
    $rom10_maxrom = qr/(?:(?:M+(?:C?D)?)?(?:X{1,4}|X?L|LX{1,4}|X?C{1,4}))/o;
    $numROM    = qr/(?:$rom10_maxrom?$rom1_10)/o;
    $numROMnotI    = qr/(?:$rom10_maxrom?$rom2_10|${rom10_maxrom}I)/o;
    $num      = qr/(?:$numFR1|$numNOSEP|$numFR2|$numUS)/o;
    $num_ext   = qr/(?:$numFR1|$numNOSEP|$numFR2|$numUS|$num_ext)/o;
    $multinum = qr/$num(?:  ?[\-\/\.\­]  ?$num)?/o;
    $multinum_ext = qr/$num_ext(?:  ?[\-\/\.\­]  ?$num_ext)?/o;
    $multinumL = qr/$num[\-\­]?[a-zA-Z](?:  ?[\-\/\.\­]  ?$num[\-\­]?[a-zA-Z])?/o;
    $multinumROMnotI = qr/$numROMnotI(?:  ?[\-\/\.\­]  ?(?:$numROM|$multinum))?/o;
    $multinumROMI = qr/I  ?[\-\/\.\­]  ?(?:$numROM|$multinum)/o;
    $fraction = qr/(?:[1-9][0-9]*  ?\/  ?[1-9][0-9]*)/o;
  }
  if ($lang eq "sk") {
    $numsuff  = qr/(?:\.)/io;
    $numsuff_space  = qr/  /io;
    $num_right_ctxt = qr/(?:rok[aoue]*) /o;
  } elsif ($lang eq "pl") {
    $numsuff  = qr/(?:\.)/io;
    $numsuff_space  = qr/  /io;
    $num_right_ctxt = qr/(?:lata?|rok[aoue]*) /o;
  } elsif ($lang eq "en") {
    $numsuff  = qr/(?:th|st|[nr]d)/io;
    $numsuff_space  = qr/ /io;
    $num_right_ctxt = qr/(?:years?) /o;
  } else {
    $numsuff  = qr/(?:i?[eè]mes?|[eè]|ers?|[eè]res?|nds?|ndes?|aines?|e)/io;
    $numsuff_space  = qr/ /io;
    $num_right_ctxt = qr/(?:ans?|jours?) /o;
  }
  $num_left_ctxt = qr/(?:[Aa]rt  ?\.?|nr  ?\.?|num  ?\.?|pkt  ?\.?|ust  ?\.?|č  ?\.?)/o;
  $romnum_lctxt     = qr/(?: )/o;
  $listnumid = qr/(?:$numROM|\d{1,2})(?: \. (?:$numROM|\d{1,2}|[A-Za-z]))*(?: \.)?(?= [^0-9])/o;
  $listnumidB = qr/(?:[A-Za-z])(?: \. (?:$numROM|\d{1,2}|[A-Za-z]))*(?: \.)?(?= [^0-9])/o;
  $listnumprefix = qr/((?:^ *|[;:,] |[^\d ] \.|_META_TEXTUAL_[^ ]+)  )/o;
  $listnumprefix2 = qr/((?:^ *|,  |[;:]  ?|[^\d ] \.|_META_TEXTUAL_[^ ]+)  ?)/o;
  $listnumprefix2b = qr/((?:^ *|,  |[;:]  ?|_META_TEXTUAL_[^ ]+)  ?)/o;
  $listnumprefix3 = qr/((?:^ *|[;:,] |_META_TEXTUAL_[^ ]+)  )/o;
  

  $_ = $s;
  
  s/([^0-9]) (,) ([0-9]+) (,) ([0-9])/$1 $2 $3$4$5/g;

  if ($lang =~/^zh-eleve/){
    s/␠/_STRANGESPACE/g;
    while (s/({[^{}]*[^{}␠])([0-9０-９○]|$digit|$order)/\1␠\2/go) {}
    s/(?<![{␠])($mixednumber|$zhnumber|[0-9０-９○]{2,})/ \{$1\} _NUM /go;
    s/(第) ($digit)/{$1$2} _ORDINAL/go;
    s/(第)  \{([^\}]+)\} _NUM/{$1 $2} _ORDINAL/go;
    s/␠//g;
    s/_STRANGESPACE/␠/g;
  } elsif ($lang eq "th"){
    s/␠/_STRANGESPACE/g;
    while (s/({[^{}]*[^{}␠])($digit)/\1␠\2/go) {}
    s/(?<![{␠])($digit+(?:,${digit}{3})*(?:\.$digit+)?)/ \{$1\} _NUM /go;
    s/␠//g;
    s/_STRANGESPACE/␠/g;
  } else {
    if ($lang eq "fr") {
      s/(?<=[^\}])([ :;\?\!\(\)\[\]\"\+\*\-]) (brevets?(?:  (?:[a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñ-]+)?)?)  ((?:n[°o](?:  de  publication)?  ?)?[0-9][0-9 \/\.-]+[0-9])/$1 $2  {$3} _PATENTID/gi;
      while (s/(_PATENTID [ :;\?\!\(\)\[\]\"\+\*\-]+ *)((?:du  \{[^}]+\} ?_DATE[^ ]* )?(?: ?et|,))  ((?:n[°o](?:  de  publication)? ?)?[0-9][0-9 \/\.-]+[0-9])/$1$2 {\3} _PATENTID/g) {
      }
      s/(?<=[^\}])([ :;\?\!\(\)\[\]\"\+\*\-]) ([A-Z]{1,2} +)\{([^{}]+\} _PATENTID)/$1 \{$2$3/g;
    }

    if ($lang eq "en") {
      s/(?<=[^\}])  (\d+ \+) ( [^\d]|[\.,;:\?\!\(\)\[\]]|$)/  {$1} _NUM $2/go;
      s/(?<=[^\}])  (\d+ (?:[km]|bn)) ( [^\d]|[\.,;:\?\!\(\)\[\]]|$)/  {$1} _NUM $2/go;
    }
    
    s/(?<=[^\}])  (\#\d+) ([ \.,;:\?\!\(\)\[\]])/  {$1} _NUM $2/go;

    # tentative de protection pour les séquences de maj et chiffres mélangés
    s/(?<=[^\}])([ :;\?\!\(\)\[\]\"\+\*\-]) ([A-Z]+\d+[A-Z\d\/-]*) ([ \.,;:\?\!\(\)\[\]])/$1 __{{$2}}__ $3/g;
    s/(?<=[^\}])([ :;\?\!\(\)\[\]\"\+\*\-]) (\d+[A-Z]+[A-Z\d\/-]*) ([ \.,;:\?\!\(\)\[\]])/$1 __{{$2}}__ $3/g;

    s/(\{[^{}]+}\s*_(?:PATENTID|NUM(?:_[^_]+_)?))/"__{{"._replace_spaces_by_tmpspace($1)."}}__"/ge;
    
    s/([ \.,;:\?\!\(\)\[\]\/\-\"]) (M) (6) ([ \.,;:\?\!\(\)\[\]\/\-\"])/$1 __\{\{$2$3\}\}__ $4/go;
    s/([ \.,;:\?\!\(\)\[\]\/\-\"]) (France) ([2345]|24) ([ \.,;:\?\!\(\)\[\]\/\-\"])/$1 __\{\{$2$3\}\}__ $4/go;
    s/([ \.,;:\?\!\(\)\[\]\/\-\"]) (Europe) ([12]) ([ \.,;:\?\!\(\)\[\]\/\-\"])/$1 __\{\{$2$3\}\}__ $4/go;
    s/([ \.,;:\?\!\(\)\[\]\/\-\"]) (RTL) ([29]) ([ \.,;:\?\!\(\)\[\]\/\-\"])/$1 __\{\{$2$3\}\}__ $4/go;
    s/([ \.,;:\?\!\(\)\[\]\/\-\"]) (TF) (1) ([ \.,;:\?\!\(\)\[\]\/\-\"])/$1 __\{\{$2$3\}\}__ $4/go;
    s/([ \.,;:\?\!\(\)\[\]\/\-\"]) (George) (V) ([ \.,;:\?\!\(\)\[\]\/\-\"])/$1 __\{\{$2$3\}\}__ $4/go;
    s/([ \.,;:\?\!\(\)\[\]\/\-\"]) (20) ([Mm]inutes) ([ \.,;:\?\!\(\)\[\]\/\-\"])/$1 __\{\{$2$3\}\}__ $4/go;
    s/([ \.,;:\?\!\(\)\[\]\/\-\"]) (Seveso) ([12]|I|II) ([ \.,;:\?\!\(\)\[\]\/\-\"])/$1 __\{\{$2$3\}\}__ $4/go;
    s/([ \.,;:\?\!\(\)\[\]\/\-\"]) (Cap) (21) ([ \.,;:\?\!\(\)\[\]\/\-\"])/$1 __\{\{$2$3\}\}__ $4/go;
    s/([ \.,;:\?\!\(\)\[\]\/\-\"]) (Rue) (89) ([ \.,;:\?\!\(\)\[\]\/\-\"])/$1 __\{\{$2$3\}\}__ $4/go;
    s/ ([Dd] ?') (Europe) ([12]) ([ \.,;:\?\!\(\)\[\]\/\-])/$1 __\{\{$2$3\}\}__ $4/go;
    s/([ \.,;:\?\!\(\)\[\]\/\-\"]) (G) ([789]|20) ([ \.,;:\?\!\(\)\[\]])/$1 __\{\{$2$3\}\}__ $4/go;
    s/([ \.,;:\?\!\(\)\[\]\/\-\"]) (4[xX]4) ([ \.,;:\?\!\(\)\[\]])/$1 __\{\{$2\}\}__ $3/go;
    s/([ \.,;:\?\!\(\)\[\]\/\-\"]) ([Nn]) ([+-]) (\d) ([\s\.,;:\?\!\(\)\[\]])/$1 __\{\{$2$3$4\}\}__ $5/go;
    s/(\&#\d+;)/\__\{\{$1\}\}__/g;
    s/(moteurs?)(V[0-9]+)([\s\.,;:\?\!\(\)\[\]])/\1 _REGLUE___{{\2}}__\3/go;
    s/(moteurs?\s+)(V[0-9]+)([\s\.,;:\?\!\(\)\[\]])/\1__{{\2}}__\3/go;
    s/(?<=[^\}]) (i\. *e\.?|e\. *g\.?|d\. *h\.?|z\. *b\.?|o\. *ä\.?|m\. *e\.?|u\. *s\. *w\.?)([\s\.,;:\?\!\(\)\[\]])/ __{{\1}}__\2/goi;
    s/(?<=[^\}]) (France\.9)([\s\.,;:\?\!\(\)\[\]])/ __{{\1}}__\2/go;
    s/(?<=[^\}])([\s\.,;:\?\!\(\)\[\]\/\-\"])(EE-LV)([\s\.,;:\?\!\(\)\[\]\/\-\"])/\1__{{\2}}__\3/go;
    s/(?<=[^\}]) ([Dd]')(EE-LV)([\s\.,;:\?\!\(\)\[\]\/\-\"])/ \1__{{\2}}__\3/go;
    s/(?<=[^\}]) (\d{1,2}(?: ?: ?\d{1,2})? *[AaPp]\.? ?[Mm]\.?)/ __{{\1}}__/go;

    if ($lang eq "fr") {
      s/ (Ian) ([ \.,;:\?\!\(\)\[\]])/ __{{$1}}__ $2/go; # protection required
    }
#    s/(\S*[A-Za-z-]\S*_UNDERSCORE\S+)/__{{\1}}__/go;
#    s/(\S+_UNDERSCORE\S*[A-Za-z-]\S*)/__{{\1}}__/go;
    s/__\{\{__\{\{(.*?)\}\}__\}\}__/__{{\1}}__/go;
    
    # reconnaissance
    s/^ *([\-\­=]+  ?&gt;)/\{$1\} _META_TEXTUAL_PONCT/go; # [début de ligne] ->
    s/$listnumprefix2((?:$numROM|\d+)  ?[\.\-\­]  ?$listnumid)  /$1\{$2\} _META_TEXTUAL_GN  /go; # [début de ligne] I.2 texte
    s/\} _META_TEXTUAL_GN(  ?[\.\)\]\/\]\-\­])  ([^0-9}])/$1\} _META_TEXTUAL_GN  $2/go; # [début de ligne] I.2) texte ### previous line MUST be the first line to insert _META_TEXTUAL_GN
    unless ($less_lists) {
      s/$listnumprefix2([\-\­])(  ?)($listnumid|$listnumidB)(  ?)([\.\)\]\/\]\-\­])(?=  ?[^0-9}])/$1\{$2\} _META_TEXTUAL_PONCT$3\{$4\} _META_TEXTUAL_GN$5\{$6\} _META_TEXTUAL_PONCT/go; # [début de ligne] - 1) texte
      s/$listnumprefix2($listnumid)(  ?)([\.\)\]\/\]\-\­])(?=  ?[^0-9}])/$1\{$2\} _META_TEXTUAL_GN$3\{$4\} _META_TEXTUAL_PONCT/go; # [début de ligne] 1) texte
      s/$listnumprefix($listnumidB)(  ?)([\.\)\]\/\]\-\­])(?=  ?[^0-9}])/$1\{$2\} _META_TEXTUAL_GN$3\{$4\} _META_TEXTUAL_PONCT/go; # [début de ligne] 1) texte
    }

    # rattrapage de sur-corrections
    if ($lang eq "fr") {
      s/$listnumprefix\{([Aa])\} _META_TEXTUAL_GN(  ?)\{(-)\} _META_TEXTUAL_PONCT(  ?t -)/$1$2$3$4$5/g;
      # ci-dessus = horreur
    }
    s/(\([^\)]*)\{([^{}]+)\} _META_TEXTUAL_GN(  ?)\{(\))\} _META_TEXTUAL_PONCT/$1$2$3$4/g;
    
    # remet M. en mode no word segmentation au lieu de _META_TEXTUAL_GN _SENT_BOUND
    s/{(Mr?|Mme|Miss|Mrs|Sir|Lady|Sgt)} _META_TEXTUAL_GN \{(\.)\} _META_TEXTUAL_PONCT/$1$2/g;
    s/{(Mr?|Mme|Miss|Mrs|Sir|Lady|Sgt)(?: (\.))?} _META_TEXTUAL_GN/$1$2/g;
    
    s/$listnumprefix\{([G-ZÉÃÀÂÊÛÎÔÄËÜÏÖÇ])\} _META_TEXTUAL_GN \{(\.)\} _META_TEXTUAL_PONCT/$1$2$3/g; # \1 est dans listnumprefix
    if ($lang =~ /^(?:fr|en|es|pt|it|ro)$/) {
      s/ ((?:pp|[pnv])  ?)\{(\d+)\} _META_TEXTUAL_GN \{(\.)\} _META_TEXTUAL_PONCT/ $1$2 $3/g;
      s/ ((?:pp|[pnv])) (\.  ?)\{(\d+)\} _META_TEXTUAL_GN \{(\.)\} _META_TEXTUAL_PONCT/ $1$2$3 $4/g;
    } elsif ($lang eq "de") {
      s/ ([snv]  ?)\{(\d+)\} _META_TEXTUAL_GN \{(\.)\} _META_TEXTUAL_PONCT/ $1$2 $3/g;
      s/ ([snv]) (\.  ?)\{(\d+)\} _META_TEXTUAL_GN \{(\.)\} _META_TEXTUAL_PONCT/ $1$2$3 $4/g;
    } elsif ($lang =~ /^(sk|cz)$/) {
      s/ ([čv]  ?)\{(\d+)\} _META_TEXTUAL_GN \{(\.)\} _META_TEXTUAL_PONCT/ $1$2 $3/g;
      s/ ([čv]) (\.  ?)\{(\d+)\} _META_TEXTUAL_GN \{(\.)\} _META_TEXTUAL_PONCT/ $1$2$3 $4/g;
    }

    if ($lang eq "en") { # le mot "a" ne peut terminer une phrase... donc on va dire artificiellement que " a[.\)...]" désigne tjs un _META_TEXTUAL_truc
      s/ ([Aa]) (\.)(?=  ?[^0-9mM])/ \{$1\} _META_TEXTUAL_GN \{$2\} _META_TEXTUAL_PONCT/go; # a. ATTENTION, TRES RISQUÉ
      # ... sauf dans certains contextes gauches
      s/((?:Mr?|M|Mme|Miss|Mrs|Sir|Lady)(?: \.)?  )\{([Aa])\} _META_TEXTUAL_GN \{(\.)\} _META_TEXTUAL_PONCT/$1$2 $3/g;
      # ... et droite
      s/\{([Aa])\} _META_TEXTUAL_GN \{(\.)\} _META_TEXTUAL_PONCT (\.)/$1 $2$3/g;
      s/\{(A)\} _META_TEXTUAL_GN \{\.\} _META_TEXTUAL_PONCT (\.)/$1 $2$3/g;
      s/\{(A)\} _META_TEXTUAL_GN \{\.\} _META_TEXTUAL_PONCT ([A-Z])/$1 $2 $3/g;
    }
    
    if ($less_lists) {
      s/$listnumprefix2b([\-\­\*]) ([^ ]*  )/$1\{$2\} _META_TEXTUAL_PONCT $3/go; # [début de ligne] - texte
      s/^( *)(\+)(?=  )/$1\{$2\} _META_TEXTUAL_PONCT/go; # [début de ligne] + texte
    } else {
      s/$listnumprefix([\-\­\*]) ([^ ]*  )/$1\{$2\} _META_TEXTUAL_PONCT $3/go; # [début de ligne] - texte
      s/$listnumprefix(\+)(?=  )/$1\{\+\} _META_TEXTUAL_PONCT/go; # [début de ligne] + texte
    }
    s/$listnumprefix3\.(\s)/$1\{\.\} _META_TEXTUAL_PONCT$2/go; # [début de ligne] . texte

    # QQUES CAS TORDUS
    s/(?<=[^0-9\{])(\d+\s*(?:[,\.]\s*|\s+)[123]\s*\/\s*[234])([\s\";:\)]|[\.,][^0-9]|$)/\{$1\} _NUM$2/go; #    9,1/4
    s/(?<=[^0-9A-Z\{_])([A-Z\d]*\d[A-Z\d]*\s*[\/-]\s*(?:[\d\/-]\s[\d\/-]|[\dA-Z\/-])*[\dA-Z])([\s\";:\)]|[\.,][^0-9]|$)/\{$1\} _NUM$2/go; #    (fractions)   2-F4      A3-249/93	0-190/93
    s/(?<=[^0-9\{])(\d+(?:[.,]\d+)?\s*x\s*\d+(?:[.,]\d+)?)([\s\";:\)]|[\.,][^0-9]|$)/\{$1\} _NUM$2/go; # 130x180
    #    s/(?<=[^0-9\{])($fraction)/\{$1\} _NUM/go; # 1/2

    s/($romnum_lctxt)($multinumROMI|$multinumROMnotI) (?=(?: ?$l|\.  $maj))/$1\{$2\} _ROMNUM /go; # IV I
    s/($romnum_lctxt)((?:$multinumROMnotI|$multinumROMI)$numsuff)($numsuff_space)/$1\{$2\} _ROMNUM$3/go; # IVème Ier
    # overmatching
    s/\{(VIE|MCI|CV)} _ROMNUM/$1/g;
    s/U.\{V} _META_TEXTUAL_GN \{.\} _META_TEXTUAL_PONCT/U.V./g;
    s/U.\{V} _ROMNUM/U.V/g;
    s/\{I-I} _ROMNUM -/I - I -/g if $lang eq "en";
    s/\{([xX])} _ROMNUM ?(-) ?(?=(?:ray|rated|shaped))/$1$2/gi;
    s/\{(.)} _ROMNUM ?(-) ?(?=(?:shaped))/$1$2/gi;
    
    s/(?<=[^0-9,\.\{] )($multinum  ?$numsuff)($numsuff_space)/\{$1\} _NUM$2/go; # 2ème
    if ($lang eq "pl") {
      s/(?<=[^0-9\{])($multinum_ext\s*(?:mld|tys|mln|bln)(?:  ?\.)?)/\{$1\} _NUM/go; # 2ème
    }
    s/(?<=[^0-9\{])($multinumL)(  ?)(?=[^ 0-9\-\­\/,\.a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ])/\{$1\} _NUM$2/go; # 2a
    
    s/($num_left_ctxt  ?)([0-9]+  ?[A-Za-e]|[A-Z])( [ \",;\.:\)]| ?$)/$1\{$2\} _NUM$3/go;
    s/\{(M)\} _(?:NUM|META_TEXTUAL_GN)(  ?\.)/$1$2/go;
    s/\{(M)\} _(?:NUM|META_TEXTUAL_GN)(  ?)\{(\.)\} _META_TEXTUAL_PONCT/$1$2$3/go;
    s/  ([0-9]*[IlO][0-9\., ]*?[0-9])(  ?$num_right_ctxt)/  \{$1\} _NUM$2/go;
    s/([^0-9\{])([0-9]*[0-9\., ]+[IlO])(  ?$num_right_ctxt)/$1\{$2\} _NUM$3/go;

    if ($less_splits) {
      while (s/([,\.\-\/\­\$€¥£ ]  ?)($multinumL)(  ?[,\.\-\/\­](?: [^0-9]|$))/$1\{$2\} _NUM$3/go) {} 
      while (s/([,\.\-\/\­\$€¥£ ]  ?)($multinum)(  ?(?:[^0-9\-\­\/,\.\} ]|[,\.\-\­\/](?: [^0-9]| ?$)))/$1\{$2\} _NUM$3/go) {} # 2
    } else {
      while (s/([^0-9\{ ]  ?)($multinumL)(  ?[,\.\-\/\­](?: [^0-9]| ?$))/$1\{$2\} _NUM$3/go) {}
      while (s/([^0-9\{ ]  ?)($multinum)(  ?(?:[^0-9\-\­\/,\.\} ]|[,\.\-\­\/](?: [^0-9]| ?$)))/$1\{$2\} _NUM$3/go) {} # 2
    }
    s/(_NUM)(\S)/$1 $2/g;
    # REGROUPE AVEC LE PRECEDENT   s/(?<=[^0-9\{])($multinum) +$/\{$1\} _NUM/go; # 2 [fin de ligne]

    # sécurités pour éviter les _NUM emboités et les reconnaissances de trucs déjà reconnus à l'avance comme 4x4
    while (s/(?<!_)\{([^{}]*)\{([^{}]*)} _NUM/{$1$2/g) {
    }
    s/\{([^}]+)}  *\{([^}]+)} _NUM/{$1} $2/g;
    
    if ($lang eq "fr") {
      s/(?<![0-9\{ ])(  ?)\{([^\}]+)\} _NUM(  ?virgule  ?)\{([^\}]+)\} _NUM/$1\{$2$3$4\} _NUM/go; # 2 virgule 5
    }
    s/  ([0-9]+)  /  \{$1\} _NUM  /go; # sécurité
    
    s/ \}/\}/go;

    # unlock protections
    s/__\{\{//g;
    s/\}\}__//g;

    if (0) { # TODO: convert and activate
      if ($lang =~ /^(ja|zh|tw)$/) {
	s/([①-⑫])/{\1} _META_TEXTUAL_GN /g;
	s/(\{&#226;&#145;&#16[0-8];})\s*_ETR/{$1} _META_TEXTUAL_GN/g;
	s/([●※＊■◆→⇒◇◎★☆〇])/{$1} _META_TEXTUAL_PONCT/g;
	s/(?<![1-9１-９〇零一二三四五六七八九○十百千萬億兆万亿])(○)(?![1-9１-９〇零一二三四五六七八九○十百千萬億兆万亿])/{\1} _META_TEXTUAL_PONCT/g;
	s/([\（\(])\{([1-9１-９])} _NUM([\）\)])/{$1$2$3} _META_TEXTUAL_PONCT _UNSPLIT__META_TEXTUAL_GN _UNSPLIT__META_TEXTUAL_PONCT/g;
	s/(\{&#226;&#151;&#143;})\s*_ETR/{\1} _META_TEXTUAL_PONCT/g;
	s/ \{([1-9１-９]+)\} _NUM(\s*)([\.\)、\）。，：:])/ {$1} _META_TEXTUAL_GN$2\{$3} _META_TEXTUAL_PONCT /g;
	s/(^|。)(\s*)(第?[一二])([\.\)、\）。，：:])/$1$2\{$3$4} _META_TEXTUAL_GN _UNSPLIT__META_TEXTUAL_PONCT /g;
      } elsif ($lang =~ /^th$/) {
	s/^(\s*)\{(๑๐|๑?[๑๒๓๔๕๖๗๘๙])\} _NUM ([\.])/$1\{$2$3\} _META_TEXTUAL_GN _UNSPLIT__META_TEXTUAL_PONCT /g;
      }
      if ($lang =~ /^(ja)/) {
	s/(·)/ \{\1} _META_TEXTUAL_PONCT /g; # diverses bullets unicode (utilisé en mandarin comme séparateur de type ponctuation (très) faible)
      }
      if ($lang eq "km") {
	s/([\P{Latin}\)])[ ​]*-[ ​]*(\P{Latin})/$1 {-} _META_TEXTUAL_PONCT $2/g;
	s/([\P{Latin}\)])[ ​]*\{(\d+)\} _NUM([,\-])[ ​]*(\P{Latin})/$1 {$2} _META_TEXTUAL_GN {$3} _META_TEXTUAL_PONCT $4/g;
      }
      s/(•)/ \{\1} _META_TEXTUAL_PONCT /g; # diverses bullets unicode
      s/(\{(?:&#226;&#128;&#162;|&#194;&#183;)})\s*_ETR/\1 _META_TEXTUAL_PONCT/g; # diverses bullets unicode yarecodées
    }
  }
  unless ($lang =~ /^zh-eleve/) {
    s/_TMP_SPACE/ /g;
    s/_TMP_COMMENT_B/{/g;
    s/_TMP_COMMENT_E/}/g;
  }
  
  while (s/{([^{}]+)}\s*_[^ {}]+\s*([^{}]*} *_SPECWORD)/$1$2/g) {}

  return $_;
}

sub _replace_spaces_by_tmpspace {
  my $s = shift;
  $s =~ s/ /_TMP_SPACE/g;
  $s =~ s/{/_TMP_COMMENT_B/g;
  $s =~ s/}/_TMP_COMMENT_E/g;
  return $s;
}

1;
