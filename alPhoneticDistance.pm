package alPhoneticDistance;
use utf8;
use strict;
use alVariables;
use Exporter;
our @ISA = 'Exporter';
@EXPORT = qw(&phonetic_distance);

our $decrease_weights_beyond_position = 3;


sub _min3 {
  my ($a,$b,$c)=@_;
  return _min($a,_min($b,$c));
}

sub _min {
  my ($a,$b)=@_;
  return $a if $a < $b;
  return $b;
}

sub _max {
  my $a = shift;
  my $b = shift;
  return $a if $a > $b;
  return $b;
}


sub _weight {
  my ($x,$y,$i,$j,$n,$m,$x_is_pg,$y_is_pg,$verbose)=@_;
  my $sim;
  my $easy_deletion = $x_is_pg;
  my $easy_insertion = $y_is_pg;
  if ($x eq '-') {
    $x = $y;
    $y = '-';
    $easy_deletion = $y_is_pg;
    $easy_insertion = $x_is_pg;
  }
  if ($x eq $y) {
    $sim = 0;
  } elsif ($y eq '-') {
    if ($x =~ /^[VaeiouǫęôêywxHʔɣː]+$/) {
      $sim = 0.05;
    } elsif ($x =~ /^[xHʔʕwɣy]$/ && $i <= 1 && $j <= 1) {
      $sim = 0.1;
    } elsif ($x =~ /^[sn]$/ && $i >= $n && $j >= $m) {
      $sim = 0.2;
    } elsif ($x =~ /^[sh]$/ && $i <= 1 && $j <= 1) {
      $sim = 0.2;
    } elsif ($x =~ /^[hs]+$/) {
      $sim = 0.2;
    } elsif ($x =~ /^[hsⁿᵐ]+$/) {
      $sim = 0.2;
    } elsif ($x =~ /^[ɣgG]$/ && $i <= 1 && $j <= 1 && $easy_insertion) {
      $sim = 0.3;
    } elsif ($x =~ /^[dlɫkgGKtTþðďť]$/ && $i <= 1 && $j <= 1 && $easy_deletion) {
      $sim = 0.5;
    } else {
      $sim = 1;
    }
  } else {
    my $z = $x.$y;
    if ($z =~ /^[ⁿn]+$/ || $z =~ /^[ᵐm]+$/) {
      $sim = 0;
    } elsif ($z =~ /^[VaeiouǫęôêywxHʔɣ]+$/) {
      $sim = 0.05;
    } elsif ($z =~ /^[kqQK]+$/) {
      $sim = 0.1;
    } elsif ($z =~ /^[ľj]+$/ || $z =~ /^[ľl]+$/) {
      $sim = 0.1;
    } elsif ($z =~ /^[gGǵḱǴ]+$/) {
      $sim = 0.1;
    } elsif ($z =~ /^[ɫw]+$/ || $z =~ /^[ɫl]+$/) {
      $sim = 0.1;
    } elsif ($z =~ /[kgGǵḱǴqQKɣŋ]/ && $z =~ /[xHʕ]+$/) {
      $sim = 0.2;
    } elsif ($x =~ /^[xHʔɣçʕ]$/) {
      $sim = 0.2;
    } elsif ($z =~ /^[pbPBfṕ]+$/) {
      $sim = 0.2;
    } elsif ($z =~ /^[kgGǵḱǴqQKɣŋ]+$/) {
      $sim = 0.2;
    } elsif ($z =~ /^[sSšžčcźćśþç]+$/) {
      $sim = 0.2;
    } elsif ($z =~ /^[tdTDzþðďť]+$/) {
      $sim = 0.2;
    } elsif ($z =~ /^[nNňŋⁿ]+$/) {
      $sim = 0.2;
    } elsif ($z =~ /^[vuw]+$/) {
      $sim = 0.2;
    } elsif ($z =~ /^[lľɫ]+$/) {
      $sim = 0.2;
    } elsif ($z =~ /^[wbmᵐv]+$/) {
      $sim = 0.3;
    } elsif ($z =~ /^[sžščźćścǵḱǴ]+$/) {
      $sim = 0.3;
    } elsif ($z =~ /^[rřldľɫDď]+$/) {
      $sim = 0.4;
    } elsif ($z =~ /^[mwpbPfṕBᵐ]+$/) {
      $sim = 0.4;
    } elsif ($z =~ /[kgGǵḱǴqQKɣŋ]/ && $z =~ /[sžščźćśc]/) {
      $sim = 0.4;
    } elsif ($z =~ /^C[bcdðfghklmnpqrstvzšśčňľćřźľɫǵḱžþɣṕŋçďťTDSHNGBǴQKCʔPⁿᵐ]$/
	    || $z =~ /^[bcdðfghklmnpqrstvzšśčňľćřźľɫǵḱžþɣṕŋçďťTDSHNGBǴQKCʔPⁿᵐ]C$/) {
      $sim = 0.4;
    } else {
      $sim = 1;
    }
  }
  # decreasing importance while reaching the end of the word after the 3rd char
  print STDERR "\($x,$y,$i,$j,$n,$m,$x_is_pg,$y_is_pg) : $sim" if $verbose == 2;
  if ($i > $decrease_weights_beyond_position && $j > $decrease_weights_beyond_position) {
    $sim *= (_max($m,$n)-_min($i,$j)+1)/(_max($m,$n)-2);
  }
  print STDERR " => $sim " if $verbose == 2;
  return $sim;
}

sub phonetic_distance {

  my ($s,$t,$s_is_pg,$t_is_pg,$verbose)=@_;

  my ($weight1, $weight2, $weight3);

  $s =~ s/n([bcdfghklmpqrstvzšśčňľćřźľɫǵḱžþɣṕŋçďťTDSHNGBǴQKCʔ])/ⁿ\1/g;
  $s =~ s/([bcdfghklmpqrstvzšśčňľćřźľɫǵḱžþɣṕŋçďťTDSHNGBǴQKCʔ])n/\1ⁿ/g;
  $s =~ s/m([bcdfghklmpqrstvzšśčňľćřźľɫǵḱžþɣṕŋçďťTDSHNGBǴQKCʔ])/ᵐ\1/g;
  $s =~ s/([bcdfghklmpqrstvzšśčňľćřźľɫǵḱžþɣṕŋçďťTDSHNGBǴQKCʔ])m/\1ᵐ/g;
  $t =~ s/n([bcdfghklnpqrstvzšśčňľćřźľɫǵḱžþɣṕŋçďťTDSHNGBǴQKCʔ])/ⁿ\1/g;
  $t =~ s/([bcdfghklnpqrstvzšśčňľćřźľɫǵḱžþɣṕŋçďťTDSHNGBǴQKCʔ])n/\1ⁿ/g;
  $t =~ s/m([bcdfghklnpqrstvzšśčňľćřźľɫǵḱžþɣṕŋçďťTDSHNGBǴQKCʔ])/ᵐ\1/g;
  $t =~ s/([bcdfghklnpqrstvzšśčňľćřźľɫǵḱžþɣṕŋçďťTDSHNGBǴQKCʔ])m/\1ᵐ/g;

  my $n=length($s);
  die "<$s>" if $n == 0;
  my $m=length($t);
  die "<$t>" if $m == 0;

  my @d;

  $d[0][0]=0;

  foreach my $i (1 .. $n) {
    $d[$i][0] = $d[$i-1][0]+_weight(substr($s,$i-1,1),'-',$i,0,$n,$m,$s_is_pg,$t_is_pg);
    print STDERR "\$d[$i][0] = $d[$i][0] (".substr($s,$i-1,1)." / -)\n" if $verbose == 2;
  }
  $d[$n+1][0] = $d[$n][0];
  print STDERR "\$d[".($n+1)."][0] = ".$d[$n+1][0]."\n" if $verbose == 2;
  foreach my $j (1 .. $m) {
    $d[0][$j] = $d[0][$j-1]+_weight(substr($t,$j-1,1),'-',0,$j,$n,$m,$s_is_pg,$t_is_pg);
    print STDERR "\$d[0][$j] = $d[0][$j] (- / ".substr($t,$j-1,1).")\n" if $verbose == 2;
  }
  $d[0][$m+1] = $d[0][$m];
  print STDERR "\$d[0][".($m+1)."] = ".$d[0][$m+1]."\n" if $verbose == 2;

  my (%back_i, %back_j);
#  my %back_weight;
  
  foreach my $i (1 .. $n+1) {
    my $s_i;
    if ($i == 0) {
      $s_i = "-";
    } elsif ($i == $n+1) {
      $s_i = "-";
    } else {
      $s_i=substr($s,$i-1,1);
    }
    foreach my $j (1 .. $m+1) {

      my $t_j;
      if ($j == 0) {
	$t_j = "-";
      } elsif ($j == $m+1) {
	$t_j = "-";
      } else {
	$t_j=substr($t,$j-1,1);
      }

      print STDERR $d[$i-1][$j]."+" if $verbose == 2;
      $weight1 = $d[$i-1][$j]+_weight($s_i,'-',$i,$j,$n,$m,$s_is_pg,$t_is_pg,$verbose);
      print STDERR " ---> $weight1\n" if $verbose == 2;
      print STDERR $d[$i][$j-1]."+" if $verbose == 2;
      $weight2 = $d[$i][$j-1]+_weight('-',$t_j,$i,$j,$n,$m,$s_is_pg,$t_is_pg,$verbose);
      print STDERR " ---> $weight2\n" if $verbose == 2;
      print STDERR $d[$i-1][$j-1]."+" if $verbose == 2;
      $weight3 = $d[$i-1][$j-1]+_weight($s_i,$t_j,$i,$j,$n,$m,$s_is_pg,$t_is_pg,$verbose);
      print STDERR " ---> $weight3\n" if $verbose == 2;

      $d[$i][$j]=_min3($weight1, $weight2, $weight3);
      print STDERR "     ---> $d[$i][$j]\n" if $verbose == 2;
      if ($d[$i][$j] == $weight3) {
	$back_i{$i}{$j} = $i-1;
	$back_j{$i}{$j} = $j-1;
#	$back_weight{$i}{$j} = $weight3;
      } elsif ($d[$i][$j] == $weight1) {
	$back_i{$i}{$j} = $i-1;
	$back_j{$i}{$j} = $j;
#	$back_weight{$i}{$j} = $weight1;
      } elsif ($d[$i][$j] == $weight2) {
	$back_i{$i}{$j} = $i;
	$back_j{$i}{$j} = $j-1;
#	$back_weight{$i}{$j} = $weight2;
      }
      print STDERR "\$d[$i][$j] = $d[$i][$j] ($s_i / $t_j)\n\n" if $verbose == 2;

    }
  }
  #  print STDERR $d[$n+1][$m+1]." / sqrt("._min(_plength($s),_plength($t)).")\n";
  if ($verbose) {
    print STDERR "\t-";
    for my $j (1..$m+1) {
      print STDERR "\t".substr($t,$j-1,1);
    }
    print STDERR "-";
    print STDERR "\n";
    for my $i (0..($n+1)) {
      if ($i == 0 || $i == $n+1) {
	print STDERR "-";
      } else {
	print STDERR substr($s,$i-1,1);
      }
      for my $j (0..($m+1)) {
	print STDERR "\t".(int(1000*$d[$i][$j])/1000);
      }
      print STDERR "\n";
    }
  }

  if ($verbose) {
    my $cur_i = $n+1;
    my $cur_j = $m+1;
    my $prev_i;
    my $prev_j;
    my $one_of_the_best_paths;
    while ($cur_i > 0 || $cur_j > 0) {
      $prev_i = $cur_i;
      $prev_j = $cur_j;
      ($cur_i,$cur_j) = ($back_i{$prev_i}{$cur_j},$back_j{$prev_i}{$cur_j});
      if ($prev_i == $cur_i) {
	$one_of_the_best_paths = "(,".substr($t,$cur_j,1).") ".$one_of_the_best_paths;
      } elsif ($prev_j == $cur_j) {
	$one_of_the_best_paths = "(".substr($s,$cur_i,1).",) ".$one_of_the_best_paths;
      } else {
	$one_of_the_best_paths = "(".substr($s,$cur_i,1).",".substr($t,$cur_j,1).") ".$one_of_the_best_paths;
      }
    }
    $one_of_the_best_paths =~ s/ \(,\) $//;
    while ($one_of_the_best_paths =~ s/ \(,([^)])\) \(,([^)]+)\)$/ (,$1$2)/) {}
    while ($one_of_the_best_paths =~ s/ \(([^)]),\) \(([^)]+),\)$/ (,$1$2)/) {}
    while ($one_of_the_best_paths =~ s/,\) \(([^,()]+),\) \(/$1,) (/) {}
    print STDERR "$one_of_the_best_paths\n";
  }
  return $d[$n+1][$m+1]/sqrt(_min(_plength($s),_plength($t)));
}

sub _plength {
  my $s = shift;
  my $l = 0;
  my $pos = 0;
  my $posfactor;
  my $length = length($s);
  while ($s =~ s/^(.)//) {
    $pos++;
    if ($pos > 3) {
      $posfactor = ($length-$pos+1)/($length-2);
    } else {
      $posfactor = 1;
    }
    if ($1 =~ /^[aeiouǫęôêy]$/) {
      $l += 0.1*$posfactor;
    } else {
      $l += $posfactor;
    }
  }
  return $l;
}

1;
