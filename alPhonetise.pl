#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use threads;
use ExtractXLSX;
use alPhonetisation;
use strict;

my $lang = shift || "fr";
my @line;

while (<>) {
  chomp;
  @line = split /( +)/, $_;
  print join "", map {phonetise($_,$lang)} @line;
  print "\n";
}
