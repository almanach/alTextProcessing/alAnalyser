#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

my @proportions;
my $inputfile;

while (1) {
  $_ = shift;
  if (/^$/) {last}
  elsif (/^\d\d?$/) {push @proportions, $_}
  elsif (-r $_) {$inputfile = $_}
  else {die "Invalid argument: '$_'"}
}
die "Please provide 2, 3 or 4 proportions (train/test, train/dev/test or  train/dev/dev2/test)" unless $#proportions >= 1 && $#proportions <= 3;
die "Please provide input file" if $inputfile eq "";
my $ptotal;
for (@proportions) {
  $ptotal += $_;
}
my $filebase = $inputfile;
my $suffix = "";
if ($filebase =~ s/(\.brown)$//) {
  $suffix = $1;
}
open TRAIN, ">$filebase.train$suffix" || die $!;
binmode TRAIN, ":utf8";
open TEST, ">$filebase.test$suffix" || die $!;
binmode TEST, ":utf8";
if ($#proportions >= 2) {
  open DEV, ">$filebase.dev$suffix" || die $!;
  binmode DEV, ":utf8";
  if ($#proportions >= 3) {
    open DEV2, ">$filebase.dev2$suffix" || die $!;
    binmode DEV2, ":utf8";
  }
}
my $r;
open INPUT, "<$inputfile" || die $!;
binmode INPUT, ":utf8";
while (<INPUT>) {
  chomp;
  $r = int(rand($ptotal));
  if ($r < $proportions[0]) {
    print TRAIN $_."\n";
  } elsif ($r < $proportions[0] + $proportions[1]) {
    if ($#proportions == 1) {
      print TEST $_."\n";
    } else {
      print DEV $_."\n";
    }
  } elsif ($#proportions >= 2 && $r < $proportions[0] + $proportions[1] + $proportions[2]) {
    if ($#proportions == 2) {
      print TEST $_."\n";
    } else {
      print DEV2 $_."\n";
    }
  } elsif ($#proportions >= 3 && $r < $proportions[0] + $proportions[1] + $proportions[2] + $proportions[3]) {
    if ($#proportions == 3) {
      print TEST $_."\n";
    } else {
      die
    }
  } else {
    die
  }
}
close TRAIN;
close TEST;
