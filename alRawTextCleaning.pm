package alRawTextCleaning;
use utf8;
use strict;
use Exporter;
use Encode;
our @ISA = 'Exporter';
our @EXPORT = qw(&altok_escape_metacharacters &altok_escape_xml &altok_deescape_xml &altok_interpret_entities);



sub altok_escape_metacharacters {
  my $s = shift;

  die "Escape metacharacters BEFORE escaping XML: $s" if $s =~ / _XML$/;

  # FTB : french treebank
  $s =~ s/_-_/-/g;
  #on échappe les \ et les {}
  $s =~ s/([\\ {}])/\\$1/g;
  
  return $s;
}

sub altok_escape_xml {
  my $s = shift;
  my $do_not_create_XML_special_tokens = shift || 0;
  unless ($do_not_create_XML_special_tokens) {
    $s =~ s/^\s*(<[\!\?]?[\w\.:_-]+(?: .*?)?\/?>(?:.*<\/[\w\.:_-]+>)?)\s*$/{$1} _XML/g;
    $s =~ s/^\s*((?:<\/?[\w\.:_-]+[^>]*\/?>)+)\s*$/{$1} _XML/g;
    $s =~ s/^\s*(<\!--[^>]+>)\s*$/{$1} _XML/g;
    $s =~ s/^\s*(<\/[\w\.:_-]+>)\s*$/{$1} _XML/g;
  }
  $s =~ s/&/&amp;/g;
  $s =~ s/</&lt;/g;
  $s =~ s/>/&gt;/g;
  #TODO BS : REGARDER
  $s =~ s/(?=\\)(\{[^\{}]*)&amp;/$1&/g;
  $s =~ s/(?=\\)(\{[^\{}]*)&lt;/$1</g;
  $s =~ s/(?=\\)(\{[^\{}]*)&gt;/$1>/g;
  unless ($do_not_create_XML_special_tokens) {
    if ($s =~ /_XML/) {
      $s =~ s/\{([^{}]+)\} _XML/"{"._double_whitespaces($1)."} _XML"/ge;
    }
  }
  return $s;
}

sub altok_deescape_xml {
  my $s = shift;
  $s =~ s/&amp;/&/g;
  $s =~ s/&lt;/</g;
  $s =~ s/&gt;/>/g;
  return $s;
}


sub altok_interpret_entities {
  #gérer entités &#
  my $s = shift;
  Encode::_utf8_off($s);
  #pour encodage 8bits
  $s = Encode::decode("iso-8859-1", $s);
  #on récupère le code point de l'entité pour trouver le caractère
  $s =~ s/&#(\d+);/chr($1)/ge;
  $s = Encode::decode("utf-8", $s);
  Encode::_utf8_on($s);
  return $s;
}

sub _double_whitespaces {
  my $s = shift;
  $s =~ s/ /  /g;
  return $s;
}

1;
