#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import regex as re

def altok_escape_metacharacters(s):
	match = re.search(r" _XML$", s)
	assert(match is None), f"Escape metacharacters BEFORE escaping XML: {s}"
	# FTB : french treebank
	s = re.sub(r"_-_", r"-", s)
	#on échappe les \ et les {}
	s = re.sub(r"([\\ {}])", r"\\\1", s)
	return s

def altok_escape_xml(s, do_not_create_XML_special_tokens = 0):
	if do_not_create_XML_special_tokens == 0:
		s = re.sub(r"^\s*(<[\!\?]?[\w\.:_-]+(?: .*?)?\/?>(?:.*<\/[\w\.:_-]+>)?)\s*$", r"{\1} _XML", s)
		s = re.sub(r"^\s*((?:<\/?[\w\.:_-]+[^>]*\/?>)+)\s*$", r"{\1} _XML", s)
		s = re.sub(r"^\s*(<\!--[^>]+>)\s*$", r"{\1} _XML", s)
		s = re.sub(r"^\s*(<\/[\w\.:_-]+>)\s*$", r"{\1} _XML", s)

	s = re.sub(r"&", r"&amp;", s)
	s = re.sub(r"<", r"&lt;", s)
	s = re.sub(r">", r"&gt;", s)
	#TODO BS : REGARDER
	s = re.sub(r"(?=\\)(\{[^\{}]*)&amp;", r"\1&", s)
	s = re.sub(r"(?=\\)(\{[^\{}]*)&lt;", r"\1<", s)
	s = re.sub(r"(?=\\)(\{[^\{}]*)&gt;", r"\1>", s)

	if not do_not_create_XML_special_tokens:
		if re.search(r"_XML", s):
			s = re.sub(r"\{([^{}]+)\} _XML", lambda x : "{" + _double_whitespaces(x.group(1)) + "} _XML", s)
	return s

def altok_interpret_entities(s):
	#gérer entités &#
	#ICI en perl on joue avec le flat utf8 normalement
	#s = s.encode("iso-8859-1").decode("iso-8859-1")
	s = re.sub(r"&#(\d+);", lambda x : chr(int(x.group(1))), s)
	#s = s.encode("utf-8").decode("utf-8")
	return s

def _double_whitespaces(s):
	s = re.sub(r" ", r"  ", s)
	return s


	