#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use strict;

while (<>) {
  s/&#(\d+);/chr($1)/ge;
  print $_;
}
