package alSentenceSplitting;
use utf8;
use strict;
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(&altok_split_sentences);

my $rom1_10 = qr/(?:III?|I?V|VIII?|VI|I?X|I)/o;
my $rom2_10 = qr/(?:III?|I?V|VIII?|VI|I?X)/o;
my $rom10_maxrom = qr/(?:(?:M+(?:C?D)?)?(?:X{1,4}|X?L|LX{1,4}|X?C{1,4}))/o;
my $numROM = qr/(?:$rom10_maxrom?$rom1_10)/o;
my $numROMnotI = qr/(?:$rom10_maxrom?$rom2_10|${rom10_maxrom}I)/o;

my $firstlistnumid = qr/(?:[I1])(?: +\. +(?:$numROM|\d{1,2}|[A-Za-z]))*(?: +\.)?(?= [^0-9])/o;
my $otherlistnumid = qr/(?:$numROMnotI|[2-9]|\d{2})(?: +\. +(?:$numROM|\d{1,2}|[A-Za-z]))*(?: +\.)?(?= [^0-9])/o;
my $listnumid = qr/(?:$numROM|[2-9]|\d{2})(?: +\. +(?:$numROM|\d{1,2}|[A-Za-z]))*(?: +\.)?(?= [^0-9])/o;
my $firstlistnumprefix = qr/(?:^|,  |[;:]|[^\d]\.|<s +type="br">) */o;
my $otherlistnumprefix = qr/(?:^|,  |;|[^\d]\.|<s +type="br">) */o;
my $listnumprefix = qr/(?:^|,  |;|[^\d]\.|<s +type="br">) */o;



sub altok_split_sentences {
  my $s = shift;		# must be already tokenised
  my $lang = shift;
  my $weak_sbound = shift || 0; # 0 = default; 1 = colons and semi-colons are sentence boundaries; 2 = for languages with no uppercase/lowercase distinction, extends case 1 (TODO: disjoin weak_bound and no-uppercase by relying on $lang)
  my $less_lists = shift || 0;
  my $noxml = shift || 0;
  my $allow_sentence_initial_lowercase = shift || 0;
  #nouvelle option pour permettre la segmentation en phrases sur la base des |
  my $split_on_pipes = shift || 0;

  return $s if $s =~ /^\s*$/ || $s =~ /(^| )(_XML|_MS_ANNOTATION|_PAR_BOUND)$/;

  my ($maj, $min, $l, $initialclass);
  if ($lang =~ /^(fr|en|it|es|pt|sv|no|de|da)/) {
    $min=qr/(?:[a-zæœàåâäãéêèëîïøöôùûüÿçóúíáòì])/;
    $maj=qr/(?:[A-ZÀÅÃÉÈÊËÂÄÔÖÛÜÇÓÚÍÁÒØÌÆŒ])/;
    $l=qr/(?:[æœàåâäãéêèëîïöôùûüÿçøóúíáòìa-zA-ZÀÅÃÉÈÊËÂÄÔÖÛÜÇÓÚÍÁÒØÌÆŒ])/;
  } elsif ($lang =~ /^(pl|cs|sk|ro|sl|hr|sr|sc|bn|tr|fa|ckb)$/) {
    $min=qr/(?:[a-záäąćčçďéęěëíĺľłńňóôöŕřśšşťúůüýźż])/;
    $maj=qr/(?:[A-ZÁÄĄĆČÇĎÉĘĚËÍİĹŁĽŃŇÓÔÖŔŘŚŠŞŤÚŮÜÝŹŻ])/;
    $l=qr/(?:[a-záäąćčďéęěëíĺľłńňóôöŕřśšťúůüýźżA-ZÁÄĄĆČÇĎÉĘĚËÍİĹŁĽŃŇÓÔÖŔŘŚŠŞŤÚŮÜÝŹŻ ])/;
  } elsif ($lang =~ /^(ru|uk|bg|bl|kk|bxr)$/) {
    $min=qr/(?:[a-zабвгдежзийклмнопрстуфхцчшщэюяыьёү])/;
    $maj=qr/(?:[A-ZАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЭЮЯЫЬЁҮ])/;
    $l=qr/(?:[a-zабвгдежзийклмнопрстуфхцчшщэюяыьёүA-ZАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЭЮЯЫЬЁҮ ])/;
  } elsif ($lang =~ /^(el)$/) {
    $min=qr/(?:[a-zα-ωάέήίόύώϊϋΐΰ])/;
    $maj=qr/(?:[A-ZΑ-ΩΆΈΉΊΌΎΏΪΫ])/;
    $l=qr/(?:[a-zA-Zα-ωάέήίόύώϊϋΐΰΑ-ΩΆΈΉΊΌΎΏΪΫ ])/;
  } else {
    $min=qr/(?:[a-zæœáäąãćčďéęěëíĺľłńňóôöŕřśšťúůüýźżàåâäãéêèëîïøöôùûüÿçóúíáòì])/;
    $maj=qr/(?:[A-ZÁÄĄÃĆČĎÉĘĚËÍĹŁĽŃŇÓÔÖŔŘŚŠŤÚŮÜÝŹŻÀÅÃÉÈÊËÂÄÔÖÛÜÇÓÚÍÁÒØÌÆŒ])/;
    $l=qr/(?:[a-zæœáäąãćčďéęěëíĺľłńňóôöŕřśšťúůüýźżàåâäãéêèëîïøöôùûüÿçóúíáòìA-ZÆŒÁÄĄÃĆČĎÉĘĚËÍĹŁĽŃŇÓÔÖŔŘŚŠŤÚŮÜÝŹŻÀÅÃÉÈÊËÂÄÔÖÛÜÇÓÚÍÁÒØÌ ])/;
  }
  my $strictmaj = $maj;
  if ($allow_sentence_initial_lowercase) {
    $maj=qr/(?:$maj|$min)/;
  }
  my $initialclass=qr/(?:$maj|[—–])/;
  my $special_split = qr/[\[_¿¡]/;

  $s =~ s/&lt; *br *(?:\/ *)?&gt;/<\/s><s  type=\"br\">/g;


  if ($lang =~ /^(ja|zh|tw)(_|$)/) {
    $s =~ s/([①-⑫●※＊■◆→⇒◇◎★☆〇·•])/<l\/>$1/g;
    $s =~ s/(?<![1-9１-９〇零一二三四五六七八九○十百千萬億兆万亿])( +)(○ +)(?![1-9１-９〇零一二三四五六七八九○十百千萬億兆万亿])/$1<l\/>$2/g;
    $s =~ s/([\（\(] *[1-9１-９] *[\）\)])/<l\/>$1/g;
    $s =~ s/(?<![\（\(A-Za-z] )(?<![0-9１-９A-Za-z])([1-9１-９]+ +[\.\)、\）。，：:])/<l\/>$1/g;
    $s =~ s/(^|。)( +)((?:第 )?[一二] [\.\)、\）。，：:])/$1$2<l\/>$3/g;
  } elsif ($lang =~ /^th(_|$)/) {
    $s =~ s/(^|<br\/?>)( +)(๑๐|๑?[๑๒๓๔๕๖๗๘๙])( \.)/$1$2<\/l>$3 $4/g;
  } elsif ($lang =~ /^km(_|$)/) {
    $s =~ s/([\P{Latin}\)][ ​]*)(-[ ​]*\P{Latin})/$1<\/l>$2/g;
    $s =~ s/([\P{Latin}\)][ ​]*)(\d+ ?[,\-][ ​]*\P{Latin})/$1<\/l>$2/g;
  } else {
    $s =~ s/ 1 : 1 / __PROTECT__1:1__PROTECT__ /g;
    $s =~ s/($firstlistnumprefix)([I1] +[\.\-\­] +$listnumid +[\.\)\]\/\]\-\­])(?= +[^0-9 ])/$1<l\/>$2/go; # [début de ligne] I.2) texte
    $s =~ s/($otherlistnumprefix)((?:$numROMnotI|[2-9]|\d{2}) +[\.\-\­] +$listnumid +[\.\)\]\/\]\-\­])(?= +[^0-9 ])/$1<l\/>$2/go; # [début de ligne] I.2) texte
    $s =~ s/($firstlistnumprefix)([I1] +[\.\-\­] +$listnumid )/$1<l\/>$2/go; # [début de ligne] I.2 texte
    $s =~ s/($otherlistnumprefix)((?:$numROMnotI|[2-9]|\d{2}) +[\.\-\­] +$listnumid )/$1<l\/>$2/go; # [début de ligne] I.2 texte
    $s =~ s/__PROTECT__1:1__PROTECT__/1 : 1/g;
    unless ($less_lists) {
      $s =~ s/($listnumprefix)([\-\­] +$listnumid +[\.\)\]\/\]])(?= +[^0-9 ])/$1<l\/>$2/go; # [début de ligne] - 1) texte
      $s =~ s/($listnumprefix)([\-\­] +$listnumid +[\-\­] )(?= +[^0-9 ])/$1<l\/>$2/go; # [début de ligne] - 1) texte
      $s =~ s/($listnumprefix)($listnumid +[\.\)\]\/\]])(?= +[^0-9 ])/$1<l\/>$2/go; # [début de ligne] 1) texte
      $s =~ s/($listnumprefix)($listnumid +[\-\­] )(?= +[^0-9 ])/$1<l\/>$2/go; # [début de ligne] 1) texte
      if ($s =~ /^ \d [,\.] /) {
	while ($s =~ s/([\.;\!]  )(\d ,)(?= +[^0-9 ])/$1<l\/>$2/o) {} # [début de ligne] 1, texte … 2, texte
      }
      $s =~ s/(\. +)(・)/$1<l\/>$2/g;
    }
  }
  $s =~ s/^( +)<l\/>/$1<s  type=\"li\">/g;
  $s =~ s/([^ ])( +)<l\/>/$1<\/s>$2<s  type=\"li\">/g;
  while ($s =~ s/<l\/>//g) {}
  
  #TODO check, j'ai ajouté un espace entre <s et type="br,li" pour bien detokeniser ensuite
  $s =~ s/<s  type="br"><\/s>( *)<s  type="li">/$1<s  type="br,li">/g;
  
  if ($lang =~ /^(ja|zh)(_|$)/) {
    $s =~ s/ ([！？\!\?。｡\.](?: [！？\!\?。｡\.])*)( +)/ \1<\/s>\2<s>/g;
    $s =~ s/([！？\!\?。｡\.])<\/s>( +)<s>([（）\(\)])/\1\2\3/g;
    if ($s =~ / <s  type="li">\d [。｡\.]<\/s>/) {
      while ($s =~ s/(<s  type="li">\d [。｡\.])<\/s>( +)<s>/\1\2/) {}
    }
    if ($weak_sbound > 0) {
      $s =~ s/ ([：；:;]|)( +)/ \1<\/s>\2<s>/g;
    }
  } elsif ($lang =~ /^km(_|$)/) {
    $s =~ s/ ([។៕])( +)/ \1<\/s>\2<s>/g;
  } elsif ($lang =~ /^th(_|$)/) {
  } else {
    $s =~ s/([…\.:;\?\!])(  +)([\"“”\˝] ${maj}[^\"“”\˝<>]*[\.:;\?\!] [\"“”\˝])(  +)($maj)/$1<\/s>$2<s>$3<\/s>$4<s>$5/g; # detection of sentences entirely surrounded by double quotes
    $s =~ s/([…\.:;\?\!])(  +)([\"“”\˝] ${maj}[^\"“”\˝<>]*[\.:;\?\!] [\"“”\˝])( +)$/$1<\/s>$2<s>$3<\/s>$4/g; # detection of sentences entirely surrounded by double quotes
    $s =~ s/(\.(?: \.)*|…)( +)(\( (?:\. \. \.|[…。]) \))( +)($maj|[\[_\{\.])/$1<\/s>$2<s>$3<\/s>$4<s>$5/g;
    #$s =~ s/([^\.][0-9\} ](?:\.(?: \.)*|[…。]))(  +)($initialclass|$special_split)/$1<\/s>$2<s>$3/g; # STANDARD CASE
    $s =~ s/([^\.0-9][\} ](?:\.(?: \.)*|[…。]))(  +)($initialclass|$special_split)/$1<\/s>$2<s>$3/g; # STANDARD CASE 1
    $s =~ s/([^\.][0-9](?:\.(?: \.)+|[…。]))(  +)($initialclass|$special_split)/$1<\/s>$2<s>$3/g; # STANDARD CASE 2
    $s =~ s/^ ([^\.][0-9]\.)(  +)($initialclass|$special_split)/$1<\/s>$2<s>$3/g; # STANDARD CASE 3
    $s =~ s/([\.;\!\?…]  [0-9]+ \.)(  +)($initialclass|$special_split)/$1<\/s>$2<s>$3/g; # STANDARD CASE 4
    $s =~ s/([^\s>0-9][0-9]+ (?:\.(?: \.)*|[…。]))(  +)($initialclass|$special_split)/$1<\/s>$2<s>$3/g; # STANDARD CASE 5
    $s =~ s/([^\.;\?\!…]  [0-9]+ (?:\.(?: \.)*|[…。]))(  +)($initialclass|$special_split)/$1<\/s>$2<s>$3/g; # STANDARD CASE 5
    $s =~ s/($maj$l+  $maj \.)<\/s>  <s>($maj$l+ )/$1  $2/g; # oversegmentation correction by the standard case (case Prénom I. Nom)
    $s =~ s/\b((?:mr?s?|me?lle|[pd]r|e \. +g) \.)<\/s>(  +)<s>((?:$maj \.  )*$maj$l+ )/$1$2$3/gi; # oversegmentation correction by the standard case (cas Mr. et autres)
    if ($lang eq "pl") {
      $s =~ s/\b((?:np|m ?\. *in) \.)<\/s>(  +)<s>((?:$maj \.  )*$maj$l+ )/$1$2$3/gi; # oversegmentation correction by the standard case (cas np. et autres)
    } elsif ($lang eq "ru") {
      $s =~ s/\b((?:Т \. +е) \.)<\/s>(  +)<s>((?:$maj \.  )*$maj$l+ )/$1$2$3/gi; # oversegmentation correction by the standard case (cas Т.е. et autres)
    } elsif ($lang eq "es") {
      $s =~ s/\b((?:Ej) \.)<\/s>(  +)<s>((?:$maj \.  )*$maj$l+ )/$1$2$3/gi; # oversegmentation correction by the standard case (cas Ej. et autres)
    } elsif ($lang eq "de") {
      $s =~ s/\b((?:bzw|z *\. *b|ggf) \.)<\/s>(  +)<s>((?:$maj \.  )*$maj$l+ )/$1$2$3/gi; # oversegmentation correction by the standard case (cas bzw. et autres)
    } elsif ($lang eq "nl") {
      $s =~ s/\b((?:bv) \.)<\/s>(  +)<s>((?:$maj \.  )*$maj$l+ )/$1$2$3/gi; # oversegmentation correction by the standard case (cas bv. et autres)
    }
    $s =~ s/( $strictmaj \.)<\/s>  <s>($min(?:$l+| \.) )/$1  $2/g; # oversegmentation correction by the standard case in mode 'allow_sentence_initial_lowercase' (examples: A.F.D.S.F.G. est une entreprise connue. | We need to R.S.V.P. a.s.a.p., you know!)
    $s =~ s/( $min \. $min \.)<\/s>  <s>($min(?:$l+| \.) )/$1  $2/g; # oversegmentation correction by the standard case in mode 'allow_sentence_initial_lowercase' (exemple: Please r.s.v.p. a.s.a.p. otherwise who know what will happen.)
    $s =~ s/(  $min \.)<\/s>  <s>($min \.  )/$1  $2/g; # oversegmentation correction by the standard case in mode 'allow_sentence_initial_lowercase' (exemple: e. g.)
    $s =~ s/($l|[\!\?])( +)(\. \. \.)( +)($maj|$special_split)/$1<\/s>$2<s>$3<\/s>$4<s>$5/g; # used to use $l instead of $maj
    $s =~ s/(\.  \.(?: \.)*)( +)($maj|$special_split)/$1<\/s>$2<s>$3/g;

    $s =~ s/(\. (?: \.)+)(  +)($initialclass)/$1<\/s>$2<s>$3/g;	     # attention !!!
    $s =~ s/(\. (?: \.)+)( +)([\[_\{\-\«¿¡])/$1<\/s>$2<s>$3/g;	     # attention !!!
    $s =~ s/([\?\!](?: ?(?: \.)+)?)(  +)($initialclass)/$1<\/s>$2<s>$3/g;   # attention !!!
    $s =~ s/([\?\!](?: ?(?: \.)+)?)( +)([\[_\{\-\«¿¡])/$1<\/s>$2<s>$3/g;   # attention !!!
    $s =~ s/([\.\?\!] +\.(?: \.)+)(  +)/$1<\/s>$2<s>/g;		     # attention
    $s =~ s/([\.\?\!,:])(  )([\-\+\«¿¡])/$1<\/s>$2<s>$3/g;		     # attention
    if ($weak_sbound > 0) {					     # if $weak_sbound, colons are sentence boundaries
      $s =~ s/(:(?: ?(?: \.)+)?)(  )($initialclass)/$1<\/s>$2<s>$3/g;	     # attention !!!
      $s =~ s/(:(?: ?(?: \.)+)?)( +)([\[_\{\-\«¿¡])/$1<\/s>$2<s>$3/g;	     # attention !!!
      $s =~ s/(: +\.(?: \.)+)(  )/$1<\/s>$2<s>/g;			     # attention
    }
  }
  $s =~ s/(?<!TA_TEXTUAL_PONCT|_META_TEXTUAL_GN)(  ?)(\{[^\}]*\} _META_TEXTUAL[A-Z_]+)/<\/s>$1<s type=\"li\">$2/g;	# attention
  if ($lang !~ /^(ja|zh|th|km)$/) {    
    while ($s =~ s/^((?:[^\"“”]*[\"“”\˝][^\"“”]*[\"“”\˝])*[^\"“”]*[\.;\?\!])(  )([\"“”\˝])/$1<\/s>$2<s>$3/g) {}				# attention
    while ($s =~ s/^([^\"“”]*[\"“”\˝](?:[^\"“”]*[\"“”\˝][^\"“”]*\"“”)*[^\"“”]*[\.;\?\!]  [\"“”\˝])(  )/$1<\/s>$2<s>/g) {}				# attention

    if ($weak_sbound > 0) {
      $s =~ s/ (;)( +)/$1;<\/s>$2<s>/g; # if $weak_sbound, semi-colons are sentence boundaries
      $s =~ s/;<\/s>( +<s>[\"“”\˝])<\/s>/; $1<\/s>/g;
      if ($weak_sbound == 2) {
	$s =~ s/ \.(  )/ \.<\/s>$1<s>/g; # cas des langues sans majuscules..
      }
    }
  }

  if ($s =~ /(  - .*?){8,}/) { # à partir de 8 (choisi au plus juste), on va considérer qu'on est face à une liste
    #ajout d'un espace en plus entre s et type
    $s =~ s/  - /<\/s>  <s  type=\"li\">- /g;
  }


  $s =~ s/<s>( *)<\/s>/$1/g;

  $s =~ s/<s[^<>]*>$//;
  $s =~ s/^<\/s>//;
  $s =~ /^ ([^ ])/ || die "ERROR [enqiTokeniser]: Internal error on '$s'";
  $s =~ /([^ ]) $/ || die "ERROR [enqiTokeniser]: Internal error on '$s'";
  $s =~ s/^ ([^ ])/ <s>$1/ unless $s =~ /^ +<s/;
  $s =~ s/([^ ]) $/$1<\/s> / unless $s =~ /<\/s> +$/;

  #ajouts pour gérer quand on a détecté des listes mais que les nombres sont séparés du tiret + gérer après quand on a des br seuls
  $s =~ s/<s>(\d+)<\/s>  <s  type="li">-/<s  type="li">$1  -/g;
  $s =~ s/<s  type="((?:br,)?li)">(\d+)<\/s>  <s  type="li">-/<s  type="$1">$2  -/g;
  $s =~ s/<s  type="br">\s*<\/s>\s*<s  type="li">-/<s  type="br,li">-/g;
  $s =~ s/<s  type="br">\s*<\/s>\s*<s  type="(?:br,)?li">/<s  type="br,li">/g;

  if ($noxml) {
    $s =~ s/<\/s> *<s[^>]*>/\n/gs;
    $s =~ s/^ *<s[^>]*> *//gs;
    $s =~ s/ *<\/s> *$//gs;
  }

  if ($split_on_pipes) {
    $s =~  s/(\w) +\|(?: +\|)* +(\w)/$1.".<\/s>  <s>".uc($2)/ge;
  }

  return $s;
}

1;
