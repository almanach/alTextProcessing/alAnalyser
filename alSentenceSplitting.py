#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import regex as re

#import sys

rom1_10 = re.compile(r"(?:III?|I?V|VIII?|VI|I?X|I)")
rom2_10 = re.compile(r"(?:III?|I?V|VIII?|VI|I?X)")
rom10_maxrom = re.compile(r"(?:(?:M+(?:C?D)?)?(?:X{1,4}|X?L|LX{1,4}|X?C{1,4}))")
numROM = re.compile(rf"(?:{rom10_maxrom.pattern}?{rom1_10.pattern})")
numROMnotI = re.compile(rf"(?:{rom10_maxrom.pattern}?{rom2_10.pattern}|{rom10_maxrom.pattern}I)")

firstlistnumid = re.compile(rf"(?:[I1])(?: +\. +(?:{numROM.pattern}|\d{{1,2}}|[A-Za-z]))*(?: +\.)?(?= [^0-9])")
otherlistnumid = re.compile(rf"(?:{numROMnotI.pattern}|[2-9]|\d{{2}})(?: +\. +(?:{numROM.pattern}|\d{{1,2}}|[A-Za-z]))*(?: +\.)?(?= [^0-9])")
listnumid = re.compile(rf"(?:{numROM.pattern}|[2-9]|\d{{2}})(?: +\. +(?:{numROM.pattern}|\d{{1,2}}|[A-Za-z]))*(?: +\.)?(?= [^0-9])")
firstlistnumprefix = re.compile(r'(?:^|,  |[;:]|[^\d]\.|<s +type="br">) *')
otherlistnumprefix = re.compile(r'(?:^|,  |;|[^\d]\.|<s +type="br">) *')
listnumprefix = re.compile(r'(?:^|,  |;|[^\d]\.|<s +type="br">) *')


def altok_split_sentences(s, lang, weak_sbound = 0, less_lists = 0, noxml = 0, allow_sentence_initial_lowercase = 0, split_on_pipes = 0):
	#nouvelle option pour permettre la segmentation en phrases sur la base des | : split_on_pipes
	#s must be already tokenised
	#weak_sbound: 0 = default; 1 = colons and semi-colons are sentence boundaries; 2 = for languages with no uppercase/lowercase distinction, extends case 1 (TODO: disjoin weak_bound and no-uppercase by relying on lang)
	if (re.search(r"^\s*$", s) or re.search(r"(^| )(_XML|_MS_ANNOTATION|_PAR_BOUND)$", s)):
		return s
	#my ($maj, $min, $l, $initialclass);
	if re.search(r"^(fr|en|it|es|pt|sv|no|de|da)", lang):
		minus = re.compile(r"(?:[a-zæœàåâäãéêèëîïøöôùûüÿçóúíáòì])")
		maj = re.compile(r"(?:[A-ZÀÅÃÉÈÊËÂÄÔÖÛÜÇÓÚÍÁÒØÌÆŒ])")
		l = re.compile(r"(?:[æœàåâäãéêèëîïöôùûüÿçøóúíáòìa-zA-ZÀÅÃÉÈÊËÂÄÔÖÛÜÇÓÚÍÁÒØÌÆŒ])")
	elif re.search(r"^(pl|cs|sk|ro|sl|hr|sr|sc|bn|tr|fa|ckb)$", lang):
		minus = re.compile(r"(?:[a-záäąćčçďéęěëíĺľłńňóôöŕřśšşťúůüýźż])")
		maj = re.compile(r"(?:[A-ZÁÄĄĆČÇĎÉĘĚËÍİĹŁĽŃŇÓÔÖŔŘŚŠŞŤÚŮÜÝŹŻ])")
		l = re.compile(r"(?:[a-záäąćčďéęěëíĺľłńňóôöŕřśšťúůüýźżA-ZÁÄĄĆČÇĎÉĘĚËÍİĹŁĽŃŇÓÔÖŔŘŚŠŞŤÚŮÜÝŹŻ ])")
	elif re.search(r"^(ru|uk|bg|bl|kk|bxr)$", lang):
		minus = re.compile(r"(?:[a-zабвгдежзийклмнопрстуфхцчшщэюяыьёү])")
		maj = re.compile(r"(?:[A-ZАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЭЮЯЫЬЁҮ])")
		l = re.compile(r"(?:[a-zабвгдежзийклмнопрстуфхцчшщэюяыьёүA-ZАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЭЮЯЫЬЁҮ ])")
	elif re.search(r"^(el)$", lang):
		minus = re.compile(r"(?:[a-zα-ωάέήίόύώϊϋΐΰ])")
		maj = re.compile(r"(?:[A-ZΑ-ΩΆΈΉΊΌΎΏΪΫ])")
		l = re.compile(r"(?:[a-zA-Zα-ωάέήίόύώϊϋΐΰΑ-ΩΆΈΉΊΌΎΏΪΫ ])")
	else:
		minus = re.compile(r"(?:[a-zæœáäąãćčďéęěëíĺľłńňóôöŕřśšťúůüýźżàåâäãéêèëîïøöôùûüÿçóúíáòì])")
		maj = re.compile(r"(?:[A-ZÁÄĄÃĆČĎÉĘĚËÍĹŁĽŃŇÓÔÖŔŘŚŠŤÚŮÜÝŹŻÀÅÃÉÈÊËÂÄÔÖÛÜÇÓÚÍÁÒØÌÆŒ])")
		l = re.compile(r"(?:[a-zæœáäąãćčďéęěëíĺľłńňóôöŕřśšťúůüýźżàåâäãéêèëîïøöôùûüÿçóúíáòìA-ZÆŒÁÄĄÃĆČĎÉĘĚËÍĹŁĽŃŇÓÔÖŔŘŚŠŤÚŮÜÝŹŻÀÅÃÉÈÊËÂÄÔÖÛÜÇÓÚÍÁÒØÌ ])")
	strictmaj = maj
	if allow_sentence_initial_lowercase:
		maj = re.compile(rf"(?:{maj.pattern}|{minus.pattern})")
	initialclass = re.compile(rf"(?:{maj.pattern}|[—–])")
	special_split = re.compile(r"[\[_¿¡]")

	s = re.sub(r"&lt; *br *(?:\/ *)?&gt;", r'</s><s  type="br">', s)

	if re.search(r"^(ja|zh|tw)(_|$)", lang):
		s = re.sub(r"([①-⑫●※＊■◆→⇒◇◎★☆〇·•])", r"<l/>\1", s)
		s = re.sub(r"(?<![1-9１-９〇零一二三四五六七八九○十百千萬億兆万亿])( +)(○ +)(?![1-9１-９〇零一二三四五六七八九○十百千萬億兆万亿])", r"\1<l/>\2", s)
		s = re.sub(r"([\（\(] *[1-9１-９] *[\）\)])", r"<l/>\1", s)
		s = re.sub(r"(?<![\（\(A-Za-z] )(?<![0-9１-９A-Za-z])([1-9１-９]+ +[\.\)、\）。，：:])", r"<l/>\1", s)
		s = re.sub(r"(^|。)( +)((?:第 )?[一二] [\.\)、\）。，：:])", r"\1\2<l/>\3", s)
	elif re.search(r"^th(_|$)", lang):
		s = re.sub(r"(^|<br\/?>)( +)(๑๐|๑?[๑๒๓๔๕๖๗๘๙])( \.)", r"\1\2</l>\3 \4", s)
	elif re.search(r"^km(_|$)", lang):
		s = re.sub(r"([\P{Latin}\)][ ​]*)(-[ ​]*\P{Latin})", r"\1</l>\2", s)
		s = re.sub(r"([\P{Latin}\)][ ​]*)(\d+ ?[,\-][ ​]*\P{Latin})", r"\1</l>\2", s)
	else:
		s = re.sub(r" 1 : 1 ", r" __PROTECT__1:1__PROTECT__ ", s)
		s = re.sub(rf"({firstlistnumprefix.pattern})([I1] +[\.\-\­] +{listnumid.pattern} +[\.\)\]\/\]\-\­])(?= +[^0-9 ])", r"\1<l/>\2", s) # [début de ligne] I.2) texte
		s = re.sub(rf"({otherlistnumprefix.pattern})((?:{numROMnotI.pattern}|[2-9]|\d{{2}}) +[\.\-\­] +{listnumid.pattern} +[\.\)\]\/\]\-\­])(?= +[^0-9 ])", r"\1<l/>\2", s) # [début de ligne] I.2) texte
		s = re.sub(rf"({firstlistnumprefix.pattern})([I1] +[\.\-\­] +{listnumid.pattern} )", r"\1<l/>\2", s) # [début de ligne] I.2 texte
		s = re.sub(rf"({otherlistnumprefix.pattern})((?:{numROMnotI.pattern}|[2-9]|\d{{2}}) +[\.\-\­] +{listnumid.pattern} )", r"\1<l/>\2", s) # [début de ligne] I.2 texte
		s = re.sub(r"__PROTECT__1:1__PROTECT__", r"1 : 1", s)

	if not less_lists:
		s = re.sub(rf"({listnumprefix.pattern})([\-\­] +{listnumid.pattern} +[\.\)\]\/\]])(?= +[^0-9 ])", r"\1<l/>\2", s) # [début de ligne] - 1) texte
		s = re.sub(rf"({listnumprefix.pattern})([\-\­] +{listnumid.pattern} +[\-\­] )(?= +[^0-9 ])", r"\1<l/>\2", s) # [début de ligne] - 1) texte
		s = re.sub(rf"({listnumprefix.pattern})({listnumid.pattern} +[\.\)\]\/\]])(?= +[^0-9 ])", r"\1<l/>\2", s) # [début de ligne] 1) texte
		s = re.sub(rf"({listnumprefix.pattern})({listnumid.pattern} +[\-\­] )(?= +[^0-9 ])", r"\1<l/>\2", s) # [début de ligne] 1) texte

		if re.search(r"^ \d [,\.] ", s) is not None:
			match_pattern = re.compile(r"([\.;\!]  )(\d ,)(?= +[^0-9 ])")
			match_check = match_pattern.search(s)
			while match_check is not None:
				s = match_pattern.sub(r"\1<l/>\2", s) # [début de ligne] 1, texte … 2, texte
				match_check = match_pattern.search(s)

		s = re.sub(r"(\. +)(・)", r"\1<l/>\2", s)

	s = re.sub(r"^( +)<l\/>", r'\1<s  type="li">', s)
	s = re.sub(r"([^ ])( +)<l\/>", r'\1</s>\2<s  type="li">', s)
	s = re.sub(r"<l\/>", r"", s)
	s = re.sub(r"<l\/>", r"", s)

	#un ajout d'espace ici en faisant le process de trad
	s = re.sub(r'<s  type="br"></s>( *)<s  type="li">', r'\1<s  type="br,li">', s)

	if re.search(r"^(ja|zh)(_|$)", lang):
		s = re.sub(r" ([！？\!\?。｡\.](?: [！？\!\?。｡\.])*)( +)", r" \1</s>\2<s>", s)
		s = re.sub(r"([！？\!\?。｡\.])<\/s>( +)<s>([（）\(\)])", r"\1\2\3", s)

		if re.search(r' <s  type="li">\d [。｡\.]<\/s>', s) is not None:
			match_pattern = re.compile(r'(<s  type="li">\d [。｡\.])<\/s>( +)<s>')
			match_check = match_pattern.search(s)
			while match_check is not None:
				s = match_pattern.sub(r"\1\2", s)
				match_check = match_pattern.search(s)

		if weak_sbound > 0:
			s = re.sub(r" ([：；:;]|)( +)", r" \1</s>\2<s>", s)
	elif re.search(r"^km(_|$)", lang):
		s = re.sub(r" ([។៕])( +)", r" \1</s>\2<s>", s)
	elif re.search(r"^th(_|$)", lang):
		pass
	else:
		s = re.sub(rf"([…\.:;\?\!])(  +)([\"“”\˝] {maj.pattern}[^\"“”\˝<>]*[\.:;\?\!] [\"“”\˝])(  +)({maj.pattern})", r"\1</s>\2<s>\3</s>\4<s>\5", s) # detection of sentences entirely surrounded by double quotes
		s = re.sub(rf"([…\.:;\?\!])(  +)([\"“”\˝] {maj.pattern}[^\"“”\˝<>]*[\.:;\?\!] [\"“”\˝])( +)$", r"\1</s>\2<s>\3</s>\4", s) # detection of sentences entirely surrounded by double quotes
		#s = re.sub(rf"(\.(?: \.)*|…)( +)(\( (?:\. \. \.|…) \))( +)({maj.pattern}|[\[_{{\.])", r"\1</s>\2<s>\3</s>\4<s>\5", s)
		s = re.sub(rf"(\.(?: \.)*|…)( +)(\( (?:\. \. \.|[…。]) \))( +)({maj.pattern}|[\[_{{\.])", r"\1</s>\2<s>\3</s>\4<s>\5", s)
		#s = re.sub(rf"([^\.][0-9}} ](?:\.(?: \.)*|…))(  +)({initialclass.pattern}|{special_split.pattern})", r"\1</s>\2<s>\3", s) # STANDARD CASE
		
		s = re.sub(rf"([^\.0-9][}} ](?:\.(?: \.)*|[…。]))(  +)({initialclass.pattern}|{special_split.pattern})", r"\1</s>\2<s>\3", s) # STANDARD CASE 1
		s = re.sub(rf"([^\.][0-9](?:\.(?: \.)+|[…。]))(  +)({initialclass.pattern}|{special_split.pattern})", r"\1</s>\2<s>\3", s) # STANDARD CASE 2
		s = re.sub(rf"^ ([^\.][0-9]\.)(  +)({initialclass.pattern}|{special_split.pattern})", r"\1</s>\2<s>\3", s) # STANDARD CASE 3
		s = re.sub(rf"([\.;\!\?…]  [0-9]+ \.)(  +)({initialclass.pattern}|{special_split.pattern})", r"\1</s>\2<s>\3", s) # STANDARD CASE 4
		s = re.sub(rf"([^\s>0-9][0-9]+ (?:\.(?: \.)*|[…。]))(  +)({initialclass.pattern}|{special_split.pattern})", r"\1</s>\2<s>\3", s) # STANDARD CASE 5
		s = re.sub(rf"([^\.;\?\!…]  [0-9]+ (?:\.(?: \.)*|[…。]))(  +)({initialclass.pattern}|{special_split.pattern})", r"\1</s>\2<s>\3", s) # STANDARD CASE 5

		s = re.sub(rf"({maj.pattern}{l.pattern}+  {maj.pattern} \.)</s>  <s>({maj.pattern}{l.pattern}+ )", r"\1  \2", s) # oversegmentation correction by the standard case (case Prénom I. Nom)
		s = re.sub(rf"\b((?:mr?s?|me?lle|[pd]r|e \. +g) \.)</s>(  +)<s>((?:{maj.pattern} \.  )*{maj.pattern}{l.pattern}+ )", r"\1\2\3", s, flags = re.IGNORECASE) # oversegmentation correction by the standard case (cas Mr. et autres)

		if lang == "pl":
			s = re.sub(rf"\b((?:np|m ?\. *in) \.)</s>(  +)<s>((?:{maj.pattern} \.  )*{maj.pattern}{l.pattern}+ )", r"\1\2\3", s, flags = re.IGNORECASE) # oversegmentation correction by the standard case (cas np. et autres)

		elif lang == "ru":
			s = re.sub(rf"\b((?:Т \. +е) \.)</s>(  +)<s>((?:{maj.pattern} \.  )*{maj.pattern}{l.pattern}+ )", r"\1\2\3", s, flags = re.IGNORECASE) # oversegmentation correction by the standard case (cas Т. е. et autres)

		elif lang == "es":
			s = re.sub(rf"\b((?:Ej) \.)</s>(  +)<s>((?:{maj.pattern} \.  )*{maj.pattern}{l.pattern}+ )", r"\1\2\3", s, flags = re.IGNORECASE) # oversegmentation correction by the standard case (cas Ej. et autres)

		elif lang == "de":
			s = re.sub(rf"\b((?:bzw|z *\. *b|ggf) \.)</s>(  +)<s>((?:{maj.pattern} \.  )*{maj.pattern}{l.pattern}+ )", r"\1\2\3", s, flags = re.IGNORECASE) # oversegmentation correction by the standard case (cas bzw. et autres)

		elif lang == "nl":
			s = re.sub(rf"\b((?:bv) \.)</s>(  +)<s>((?:{maj.pattern} \.  )*{maj.pattern}{l.pattern}+ )", r"\1\2\3", s, flags = re.IGNORECASE) # oversegmentation correction by the standard case (cas bv. et autres)

		s = re.sub(rf"( {strictmaj.pattern} \.)</s>  <s>({minus.pattern}(?:{l.pattern}+| \.) )", r"\1  \2", s) # oversegmentation correction by the standard case in mode 'allow_sentence_initial_lowercase' (examples: A.F.D.S.F.G. est une entreprise connue. | We need to R.S.V.P. a.s.a.p., you know!)
		s = re.sub(rf"( {minus.pattern} \. {minus.pattern} \.)</s>  <s>({minus.pattern}(?:{l.pattern}+| \.) )", r"\1  \2", s) # oversegmentation correction by the standard case in mode 'allow_sentence_initial_lowercase' (exemple: Please r.s.v.p. a.s.a.p. otherwise who know what will happen.)
		s = re.sub(rf"( {minus.pattern} \.)</s>  <s>({minus.pattern} \.  )", r"\1  \2", s) # oversegmentation correction by the standard case in mode 'allow_sentence_initial_lowercase' (exemple: e. g.)
		s = re.sub(rf"({l.pattern}|[\!\?])( +)(\. \. \.)( +)({maj.pattern}|{special_split.pattern})", r"\1</s>\2<s>\3</s>\4<s>\5", s) # used to use $l instead of $maj
		s = re.sub(rf"(\.  \.(?: \.)*)( +)({maj.pattern}|{special_split.pattern})", r"\1</s>\2<s>\3", s)

		s = re.sub(rf"(\. (?: \.)+)(  +)({initialclass.pattern})", r"\1</s>\2<s>\3", s) # attention !!!
		s = re.sub(r"(\. (?: \.)+)( +)([\[_\{\-\«¿¡])", r"\1</s>\2<s>\3", s)  # attention !!!
		s = re.sub(rf"([\?\!](?: ?(?: \.)+)?)(  +)({initialclass.pattern})", r"\1</s>\2<s>\3", s) # attention !!!
		s = re.sub(r"([\?\!](?: ?(?: \.)+)?)( +)([\[_\{\-\«¿¡])", r"\1</s>\2<s>\3", s) # attention !!!
		s = re.sub(r"([\.\?\!] +\.(?: \.)+)(  +)", r"\1</s>\2<s>", s) # attention
		s = re.sub(r"([\.\?\!,:])(  )([\-\+\«¿¡])", r"\1</s>\2<s>\3", s) # attention

		if weak_sbound > 0: # if $weak_sbound, colons are sentence boundaries
			s = re.sub(rf"(:(?: ?(?: \.)+)?)(  )({initialclass.pattern})", r"\1</s>\2<s>\3", s) # attention !!!
			s = re.sub(r"(:(?: ?(?: \.)+)?)( +)([\[_\{\-\«¿¡])", r"\1</s>\2<s>\3", s) # attention !!!
			s = re.sub(r"(: +\.(?: \.)+)(  )", r"\1</s>\2<s>", s) # attention

	s = re.sub(r"(?<!TA_TEXTUAL_PONCT|_META_TEXTUAL_GN)(  ?)(\{[^\}]*\} _META_TEXTUAL[A-Z_]+)", r'</s>\1<s type="li">\2', s) # attention

	if re.search(r"^(ja|zh|th|km)$", lang) is None:
		match_pattern = re.compile(r"^((?:[^\"“”]*[\"“”\˝][^\"“”]*[\"“”\˝])*[^\"“”]*[\.;\?\!])(  )([\"“”\˝])")
		match_check = match_pattern.search(s)
		while match_check is not None: # attention
			s = match_pattern.sub(r"\1</s>\2<s>\3", s)
			match_check = match_pattern.search(s)

		match_pattern2 = re.compile(r"^([^\"“”]*[\"“”\˝](?:[^\"“”]*[\"“”\˝][^\"“”]*\"“”)*[^\"“”]*[\.;\?\!]  [\"“”\˝])(  )")
		match_check2 = match_pattern2.search(s)
		while match_check2 is not None: # attention
			s = match_pattern2.sub(r"\1</s>\2<s>", s)
			match_check2 = match_pattern2.search(s)

		if weak_sbound > 0:
			s = re.sub(r" (;)( +)", r"\1;</s>\2<s>", s) # if $weak_sbound, semi-colons are sentence boundaries
			s = re.sub(r";</s>( +<s>[\"“”\˝])</s>", r"; \1</s>", s)
			if weak_sbound == 2:
				s = re.sub(r" \.(  )", r" \.</s>\1<s>", s) # cas des langues sans majuscules..

	if re.search(r"(  - .*?){8,}", s): # à partir de 8 (choisi au plus juste), on va considérer qu'on est face à une liste
		#ajout d'un espace en plus entre s et type
		s = re.sub(r"  - ", r'</s>  <s  type="li">- ', s)

	s = re.sub(r"<s>( *)</s>", r"\1", s)
	s = re.sub(r"<s[^<>]*>$", r"", s)
	s = re.sub(r"^</s>", r"", s)

	match_or_die = re.search(r"^ ([^ ])", s)
	assert(match_or_die is not None), f"ERROR [enqiTokeniser]: Internal error on '{s}'"

	match_or_die2 = re.search(r"([^ ]) $", s)
	assert(match_or_die2 is not None), f"ERROR [enqiTokeniser]: Internal error on '{s}'"

	if not re.search(r"^ +<s", s):
		s = re.sub(r"^ ([^ ])", r" <s>\1", s)
	if not re.search(r"</s> +$", s):
		s = re.sub(r"([^ ]) $", r"\1</s> ", s)
	
	#ajouts pour gérer quand on a détecté des listes mais que les nombres sont séparés du tiret + gérer après quand on a des br seuls
	s = re.sub(r'<s>(\d+)<\/s>  <s  type="li">-', r'<s  type="li">\1  -', s)
	s = re.sub(r'<s  type="((?:br,)?li)">(\d+)<\/s>  <s  type="li">-', r'<s  type="\1">\2  -', s)
	s = re.sub(r'<s  type="br">\s*<\/s>\s*<s  type="li">-', r'<s  type="br,li">-', s)
	s = re.sub(r'<s  type="br">\s*<\/s>\s*<s  type="(?:br,)?li">', r'<s  type="br,li">', s)

	if noxml:
		s = re.sub(r"</s> *<s[^>]*>", r"\n", s, flags = re.DOTALL)
		s = re.sub(r"^ *<s[^>]*> *", r"", s, flags = re.DOTALL)
		s = re.sub(r" *</s> *$", r"", s, flags = re.DOTALL)

	if split_on_pipes:
		s = re.sub(r"(\w) +\|(?: +\|)* +(\w)", lambda x : x.group(1) + ".</s>  <s>" + x.group(2).upper(), s)

	return s

