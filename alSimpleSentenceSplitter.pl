#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use threads;
use alRawTextCleaning;
use alURL;
use alEmail;
use alTokenisation;
use alCPunct;
use alNormalisation;
use alSmiley;
use alSentenceSplitting;
use strict;

my $lang = "";
my $ell = 1;
my $orig_token_mode = 3;
my $split_sentences = 0;
my $verbose = 0;

while (1) {
  $_ = shift;
  if (/^$/) {last}
  elsif (/^-l$/) {$lang = shift || die "Option '-l' must be followed by a language code"}
  else {die "Unknown option '$_'"}
}

while (<>) {
  chomp;
  my $l = $lang;
  s/ / /g;
  s/  +/ /g;
  $_ = altok_escape_metacharacters($_);
  $_ = altok_tokenise($_, $l);
  $_ = altok_normalise($_, $l);
  $_ = altok_URL($_, $l);
  $_ = altok_smiley($_);
  $_ = altok_remove_embedded_contents($_);
  $_ = altok_detokenise_multiple_punctuations($_);
  $_ = altok_remove_embedded_contents($_);
  $_ = altok_retokenise_lexical_punctuations($_, $l);
  $_ = altok_remove_embedded_contents($_);
  $_ = altok_detokenise_entity_content($_, 1);
  $_ = altok_remove_embedded_contents($_);
  $_ = altok_restore_original_tokens($_, 1);
  $_ = altok_split_sentences($_,$l,0,1,1,1);
  $_ = altok_detokenise($_, $l,0,1);
  print $_."\n";
}
