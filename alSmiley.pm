package alSmiley;
use utf8;
use strict;
use alVariables;
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(&altok_smiley);

sub altok_smiley {
  my $s = shift;
  my $fewer_smileys = shift || 0;
  
  if ($fewer_smileys) {
    $s =~ s/ ([:;]) ([-=]) ([()]) ( |$)/ $1$2$3 $4/g;
    $s =~ s/ ([:;]) ([()]) ( |$)/ $1$2 $3/g;
  } else {
    # reconnaissance
    # (texte ;-))
    $s .= "  ";
    $s =~ s/(?<= )([;:=] *(?:' +)?(?:-(?: -)* +)?(?:(?:\(|-LRB-)(?: +(?:\(|-LRB-))*|(?:\)|-RRB-)(?: +(?:\)|-RRB-))*|\[|\]|[DdPpOoSs]|\$+))(?= [ \!])/{$1} _SMILEY/go;
    $s =~ s/(?<= )(x (?:(?:\)|-RRB-)(?: +(?:\)|-RRB-))*))(?= [ \!])/{$1} _SMILEY/go;
    $s =~ s/((?:^|[^\d]) )(: 3)(?= [ \!])/$1\{$2\} _SMILEY/go;
    $s =~ s/(?<=  )([xX][dD]|wsh|mdr|MDR|ptdr+|PTDR+|-(?: -)+ ?'|(?:<|&lt;) 3|(?:>|&gt;)(?: \\_)+ (?:<|&lt;)|lol+|- \\_(?: \\_)* -|\\\\ o+ \/|[Oo] \\_ [Oo]|\^(?: \\_(?: \\_)*)? \^(?: \^)*(?: ['"]*)?)(?=  )/{$1} _SMILEY/go;
    $s =~ s/(?<=  )([\&\@\#] ?[\!\?\&\@\#]+)(?=  )/{$1} _SMILEY/g;
    $s =~ s/([\!\?;]  +)([\!\?](?: [\!\?])+)(?=  )/$1\{$2\} _SMILEY/g;
    $s =~ s/(?<= )([\!\?](?: [\!\?])* [\&\@\#] ?(?:[\!\?\&\@\#](?: [\!\?\&\@\#])*)?)(?= )/{$1} _SMILEY/g;
    $s =~ s/  $//g || die;
    
    # correction de sur-reconnaissance
    $s =~ s/^ *(\([^\)]*)\{($comment_content\))} _SMILEY([ \.]*)$/$1$2$3/go;
    $s =~ s/\{([^}]*\[)} _SMILEY( (?:[^:]|{$comment_content})*\])/$1$2/go;
    $s =~ s/\{([^}]*\()} _SMILEY( (?:[^:]|{$comment_content})*\))/$1$2/go;
    
    # gestion des cas où un commentaire existait déjà (on est sûrs que les {...} _SMILEY internes ne sont pas contre un { ou un })
    while (s/\{($comment_content)\{($comment_content)} *_SMILEY($comment_content)}/{$1$2$3}/g) {}
    $s =~ s/(\{$comment_content}) *\{$comment_content} *_SMILEY/$1 _SMILEY/g;

    # overdetection
    $s =~ s/([^ ]) \{([^{}]+  [^{}]+)\} _SMILEY/$1 $2/g;
  }

  return $s;
}

1;
