#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import alVariables

import regex as re

def altok_smiley(s, fewer_smileys = 0):
	if fewer_smileys:
		s = re.sub(r" ([:;]) ([-=]) ([()]) ( |$)", r" \1\2\3 \4", s)
		s = re.sub(r" ([:;]) ([()]) ( |$)", r" \1\2 \3", s)
	else:
		# reconnaissance
		# (texte ;-))
		s += "  "
		s = re.sub(r"(?<= )([;:=] *(?:' +)?(?:-(?: -)* +)?(?:(?:\(|-LRB-)(?: +(?:\(|-LRB-))*|(?:\)|-RRB-)(?: +(?:\)|-RRB-))*|\[|\]|[DdPpOoSs]|\$+))(?= [ \!])", r"{\1} _SMILEY", s)
		s = re.sub(r"(?<= )(x (?:(?:\)|-RRB-)(?: +(?:\)|-RRB-))*))(?= [ \!])", r"{\1} _SMILEY", s)
		s = re.sub(r"((?:^|[^\d]) )(: 3)(?= [ \!])", r"\1{\2} _SMILEY", s)
		s = re.sub(r"(?<=  )([xX][dD]|wsh|mdr|MDR|ptdr+|PTDR+|-(?: -)+ ?'|(?:<|&lt;) 3|(?:>|&gt;)(?: \\_)+ (?:<|&lt;)|lol+|- \\_(?: \\_)* -|\\\\ o+ \/|[Oo] \\_ [Oo]|\^(?: \\_(?: \\_)*)? \^(?: \^)*(?: ['\"]*)?)(?=  )", r"{\1} _SMILEY", s)
		s = re.sub(r"(?<=  )([\&\@\#] ?[\!\?\&\@\#]+)(?=  )", r"{\1} _SMILEY", s)
		s = re.sub(r"([\!\?;]  +)([\!\?](?: [\!\?])+)(?=  )", r"\1{\2} _SMILEY", s)
		s = re.sub(r"(?<= )([\!\?](?: [\!\?])* [\&\@\#] ?(?:[\!\?\&\@\#](?: [\!\?\&\@\#])*)?)(?= )", r"{\1} _SMILEY", s)
		die_if_not_match = re.search(r"  $", s)
		#on die si on peut pas faire le sub
		assert(die_if_not_match is not None), f""
		s = re.sub(r"  $", r"", s)

		# correction de sur-reconnaissance
		s = re.sub(rf"^ *(\([^\)]*){{({alVariables.comment_content.pattern}\))}} _SMILEY([ \.]*)$", r"\1\2\3", s)
		s = re.sub(rf"{{([^}}]*\[)}} _SMILEY( (?:[^:]|{{{alVariables.comment_content.pattern}}})*\])", r"\1\2", s)
		s = re.sub(rf"{{([^}}]*\()}} _SMILEY( (?:[^:]|{{{alVariables.comment_content.pattern}}})*\))", r"\1\2", s)

		# gestion des cas où un commentaire existait déjà (on est sûrs que les {...} _SMILEY internes ne sont pas contre un { ou un })
		check_pattern = re.compile(rf"{{({alVariables.comment_content.pattern}){{({alVariables.comment_content.pattern})}} *_SMILEY({alVariables.comment_content.pattern})}}")
		pattern_match = check_pattern.search(s)
		while pattern_match is not None:
			s = check_pattern.sub(r"{\1\2\3}")
			pattern_match = check_pattern.search(s)

		s = re.sub(rf"({{{alVariables.comment_content.pattern}}}) *{{{alVariables.comment_content.pattern}}} *_SMILEY", r"\1 _SMILEY", s)

		# overdetection
		s = re.sub(r"([^ ]) {([^{}]+  [^{}]+)} _SMILEY", r"\1 \2", s)
	return s

