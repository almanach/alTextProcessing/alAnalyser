#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use strict;

my ($w,$sp,$comment_content,$i,$output);

while (<>) {
  chomp;
  next if /^$/;
  s/^\s+//;
  s/\s+$//;
  s/&lt;/</g;
  s/&gt;/>/g;
  s/&amp;/&/g;
#  s/\\(.)/\1/g;
  $i = 0;
  $output = $_;
  $output =~ s/ ( *)/\1/g;
  $output = "# text = ".$output."\n";
  while (s/^(?:{([^{}]*)} )?([\S ]+)( +|$)//) {
    $comment_content = $1;
    $w = $2;
    $sp = $3;
    if ($comment_content ne "") {
      $w = $comment_content;
      $w =~ s/ ( *)/\1/g;
    }
    if ($w =~ / /) {
      my $n = ($w=~s/ / /g);
      die if $n == 0;
      $output .= ($i+1);
      $output .= "-";
      $output .= ($i+1+$n);
      $output .= "\t$w\t_\t_\t_\t_\t_\t_\t_\t".($sp eq " " ? "SpaceAfter=No" : "_")."\n";
    }
    for (split / /, $w) {
      $i++;
      s/ / /g;
      $output .= "$i\t$_\t_\t_\t_\t_\t".($i == 1 ? "0" : "1")."\t_\t_\t".($sp eq " " ? "SpaceAfter=No" : "_")."\n";
    }
  }
  $output .= "\n";
  print $output;
}
