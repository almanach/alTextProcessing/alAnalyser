#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

$|=1;

while (<>) {
  chomp;
  s/ ( *)/$1/g;
  s/ / /g;
  print "$_\n";
}
