package alTokenisation;
use utf8;
use strict;
use alVariables;
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(&altok_pretokenise &altok_tokenise &altok_detokenise &altok_remove_embedded_contents &altok_remove_original_tokens &altok_partly_remove_entities &altok_remove_entities &altok_restore_original_tokens &altok_detokenise_entity_content);

sub altok_pretokenise {
  my $s = shift;
  my $lang = shift;
  return $s if $s =~ / _XML$/;
  
  $s =~ s/ /#_#_#/g; # protection of input spaces
  if ($lang =~ /^(zh|ja|th)/
      || ($lang =~ /^(ko)/ && $s =~ /[^ ]{7}/)) {
#    $s =~ s/(\\?[^A-Za-z0-9 ])/ \1 /g; # replaced by the two following lines, as this line does not work on old perl versions
    $s =~ s/([^A-Za-z0-9 ])/ \1 /g;
    $s =~ s/\\ +(.)/\\\1/g;
  } else {
    $s =~ s/($alPunctuation)/ $1 /g;
    $s =~ s/(\p{Han})/ $1 /g;
  }
#  $s =~ s/  +/ /g;
  while ($s =~ s/  / /g) {}
  $s =~ s/ # _ # _ # /  /g; # restoring input spaces
  $s =~ s/# _ # _ # / /g; # restoring input spaces
  $s =~ s/^ */ /;
  $s =~ s/ *$/ /;
  #negative lookahead, un - pas suivi par un espace
  $s =~ s/ -(?! )/ - /g;
  $s =~ s/< br >/<br>/g;
  $s =~ s/& gt ;/&gt;/g;
  $s =~ s/& lt ;/&lt;/g;
  $s =~ s/& amp ;/&amp;/g;
  #TRAD : ces deux lignes n'étaient pas là...
  $s =~ s/& apos ;/&apos;/g;
  $s =~ s/& quot ;/&quot;/g;
  $s =~ s/^ */ /g;
  return $s;
}

sub altok_tokenise {
  my $s = shift;
  my $lang = shift;

  return $s if $s =~ / _XML$/;

  my ($lq,$rq_no_s,$sq);

  if ($lang =~ /(?:am|tig?|gez)/) { # Alphasyllabaire guèze: amharique, tigrigna, tigré, guèze
    s/፡/ /g;
  }
  if ($lang !~ /^(ja|zh|tw|th|km)/) {                               # si on peut tokeniser soi-même...
    $_ = $s;
    s/(?<=[^\.])((?: \.)+)(?=    )/" "._altok_remove_spaces($1)/ge; # si suivi de deux blancs (ou TABs) ou plus, point+ est un token

    if ($lang =~ /^(fr|es|it)$/) {
      if ($lang eq "it") {
	s/ e '(?= [ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.])/ e'/g;
	s/(?<=[ \/\\:;\"“”\+-] )([A-Za]?[a-z]+(?:[ielsrtn]t|[eirdavtp]r|ci|al|ap|of|ol|ag))a '(?= [ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.])/$1a'/g;
	s/(?<=[ \/\\:;\"“”\+-] )([A-Za]?[a-z]+(?:pi|n[dt]|bl|iv|u[ld]|gi|ab))u '(?= [ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.])/$1u'/g;
	s/(?<=[ \/\\:;\"“”\+-] )([A-Za]?[a-z]+(?:[eubfvpgmsdczlntir]))o '(?= [ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.])/$1o'/g;
      }
      # en français, espagnol et italien, les autres guillemets sont détachés
      s/(?<=\s)\' ([0-9]{2})(?=[ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.])/'$1/g;
    } elsif ($lang eq "en") {
      s/([a-z]) ' ([a-z])/\1__APOS__\2/g;
      $lq = s/(?<=[ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.])(['`])(?=[^ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.])/\1/g;
      $rq_no_s = s/(?<=[^ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.Ss])'(?=[ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.])/'/g;
      $sq = s/(?<=[Ss])\'(?=[ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.])/'/g;
      if ($sq == 0 && $lq == 0 && $rq_no_s == 0) {
      } elsif ($sq == 0 && $lq == $rq_no_s) {
	s/(?<=[ \(\[] )(['\`]) (?!em )([^ '](?:[^'])*?[^ 'sS]) ' (?=[ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.])/ ' \2 ' /g; # les apostrophes peuvent servir à quoter... WAS {\1} ` $2 '
      } elsif ($sq == 1 && $lq == $rq_no_s) {
	s/(?<=[ \(\[] )(['\`]) (?!em )([^ '](?:[^']|[sS] ')*?[^ 'sS]) ' (?=[ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.])/ ' \2 ' /g; # les apostrophes peuvent servir à quoter... WAS {\1} ` $2 '
      } else {
	$_ = reverse($_);
	s/(?<=[ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.]) ' (?!em )([^ '](?:[^']|' [sS])*?[^ '])(['\`])(?=[ \(\[])/ ' $1 ` /g; # les apostrophes peuvent servir à quoter... WAS  ' \1 ` \}\2\{ 
	$_ = reverse($_);
      }
      s/__APOS__/'/g;
    } elsif ($lang !~ /^(ckb|fa)/) {
      s/[ \(\[] ' ([^ '][^']*?[^ ']) ' (?=[ ,;?\!:\"“”\)\(\*\#<>\[\]\%\/\\\=\+\«\»—–\˝\&\`\.])/ __APOSQUOTE__ \1 __APOSQUOTE__ /g; # les apostrophes peuvent servir à quoter...
      s/(?<! ) '/'/g;
      s/' (?! )/'/g;
      s/__APOSQUOTE__/'/g;
    }

    if ($lang eq "fr") {
      s/ et \/ ou / et\/ou /g; # cas particulier: cas particulier pour et/ou
    } elsif ($lang eq "en") {
      s/ and \/ or / and\/or /g; # cas particulier: cas particulier pour and/or
    }

    # les entnom sont des tokens
    s/\} *(_[A-Za-z_]*[A-Za-z](?:_[^_]+_)?)(?>! )/"} $1".space_unless_is_SPECWORD($1)/ge;

    # POINT après une minuscule
    s/(Mrs?|Sgt) \. /$1. /g;
    s/($alLowercase) (\.)  (\.)/$1$2  $3/g; # avant un autre point il faut un blanc entre les deux
    s/(<!$alUppercase)($alUppercase) (\.)  ($alUppercase)/$1$2  $3/g; # les séquences $alUppercase$alUppercase. $alUppercase font du point une ponct (très risqué)
    
    # POINT après une majuscule
    s/\b($alUppercase) (\.) ($alUppercase$alLowercase)/$1$2 $3/g; # insertion d'un blanc entre maj-point et maj-min
    s/(<!$alUppercase)($alUppercase{1,2}) (\.)/$1$2/g; # 3 majuscules de suite puis point -> le point est une ponct
    s/(\.) ($alUppercase{1,2})/$1$2/g; # point puis 3 majuscules de suite -> le point est une ponct
    
    # POINT après un chiffre
    s/(\d) (\.+) (\d)/$1$2$3/g; # chiffre point non-chiffre -> le point est une ponct
    
    # TIRETS et slashes
    s/- ([LR][RS]B) -/-$1-/g;
    s/(\d) (\-) (\D)/$1$2$3/g; # le tiret entre 2 chiffres est une ponct
    s/\. \-/\.-/g; # le tiret après une ponct autre que point en est séparé
    s/\- \./-./g; # le tiret avant une ponct autre que point en est séparé
  }

  # Recollages particuliers
  s/(?<=[^\}]) (R \& [Dd]) / {$1} R\&D /go;
  if ($lang eq "fr") {      # ATTENTION : blanc non convertis en \s
    s/(?<=[^\}]) (C \. N \. R \. S \.) / {$1} C.N.R.S. /go;
    s/(?<=[^\}]) (S \. A \. R \. L \.) / {$1} S.A.R.L. /go;
    s/(?<=[^\}]) (S \. A \.) / {$1} S.A. /go;
    s/(?<=[^\}]) M \. / M. /go;
    s/(?<=[^\}]) ([tT][eéEÉ][Ll]) \. / $1. /go;
    s/(?<=[^\}]) \+ \+ / ++ /go;
    s/(?<=[^\}]) \+ \/ \- / +\/- /go;
  }
  s/(?<=[^\}]) ([mpd]r(?:of)?) \. / $1. /gio;

  if ($lang eq "fr") {
    # hyphenated suffixes
    # doit-elle => doit -elle
    # ira-t-elle => ira -t-elle
    # ira - t -elle => ira => -t-elle
    s/ (?:(-) (t ))?(-) (ce|elles?|ils?|en|on|je|la|les?|leur|lui|[mt]oi|[vn]ous|tu|y)(?![a-zA-Z0-9éèêîôûëïüäù])/ $1$2 $3$4/go ;
    # donne-m'en => donne -m'en
    s/ (?:(-) (t ))?(-) ([mlt]) (') / $1$2$3$4$5 /go ;
    s/ (-) ?(née?s?|cl(?:é|ef)s?|ci|là)(?![a-zA-Z0-9éèêîôûëïüäù])/ $1$2/go ;
    # hyphenated prefixes
    while (s/(?<=[^\}]) (qu|lorsqu|puisqu|quelqu|quoiqu|\-?[dlnmtcjs]) (')(?=[^ ])/ $1$2 /goi) {}
    # ATTENTION:  normalement, ce travail est fait plus tard, par text2dag. Ceci ne devrait être utilisé que si l'on utilise sxpipe comme tokenizer pur (i.e., sans faire de vraie différence entre token et forme)
    if ($lang eq "fr") {
      # hyphenated prefixes
      # anti-bush => anti- bush
      # franco-canadien => franco- canadien
      s/ (anti|non|o|ex|pro) (-) / $1$2 /igo ;
    }
  } elsif ($lang eq "en") {
    s/(  - )(\w(?:\w| ?\- ?)+\w)( -  )/"$1"._altok_remove_spaces($2)."$3"/ge;
    s/(?<=[^\}]) (\w{3,}) ?(-) ?(free)/ $1$2$3/gi;
    s/(?<=[^\}]) (short|medium|long) ?(-) ?(term)/ $1$2$3/gi;
    s/(?<=[^\}]) (half|first|second) ?(-) ?(mixed)/ $1$2$3/gi;
    s/(?<=[^\}]) ([a-z]{3,}ty) ?(-) ?(one|two|three|four|five|six|seven|eight|nine) / $1$2$3 /gi;
    s/ (_NUM|[1-9][0-9]*) ?(-) ?((?:week|year|day)s?) ?(-) ?(old) / $1$2$3$4$5 /gi;
    s/(?<=[^\}]) (one|two|three|four|five|six|seven|eight|nine|ten|eleven|twelve|[a-z]+teen|[a-z]+ty) (-) ((?:week|year|day)s?)(-) ?(old) / $1$2$3$4$5 /gi;
    s/ (_NUM|[1-9][0-9]*) ?(-) ?(week|year|day|sided) / $1$2$3 /gi;
    s/(?<=[^\}]) (one|two|three|four|five|six|seven|eight|nine|ten|eleven|twelve|[a-z]+teen|[a-z]+ty|pay-by-the|last|next) ?(-) ?(week|year|minute|hour|second|day) / $1$2$3 /gi;
    s/(?<=[^\}]) (lop|blind) ?(-) ?(sided) / $1$2$3 /gi;
    s/(?<=[^\}]) (square|ill|odd|round) ?(-) ?(shaped) / $1$2$3 /gi;
    s/(?<=[^\}]) (good) (-) (looking) / $1$2$3 /gi;
    s/(?<=[^\}]) ([Iia]|' m|[A-Za-z](?:[A-Za-z]| ' )+) (-) /" "._altok_remove_spaces($1).$2." "/ge;
    s/ (what|that|nang|wait|with|I) (-) (\1) (-) (\1) (-) / $1$2$3$4$5$6 /gi;
    s/ (what|that|nang|wait|with|I) (-) (\1) (-) / $1$2$3$4 /gi;
    s/ ((?:[a-zA-Z']+(?: - ))the(?: - [a-zA-Z']+)+) /" "._altok_remove_spaces($1)." "/gei;
  }

  if ($lang eq "en") {
    #preliminary corrections
    s/(?<=[^\}]) ((?:some|every|any|no)(?:one|body))(s) (')(?=  )/ $1 $2$3/goi; # somebodys' > somebody's > somebody 's (and similar cases)
    #processing
    s/(?<=[^\}]) ([cC][aA])([Nn]'[tT]) / $1 $2 /go; # can't > can n't
    s/(?<=[^\}]) (WO)(N't) / $1 $2 /goi; # (same)
    s/(?<=[^\}]) (wo)(n't) / $1 $2 /goi; # (same)
    s/(?<=[^\}]) (SHA)(N't) / $1 $2 /goi; # (same)
    s/(?<=[^\}]) (sha)(n't) / $1 $2 /goi; # (same)
    s/(?<=[^\}]) ([^_ {}][^ {}]+)(n't) / $1 $2 /goi;# <anythingElse>n't > <anythingElse> n't

    s/(' (?:ll|[rv]e|d))(just)/$1 $2/goi; # <anythingElse>'lljust > <anythingElse>'ll just [the "'ll" part will be dealt with later] (same with 're, 've, 'd)
    s/ ([Ii]) (') ([mMdD]|[lL][lL]|[vV][eE]) / $1 $2$3 /go; # ito be corrected into I
    s/ (you|he|she|we|they|there|this|that|it|who|how|what|where|why|when|hell|(?:no|any|some|every)(?:body|one)|people|alibi|guy|father|many|money)(')(d|ll) / $1 $2$3 /goi; # <adequate_token>'ll > <adequate_token> 'll (same with 'd)
    s/ (they|what|how|where|there|why|who|you|we)(')(re) / $1 $2$3 /goi; # <adequate_token>'re > <adequate_token> 're
    s/ ([îLl])(')(d|ll|ve) / $1 $2$3 /go; # to be corrected into I
    s/ ([Yy]öu|[yuû]|[Yy][eu]|[Yy]our)(')([vr]e|ll|d) / $1 $2$3 /go; # to be corrected into you
    s/ ([îL]t)(')(d|ll) / $1 $2$3 /go; # ît and Lt should be corrected into it
    s/(?<! ) (n't|'d)(')(ve) / $1 $2$3 /goi; # n't've > n't 've (for cases when already detached)
    s/ (i|[a-z]{2,})(')(ve) / $1 $2$3 /goi;
    s/ (if|think|thought|something|kn[oe]w|what|but|that|like|believe)(you|I) ?(')(d|[rv]e) / $1 $2 $3$4 /goi;
    s/(?<=[^\}]) ([^ {}]*[^ s_{}])(')([sS])(?=  )/ $1 $2$3/go;
    s/(?<=[^\}]) ([^ _{}][^ {}]*s)'(?=[a-z] )/ $1 __APOS__/goi;
    s/(?<=[^\}]) (jes|las) '(?=  )/ $1__APOS__/goi; # jes' = jus' = just
    s/(?<=[^\}]) ([^ _{}][^ {}]*)(s)(')(?![SsDd]  |[lL][lL])/ $1$2$3 /go; #$1$2 {'} '$2
    s/__APOS__/'/g;
    s/([a-z]{2,})(')(ll)(?= )/$1 $2$3/goi;

    s/(?<=[^\}]) (can|would|did|should|was|is|could|were|are|does|had|have|will|shall)(nae) / $1 $2 /goi;
    s/ (I|you)  (din)(nae *)$/ $1  $2 $3/goi;
    s/(?<=[^\}]) (din)(nae)  (ye|you) / $1 $2  $3 /goi;
    s/^( *[Dd]in)(nae)(?=  )/$1 $2/go;
    s/^( *DIN)(NAE)(?=  )/$1 $2/go;
    s/^( *din)(nae)(?=  )/$1 $2/goi;

    s/(?<=[^\}]) (y)(')(know) / $1$2 $3 /goi;
    s/(?<=[^\}]) (gon)(')(get) / $1$2 $3 /goi;

    s/(?<=[^\}]) ([A-Z][A-Z'\-]*[A-Z']IN) (')  / $1$2  /go;
    s/(?<=[^\}]) ([a-z][a-z'\-]*[a-z']in) (')  / $1$2  /goi;
    s/(?<=[^\}]) ([A-Z][A-Z'\-]*[A-Z']IN)(')(E(?:ST|R))(?= )/ $1$2 $3/go;
    s/(?<=[^\}]) ([a-z][a-z'\-]*[a-z']in)(')(e(?:st|r))(?= )/ $1$2 $3/goi;
    s/(?<=[^\}]) ([A-Z][A-Z'\-]*[A-Z']IN)(')([a-z]+)/ {$1$2$3} $1$2 $3/go;
    s/(?<=[^\}]) ([a-z][a-z'\-]*[a-z']in)(')([a-z]+)/ {$1$2$3} $1$2 $3/goi;
    s/(?<=[^\}]) ([A-Z][A-Z'\-]*[A-Z']IN) (') (-) ([a-z]+)/ {$1$2$3} $1$2 $3 $4/go;
    s/(?<=[^\}]) ([a-z][a-z'\-]*[a-z']in) (') (-) ([a-z]+)/ {$1$2$3} $1$2 $3 $4/goi;
  } elsif ($lang eq "cs" || $lang eq "cz") {
    s/(?<=[^\}]) ([^ {}]{2,}) (-) (li)(?=  )/ $1 $2$3/go;
    s/(?<=[^\}]) ([Tt]e|[Bb]u)(d ')(?=  )/ {$1$2} $1ď/go;
    s/(?<=[^\}]) ([Tt][yo]|[Žž]e) (') (s)(?=  )/ $1 $2$3/go;
  } elsif ($lang eq "fr") {
    s/(?<=[^\}])  ([Ss]) ' (\S+)/  $1' $2/goi;
  }


  s/(?<=[^\}]) \'([^ ]+)\'(?= )/ {'\1'} ' \1 '/goi;
  
  s/(?<=[^\}])(\s+)((?:\( |-LRB- ?)\.\.\.(?: \)| ?-RRB-))(\s+)/$1\{$2} (...)$3/go;
  s/(?<=[^\}])(\s+)((?:\[ |-LSB- ?)\.\.\.(?: \]| ?-RSB-))(\s+)/$1\{$2} (...)$3/go;
  s/(?<=[^\}])(\s+)(\! \?)(\s+)/$1\{$2} !?$3/go;
  s/(?<=[^\}])(\s+)(\? \!)(\s+)/$1\{$2} ?!$3/go;
  s/(?<=[^\}])(\s+)(\!(?: \!)+)(\s+)/$1\{$2} !!!$3/go;
  s/(?<=[^\}])(\s+)(\?(?: \?)+)(\s+)/$1\{$2} ???$3/go;
  s/(?<=[^\}])(\s+)(\^\s+\^\s+)([^ \{\}]+)/$1\{$2$3\} $3 /go;
  

  ## EVDLC: added some protection around ^
  ## for cases like: - ^ il part
  s/_UNSPLIT__SENT_BOUND\s+\^/_UNSPLIT__SENT_BOUND\^/go; # protect some ^
  s/(?<=[^\}]) ([^ \{\}\(\[]+) \^ ([^ \{\}]+)/{$1 ^ $2} $1\^$2/go;
  s/_UNSPLIT__SENT_BOUND\^/_UNSPLIT__SENT_BOUND \^/go; # remove protect
  
  s/^\s+([^\s\{\}\(\[]+)(\s\^\s)([^\s\{\}]+)/\{$1$2$3\} $1\^$2/go;
  s/((?:_UNDERSCORE\s?)+_UNDERSCORE)([^_]|$)/ {$1} _UNDERSCORE $2/go;
  s/((?:_ACC_O\s?)+_ACC_O)(\s|$)/ {$1} _ACC_O$2/go;
  s/((?:_ACC_F\s?)+_ACC_F)(\s|$)/ {$1} _ACC_F$2/go;
  
  return $_;
}

sub altok_detokenise {
  my $s = shift;
  return $1 if $s =~ /^\s*\{(.*)} _XML$/;
  $s =~ s/(?<!\\)\{($comment_content)\} _[A-Z][^ ]+/$1/g;
  $s =~ s/(?<!\\)\{$comment_content\} //g;
  #pas d'espace avant et pas d'espace après notre position courante, on repère un espace suivi ou pas d'un ou plusieurs espaces et on remplace par ce qui est après notre premier espace : un espace -> 0 espace, deux espaces -> un espace, trois espaces -> deux espaces etc.
  $s =~s/(?<! ) ( *)(?! )/$1/g;
  $s =~ s/\\(.)/$1/g;
  #remplacer l'espace insécable par un espace
  $s =~ s/ / /g;
  return $s;
}

sub altok_remove_embedded_contents {
  my $s = shift;

  return $s if $s =~ /^\s*\{.*} _XML$/;

  while ($s =~ s/\{([^{}]*)\{([^{}]*)} _[^ {}]+/{$1$2/g) {}

  return $s;
}

sub altok_remove_original_tokens {
  my $s = shift;

  return $1 if $s =~ /^\s*\{(.*)} _XML$/;

  $s =~ s/(?<!\\)\{$comment_content\} ([^ ]+)/$1/g;

  return $s;
}

sub altok_partly_remove_entities {
  my $s = shift;

  return $1 if $s =~ /^\s*\{(.*)} _XML$/;

  $s =~ s/(?<!\\)\{($comment_content)\} _(?:SMILEY|TWHASHTAG|CPUNCT|ROMNUM|NUM|META_TEXTUAL[A-Z_]+)/$1/g;

  return $s;
}

sub altok_remove_entities {
  my $s = shift;

  return $1 if $s =~ /^\s*\{(.*)} _XML$/;

  $s =~ s/(?<!\\)\{($comment_content)\} _[^ ]+/$1/g;

  return $s;
}

sub altok_detokenise_entity_content {
  my $s = shift;

  return $1 if $s =~ /^\s*\{(.*)} _XML$/;

  $s =~ s/(?<!\\)\{($comment_content)\} ([^ ]+)/"{"._altok_detokenise_nooutputspace_nounescape($1)."} $2"/ge;

  return $s;
}

sub altok_restore_original_tokens {
  my $s = shift;

  return $1 if $s =~ /^\s*\{(.*)} _XML$/;

  $s =~ s/(?<!\\)\{($comment_content)\} [^ ]+/$1/g;

  return $s;
}

sub _altok_detokenise_nooutputspace_nounescape {
  my $s = shift;

  $s =~ s/(?<!\\)\{($comment_content)\} _[A-Z][^ ]+/$1/g;
  $s =~ s/(?<!\\)\{$comment_content\} //g;
  $s =~ s/ / /g;
  $s =~ s/(?<! ) ( *)(?! )/$1/g;
  
  return $s;
}

sub _altok_remove_spaces {
  my $s = shift;

  $s =~ s/(?<! ) (?! )//g; 
  
  return $s;
}

sub space_unless_is_SPECWORD {
  my $s = shift;
  if ($s =~ /^_SPECWORD/) {return ""}
  return " ";
}

1;
