#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import regex as re

import alVariables

#alPunctuation = alVariables.get_alPunctuation()

def altok_pretokenise(s,lang):
	if re.search(r" _XML$", s):
		return s
	s = re.sub(r" ", r"#_#_#", s) # protection of input spaces
	if (re.search(r"^(zh|ja|th)", lang) or (re.search(r"^(ko)", lang) and re.search(r"[^ ]{7}", s))):
		s = re.sub(r"([^A-Za-z0-9 ])", r" \1 ", s)
		s = re.sub(r"\\ +(.)", r"\\\1", s)
	else:
		#capturepunct = re.compile(f"({alVariables.alPunctuation.pattern})")
		#s = capturepunct.sub(r" \1 ", s)
		s = re.sub(rf"({alVariables.alPunctuation.pattern})", r" \1 ", s)

		#tous les kanjis
		#s = re.sub(r"([\x3400-\x4DB5\x4E00-\x9FCB\xF900-\xFA6A])", r" \1 ", s)
		s = re.sub(r"(\p{Han})", r" \1 ", s)
	#print(f"BEFORE SUB {s}")
	match = re.search(r"  ", s)
	while match is not None:
		s = re.sub(r"  ", r" ", s)
		match = re.search(r"  ", s)
	s = re.sub(r" # _ # _ # ", r"  ", s) # restoring input spaces
	s = re.sub(r"# _ # _ # ", r" ", s) # restoring input spaces
	#ICI une seule sub pour corriger le pb avec la fin de chaîne : pas global en perl
	s = re.sub(r"^ *", r" ", s, 1)
	s = re.sub(r" *$", r" ", s, 1)
	#negative lookahead, un - pas suivi par un espace
	s = re.sub(r" -(?! )", r" - ", s)
	s = re.sub(r"< br >", r"<br>", s)  
	s = re.sub(r"& gt ;", r"&gt;", s)
	s = re.sub(r"& lt ;", r"&lt;", s)
	s = re.sub(r"& amp ;", r"&amp;", s)
	#TRAD : ces deux lignes n'étaient pas là...
	s = re.sub(r"& apos ;", r"&apos;", s)
	s = re.sub(r"& quot ;", r"&quot;", s)
	s = re.sub(r"^ *", r" ", s)  
	return s

def altok_detokenise(s):
	pattern_match = re.search(r"^\s*{(.*)} _XML$", s)
	if pattern_match is not None:
		return pattern_match.group(1)
	s = re.sub(rf"(?<!\\){{({alVariables.comment_content.pattern})}} _[A-Z][^ ]+", r"\1", s)
	s = re.sub(rf"(?<!\\){{{alVariables.comment_content.pattern}}} ", r"", s)
	#pas d'espace avant et pas d'espace après notre position courante, on repère un espace suivi ou pas d'un ou plusieurs espaces et on remplace par ce qui est après notre premier espace : un espace -> 0 espace, deux espaces -> un espace, trois espaces -> deux espaces etc.
	s = re.sub(r"(?<! ) ( *)(?! )", r"\1", s)
	s = re.sub(r"\\(.)", r"\1", s)
	#remplacer l'espace insécable par un espace
	s = re.sub(r" ", r" ", s)
	return s

def altok_remove_embedded_contents(s):
	if re.search(r"^\s*{.*} _XML$", s):
		return s
	check_pattern = re.compile(r"{([^{}]*){([^{}]*)} _[^ {}]+")
	pattern_match = check_pattern.search(s)
	while pattern_match is not None:
		s = check_pattern.sub(r"{\1\2", s)
		pattern_match = check_pattern.search(s)
	return s

def altok_remove_entities(s):
	pattern_match = re.search(r"^\s*{(.*)} _XML$", s)
	if pattern_match:
		return pattern_match.group(1)
	s = re.sub(rf"(?<!\\){{({alVariables.comment_content.pattern})}} _[^ ]+", r"\1", s)
	return s

def altok_detokenise_entity_content(s):
	pattern_match = re.search(r"^\s*{(.*)} _XML$", s)
	if pattern_match:
		return pattern_match.group(1)
	#pour pouvoir appliquer une fonction à un reference groupe, on est obligé de faire une fonction lambda
	s = re.sub(rf"(?<!\\){{({alVariables.comment_content.pattern})}} ([^ ]+)", lambda x : "{" + _altok_detokenise_nooutputspace_nounescape(x.group(1)) + "} " + x.group(2), s)
	return s

def _altok_detokenise_nooutputspace_nounescape(s):
	s = re.sub(rf"(?<!\\){{({alVariables.comment_content.pattern})}} _[A-Z][^ ]+", r"\1", s)
	s = re.sub(rf"(?<!\\){{{alVariables.comment_content.pattern}}} ", r"", s)
	s = re.sub(r" ", r" ", s)
	s = re.sub(r"(?<! ) ( *)(?! )", r"\1", s)
	return s







