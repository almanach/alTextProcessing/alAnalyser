#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use FindBin;
use lib $FindBin::Bin;
use utf8;
use threads;
use alLanguageDetection;
use alRawTextCleaning;
use alURL;
use alEmail;
use alTokenisation;
use alCPunct;
use alNormalisation;
use alSmiley;
use alNumbers;
use alUTF8;
use alSentenceSplitting;
use strict;

$| = 1;

my $lang = "";
my $ell = 1;
my $orig_token_mode = 3;
my $split_sentences = 0;
my $verbose = 0;
my $uniformise_spaces = 0;
my $process_numbers = 0;
my $tokenise = 0;

while (1) {
  $_ = shift;
  if (/^$/) {last}
  elsif (/^-l$/) {$lang = shift || die "Option '-l' must be followed by a language code"}
  elsif (/^-u$/) {$uniformise_spaces = 1}
  elsif (/^-e$/) {$ell = shift || die "Option '-e' must be followed by a valid entity normalisation level (1=default, 2, 3)"}
  elsif (/^-o$/) {$orig_token_mode = shift || die "Option '-o' must be followed by a valid original token behaviour level (1=output only _MARKERs, 2=output is of the form {original token} _MARKER, 3=default=output only original token)"}
  elsif (/^-s$/) {$split_sentences = 1}
  elsif (/^-v$/) {$verbose = 1}
  elsif (/^-n$/) {$process_numbers = 1}
  elsif (/^-t$/) {$tokenise = 1}
  else {die "Unknown option '$_'"}
}

while (<>) {
  chomp;
  my $l = $lang;
  $_ = altok_clean_utf8($_);
  $l = altok_detect_language($_,$lang);
  print STDERR "### Language detected: '$l'\n" if $verbose;
  #  $_ = altok_interpret_entities($_);
  $_ = altok_escape_metacharacters($_);
  $_ = altok_escape_xml($_);
  $_ = altok_pretokenise($_, $l);
  $_ = altok_normalise($_, $l);
  $_ = altok_URL($_, $l);
  $_ = altok_email($_);
  $_ = altok_smiley($_);
  $_ = altok_numbers($_) if $process_numbers;
  $_ = altok_remove_embedded_contents($_);
  $_ = altok_detokenise_multiple_punctuations($_);
  $_ = altok_remove_embedded_contents($_);
  $_ = altok_retokenise_lexical_punctuations($_, $l);
  $_ = altok_remove_embedded_contents($_);
  $_ = altok_detokenise_entity_content($_, 1);
  $_ = altok_remove_embedded_contents($_);
  $_ = altok_tokenise($_,$l) if $tokenise;
  $_ = altok_split_sentences($_,$l,0,1,1) if $split_sentences;
  if ($ell == 2) {
    $_ = altok_partly_remove_entities($_, 1);
  } elsif ($ell == 3) {
    $_ = altok_remove_entities($_, 1);
  }
  if ($orig_token_mode == 1) {
    $_ = altok_remove_original_tokens($_, 1);
  } elsif ($orig_token_mode == 3) {
    $_ = altok_restore_original_tokens($_, 1);
  }
  if ($uniformise_spaces) {
    s/^ +//;
    s/ +$//;
    s/  +/ /g;
  }
  print $_."\n";
}
