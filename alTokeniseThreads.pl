#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use threads;
use Thread::Queue;
use alLanguageDetection;
use alRawTextCleaning;
use alURL;
use alEmail;
use alTokenisation;
use alCPunct;
use alNormalisation;
use alSmiley;
use alSentenceSplitting;
use strict;

my $lang = "";
my $ell = 1;
my $orig_token_mode = 2;

while (1) {
  $_ = shift;
  if (/^$/) {last}
  elsif (/^-l$/) {$lang = shift || die "Option '-l' must be followed by a language code"}
  elsif (/^-e$/) {$ell = shift || die "Option '-e' must be followed by a valid entity normalisation level (1=default, 2, 3)"}
  elsif (/^-o$/) {$orig_token_mode = shift || die "Option '-o' must be followed by a valid original token behaviour level (1, 2=default, 3)"}
  else {die "Unknown option '$_'"}
}

my (@queues,@threads);
for (0..12) {
  $queues[$_] = Thread::Queue->new();
}
my $tid = 0;
$threads[$tid] = threads->create(sub {while ($_=$queues[$tid]->dequeue()) {
  my $l = $lang;
  my $prevl = "unknown language";
  $l = altok_detect_language($_,$lang);
  $_ = altok_interpret_entities($_);
  $_ = altok_escape_xml($_);
  $_ = altok_escape_metacharacters($_);
  $queues[$tid+1]->enqueue("// lang=$l") if $prevl ne $l;$prevl=$l;$queues[$tid+1]->enqueue($_);} $queues[$tid+1]->enqueue(undef);});
$tid++;
$threads[$tid] = threads->create(sub {while ($_=$queues[$tid]->dequeue()) {
  my $l = $lang;
  if (/^\/\/ lang=(.*)$/) {
    $l = $1;
  } else {
    print STDERR "$_\n";
    $_ = altok_tokenise($_, $l);
    $_ = altok_normalise($_, $l);
    $_ = altok_URL($_, $l);
    $_ = altok_smiley($_);
    $_ = altok_remove_embedded_contents($_);
    $_ = altok_detokenise_multiple_punctuations($_);
    $_ = altok_remove_embedded_contents($_);
    $_ = altok_retokenise_lexical_punctuations($_, $l);
    $_ = altok_remove_embedded_contents($_);
    $_ = altok_detokenise_entity_content($_, 1);
    $_ = altok_remove_embedded_contents($_);
    if ($ell == 2) {
      $_ = altok_partly_remove_entities($_, 1);
    } elsif ($ell == 3) {
      $_ = altok_remove_entities($_, 1);
    }
    if ($orig_token_mode == 1) {
      $_ = altok_remove_original_tokens($_, 1);
    } elsif ($orig_token_mode == 3) {
      $_ = altok_restore_original_tokens($_, 1);
    }
    print $_."\n";
  }
}});


while (<>) {
  chomp;
  $queues[0]->enqueue($_);
}
$queues[0]->enqueue(undef);
for (reverse 0..$#threads) {
  $tid++;
  $threads[$_]->join();
}
