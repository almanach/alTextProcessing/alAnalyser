#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my ($prev_uid, $cur_uid);
my (@norm, @pos, @ms);

while (<>) {
  chomp;
  if (/^\\u_id +(.+?) *$/) {
    $prev_uid = $cur_uid;
    $cur_uid = $1;
    die "U_ID $cur_uid found twice" if $prev_uid eq $cur_uid;
    process_cur_sentence();
    @norm = ();
    @pos = ();
    @ms = ();
  } elsif (/^\\norm (.+?) *$/) {
    die if $cur_uid eq "";
    @norm = split / +/, $1;
  } elsif (/^\\pos (.+?) *$/) {
    die if $cur_uid eq "";
    @pos = split / +/, $1;
    die "Number of POS different from number of forms ('norm') for u_id $cur_uid" if $#norm != $#pos;
  } elsif (/^\\morphosyn (.+?) *$/) {
    die if $cur_uid eq "";
    @ms = split / +/, $1;
    die "Number of MS different from number of forms ('norm') for u_id $cur_uid" if $#norm != $#ms;
  }
}

sub process_cur_sentence {
  my $brown_sentence = "";
  for my $i (0..$#norm) {
    $brown_sentence .= " " unless $brown_sentence eq "";
    $brown_sentence .= $norm[$i]."/".$pos[$i];
  }
  print $brown_sentence."\n";
}
