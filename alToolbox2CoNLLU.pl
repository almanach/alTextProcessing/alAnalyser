#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use alTokenisation;
use strict;

my ($prev_uid, $cur_uid);
my (@norm, @pos, @cpos, @ms, @lemma, @lemma_en, $text, $com, $problems, $ELANp, $ELANb, $ELANe, $mmarker, $ger_u);

while (1) {
  $_ = shift;
  if (/^$/) {last}
  else {die "Unknown option '$_'"}
}

while (<>) {
  chomp;
  if (/^\\(_|ELANMedia(?:URL|MIME|Extracted))(.*)/) {
    print "# $1$2\n";
  } elsif (/^\\u_id +(.+?) *$/) {
    $prev_uid = $cur_uid;
    $cur_uid = $1;
    die "U_ID $cur_uid found twice" if $prev_uid eq $cur_uid;
    process_cur_sentence() unless $prev_uid eq "";
    @lemma = ();
    @lemma_en = ();
    @norm = ();
    @pos = ();
    @ms = ();
    $text = "";
    $com = "";
    $problems = "";
    $ELANp = "";
    $ELANb = "";
    $ELANe = "";
    $mmarker = "";
    $ger_u = "";
  } elsif (/^\\text *$/) {
    die if $cur_uid eq "";
  } elsif (/^\\text (.+?) *$/) {
    die if $cur_uid eq "";
    $text = $1;
  } elsif (/^\\com *$/) {
  } elsif (/^\\com (.+?) *$/) {
    die if $cur_uid eq "";
    $com = $1;
  } elsif (/^\\problems *$/) {
  } elsif (/^\\problems (.+?) *$/) {
    die if $cur_uid eq "";
    $problems = $1;
  } elsif (/^\\ELANParticipant *$/) {
  } elsif (/^\\ELANParticipant (.+?) *$/) {
    die if $cur_uid eq "";
    $ELANp = $1;
  } elsif (/^\\ELANBegin (.+?) *$/) {
    die if $cur_uid eq "";
    $ELANb = $1;
  } elsif (/^\\ELANEnd (.+?) *$/) {
    die if $cur_uid eq "";
    $ELANe = $1;
  } elsif (/^\\mmarker (.+?) *$/) {
    die if $cur_uid eq "";
    $mmarker = $1;
  } elsif (/^\\ger_u *$/) {
  } elsif (/^\\ger_u (.+?) *$/) {
    die if $cur_uid eq "";
    $ger_u = $1;
  } elsif (/^\\(?:norm|full) (.+?) *$/) {
    die if $cur_uid eq "";
    @norm = split / +/, $1;
  } elsif (/^\\lemma (.+?) *$/) {
    die if $cur_uid eq "";
    @lemma = split / +/, $1;
  } elsif (/^\\lemma_en (.+?) *$/) {
    die if $cur_uid eq "";
    @lemma_en = split / +/, $1;
  } elsif (/^\\pos (.+?) *$/) {
    die if $cur_uid eq "";
    my $pos = $1;
    @pos = split / +/, $pos;
    die "Number of POS different from number of forms ('norm') for u_id $cur_uid" if $#norm != $#pos;
  } elsif (/^\\cpos (.+?) *$/) {
    die if $cur_uid eq "";
    my $cpos = $1;
    @cpos = split / +/, $cpos;
    die "Number of CPOS different from number of forms ('norm') for u_id $cur_uid" if $#norm != $#cpos;
  } elsif (/^\\morphosyn (.+?) *$/) {
    die if $cur_uid eq "";
    @ms = split / +/, $1;
    die "Number of MS different from number of forms ('norm') for u_id $cur_uid" if $#norm != $#ms;
  } elsif (/^\s*$/) {
  } else {
    die $_;
  }
}
process_cur_sentence();

sub process_cur_sentence {
  print "# sent_id = $prev_uid\n";
  print "# text = $text\n";
#  print "# lemma = ".(join " ", @lemma)."\n";
  print "# lemma_en = ".(join " ", @lemma_en)."\n";
  print "# ger_u = $ger_u\n";
  print "# com = $com\n";
  print "# problems = $problems\n";
  print "# ELANParticipant = $ELANp\n";
  print "# ELANBegin = $ELANb\n";
  print "# ELANEnd = $ELANe\n";
  print "# mmarker = $mmarker\n";
  if ($#norm == -1) {
    my $tok = altok_tokenise($text,"roh");
    $tok =~ s/\* \? \?/*??/g;
    for (split / +/, $tok) {
      push @norm, $_ unless $_ eq "";
    }
  }
  for my $i (0..$#norm) {
    print ($i+1);
    print "\t".$norm[$i];
    print "\t".(defined $lemma[$i] ? $lemma[$i] : "_");
    print "\t".(defined $pos[$i] ? $pos[$i] : "_");
    print "\t".(defined $cpos[$i] ? $cpos[$i] : "_");
    print "\t".(defined $ms[$i] ? $ms[$i] : "_");
    print "\t"."_"; # dep
    print "\t"."_"; # dep label
    print "\t"."_";
    print "\t"."_";
    print "\n";
  }
  print "\n";
}
