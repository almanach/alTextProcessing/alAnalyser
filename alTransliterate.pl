#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

use alTransliteration;

my $lang;
my $reverse; # default = 0, i.e. in direction from other script to latin script. Use option '-r' to get the other direction

while (1) {
  $_ = shift;
  if (/^$/) {last}
  elsif (/^-l$/) {$lang = shift || die "Option '-l' must be followed by a language code"}
  elsif (/^-r$/) {$reverse = 1;}
  else {die "Unknown option '$_'"}
}

die "Please specify a language (-l <lang>, or '-l cyr' to transliterate to/from cyrillic in a language-independent way)" if $lang eq "";

while (<>) {
  chomp;
  print al_transliterate($_,$lang,$reverse)."\n";
}
