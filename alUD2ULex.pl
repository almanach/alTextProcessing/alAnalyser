#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

my ($mwt_start, $mwt_end, $rem, $cur_multiword_token, $cur_multiword_lines, $cur_multiword_tokens);
my ($nb_amlgm_forms, $nb_simple_forms);
my (%UDcat_form_lemma_ms2occ,%UDamlgm);
my $total_occ;
$mwt_start = -1;
while (<>) {
  chomp;
  if (/^$/ || /^#.*/) {
  } elsif (/^(\d+)-(\d+)\t([^\t]+)/) {
    $mwt_start = $1;
    $mwt_end = $2;
    $cur_multiword_token = $3;
  } elsif (/^(\d+)\t([^\t]+)\t([^\t]+)\t([^\t]+)\t[^\t]*\t([^\t]+)\t/) {
    my ($id, $form, $lemma, $cat, $ms) = ($1, $2, $3, $4, $5);
    $UDcat_form_lemma_ms2occ{$cat}{$form}{$lemma}{$ms}++;
    $total_occ++;
    if ($mwt_start >= 0) {
      if ($mwt_start == $id) {
	$cur_multiword_tokens = $form;
      } else {
	$cur_multiword_tokens .= " ".$form;
      }
      if ($id < $mwt_end) {
	$cur_multiword_lines .= ($id-$mwt_start)."\t".($id-$mwt_start+1)."\t$form\t$lemma\t$cat\t$ms\t_\n";
      } elsif ($id == $mwt_end) {
	$cur_multiword_lines .= ($id-$mwt_start)."\t".($id-$mwt_start+1)."\t$form\t$lemma\t$cat\t$ms\t_\n";
	if (lc($cur_multiword_tokens) eq $cur_multiword_tokens && lc($cur_multiword_token) eq $cur_multiword_token) {
	  $UDamlgm{$cur_multiword_token}{"0-".($mwt_end-$mwt_start+1)."\t$cur_multiword_token\t_\t_\t_\t_\t_\t_\t_\t_"}{$cur_multiword_lines}++;
	}
	$mwt_start = -1;
	$mwt_end = -1;
	$cur_multiword_lines = "";
      }
    }
  }
}

# computing occ 99% coverage percentile
my @occs;
for my $cat (keys %UDcat_form_lemma_ms2occ) {
  for my $form (keys %{$UDcat_form_lemma_ms2occ{$cat}}) {
    for my $lemma (keys %{$UDcat_form_lemma_ms2occ{$cat}{$form}}) {
      for my $ms (keys %{$UDcat_form_lemma_ms2occ{$cat}{$form}{$lemma}}) {
	push @occs, $UDcat_form_lemma_ms2occ{$cat}{$form}{$lemma}{$ms};
      }
    }
  }
}
@occs = sort {$b <=> $a} @occs;
my ($tmp, $occ_threshold);
for (@occs) {
  $tmp+=$_;
  if ($tmp >= 0.9*$total_occ) {
    $occ_threshold = $_;
    last;
  }
}
if ($occ_threshold == 1) {
  $tmp = 0;
  for (@occs) {
    $tmp+=$_;
    if ($tmp >= 0.75*$total_occ) {
      $occ_threshold = $_;
      last;
    }
  }
  $occ_threshold = 2 if $occ_threshold > 2;
} elsif ($occ_threshold > 3) {
  $occ_threshold = 3;
}
print STDERR "occ_threshold: $occ_threshold\n";

for my $cat (keys %UDcat_form_lemma_ms2occ) {
  next if $cat eq "X";
  for my $form (keys %{$UDcat_form_lemma_ms2occ{$cat}}) {
    next if $cat eq "NUM" && ($form =~ /\d/ || $form =~ /^[A-Z]/);
    next if (lc($form) ne $form && defined($UDcat_form_lemma_ms2occ{$cat}{lc($form)}));
    for my $lemma (keys %{$UDcat_form_lemma_ms2occ{$cat}{$form}}) {
      for my $ms (keys %{$UDcat_form_lemma_ms2occ{$cat}{$form}{$lemma}}) {
	next if $UDcat_form_lemma_ms2occ{$cat}{$form}{$lemma}{$ms} < $occ_threshold;
	if (lc($lemma) eq $lemma && $form ne lc($form)) {
	  $form = lc($form);
	  print "0\t1\t$form\t$lemma\t$cat\t_\t$ms\t_\n" unless defined $UDcat_form_lemma_ms2occ{$cat}{$form}{$lemma}{$ms}; # $form might have just been lowercased, and we dont want duplicates
	} else {
	  print "0\t1\t$form\t$lemma\t$cat\t_\t$ms\t_\n";
	}
      }
    }
  }
}

for my $a (sort {$a cmp $b} keys %UDamlgm) {
  for my $aline (sort {$a cmp $b} keys %{$UDamlgm{$a}}) {
    for my $adecomposition (sort {$a cmp $b} keys %{$UDamlgm{$a}{$aline}}) {
      next if $UDamlgm{$a}{$aline}{$adecomposition} < $occ_threshold;
      print $aline."\n".$adecomposition;
    }
  }
}
