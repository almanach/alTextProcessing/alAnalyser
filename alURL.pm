package alURL;
use utf8;
use strict;
use alVariables;
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(&altok_URL);

my $a  =    qr/[a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ]/o;

my %safeURLext = (
		  'sk' => qr/(?:com|org|edu|gov|net|mil|aero|biz|coop|info|museum|name|pro|int|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|fi|fj|fk|fm|fo|fr|fx|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw|bo|ci|co|in|im|me)/o,
		  'pl' => qr/(?:com|org|edu|gov|net|mil|aero|biz|coop|info|museum|name|pro|int|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|br|bs|bt|bv|bw|bz|ca|cc|cd|cf|cg|ch|ck|cl|cm|cn|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|fx|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mv|mw|mx|mz|nc|ne|nf|ng|ni|nl|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tk|tm|tn|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|zm|zw|me)/o,
		  'en' => qr/(?:com|org|edu|gov|net|mil|aero|biz|coop|info|museum|name|pro|int|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|au|aw|az|ba|bb|bd|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|dj|dk|dm|dz|ec|ee|eg|eh|er|es|fi|fj|fk|fm|fo|fr|fx|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|mz|na|nc|ne|nf|ng|ni|nl|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw|de|et|eu)/o,
		  '' => qr/(?:com|org|edu|gov|net|mil|aero|biz|coop|info|museum|name|pro|int|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|fi|fj|fk|fm|fo|fr|fx|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)/o,
		  );
my %unsafeURLext = (
		    'sk' => qr/(?:je|by|do|je|ma|mu|my|na|no|tj|to|za|info|jobs|museum|name|asia)/o,
		    'pl' => qr/(?:bo|by|ci|co|do|im|in|je|ma|mu|my|na|no|tj|to|za|info|jobs|museum|name|asia)/o,
		    'en' => qr/(?:it|to|at|do|is|my|no|so|us|info|jobs|museum|name|asia|me)/o,
		    '' => qr/(?:je|de|et|eu|be|info|jobs|museum|name|asia|me)/o,
		   );
my $URLprotocole = qr/https?|ftp|telnet|gopher/o;
my $dirname   = qr/ *[\/／] *(?:  (?:\\_|\.|．|\~|\?|=) ?| ?(?:\\_|\.|．|\~|\?|=)  |[\w－\-\.．\~_\?=%&#] ?)*\w/o;
my $dom1      = qr/${a}(?:\w| [－\-]| \\_)* [\.．] /o;
my $dom2_allow_spaces      = qr/${a}(?:\w| [－\-]| \\_)* +[\.．] +/o;
my $domN      = qr/${a}(?:\w| [－\-]| \\_)* ?[\.．] ?(?:(?:\w| [－\-]| \\_){2,} ?[\.．] ?)+ /o;
my $domN_allow_spaces      = qr/${a}(?:\w| [－\-]| \\_)* +[\.．] +(?:(?:\w| [－\-]| \\_){2,} +[\.．] +)+/o;
my $POSTvar   = qr/(?:(?:[a-zA-Z0-9\?=－\-\_] ?)+ +=(?: +(?:[a-zA-Z0-9\?=－\-\_] ?)+)?)/o;
my (%refOK, %refOK_allow_spaces);
for ("sk", "pl", "en", "") {
  $refOK{$_}     = qr/(?:$domN(?:$safeURLext{$_}|$unsafeURLext{$_})|$dom1$safeURLext{$_})(?: $dirname)*(?: *[\/／])?(?: *\? *$POSTvar(?: *\&amp; *$POSTvar)*)?/o;
  $refOK_allow_spaces{$_}     = qr/(?:$domN_allow_spaces(?:$safeURLext{$_}|$unsafeURLext{$_})|$dom1$safeURLext{$_})/o;
}

sub altok_URL {
  my $s = shift;		# must be already tokenised
  my $lang = shift;

  return $s if $s =~ /_XML$/;

  # minitel
  if ($lang =~ /^fr/) {
    $s =~ s/(^|\s)(36-?1[567]\s[^\s]+)(\s|$)/$1\{$2} _URL$3/;
  }

  # URLs
  my $url_lang = $lang;
  $url_lang = "" unless $url_lang =~ /^(?:sk|pl|en)(?:_|$)/;
  ### urls faciles (deux points dans le nom de domaine ou alors extension autre que .et et .de)
  $s =~ s/(?<= )((?:&lt; ?)?(?:$URLprotocole +: +[\/／] [\/／] +)?$refOK{$url_lang}(?: ?&gt;)?)(?= )/\{$1\} _URL/go;
  $s =~ s/(?<= )((?:&lt; ?)?$URLprotocole +: +[\/／] [\/／] +$refOK_allow_spaces{$url_lang}(?: [\/／])?(?: ?&gt;)?)(?= )/\{$1\} _URL/go;
  ### urls de type (http://)?nomsanspoint.(et|de)/deschosesobligatoirement
  $s =~ s/(?<= )((?:&lt; ?)?(?:$URLprotocole +: +[\/／] [\/／] +)?$dom1$unsafeURLext{$url_lang}(?:$dirname)+[\/／]?(?: ?&gt;)?)(?= )/\{$1\} _URL/go;
  $s =~ s/(?<= )((?:&lt; ?)?$URLprotocole +: +[\/／] [\/／] +$dom2_allow_spaces$unsafeURLext{$url_lang}(?: [\/／])?(?: ?&gt;)?)(?= )/\{$1\} _URL/go;
  ### urls de type http://nomsanspoint.(et|de)(/deschosesfacultatives)?
  $s =~ s/(?<= )((?:&lt; ?)?(?:$URLprotocole +: +[\/／] [\/／] +)$dom1$unsafeURLext{$url_lang}(?:$dirname)*[\/／]?(?: ?&gt;)?)(?= )/\{$1\} _URL/go;
  $s =~ s/(\s)([\.．])\} _URL/\} _URL$1$2/go || $s =~ s/([\.．])\} _URL/\} _URL$1/go;

  # removing embedded URLs
  while ($s =~ s/{($comment_content)} _URL($comment_content)} _URL/$1$2} _URL/g) {}

  ### twitter
  $s = " $s ";
  $s =~ s/^ ([^ ] ?)([\@\#] ?[a-zA-Z0-9])/ $1{$2}_TWFIXATMENTION/go; # so-called "fix-replies"
  $s =~ s/(?<=[ ,;?\!:\"\)\(\*\#<>\[\]\%\/／\=\+\«\»\˝\`])(\@ ?(?:$alAlphanum| \\_ ?)+)(?= [ ,;?\!:\"\)\(\*\#<>\[\]\%\/／\=\+\«\»\˝\`\.．])/{$1} _TWATMENTION/go; # "hashtags" and "replies"
  $s =~ s/(?<=[ ,;?\!:\"\)\(\*\#<>\[\]\%\/／\=\+\«\»\˝\`])(\# ?(?:$alAlphanum| \\_ ?)+)(?= [ ,;?\!:\"\)\(\*\#<>\[\]\%\/／\=\+\«\»\˝\`\.．])/{$1} _TWHASHTAG/go; # "hashtags" and "replies"
  $s =~ s/(?<=  )(\. ?)(\@ ?(?:$alAlphanum| \\_ )+)(?= [ ,;?\!:\"\)\(\*\#<>\[\]\%\/／\=\+\«\»\˝\`\.．])/{$1$2} _TWDOTATMENTION/go; # ".replies"
  $s =~ s/^ (.*) $/$1/ || die "|$_|";

  return $s;
}
1;
