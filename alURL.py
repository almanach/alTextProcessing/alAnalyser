#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import alVariables

import regex as re

a = re.compile(r"[a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ]")

safeURLext = {
	"sk" : re.compile(r"(?:com|org|edu|gov|net|mil|aero|biz|coop|info|museum|name|pro|int|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|fi|fj|fk|fm|fo|fr|fx|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw|bo|ci|co|in|im|me)"),
	"pl" : re.compile(r"(?:com|org|edu|gov|net|mil|aero|biz|coop|info|museum|name|pro|int|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|br|bs|bt|bv|bw|bz|ca|cc|cd|cf|cg|ch|ck|cl|cm|cn|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|fx|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mv|mw|mx|mz|nc|ne|nf|ng|ni|nl|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tk|tm|tn|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|zm|zw|me)"),
	"en" : re.compile(r"(?:com|org|edu|gov|net|mil|aero|biz|coop|info|museum|name|pro|int|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|au|aw|az|ba|bb|bd|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|dj|dk|dm|dz|ec|ee|eg|eh|er|es|fi|fj|fk|fm|fo|fr|fx|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|mz|na|nc|ne|nf|ng|ni|nl|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw|de|et|eu)"),
	"" : re.compile(r"(?:com|org|edu|gov|net|mil|aero|biz|coop|info|museum|name|pro|int|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|fi|fj|fk|fm|fo|fr|fx|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)")
}

unsafeURLext = {
	"sk" : re.compile(r"(?:je|by|do|je|ma|mu|my|na|no|tj|to|za|info|jobs|museum|name|asia)"),
	"pl" : re.compile(r"(?:bo|by|ci|co|do|im|in|je|ma|mu|my|na|no|tj|to|za|info|jobs|museum|name|asia)"),
	"en" : re.compile(r"(?:it|to|at|do|is|my|no|so|us|info|jobs|museum|name|asia|me)"),
	"" : re.compile(r"(?:je|de|et|eu|be|info|jobs|museum|name|asia|me)")
}

URLprotocole = re.compile(r"https?|ftp|telnet|gopher")
dirname = re.compile(r" *[\/／] *(?:  (?:\\_|\.|．|\~|\?|=) ?| ?(?:\\_|\.|．|\~|\?|=)  |[\w－\-\.．\~_\?=%&#] ?)*\w")
dom1 = re.compile(rf"{a.pattern}(?:\w| [－\-]| \\_)* [\.．] ")
dom2_allow_spaces = re.compile(rf"{a.pattern}(?:\w| [－\-]| \\_)* +[\.．] +")
domN = re.compile(rf"{a.pattern}(?:\w| [－\-]| \\_)* ?[\.．] ?(?:(?:\w| [－\-]| \\_){{2,}} ?[\.．] ?)+ ")
domN_allow_spaces = re.compile(rf"{a.pattern}(?:\w| [－\-]| \\_)* +[\.．] +(?:(?:\w| [－\-]| \\_){{2,}} +[\.．] +)+")
POSTvar = re.compile(r"(?:(?:[a-zA-Z0-9\?=－\-\_] ?)+ +=(?: +(?:[a-zA-Z0-9\?=－\-\_] ?)+)?)")

refOK, refOK_allow_spaces = {}, {}

for lang in ["sk", "pl", "en", ""]:
	refOK[lang] = re.compile(rf"(?:{domN.pattern}(?:{safeURLext[lang].pattern}|{unsafeURLext[lang].pattern})|{dom1.pattern}{safeURLext[lang].pattern})(?: {dirname.pattern})*(?: *[\/／])?(?: *\? *{POSTvar.pattern}(?: *\&amp; *{POSTvar.pattern})*)?")
	refOK_allow_spaces[lang] = re.compile(rf"(?:{domN_allow_spaces.pattern}(?:{safeURLext[lang].pattern}|{unsafeURLext[lang].pattern})|{dom1}{safeURLext[lang].pattern})")

def altok_URL(s, lang):
	if re.search(r"_XML$", s):
		return s

	# minitel
	if re.search(r"^fr", lang):
		s = re.sub(r"(^|\s)(36-?1[567]\s[^\s]+)(\s|$)", r"\1{\2} _URL\3", s)

	# URLs
	url_lang = lang
	if not re.search(r"^(?:sk|pl|en)(?:_|$)", url_lang):
		url_lang = ""
	### urls faciles (deux points dans le nom de domaine ou alors extension autre que .et et .de)
	s = re.sub(rf"(?<= )((?:&lt; ?)?(?:{URLprotocole.pattern} +: +[\/／] [\/／] +)?{refOK[url_lang].pattern}(?: ?&gt;)?)(?= )", r"{\1} _URL", s)
	s = re.sub(rf"(?<= )((?:&lt; ?)?{URLprotocole.pattern} +: +[\/／] [\/／] +{refOK_allow_spaces[url_lang].pattern}(?: [\/／])?(?: ?&gt;)?)(?= )", r"{\1} _URL", s)
	### urls de type (http://)?nomsanspoint.(et|de)/deschosesobligatoirement
	s = re.sub(rf"(?<= )((?:&lt; ?)?(?:{URLprotocole.pattern} +: +[\/／] [\/／] +)?{dom1.pattern}{unsafeURLext[url_lang].pattern}(?:{dirname.pattern})+[\/／]?(?: ?&gt;)?)(?= )", r"{\1} _URL", s)
	s = re.sub(rf"(?<= )((?:&lt; ?)?{URLprotocole.pattern} +: +[\/／] [\/／] +{dom2_allow_spaces}{unsafeURLext[url_lang].pattern}(?: [\/／])?(?: ?&gt;)?)(?= )", r"{\1} _URL", s)
	### urls de type http://nomsanspoint.(et|de)(/deschosesfacultatives)?
	s = re.sub(rf"(?<= )((?:&lt; ?)?(?:{URLprotocole.pattern} +: +[\/／] [\/／] +){dom1.pattern}{unsafeURLext[url_lang]}(?:{dirname.pattern})*[\/／]?(?: ?&gt;)?)(?= )", r"{\1} _URL", s)
	s = re.sub(r"(\s)([\.．])\} _URL", r"} _URL\1\2", s)
	s = re.sub(r"([\.．])\} _URL", r"} _URL\1", s)

	# removing embedded URLs
	#ici j'ai compilé la regex car on l'utilise plusieurs fois pour imiter un sub dans un while
	check_pattern = re.compile(rf"{{({alVariables.comment_content.pattern})}} _URL({alVariables.comment_content.pattern})}} _URL")
	pattern_match = check_pattern.search(s)
	while pattern_match is not None:
		s = check_pattern.sub(r"\1\2} _URL", s)
		pattern_match = check_pattern.search(s)

	### twitter
	s = f" {s} "
	s = re.sub(r"^ ([^ ] ?)([\@\#] ?[a-zA-Z0-9])", r" \1{\2}_TWFIXATMENTION", s) # so-called "fix-replies"
	s = re.sub(rf"(?<=[ ,;?\!:\"\)\(\*\#<>\[\]\%\/／\=\+\«\»\˝\`])(\@ ?(?:{alVariables.alAlphanum.pattern}| \\_ ?)+)(?= [ ,;?\!:\"\)\(\*\#<>\[\]\%\/／\=\+\«\»\˝\`\.．])", r"{\1} _TWATMENTION", s) # "hashtags" and "replies"
	s = re.sub(rf"(?<=[ ,;?\!:\"\)\(\*\#<>\[\]\%\/／\=\+\«\»\˝\`])(\# ?(?:{alVariables.alAlphanum.pattern}| \\_ ?)+)(?= [ ,;?\!:\"\)\(\*\#<>\[\]\%\/／\=\+\«\»\˝\`\.．])", r"{\1} _TWHASHTAG", s) # "hashtags" and "replies"
	s = re.sub(rf"(?<=  )(\. ?)(\@ ?(?:{alVariables.alAlphanum.pattern}| \\_ )+)(?= [ ,;?\!:\"\)\(\*\#<>\[\]\%\/／\=\+\«\»\˝\`\.．])", r"{\1\2} _TWDOTATMENTION", s) # ".replies"

	die_if_not_match = re.search(r"^ (.*) $", s)
	#on die si on peut pas faire le sub
	assert(die_if_not_match is not None), f"|{s}|"
	s = re.sub(r"^ (.*) $", r"\1", s)

	return s