package alUTF8;
use utf8;
use strict;
use Encode;
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(&altok_clean_utf8 &altok_force_utf8);


sub altok_clean_utf8 {
  my $s = shift;
  $s = encode('UTF-8', $s, Encode::FB_DEFAULT);
  return decode('UTF-8', $s, Encode::FB_CROAK);
}

sub altok_force_utf8 {
  my $s = shift;
  my $enc8bits = shift || "l1";
  # Converts a text whose encoding is unknown, but known to be either <8-bit-encoding> (default: L1), UTF-8 or UTF-8 reencoded in UTF-8 as if it were in <8-bit-encoding>, into UTF-8 text
  # Examples of alForceUTF8 (which applies this function to each line) with enc8bits = l1:
  #$ echo "ä â à á é è ê ë î ï í ì ô ö ò ó ú ù û ü ÿ ç Ä Â À Á É È Ê Ë Î Ï Í Ì Ô Ö Ò Ó Ú Ù Û Ü Ç" | perl /Users/sagot/Documents/alAnalyser/alForceUTF8.pl 
  #ä â à á é è ê ë î ï í ì ô ö ò ó ú ù û ü ÿ ç Ä Â À Á É È Ê Ë Î Ï Í Ì Ô Ö Ò Ó Ú Ù Û Ü Ç
  #$ echo "ä â à á é è ê ë î ï í ì ô ö ò ó ú ù û ü ÿ ç Ä Â À Á É È Ê Ë Î Ï Í Ì Ô Ö Ò Ó Ú Ù Û Ü Ç" | recode u8..l1 | perl /Users/sagot/Documents/alAnalyser/alForceUTF8.pl 
  #ä â à á é è ê ë î ï í ì ô ö ò ó ú ù û ü ÿ ç Ä Â À Á É È Ê Ë Î Ï Í Ì Ô Ö Ò Ó Ú Ù Û Ü Ç
  #$ echo "ä â à á é è ê ë î ï í ì ô ö ò ó ú ù û ü ÿ ç Ä Â À Á É È Ê Ë Î Ï Í Ì Ô Ö Ò Ó Ú Ù Û Ü Ç" | recode l1..u8 | perl /Users/sagot/Documents/alAnalyser/alForceUTF8.pl 
  #ä â à á é è ê ë î ï í ì ô ö ò ó ú ù û ü ÿ ç Ä Â À Á É È Ê Ë Î Ï Í Ì Ô Ö Ò Ó Ú Ù Û Ü Ç
  # works (for now) on latin-[12]-compatible characters
  
  my $encoding = $enc8bits;
  my $recoded = $s;
  $recoded =~ s/(\P{ASCII})/"&#".ord($1).";"/ge;
  if ($recoded =~ /&#195;&#13[1-5];&#194;&#1(?:2[89]|[3-9][0-9]);|&#195;&#162;&#194;&#128;&#194;&#15[2-9]|&#195;&#130;&#194;&#1[78][0-9];/) {
    $s = decode("UTF-8", $s, Encode::FB_CROAK);
    $s = encode($enc8bits, $s, Encode::FB_CROAK);
  }
  if ($recoded =~ /&#19[5-7];&#1(?:2[89]|[3-9][0-9]);|&#226;&#128;&#15[2-9];/) {
    $encoding = "UTF-8";
  }  
  return decode($encoding, $s, Encode::FB_CROAK);
}

1;
