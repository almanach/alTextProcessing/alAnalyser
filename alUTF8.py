#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def altok_clean_utf8(s):
	s = s.encode("utf-8", errors = "replace")
	return s.decode("utf-8", errors = "strict")