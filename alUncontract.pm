package alUncontract;
use utf8;
use strict;
use alVariables;
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(&altok_uncontract);

sub altok_uncontract {
  my $s = shift;
  my $lang = shift;
  my $correct = shift || 0;

  if ($lang eq "en") {
    s/(?<=  )((?:some|every|any|no)(?:one|body))(s)(')(?=  )/{$1$2$3} $1 {} '$2/goi if $correct; # somebodys' > somebody's > somebody 's (and similar cases)

    s/(?<=  )([cC][aA])([Nn])('[tT])(?= )/{$1$2$3} $1$2 {} $2$3/go; # can't > can n't
    s/(?<=  )WON'T(?= )/{WON'T} WILL {} N'T/go; # won't > will n't
    s/(?<=  )(w)(o)(n't)(?= )/{$1$2$3} $1ill {} $3/goi; # (same)
    s/(?<=  )SHAN'T(?= )/{SHAN'T} SHALL {} N'T/go; # shan't > shall n't
    s/(?<=  )(sh)(a)(n't)(?= )/{$1$2$3} $1$2ll {} $3/goi; # (same)
    s/(?<=  )([^_ {}][^ {}]+)(n't)(?= )/{$1$2} $1 {} $2/goi;# <anythingElse>n't > <anythingElse> n't

    s/(?<= )([^ ]+'(?:ll|[rv]e|d))(just)(?= )/{$1$2} $1 {} $2/goi; # <anythingElse>'lljust > <anythingElse>'ll just [the "'ll" part will be dealt with later] (same with 're, 've, 'd)
    s/(?<= )(I)('(?:[mMdD]|[lL][lL]|[vV][eE]))(?= )/{$1$2} $1 {<} $2/go; # I'm > I 'm (same with 'd, 'll, 've)
    if ($correct) {
      s/(?<=  )i('(?:[mMdD]|[lL][lL]|[vV][eE]))(?= )/{i$2} I {} '$1/go;# i'm > I 'm (same with 'd, 'll, 've)
    } else {
      s/(?<=  )i('(?:[mMdD]|[lL][lL]|[vV][eE]))(?= )/{i$2} i {} '$1/go;# i'm > I 'm (same with 'd, 'll, 've)
    }
    s/(?<= )(you|he|she|we|they|there|this|that|it|who|how|what|where|why|when|hell|(?:no|any|some|every)(?:body|one)|people|alibi|guy|father|many|money)('(?:d|ll))(?= )/{$1$2} $1 {} $2/goi; # <adequate_token>'ll > <adequate_token> 'll (same with 'd)
    s/(?<= )(they|what|how|where|there|why|who|you|we)('re)(?= )/{$1$2} $1 $2/goi; # <adequate_token>'re > <adequate_token> 're
    if ($correct) {
      s/(?<=  )([îLl])('(?:d|ll|ve))(?= )/{$1$2} I {} $2/go; # l'd > I 'd (same with î and L as errors for I, and with 'll and 've)
      s/(?<=  )([Yy]öu|[yuû]|[Yy][eu]|[Yy]our)('(?:[vr]e|ll|d))(?= )/{$1$2} you {} $2/go; # yöu're > you 're (same with other variants, erroneous or not, of 'you' as well as 've, 'll and 'd)
      s/(?<=  )(ît)('(?:d|ll))(?= )/{$1$2} it {} $2/go; # ît'll > it 'll (same with 'd)
      s/(?<=  )(Lt)('(?:d|ll))(?= )/{$1$2} It {} $2/go; # Lt'll > It 'll (same with 'd)
    }
    s/(?<=  )(n't|'d)('ve)(?= )/{$1$2} $1 {} $2/goi; # n't've > n't 've
    s/{} (n't|'d)('ve)(?= )/{} $1 {} $2/goi; # n't've > n't 've (for cases when already detached)
    s/(?<= )(i|[a-z]{2,})('ve)(?= )/$1 '$2/goi;
    s/(?<= )(if|think|thought|something|kn[oe]w|what|but|that|like|believe)(you|I)('(?:d|[rv]e))\b/{$1$2$3} $1 {} $2 {} $3/goi;
    s/(?<=  )([^ {}]*[^ s_{}])('[sS])(?=  )/{$1$2} $1 {} $2/go;
    s/(?<=  )([^ {}]*[^ s_{}]){''} " ([smd]|re|ve|ll)(?= )/ {$1''$2} $1 {} '$2/goi;
    s/(?<=  )([^ _{}][^ {}]*s)'([a-z])(?=  )/{$1__APOS__$2} $1 {} __APOS__$2/goi;
    s/(?<=  )(jes|las)'(?=  )/$1__APOS__/goi; # jes' = jus' = just
    s/(?<=  )([^ _{}][^ {}]*)(s)(')(?![SsDd]  |[lL][lL].|\}..)/{$1$2$3} $1$2 {} $3$2/go;
    s/__APOS__/'/g;
    s/(?<= )([^ ]*[a-z]{2,})('ll)(?=  )/{$1$2} $1 {} $2/goi;

    s/(?<=  )(can|would|did|should|was|is|could|were|are|does|had|have|will|shall)(nae)(?= )/{$1$2} $1 {} $2/goi;
    s/(?<= )(I|you) (d)(i)(n)(nae)(?= )/$1 {$2$3$4$5} $2$3$2 {} $5/goi;
    s/(?<=  )(d)(i)(n)(nae) (ye|you)(?= )/{$1$2$3$4} $1$2$1 {} $4 $5/goi;
    s/^( *)([Dd])(in)(nae)(?= )/$1\{$2$3$4} $2o {} $4/go;
    s/^( *)(D)(IN)(NAE)(?= )/$1\{$2$3$4} $2O {} $4/go;
    s/^( *)(d)(in)(nae)(?= )/$1\{$2$3$4} $2o {} $4/goi;

    if ($correct) {
      s/(?<=  )(y')(know)(?= )/{$1} you {} $2/goi;
      s/(?<=  )(gon')(get)(?= )/{$1} gonna {} get/goi;

      s/(?<=  )([A-Z][A-Z'\-]*[A-Z']IN)(')(?=  )/ {$1$2} $1G/go;
      s/(?<=  )([a-z][a-z'\-]*[a-z']in)(')(?=  )/ {$1$2} $1g/goi;
      s/(?<=  )([A-Z][A-Z'\-]*[A-Z']IN)(')(E(?:ST|R))(?=  )/ {$1$2$3} $1G$3/go;
      s/(?<=  )([a-z][a-z'\-]*[a-z']in)(')(e(?:st|r))(?=  )/ {$1$2$3} $1g$3/goi;
      s/(?<=  )([A-Z][A-Z'\-]*[A-Z']IN)(')([a-z]+)/ {$1$2$3} $1G {} $3/go;
      s/(?<=  )([a-z][a-z'\-]*[a-z']in)(')([a-z]+)/ {$1$2$3} $1g {} $3/goi;
      s/{(som'in')} som'ing\b/{$1} something/goi;
    }
  }
  return $s;
}
