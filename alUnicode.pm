package alUnicode;
use utf8;
use strict;
use charnames ':full';
use Unicode::Normalize qw(NFD NFC);
use alVariables;
use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(&al_explainUnicodeSequence);

sub al_explainUnicodeSequence {
  my $s = shift;
  while ($s =~ m/(\X)/g ) {
    print al_explainUnicodeCharacter($1)."\n";
  }
}

sub al_explainUnicodeCharacter {
  my $grapheme = shift;
  my %pri = al_decomposeUnicodeSequence( $grapheme );
  my %base = al_decomposeUnicodeSequence( $pri{base} );
  my $output = <<END_OUTPUT;
Grapheme:($grapheme)
    Dec, Hex, Name:           [$pri{cp}], [$pri{hex_str}], '$pri{name}'
    Case: (Lower,Upper):      ($pri{lc}), ($pri{uc})
END_OUTPUT
  if ($pri{base} eq $grapheme) {
    $output .= "    No Combining Marks\n";    
  } else {
    $output .= "    Grapheme Base:            ($pri{base}), [$base{hex_str}], '$base{name}'\n";
  }
  foreach my $extend ( @{$pri{comb}} ) {
    my %ext = al_decomposeUnicodeSequence( $extend );
    $output .= <<END_OUTPUT;
    Combining Mark: ($ext{grapheme} )
        Dec, Hex, Name: [$ext{cp}], [$ext{hex_str}], '$ext{name}'
END_OUTPUT
  }
  return $output;
}


sub al_decomposeUnicodeSequence {
  my $grapheme = shift;
  my $decomp   = NFD( $grapheme );
  my $cp       = ord $grapheme;
  my ( $base ) = substr($decomp, 0, 1 );
  my ( @comb ) = map { substr $decomp, $_, 1 } 1 .. length($decomp)-1;
  return (
    grapheme => $grapheme,
    cp       => $cp,
    hex_str  => sprintf( "%#0.4x", $cp ),
    name     => charnames::viacode( $cp ),
    lc       => lc $grapheme,
    uc       => uc $grapheme,
    base     => $base,
    comb     => [ @comb ],
  );
}

1;
