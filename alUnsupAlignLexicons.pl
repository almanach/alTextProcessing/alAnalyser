#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

use alEditDistance;

my $trglex = shift;
my $srclex = shift;

my (%trglex, %srclex, $trgtotal, $srctotal);

die unless -r $trglex;
open TRGLEX, "<$trglex" || die;
binmode TRGLEX, ":utf8";
while (<TRGLEX>) {
  chomp;
  /^(\d+)\t(.*)$/ || die;
  next if $2 eq "";
  $trglex{$2} = $1;
  $trgtotal += $1;
}

die unless -r $srclex;
open SRCLEX, "<$srclex" || die;
binmode SRCLEX, ":utf8";
while (<SRCLEX>) {
  chomp;
  /^(\d+)\t(.*)$/ || die;
  next if $2 eq "";
  $srclex{$2} = $1;
  $srctotal += $1;
}

my ($tw, $sw, $ed);
my (%tw2best_sw, $best_ed, $transd, $ed_transd);
my $i;
my %weights;
my $maxiter = 2;
for my $iter (1..2) {
  for $tw (keys %trglex) {
    print STDERR (++$i).":$tw/".(scalar keys %trglex)."\r";
    next if $trglex{$tw} < 10;
    next unless lc($tw) eq $tw;
    $best_ed = 1000;
    if (defined($srclex{$tw}) && abs(log($trglex{$tw}/$trgtotal)-log($srclex{$tw}/$srctotal)) < 1) {
      $tw2best_sw{$tw} = $tw;
      $best_ed = 0;
    } else {
      for $sw (keys %srclex) {
	next if $srclex{$sw} < 2;
	next unless lc($sw) eq $sw;
	next if abs(log($trglex{$tw}/$trgtotal)-log($srclex{$sw}/$srctotal)) < 1;
	$ed = edit_distance_fast($tw,$sw,0);
	next if length($sw)/2-1 <= $ed;
	next if length($tw)/2-1 <= $ed;
	if ($ed < $best_ed) {
	  $tw2best_sw{$tw} = $sw;
	  $best_ed = $ed;
	}
      }
    }
    if ($best_ed <= 1 && $iter < $maxiter) {
      print STDERR "\n$tw\t$tw2best_sw{$tw}\t$best_ed\n" if defined($tw2best_sw{$tw});
      my $ed_transd = edit_distance($tw,$tw2best_sw{$tw},1,0);
      print STDERR "\n$ed_transd\n";
      $ed_transd =~ /^([\d\.e\+\-]*)\t(.*)$/ || die $ed_transd;
      $ed = $1;
      $transd = $2;
      for (split / /, $transd) {
	/^\((.*)\|(.*)\)$/ || die $transd;
	$weights{$1}{__ALL__}++;
	$weights{$1}{$2}++;
      }
    }
  }
  unless ($iter == $maxiter) {
    for my $c1 (sort {$weights{$b}{__ALL__} <=> $weights{$a}{__ALL__}} keys %weights) {
      for my $c2 (sort {$weights{$c1}{$b} <=> $weights{$c1}{$a}} keys %{$weights{$c1}}) {
	next if $c2 eq "__ALL__";
	update_edit_distance_weight($c1,$c2,1-sqrt($weights{$c1}{$c2}/$weights{$c1}{__ALL__}));
      }
    }
  }
}
for $tw (sort {$trglex{$a} <=> $trglex{$b}} keys %trglex) {
  print "$tw\t$tw2best_sw{$tw}\n";
}
