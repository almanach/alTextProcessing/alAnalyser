#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;
use alEditDistance;
use Text::LevenshteinXS qw(distance);

my $src_raw_text = shift || die;
my $trg_raw_text = shift || die;
my $src_conllu_data = shift || die;
my $bilex = shift || "";

my $max_iter = 10;

my $testmode = 0;

die unless -r $src_raw_text;
die unless -r $trg_raw_text;

my (%src2, %src_wds, $src_total);
my (%trg2, %trg_wds, $trg_total);
my (%srcconllu_wds, %srcconllu2);
my $l;

if (0) {
print STDERR "  Reading src raw text...";
open SRCRAW, "<$src_raw_text" || die $!;
binmode SRCRAW, ":utf8";
while (<SRCRAW>) {
  chomp;
  $l++;
  print STDERR "\r  Reading src raw text...$l/$src_total" if $l % 100 == 0;;
  s/^ */  /;
  s/ *$/  /;
  if ($src_total <= ($testmode ? 300000 : 30000000)) { #30000000
    while (s/^([^ ]+) +([^ ]+)/$2/) {
      $src2{$1." ".$2}++;
      $src_wds{$2}++;
      $src_total++;
    }
  } else {
    last
  }
}
close SRCRAW;
print STDERR "\r  Reading src raw text...done                         \n";
}

print STDERR "  Reading trg raw text...";
open TRGRAW, "<$trg_raw_text" || die $!;
binmode TRGRAW, ":utf8";
while (<TRGRAW>) {
  chomp;
  s/^ */ /;
  s/ *$/ /;
  if ($trg_total <= 30000000) { #30000000
    while (s/^([^ ]+) +([^ ]+)/$2/) {
      $trg2{$1." ".$2}++;
      $trg_wds{$2}++;
      $trg_total++;
    }
  }
}
close TRGRAW;
print STDERR "done\n";

my %src2trg;

print STDERR "  Reading src ConLL-U data...";
open SRCCONLLU, "<$src_conllu_data" || die $!;
binmode SRCCONLLU, ":utf8";
my ($cur, $prev, $prev2);
$prev = " ";
while (<SRCCONLLU>) {
  chomp;
  if (/^$/) {
    if ($prev ne " ") {
      die if $prev2 eq "";
      $srcconllu2{$prev2." ".$prev}++;
      $srcconllu_wds{$prev}++;
      $src_total++;
    }
    $prev2 = "";
    $prev = " ";
    next;
  }
  next unless /^(?:\d+(?:\.\d+)?)\t([^\t]+)/;
  $cur = $1;
  if ($prev2 ne "") {
    $srcconllu2{$prev2." ".$prev}++;
    $srcconllu_wds{$prev}++;
    $src_total++;
  }
  $prev2 = $prev;
  $prev = $cur;
}
close SRCCONLLU;
print STDERR "done\n";

if ($bilex) {
  print STDERR "  Reading seed bilingual lexicon...";
  open BILEX, "<$bilex" || die $!;
  binmode BILEX, ":utf8";
  my ($cur, $prev, $prev2);
  $prev = " ";
  while (<BILEX>) {
    chomp;
    /^([^\t]+)\t([^\t]+)\t([\d\.]+)/ || die $_;
    next unless $3 < 1;
    $src2trg{$2}{$1} = $3;
  }
  close BILEX;
  print STDERR "done\n";
}

my ($ed, $edctxt);
for (keys %src_wds) {
  $src2trg{$_}{$_} = 0 if defined($trg_wds{$_});
}

$l=0;
my ($t1, $t2, $s1, $s2);
for my $s (keys %srcconllu2) {
  $l++;
  next if $s =~ /\p{Punct}/;
  print STDERR "$l/".(scalar keys %srcconllu2)."\r";
  for my $t (keys %trg2) {
    next if $t =~ /\p{Punct}/;
    my $ed = distance(lc($s),lc($t));
    if ($ed <= (length($s)+length($t))/8-0.5) {
      $s =~ /^(.+) (.+)$/ || die $t;
      $s1 = $1; $s2 = $2;
      $t =~ /^(.+) (.+)$/ || die $t;
      $t1 = $1; $t2 = $2;
      my $ed1 = distance(lc($s1),lc($t1));
      if ($ed1 <= (length($s1)+length($t1))/8-0.5) {
	my $ed2 = distance(lc($s2),lc($t2));
	if ($ed2 <= (length($s2)+length($t2))/8-0.5) {
	  print STDERR "$s\t$t\t$ed\t$ed1\t$ed2\n";
	}
      }
    }
  }
}
exit(0);

my %weights;
for my $src (keys %src2trg) {
  for my $trg (keys %{$src2trg{$src}}) {
    next if $src eq $trg;
    my $transd = edit_distance($src,$trg,1);
    $transd =~ s/^[\d\.e\+\-]*\t(.*)$/$1/ || die $transd;
    for (split / /, $transd) {
      /^\((.*)\|(.*)\)$/ || die $transd;
      $weights{$1}{__ALL__}++;
      $weights{$1}{$2}++;
    }
  }
}
for my $c1 (sort {$weights{$b}{__ALL__} <=> $weights{$a}{__ALL__}} keys %weights) {
  for my $c2 (sort {$weights{$c1}{$b} <=> $weights{$c1}{$a}} keys %{$weights{$c1}}) {
    next if $c2 eq "__ALL__";
#    print STDERR "$c1\t$c2\t".(1-sqrt($weights{$c1}{$c2}/$weights{$c1}{__ALL__}))."\n";
    update_edit_distance_weight($c1,$c2,1-sqrt($weights{$c1}{$c2}/$weights{$c1}{__ALL__}));
  }
}

for my $src (keys %src2trg) {
  for my $trg (keys %{$src2trg{$src}}) {
    next if $src eq $trg;
    my $ed = ($src2trg{$src}{$trg} + edit_distance($src,$trg,0)/(length($src)+2))/2;
    if ($ed > 0.5) {
      delete $src2trg{$src}{$trg};
    } else {
      $src2trg{$src}{$trg} = $ed;
      print STDERR "$src\t$trg\t(l _ r)\t$ed\n";
    }
  }
}

print STDERR "==== unigrams ====\n";
$l = 0;
for my $src (keys %srcconllu_wds) {
  $l++;
  print STDERR "$l/".(scalar keys %srcconllu_wds)."\r";
  next if $testmode;
  next if defined($src2trg{$src});
  next if $src =~ /^[\d\p{Punct}]+$/;
  my $max_ed = 0.2;
  for my $trg (keys %trg_wds) {
    next if $src eq $trg;
#    next if $trg_wds{$trg} <= 3;
    next unless abs(length($trg)-length($src)) <= 3;
    next if $trg =~ /^[\d\p{Punct}]+$/;
    next unless has_common_substring($trg,$src,3);
    next if edit_distance_fast($trg,$src,0,$max_ed*(length($src)+2)) > $max_ed*(length($src)+2);
    $ed = edit_distance($src,$trg,0,$max_ed*(length($src)+2)) / (length($src)+2);
    $max_ed = $ed if $ed < $max_ed;
    next if $ed > 0.2;
    $ed += 0.4;
    $src2trg{$src}{$trg} = $ed;
    print STDERR "$src\t$trg\t( _ )\t$ed\n";
  }
}


print STDERR "  \"Translating\" src ConLL-U data...";

for my $src (sort keys %src2trg) {
  for my $trg (sort keys %{$src2trg{$src}}) {
    print STDERR "# TRANSFER $src\t$trg\t$src2trg{$src}{$trg}\n";
  }
}

open SRCCONLLU, "<$src_conllu_data" || die $!;
binmode SRCCONLLU, ":utf8";
my ($prev_line, $prev2_line);
my ($trg_prev, $trg_prev2, $trg_prev3);
$prev2 = "";
$prev = " ";
while (<SRCCONLLU>) {
  chomp;
#  print STDERR "PREV2:$prev2\nPREV:$prev\nCUR:$cur\n\n";
  if (/^#/ || /^\d+-\d+/) {
    next;
  }
  if (/^(?:\d+(?:\.\d+)?)\t([^\t]+)/) {
    $cur = $1;
  } elsif (/^$/) {
    $cur = " ";
  } else {
    next;
  }
  if ($prev2 ne "") {
    my $best = 1000;
    my $score;
    $trg_prev = "";
    my $transfer_level = 0;
    my $ncand = 0;
    $ncand = 0;
    if ($transfer_level == 0) {
      if (defined($src2trg{$prev})) {
	for my $trg_prev_candidate (keys %{$src2trg{$prev}}) {
	  $score = $src2trg{$prev}{$trg_prev_candidate};
	  $ncand++;
	  if (defined($trg_wds{$trg_prev2})) {
	    $score -= 0.1 if defined($trg2{$trg_prev2." ".$trg_prev_candidate});
	  }
	  if ($score < $best) {
	    $best = $score;
	    $trg_prev = $trg_prev_candidate;#."_1";
	    $transfer_level = 1;
	  }
	}
      }
    }
    if ($transfer_level == 0) {
      $trg_prev = $prev;
    }
    $prev_line =~ /^(\d+(?:\.\d+)?)\t[^\t]+\t(.*)$/ || die "|$prev_line|";
    print "# Transfer level $transfer_level: $prev > $trg_prev ($best) - #cand: $ncand\n";
    print $1."\t".$trg_prev."\t".$2."\n";
    $trg_prev3 = $trg_prev2;
    $trg_prev2 = $trg_prev;
  }
  if (/^$/) {
    print "\n";
    $prev2 = "";
    $prev2_line = "";
    $prev = " ";
    $prev_line = "";
    next;
  } else {
    $prev2 = $prev;
    $prev2_line = $prev_line;
    $prev = $cur;
    $prev_line = $_;
  }
}
close SRCCONLLU;
print STDERR "done\n";

sub has_common_substring {
  my ($s, $t, $l) = @_;
  my $sl = length($s);
  my $p;
  for (0..($sl-$l)) {
    return 1 if index ($t, substr($s,$_,$l)) > -1;
  }
  return 0;
}

sub has_common_substring_re {
  my ($s, $t, $l) = @_;
  die if $l < 2;
  $l--;
  my $p;
  while ($s =~ s/^(.)(.{$l})/$2/) {
    $p = quotemeta($1.$2);
    return 1 if $t =~ /$p/;
  }
  return 0;
}
