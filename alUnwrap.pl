#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

my $bool = 0;
while (<>) {
  chomp;
  if (/^$/) {
    print "\n";
    $bool = 0;
  } else {
    print " " if $bool;
    $bool = 1;
    print $_;
  }
}
print "\n" if $bool;

