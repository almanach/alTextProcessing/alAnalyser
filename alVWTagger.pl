#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

$|=1;

my $format = ""; # default value
my $output_format = ""; # default value
my $feats;
my (@toks, @rtoks, @tags, @ms, @col0, @col2, @col4, @col5, @col6ff, @spaces, @origtoks, @cmpnd_line);
my (%tag2tagid,%testtag2testtagid,$maxtagid,$maxtesttagid,@tagid2tag,@testtagid2testtag);
my (%ms2msid,%testms2testmsid,$maxmsid,$maxtestmsid,@msid2ms,@testmsid2testms);
my (%feat2featid,@featid2feat,$maxfeatid);
my (@model,%msmodel);
my (%lex,%flex, %mlex);
my %feat2occ;
my (%curfeats,@score,@msscore,@cursfeats);
my %training_corpus_words;
my (%token2tfeatid_cache, $token2tfeatid_cache_size, %token2ctfeatid_cache, $token2ctfeatid_cache_size, $cache_hits);
my (%mstd, $mstd);
my $token2tfeatid_max_cache_size = 10000;
my $token2ctfeatid_max_cache_size = 100000;

# best to date: $maxpreflength=4;$maxsufflength=5;$maxpreflength=4;$maxcontextualsufflength=5;$maxcontextualpreflength=2;$tokenwindowwidth=3;$tagwindowwidth=4

my $passnb=4;
my $minfeatocc=1;

my $maxsufflength=5;
my $maxpreflength=4;
my $maxcontextualsufflength=5;
my $maxcontextualpreflength=2;
my $tokenwindowwidth=3;
my $tokenwindowwidth2=2;
my $tagwindowwidth=4;
my $tagwindowwidth2=2;
#97.4424999%


my $force_delete_existing_model = 0;
my $keep_temporary_files = 0;
  
my $trainfile;
my $testfile;
my $conlljkretagfile;
my $lexiconfile;
my $modeldir;
my $modelname;
my $beamsize = 1;
my $try_flex = 0;
my $try_mlex = 0;
my $jk = 0; # jackknife mode (if > 0, stores the nb of slices)
my $jkmode = "rot";
my $printprobas = 0;
my $has_comments = 0;
my $ms_mode = 0;
my $wiw_mode = 0;
my $ms_feature_list = "";
my $ms_feature_excl_list = "";

my $is_xconll = 0;

my $filereadmode;

my $command = $0." ".join(" ",@ARGV);

#MODES:
#1=pretrain
#2=train
#3=test
#4=tag

my $n = max(max($tokenwindowwidth,$tokenwindowwidth2),max($tagwindowwidth,$tagwindowwidth2));

my $maj = qr/[A-ZA-ZΑ-ΩÀÁÂÃÄÅÆÇÈÉÊËÌÍÒÓÔÖØÚÛÜÝĄĆČĎĘĚĹĽŁŃŇŒŔŘŚŠŤŮŹŻΆΈΉΊΌΎΏἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍῬὙὛὝὟὨὩὪὫὬὭὮὯΪΫЁАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЬЭЮЯ]/o;

while (1) {
  $_ = shift;
  if (/^$/) {last}
  elsif (/^(-l|--lex(?:icon)?)$/) {$lexiconfile = shift || die "Option '-l' must be followed by a lexicon file"}
  elsif (/^--flex$/) {$try_flex = 1}
  elsif (/^--mlex$/) {$try_mlex = 1}
  elsif (/^--passnb$/) {$passnb = shift || die "Option '--passnb' must be followed by the number of passes"; die "'$passnb' is not a valid number of passes" unless $passnb =~ /^\d+$/}
  elsif (/^--test$/) {$testfile = shift || die "Option '--test' must be followed by a test file in Brown format (or CoNLL format if using -f conll)"}
  elsif (/^--conlljkretag$/) {$conlljkretagfile = shift || die "Option '--conlljkretag' must be followed by a CoNLL-U file in which the UPOS column will be replaced"}
  elsif (/^--train$/) {$trainfile = shift || die "Option '--train' must be followed by a training file in Brown format (or CoNLL format if using -f conll)"}
  elsif (/^--jk$/) {$jk = shift || die "Option '--jk' must be followed by the number of slices"; die "'$jk' is not a valid number of slices" unless $jk =~ /^\d+$/}
  elsif (/^--jkmode$/) {$jk = shift || die "Option '--jkmode' must be followed by a jackknifing mode ('rot' or 'split')"; die "'$jkmode' is not a valid jackknifing mode (known modes: 'rot' and 'split')" unless $jk =~ /^rot|split$/}
  elsif (/^(-m|--model)$/) {$modeldir = shift || die "Option '-m' must be followed by a model folder"}
  elsif (/^(-f|--format)$/) {
    $format = shift || die "Option '-f' or '--format' must be followed by a format name ('brown', 'conll' or 'xconll')";
    if ($format eq "xconll") {
      $format = "conll";
      $is_xconll = 1;
    }
  }
  elsif (/^(-of|--output_format)$/) {$output_format = shift || die "Option '-of' or '--output_format' must be followed by a format name ('brown' or 'conll')"}
  elsif (/^--?wiw$/) {$wiw_mode = 1}
  elsif (/^--ms_multiple_classifiers$/) {$ms_mode = 1}
  elsif (/^--ms$/) {$ms_mode = 2}
  elsif (/^--ms_feature_list$/) {$ms_feature_list = shift || die "Option '--ms_feature_list' must be followed by a comma-separated list of attributes that will be kept (example: '--ms_feature_list Case,Mood')"}
  elsif (/^--ms_feature_excl_list$/) {$ms_feature_excl_list = shift || die "Option '--ms_feature_excl_list' must be followed by a comma-separated list of attributes that will be discarded (example: '--ms_feature_excl_list Style,NameType,NumType,ConjType,NumForm,AdpType,Variant,Animacy,Abbr')"}
  elsif (/^--?mfo$/) {$minfeatocc = shift || die "Option '--mfo' must be followed by the minimum number of occurrences a feature must have to be used"; die "'$minfeatocc' is not a valid number of occurrences" unless $minfeatocc =~ /^\d+$/}
  elsif (/^--hc$/) {$has_comments = 1}
  elsif (/^--pp$/) {$printprobas = 1}
  elsif (/^--keep_temporary_files$/) {$keep_temporary_files = 1}
  elsif (/^--force_delete_existing_model$/) {$force_delete_existing_model = 1}
  #  elsif (/^-b$/) {$beamsize = shift || die "Option '-b' must be followed by an positive non-null integer (beam size)"; die "Option '-b' must be followed by an positive non-null integer (beam size)" unless $b =~ /^[1-9][0-9]*$/}
  elsif (/^--?h(?:elp)?$/) {
    print STDERR <<END;
Typical usage: cat input_file | perl alVWTagger.pl -m tagging_model

Options:
  Global and tagging option
  -m, --model <modeldir>			The tagging model is <modeldir> (create the model in train mode, use the model in tagging mode)
  -f [brown|conll|xconll],
    --format [brown|conll|xconll]		Input tagged files involved are in <format> format
  -of [brown|conll]				Output tagged files will be in <format> format
  -h, --help					Print this help message
  --hc						The input has SxPipe-like comments (between curly brackets) (default: no comments, everything must be tagged)
  --pp						Print probabilities associated with each tag

  Training options
  -l <lexfile>, --lex <lexfile>			Use external lexicon <lexfile>
  --flex					(TBD)
  --mlex					(TBD)
  --passnb <npasses>				In training mode, use <npasses> passes (default: 4)
  --train <trainfile>				Training mode: use train file <trainfile>. It must be in Brown format (or in CoNLL format if using option '-f conll' is specified)
  --test <testfile>				In training mode, use test file <testfile>. It must be in Brown format (or in CoNLL format if using option '-f conll' is specified)
  --jk <slicenb>				Use jackknifing mode, using <slicenb> slices (default = 0, i.e. no jackknifing mode)
  --conlljkretag <conllufile>			In jackknifing mode, specify the CoNLL-U file in which the UPOS column will be replaced using a jackknifing training/tagging strategy
  --jkmode [rot|split]				Use jackknifing mode 'rot' (every <slicenb>th sentence goes in test) or 'split' (the test is one of the <slicenb> consecutive <slicenb>ths of the file) (default: rot)
  --wiw						(TBD)
  --ms						Use a single (multi-features) morphological companion model
  --ms_multiple_classifiers			Use several morphological companion models, one per morphological features
  --ms_feature_list <featurelist>		Specify the feature list (comma-separated) to be used by the morphological model (example: '--ms_feature_list Case,Mood') (default: empty)
  --ms_feature_excl_list <featurelist>		Specify the feature list (comma-separated) to be discarder by the morphological model (example: '--ms_feature_excl_list Style,NameType,NumType,ConjType,NumForm,AdpType,Variant,Animacy,Abbr') (default: empty)
  --mfo						Specify the minimum number of occurrences a feature must have to be used
  --keep_temporary_files			(Debug: do not clean temporary files after training is completed)
  --force_delete_existing_model			(Debug: when asked to train a model which already exist, rather than refusing to train it, delete the existing model and do train a new one)
END
    exit(0);
  }
  else {die "Option '$_' unknown"}
}

$format = "brown" if $format eq "";
$output_format = $format if $output_format eq "";

die if $jk == 0 && $conlljkretagfile ne "";

#die "ERROR: option '-wiw' is not compatible with providing a test file; to evaluate your model, use it to tag the test file separately, then evaluate the result using the CoNLL 2017 evaluation script" if $wiw_mode && $testfile ne "";

die "ERROR: options '--ms_feature_list' and '--ms_feature_excl_list' cannot be used simultaneously" if $ms_feature_list ne "" && $ms_feature_excl_list ne "";

die "ERROR: invalid model folder name '$modeldir'" if $modeldir eq "" || $modeldir !~ /^[A-Za-z0-9\-_\.\/]+$/;
$modelname = $modeldir;
$modelname =~ s/^.*\///;
die "ERROR: invalid model folder name '$modeldir'" if $modelname eq "";

die "ERROR: option '-f' or '--format' must be followed by one of the following arguments: 'brown', 'conll' or 'xconll'" if $format !~ /^(brown|conll)$/;

die "ERROR: option '--force_delete_existing_model' can only be used in train mode (option '--train')" if $trainfile eq "" && $force_delete_existing_model;

die "ERROR: option '--jk' can only be used in CoNLL-jackknifed-train mode (option '--conlljkretag') and without any other mode (options '--test' or '--conllretag')" if $jk > 0 && ($conlljkretagfile eq "" || $testfile ne "");

if ($trainfile) {
  die "ERROR: training file '$trainfile' not found" unless -r $trainfile;
}
if ($conlljkretagfile) {
  die "ERROR: CoNLL file to be jackknifed-retagged '$conlljkretagfile' not found" unless -r $conlljkretagfile;
}
if ($testfile) {
  die "ERROR: testing file '$testfile' not found" unless -r $testfile;
}

if ($trainfile || $conlljkretagfile) {
  if (-d $modeldir) {
    if ($force_delete_existing_model) {
      `rm -f $modeldir/*`;
      `rmdir $modeldir`;
    } else {
      print STDERR "ERROR: folder '$modeldir' already exists\n";
      exit;
    }
  }
  `mkdir $modeldir`;
  `cp "$lexiconfile" $modeldir/$modelname.vwalex` unless $lexiconfile eq "";
} else {
  unless (-d $modeldir) {
    die "ERROR: model folder '$modeldir' does not exist";
  }
  $lexiconfile = $modeldir."/$modelname.vwalex" if $lexiconfile eq "";
}

if (!$trainfile && !$conlljkretagfile) {
  print STDERR "  Reading features...";
  my $line = 0;
  local $/;
  $/ = "\n";
  open FEATS, "<$modeldir/$modelname.vwaf" || die $!;
  binmode FEATS, ":utf8";
  while (<FEATS>) {
    chomp;
    /^(\d+)\t(.+)$/ || die "ERROR in feature file line $line: incorrect line '$_'";
    $feat2featid{$2} = $1;
    $featid2feat[$1] = $2;
    $maxfeatid = $1 if $maxfeatid < $1;
  }
  close FEATS;
  print STDERR "done\n";
  print STDERR "  Reading tags...";
  $line = 0;
  open TAGS, "<$modeldir/$modelname.vwat" || die $!;
  binmode TAGS, ":utf8";
  while (<TAGS>) {
    chomp;
    /^(\d+)\t(.*)$/ || die "ERROR in tag file line $line: incorrect line '$_'";
    $tag2tagid{$2} = $1;
    $tagid2tag[$1] = $2;
    $maxtagid = $1 if $maxtagid < $1;
  }
  close TAGS;
  print STDERR "done\n";
  print STDERR "  Reading mstags...";
  $line = 0;
  open MSTAGS, "<$modeldir/$modelname.vwamst" || die $!;
  binmode MSTAGS, ":utf8";
  while (<MSTAGS>) {
    chomp;
    /^(\d+)\t(.*)$/ || die "ERROR in mstag file line $line: incorrect line '$_'";
    $ms2msid{$2} = $1;
    $msid2ms[$1] = $2;
    $maxmsid = $1 if $maxmsid < $1;
  }
  close MSTAGS;
  print STDERR "done\n";
  print STDERR "  Reading training corpus lexicon...";
  open CLEX, "<$modeldir/$modelname.vwaclex" || die $!;
  binmode CLEX, ":utf8";
  while (<CLEX>) {
    chomp;
    $training_corpus_words{$_} = 1;
  }
  close CLEX;
  open TCOMMAND, "<$modeldir/$modelname.train_command" || die $!;
  binmode TCOMMAND, ":utf8";
  while (<TCOMMAND>) {
    chomp;
    next unless /^OVERALL COMMAND/;
    $try_flex = 1 if /--flex/;
    $try_mlex = 1 if /--mlex/;
    $ms_mode = 1 if /--ms_multiple_classifier( |$)/;
    $ms_mode = 2 if /--ms( |$)/;
    if (/--ms_feature_list\s+([^ ]+)( |$)/) {
      $ms_feature_list = $1;
    }
    if (/--ms_feature_excl_list\s+([^ ]+)( |$)/) {
      $ms_feature_excl_list = $1;
    }
  }
  close TCOMMAND;
  print STDERR "done\n";
}

my $ms_feature_list_re = $ms_feature_list;
$ms_feature_list_re =~ s/,/\|/g;
$ms_feature_list_re = qr/(?:$ms_feature_list_re)/;

my $ms_feature_excl_list_re = $ms_feature_excl_list;
$ms_feature_excl_list_re =~ s/,/\|/g;
$ms_feature_excl_list_re = qr/(?:$ms_feature_excl_list_re)/;

my $lex_is_flex;
if ($lexiconfile ne "") {
  print STDERR "  Reading lexicon '$lexiconfile'...";
  my $line = 0;
  local $/;
  $/ = "\n";
  open LEX, "<$lexiconfile" || die $!;
  binmode LEX, ":utf8";
  while (<LEX>) {
    $line++;
    chomp;
    unless (/^([^\t]+)\t([^\/\t]+)/) {
      print STDERR "ERROR in lexicon line $line: skipping incorrect entry '$_'\n";
      next;
    }
    my ($f, $t) = ($1, $2);
    $f =~ s/ / /g;
    $flex{$f}{$t} = 1;
    if ($try_flex || $try_mlex) {
      $lex_is_flex = 1 if $t =~ s/^(.+?)\#(.*)/\1/;
      if ($try_mlex) {
	my $m = $2;
	for (split /\|/, $m) {
	  next if /^$/;
	  $mlex{$f}{$t}{$_} = 1;# if /^(Case|VerbForm|PronForm|Gender|Number)=/;
	}
      }
    }
    $lex{$f}{$t} = 1;
  }
  close LEX;
  print STDERR "done ($lex_is_flex)\n";
}


my ($maxscore,$bestc,$bestms);

if ($jk == 0) {
  train($trainfile,$modelname,$format) if $trainfile;
  read_model($modelname) if !$trainfile || $testfile;
  test($testfile,$modelname,$format) if $testfile;
  evaluate($modelname) if $testfile;
  process_file(4,"-",$format) if !$trainfile && !$testfile; #tag
} else {
  die unless $trainfile || $conlljkretagfile;
  my $inputfile = $trainfile || $conlljkretagfile;
  die if $testfile;
  print STDERR "  Generating $jk splits...";
  my @trainsplits;
  my @testsplits;
  for (1..$jk) {
    open $trainsplits[$_], ">$modeldir/trainsplit.$_";
    binmode $trainsplits[$_], ":utf8";
    open $testsplits[$_], ">$modeldir/testsplit.$_";
    binmode $testsplits[$_], ":utf8";
  }
  my $l = 0;
  local $/;
  if ($conlljkretagfile ? "conll" : "brown") {
    $/ = "\n\n";
  } else {
    $/ = "\n";
  }
  my $nb_sentences_in_input_file = 0;
  if ($jkmode eq "split") {
    open DATA, "<$inputfile" || die $!;
    binmode DATA, ":utf8";
    while (<DATA>) {
      $nb_sentences_in_input_file++;
    }
    close DATA;
  }
  open DATA, "<$inputfile" || die $!;
  binmode DATA, ":utf8";
  while (<DATA>) {
    $l++;
    for my $i (1..$jk) {
      if ($jkmode eq "rot") {
	print {$trainsplits[$i]} $_ unless $l % $jk == $i % $jk;
	print {$testsplits[$i]} $_ if $l % $jk == $i % $jk;
      } elsif ($jkmode = "split") {
	if (int($l / $nb_sentences_in_input_file) == $i
	    || (int($l / $nb_sentences_in_input_file) == 0 && $i == $jk)) {
	  print {$testsplits[$i]} $_;
	} else {
	  print {$trainsplits[$i]} $_ unless $l % $jk == $i % $jk;
	}
      } else {die}
    }
  }
  close DATA;
  for (1..$jk) {
    close $trainsplits[$_];
    close $testsplits[$_];
  }
  print STDERR "done\n";
  my ($correct, $total, $correct_ootrain, $total_ootrain, $mscorrect, $mscorrect_ootrain);
  for my $i (1..$jk) {
    print STDERR "  Training tagger for split $i\n";
    `mkdir "$modeldir/jkmodel.$i"`;
    %training_corpus_words = ();
    %feat2occ = ();
    %curfeats = ();
    @model = ();
    train("$modeldir/trainsplit.$i","jkmodel.$i",$conlljkretagfile ? "conll" : "brown");
    unless ($wiw_mode) {
      read_model("jkmodel.$i");
      test("$modeldir/testsplit.$i","jkmodel.$i",$conlljkretagfile ? "conll" : "brown");
      my ($split_correct, $split_total, $split_correct_ootrain, $split_total_ootrain, $split_mscorrect, $split_mscorrect_ootrain) = evaluate("jkmodel.$i");
      $correct += $split_correct;
      $total += $split_total;
      $correct_ootrain += $split_correct_ootrain;
      $total_ootrain += $split_total_ootrain;
      $mscorrect += $split_mscorrect;
      $mscorrect_ootrain += $split_mscorrect_ootrain;
    }
  }
  my (@pred, @gold);
  open PRED, ">$modeldir/$modelname.testingdata.pred" || die $!;
  open GOLD, ">$modeldir/$modelname.testingdata.gold" || die $!;
  binmode PRED, ":utf8";
  binmode GOLD, ":utf8";
  for (1..$jk) {
    open $pred[$_], "<$modeldir/jkmodel.$_.testingdata.pred" || die $!;
    open $gold[$_], "<$modeldir/jkmodel.$_.testingdata.gold" || die $!;
    binmode $pred[$_], ":utf8";
    binmode $gold[$_], ":utf8";
  }
  open DATA, "<$inputfile" || die $!;
  binmode DATA, ":utf8";
  $l = 0;
  my $i = 0;
  my ($fh, $predsent, $goldsent, $origsent);
  while (chomp ($origsent = <DATA>)) {
    $l++;
    if ($jkmode eq "rot") {
      $i++;
      $i = 1 if $i == $jk+1;
    } elsif ($jkmode eq "split") {
      $i = int($l/$nb_sentences_in_input_file);
      $i = $jk if $i == 0;
    } else {die}
    $fh = $pred[$i];
    chomp ($predsent = <$fh>);
    $fh = $gold[$i];
    chomp ($goldsent = <$fh>);
    print PRED $predsent."\n";
    print GOLD $goldsent."\n";
    my @p = ();
    my @pms = ();
    for (split /[\n\r]+/s, $predsent) {
      if ($ms_mode) {
	/^([^\t]+)#([^\t#]*)\t/ || die "|$_|";
	push @p, $1;
	push @pms, $2;
      } else {
	/^([^\t]+)\t/ || die "|$_|";
	push @p, $1;
      }
    }
    my $k = -1;
    for (split /[\n\r]+/s, $origsent) {
      if (/^#/) {
	print $_."\n";
      } elsif (/^\d+-\d+/) {
	print $_."\n";
      } elsif (/^(\d+(?:\.\d+)?\t[^\t]+\t[^\t]+\t)[^\t]+(\t[^\t]*\t)([^\t]+)(.*)$/) {
	$k++;
	if ($ms_mode) {
	  print $1.$p[$k].$2.($pms[$k] || "_").$4."\n";
	} else { 
	  print $1.$p[$k].$2.$3.$4."\n";
	}
      } else {die $_}
    }
    print $_."\n";
  }
  close DATA;
  for (1..$jk) {
    close $pred[$_];
    close $gold[$_];
  }
  unless ($wiw_mode) {
    open LOG, ">$modeldir/$modelname.eval_log";
    binmode LOG, ":utf8";
    print LOG "  Overall accuracy: $correct/$total = ".(int(100000*$correct/$total+0.4999)/1000)."%\n";
    print STDERR "  Overall accuracy: $correct/$total = ".(int(100000*$correct/$total+0.4999)/1000)."%\n";
    if ($total_ootrain) {
      print LOG "  OOTC accuracy: $correct_ootrain/$total_ootrain = ".(int(100000*$correct_ootrain/$total_ootrain+0.4999)/1000)."%\n";
      print STDERR "  OOTC accuracy: $correct_ootrain/$total_ootrain = ".(int(100000*$correct_ootrain/$total_ootrain+0.4999)/1000)."%\n";
    } else {
      print LOG "  OOTC accuracy: -no OOTC-\n";
      print STDERR "  OOTC accuracy: -no OOTC-\n";
    }
    close LOG;
  }
}

unless ($keep_temporary_files) {
  if ($trainfile || $testfile) {
    `rm -f $modeldir/$modelname.{training,testing}data*`;
  }
}

sub train {
  my ($trainfile, $modelname, $format) = @_;
  open COMMAND, ">$modeldir/$modelname.train_command";
  binmode COMMAND, ":utf8";
  print COMMAND "OVERALL COMMAND: $command\n";
  print COMMAND "PARAMETERS: \$maxsufflength=$maxsufflength;\$maxpreflength=$maxpreflength;\$maxcontextualsufflength=$maxcontextualsufflength;\$maxcontextualpreflength=$maxcontextualpreflength;\$tokenwindowwidth=$tokenwindowwidth;\$tokenwindowwidth2=$tokenwindowwidth2;\$tagwindowwidth=$tagwindowwidth;\$tagwindowwidth2=$tagwindowwidth2\n";
  print STDERR "OVERALL COMMAND: $command\n";
  print STDERR "PARAMETERS: \$maxsufflength=$maxsufflength;\$maxpreflength=$maxpreflength;\$maxcontextualsufflength=$maxcontextualsufflength;\$maxcontextualpreflength=$maxcontextualpreflength;\$tokenwindowwidth=$tokenwindowwidth;\$tokenwindowwidth2=$tokenwindowwidth2;\$tagwindowwidth=$tagwindowwidth;\$tagwindowwidth2=$tagwindowwidth2\n";
  print STDERR "TRAINING FILE: $trainfile\n";
  print STDERR "  Generating training data...";
  if ($minfeatocc > 1 || $ms_mode) {
    process_file(1,$trainfile,$format);		#pretrain
  }
  process_file(2,$trainfile,$format,$modelname);		#train

  open FEATS, ">$modeldir/$modelname.vwaf" || die $!;
  binmode FEATS, ":utf8";
  for (sort {$feat2featid{$a} <=> $feat2featid{$b}} keys %feat2featid) {
    print FEATS "$feat2featid{$_}\t$_\n";
    $featid2feat[$feat2featid{$_}] = $_;
  }
  close FEATS;
  open TAGS, ">$modeldir/$modelname.vwat" || die $!;
  binmode TAGS, ":utf8";
  for (sort {$tag2tagid{$a} <=> $tag2tagid{$b}} keys %tag2tagid) {
    print TAGS "$tag2tagid{$_}\t$_\n";
    $tagid2tag[$tag2tagid{$_}] = $_;
  }
  close TAGS;
  if ($ms_mode) {
    open MSTAGS, ">$modeldir/$modelname.vwamst" || die $!;
    binmode MSTAGS, ":utf8";
    for (sort {$ms2msid{$a} <=> $ms2msid{$b}} keys %ms2msid) {
      print MSTAGS "$ms2msid{$_}\t$_\n";
      $msid2ms[$ms2msid{$_}] = $_;
    }
    close MSTAGS;
  }
  open CLEX, ">$modeldir/$modelname.vwaclex" || die $!;
  binmode CLEX, ":utf8";
  print CLEX $_."\n" for keys %training_corpus_words;
  close CLEX;

  print STDERR "done\n";
  print STDERR "  Training using vowpal wabbit\n";
  die if $maxtagid == 0;
  my $vwcommand = "vw --noconstant --cache_file $modeldir/$modelname.cache_train -k --oaa $maxtagid -b ".(1+int(log((1+$maxtagid)*(1+$maxfeatid))/log(2)))." --passes $passnb --readable_model $modeldir/$modelname.vwarm $modeldir/$modelname.trainingdata.brown";

  print COMMAND "Vowpal Wabbit command: $vwcommand\n";
  print STDERR "Vowpal Wabbit command: $vwcommand\n";
  `$vwcommand`;
  if ($ms_mode) {
    print STDERR "  Training MS using vowpal wabbit\n";
    die if $maxmsid == 0;
    if ($ms_mode == 2) {
      $vwcommand = "vw --noconstant --cache_file $modeldir/$modelname.cache_train -k --oaa $maxmsid -b ".(1+int(log((1+$maxmsid)*(1+$maxfeatid))/log(2)))." --passes $passnb --readable_model $modeldir/$modelname.vwamsrm $modeldir/$modelname.mstrainingdata.brown";
      
      print COMMAND "Vowpal Wabbit command (MS): $vwcommand\n";
      print STDERR "Vowpal Wabbit command (MS): $vwcommand\n";
      `$vwcommand`;
    } elsif ($ms_mode == 1) {
      for (keys %tag2tagid) {
	$vwcommand = "vw --noconstant --cache_file $modeldir/$modelname.$_.cache_train -k --oaa $maxmsid -b ".(1+int(log((1+$maxmsid)*(1+$maxfeatid))/log(2)))." --passes $passnb --readable_model $modeldir/$modelname.$_.vwamsrm $modeldir/$modelname.mstrainingdata.$_.brown";
	
	print COMMAND "Vowpal Wabbit command (MS:$_): $vwcommand\n";
	print STDERR "Vowpal Wabbit command (MS:$_): $vwcommand\n";
	`$vwcommand`;
      }
    }
  }
  close COMMAND;
}

sub test {
  my ($testfile, $modelname, $format) = @_;
  print STDERR "  Tagging test data '$testfile' ($format format)...";
  open PRED, ">$modeldir/$modelname.testingdata.pred";
  binmode PRED, ":utf8";
  open GOLD, ">$modeldir/$modelname.testingdata.gold";
  binmode GOLD, ":utf8";
  process_file(3,$testfile,$format,"$modeldir/$modelname");# test
  print STDERR "done\n";
  close PRED;
  close GOLD;
}

sub evaluate {
  my ($modelname) = @_;
  my ($total, $correct, $total_ootrain, $correct_ootrain, $correct_coarse, $correct_ootrain_coarse, $mscorrect, $mscorrect_ootrain);
  my ($pred, $gold, $tok, $coarsepred, $coarsegold, $mspred, $msgold);
  my (%conf_mat, %seen_gold_tags);
  open PRED, "<$modeldir/$modelname.testingdata.pred" || die $!;
  open GOLD, "<$modeldir/$modelname.testingdata.gold" || die $!;
  binmode PRED, ":utf8";
  binmode GOLD, ":utf8";
  open DIFF, ">$modeldir/$modelname.testingdata.diff";
  binmode DIFF, ":utf8";
  local $/;
  $/ = "\n";
  while (<PRED>) {
    chomp($pred = $_);
    chomp($gold = <GOLD> || die);
    if ($gold eq "") {
      next if $pred eq "";
      die;
    }
    $gold =~ s/^([^\t]+)\t(.*)$/\1/ || die "|$gold|\n";
    $tok = $2;
    $pred =~ s/\t.*//;
    $seen_gold_tags{$gold} = 1;
    $coarsepred = $pred;
    $coarsegold = $gold;
    $mspred = "";
    $msgold = "";
    if ($coarsepred =~ s/#(.*)//) {
      $mspred = $1;
    }
    if ($coarsegold =~ s/#(.*)//) {
      $msgold = $1;
    }
    $total++;
    $correct++ if $pred eq $gold;
    $mscorrect++ if $mspred eq $msgold;
    $correct_coarse++ if $coarsepred eq $coarsegold;
    print DIFF "$tok\t$gold\t$pred\n" if $pred ne $gold;
    unless (defined($training_corpus_words{$tok})) {
      $total_ootrain++;
      $correct_ootrain++ if $pred eq $gold;
      $mscorrect_ootrain++ if $mspred eq $msgold;
      $correct_ootrain_coarse++ if $coarsepred eq $coarsegold;
    }
    $conf_mat{$pred}{$gold}++;
  }
  close PRED;
  close GOLD;
  close DIFF;
  open LOG, ">$modeldir/$modelname.eval_log";
  binmode LOG, ":utf8";
  print LOG "\t".$_ for (sort {$a cmp $b} keys %seen_gold_tags);
  print LOG "\n";
  for $pred (sort {$a cmp $b} keys %conf_mat) {
    print LOG $pred;
    print LOG "\t".$conf_mat{$pred}{$_} for (sort {$a cmp $b} keys %seen_gold_tags);
    print LOG "\n";
  }
  print LOG "  Overall accuracy: $correct/$total = ".(int(100000*$correct/$total+0.4999)/1000)."%\n";
  print STDERR "  Overall accuracy: $correct/$total = ".(int(100000*$correct/$total+0.4999)/1000)."%\n";
  if ($total_ootrain) {
    print LOG "  OOTC accuracy: $correct_ootrain/$total_ootrain = ".(int(100000*$correct_ootrain/$total_ootrain+0.4999)/1000)."%\n";
    print STDERR "  OOTC accuracy: $correct_ootrain/$total_ootrain = ".(int(100000*$correct_ootrain/$total_ootrain+0.4999)/1000)."%\n";
  } else {
    print LOG "  OOTC accuracy: -no OOTC-\n";
    print STDERR "  OOTC accuracy: -no OOTC-\n";
  }
  print LOG "  Overall coarse accuracy: $correct_coarse/$total = ".(int(100000*$correct_coarse/$total+0.4999)/1000)."%\n";
  print STDERR "  Overall coarse accuracy: $correct_coarse/$total = ".(int(100000*$correct_coarse/$total+0.4999)/1000)."%\n";
  if ($total_ootrain) {
    print LOG "  OOTC coarse accuracy: $correct_ootrain_coarse/$total_ootrain = ".(int(100000*$correct_ootrain_coarse/$total_ootrain+0.4999)/1000)."%\n";
    print STDERR "  OOTC coarse accuracy: $correct_ootrain_coarse/$total_ootrain = ".(int(100000*$correct_ootrain_coarse/$total_ootrain+0.4999)/1000)."%\n";
  } else {
    print LOG "  OOTC coarse accuracy: -no OOTC-\n";
    print STDERR "  OOTC coarse accuracy: -no OOTC-\n";
  }
  if ($ms_mode) {
    print LOG "  Overall MS accuracy: $mscorrect/$total = ".(int(100000*$mscorrect/$total+0.4999)/1000)."%\n";
    print STDERR "  Overall MS accuracy: $mscorrect/$total = ".(int(100000*$mscorrect/$total+0.4999)/1000)."%\n";
    if ($total_ootrain) {
      print LOG "  OOTC MS accuracy: $mscorrect_ootrain/$total_ootrain = ".(int(100000*$mscorrect_ootrain/$total_ootrain+0.4999)/1000)."%\n";
      print STDERR "  OOTC MS accuracy: $mscorrect_ootrain/$total_ootrain = ".(int(100000*$mscorrect_ootrain/$total_ootrain+0.4999)/1000)."%\n";
    } else {
      print LOG "  OOTC MS accuracy: -no OOTC-\n";
      print STDERR "  OOTC MS accuracy: -no OOTC-\n";
    }
  }
  close LOG;
  return ($correct, $total, $correct_ootrain, $total_ootrain, $mscorrect, $mscorrect_ootrain);
}

sub add_current_token_features {
  my ($i,$t) = @_;
  my $w = $toks[$i];
  if ($wiw_mode) {
    for (reverse 0..$i-1) {
      if ($tags[$_] eq "INWORD") {
	$w = $toks[$_].$w;
      } else {last}
    }
  }
  my $length = length($w);
  if ($filereadmode >= 2 && defined($token2tfeatid_cache{$w})) {
    $cache_hits++;
    for (@{$token2tfeatid_cache{$w}}) {
      if ($filereadmode == 2) { # train
	$curfeats{$_}++;
      } else { # test || tag
	if ($t eq "") {
	  update_score($_);
	} else {
	  update_msscore($_,$t);
	}
      }
    }
  } else {
    add_feature("w=$w",1,$w,$t);
    for my $j (1..$maxpreflength) {
      add_feature("p$j=".substr($w,0,$j),1,$w,$t) if $length >= $j;
    }
    for my $j (1..$maxsufflength) {
      add_feature("s$j=".substr($w,-$j),1,$w,$t) if $length >= $j;
    }
    add_feature("nb=".($w =~ /[0-9]/ ? "1" : "0"),1,$w,$t);
    add_feature("an=".($w =~ /^[0-9\.,]+$/ ? "1" : "0"),1,$w,$t);
    add_feature("hy=".($w =~ /-/ ? "1" : "0"),1,$w,$t);
    add_feature("uc=".($w =~ /$maj/o ? "1" : "0"),1,$w,$t);
    add_feature("au=".($w =~ /^$maj+$/o ? "1" : "0"),1,$w,$t);
    add_feature("ni=".(($i > $n && $w =~ /$maj/o) ? "1" : "0"),1,$w,$t);
  }
}

sub add_contextual_token_features {
  my ($i,$t) = @_;
  for my $k (-$tokenwindowwidth..$tokenwindowwidth) {
    next if $k == 0;
    my $w = $toks[$i+$k];
    my $length = length($w);
    add_feature("w($k)=$w",0,"",$t);
  }
  if ($tokenwindowwidth > 0 && $t eq "") {
    add_feature("w(-1#0#1)=$toks[$i-1]#$toks[$i]#$toks[$i+1]",0,"",$t);
    add_feature("w(-1#0)=$toks[$i-1]#$toks[$i]",0,"",$t);
    add_feature("w(-1#1)=$toks[$i-1]#$toks[$i+1]",0,"",$t);
  }
  for my $k (-$tokenwindowwidth2..$tokenwindowwidth2) {
    next if $k == 0;
    my $w = $toks[$i+$k];
    my $length = length($w);
    my $key = "$w($k)";
    unless ($w eq "__#__") {
      if ($filereadmode >= 2 && defined($token2ctfeatid_cache{$key})) {
	$cache_hits++;
	for (@{$token2ctfeatid_cache{$key}}) {
	  if ($filereadmode == 2) { # train
	    $curfeats{$_}++;
	  } else {		# test || tag
	    if ($t eq "") {
	      update_score($_);
	    } else {
	      update_msscore($_,$t);
	    }
	  }
	}
      } else {
	$token2ctfeatid_cache_size++;
	for my $j (1..$maxcontextualpreflength) {
	  add_feature("p$j($k)=".substr($w,0,$j),2,$key,$t) if $length >= $j;
	}
	for my $j (1..$maxcontextualsufflength) {
	  add_feature("s$j($k)=".substr($w,-$j),2,$key,$t) if $length >= $j;
	}
	add_feature("nb($k)=".($w =~ /[0-9]/ ? "1" : "0"),2,$key,$t);
	add_feature("an($k)=".($w =~ /^[0-9\.,]+$/ ? "1" : "0"),2,$key,$t);
	add_feature("hy($k)=".($w =~ /-/ ? "1" : "0"),2,$key,$t);
	add_feature("uc($k)=".($w =~ /$maj/o ? "1" : "0"),2,$key,$t);
	add_feature("au($k)=".($w =~ /^$maj+$/o ? "1" : "0"),2,$key,$t);
	add_feature("ni($k)=".(($i+$k > $n && $w =~ /$maj/o) ? "1" : "0"),2,$key,$t);
      }
    }
  }
}

sub add_contextual_tag_features {
  my ($i,$t) = @_;
  for my $k (-$tagwindowwidth..-1) {
    next if $k == 0;
    add_feature("t($k)=".$tags[$i+$k],0,"",$t);
  }
  add_feature("t(-2#-1)=$tags[$i-2]#$tags[$i-1]",0,"",$t) if $tagwindowwidth2 >= 2;
  add_feature("t(-3#-2#-1)=$tags[$i-3]#$tags[$i-2]#$tags[$i-1]",0,"",$t) if $tagwindowwidth2 >= 3;
  add_feature("t(-4#-3#-2#-1)=$tags[$i-4]#$tags[$i-3]#$tags[$i-2]#$tags[$i-1]",0,"",$t) if $tagwindowwidth2 >= 4;
}

sub add_curtag_features {
  my ($i,$t) = @_;
  add_feature("t(0)=".$tags[$i],0,"",$t);
  add_feature("t(-1#0)=$tags[$i-1]#$tags[$i]",0,"",$t) if $tagwindowwidth2 >= 1;
  add_feature("t(-2#-1#0)=$tags[$i-2]#$tags[$i-1]#$tags[$i]",0,"",$t) if $tagwindowwidth2 >= 2;
  add_feature("t(-3#-2#-1#0)=$tags[$i-3]#$tags[$i-2]#$tags[$i-1]#$tags[$i]",0,"",$t) if $tagwindowwidth2 >= 3;
}

sub add_current_lexical_features {
  my ($i,$t) = @_;
  my $w = $toks[$i];
  if ($wiw_mode) {
    for (reverse 0..$i-1) {
      if ($tags[$_] eq "INWORD") {
	$w = $toks[$_].$w;
      } else {last}
    }
  }
  $w = lc($w) if !defined($lex{$w}) && defined($lex{lc($w)});
  if (defined($lex{$w})) {
    my $featsuff = "u";
    if (scalar keys %{$lex{$w}} > 1) {
      $featsuff = "i";
    }
    if (!$lex_is_flex || !$try_flex) {
      if (scalar keys %{$lex{$w}} > 1) {
	add_feature("ld=".(join "|", sort keys %{$lex{$w}}),0,"",$t);
      }
      for (keys %{$lex{$w}}) {
	add_feature("l$featsuff=$_",0,"",$t);
      }
    }
    if ($lex_is_flex && $try_mlex) {
      for my $lexlabel (keys %{$mlex{$w}}) {
    	for my $m (keys %{$mlex{$w}{$lexlabel}}) {
    	  add_feature("lm$featsuff($lexlabel).$m",0,"",$t);
    	}
      }
    }
    if ($lex_is_flex && $try_flex) {
      if (scalar keys %{$flex{$w}} > 1) {
	$featsuff = "i";
	add_feature("fld=".(join "|", sort keys %{$flex{$w}}),0,"",$t);
      }
      for (keys %{$flex{$w}}) {
	add_feature("fl$featsuff=$_",0,"",$t);
      }
    }
  } else {
    add_feature("l=__UNKNOWN__",0,"",$t);
  }
}

sub add_contextual_lexical_features {
  my ($i,$t) = @_;
  for my $k (1..$tokenwindowwidth) {
    next if $k == 0;
    my $w = $toks[$i+$k];
    $w = lc($w) if !defined($lex{$w}) && defined($lex{lc($w)});
    if ($w eq "__#__") {
      add_feature("l($k)=__#__",0,"",$t);
    } elsif (defined($lex{$w})) {
      my $featsuff = "u";
      if (scalar keys %{$lex{$w}} > 1) {
	$featsuff = "i";
	add_feature("ld($k)=".(join "|", sort keys %{$lex{$w}}),0,"",$t);
      }
      for my $lexlabel (keys %{$lex{$w}}) {
	add_feature("l$featsuff($k)=$lexlabel",0,"",$t);
	if ($lex_is_flex && $try_mlex && $k == 1) {
	  for my $m (keys %{$mlex{$w}{$lexlabel}}) {
	    add_feature("lm$featsuff($k).$m",0,"",$t);
	  }
	}
      }
    } else {
      add_feature("l($k)=__UNKNOWN__",0,"",$t);
    }
  }
}

sub add_contextual_mixed_features {
  my ($i,$t) = @_;
  if ($tagwindowwidth >= 1) {
    my $f = $toks[$i+1];
    if ($lexiconfile ne "") {
      if (defined($lex{$f}) == 1) {
	add_feature("w#lu(1)=".$toks[$i]."##".(join "#", sort keys %{$lex{$f}}),0,"",$t) if $tagwindowwidth >= 1;
	add_feature("t(-1)#lu(1)=".$tags[$i-1]."##".(join "#", sort keys %{$lex{$f}}),0,"",$t) if $tagwindowwidth >= 1;
	add_feature("t(-2#-1)#lu(1)=".$tags[$i-2]."#".$tags[$i-1]."##".(join "#", sort keys %{$lex{$f}}),0,"",$t) if $tagwindowwidth2 >= 2;
      } else {
	add_feature("w#lu(1)=".$toks[$i]."##"."##__UNKNOWN__",0,"",$t) if $tagwindowwidth >= 1;
	add_feature("t(-1)#lu(1)=".$tags[$i-1]."##__UNKNOWN__",0,"",$t) if $tagwindowwidth >= 1;
	add_feature("t(-2#-1)#lu(1)=".$tags[$i-2]."#".$tags[$i-1]."##__UNKNOWN__",0,"",$t) if $tagwindowwidth2 >= 2;
      }
    }
    add_feature("t(-1)#w=".$tags[$i-1]."##".substr("#####$toks[$i]",-5),0,"",$t) if $tagwindowwidth2 >= 1;
    add_feature("t(-2#-1)#w=".$tags[$i-2]."#".$tags[$i-1]."##".$toks[$i],0,"",$t) if $tagwindowwidth2 >= 2;
    add_feature("t(-3#-2#-1)#w=".$tags[$i-3]."#".$tags[$i-2]."#".$tags[$i-1]."##".$toks[$i],0,"",$t) if $tagwindowwidth2 >= 3;
    add_feature("t(-4#-2#-1)#w=".$tags[$i-4]."#".$tags[$i-3]."#".$tags[$i-2]."#".$tags[$i-1]."##".$toks[$i],0,"",$t) if $tagwindowwidth2 >= 4; # NAME OF FEATURE: TO BE DEBUGGED
  }
}

sub add_contextual_mixed_features2 {
  my ($i,$t) = @_;
  if ($tagwindowwidth >= 1) {
    my $f = $toks[$i+1];
    if ($lexiconfile ne "") {
      if (defined($lex{$f}) == 1) {
	add_feature("t(0)#lu(1)=".$tags[$i]."##".(join "#", sort keys %{$lex{$f}}),0,"",$t) if $tagwindowwidth >= 1;
	add_feature("t(-1#0)#lu(1)=".$tags[$i-1]."#".$tags[$i]."##".(join "#", sort keys %{$lex{$f}}),0,"",$t) if $tagwindowwidth >= 1;
	add_feature("t(-2#-1#0)#lu(1)=".$tags[$i-2]."#".$tags[$i-1]."#".$tags[$i]."##".(join "#", sort keys %{$lex{$f}}),0,"",$t) if $tagwindowwidth2 >= 2;
      } else {
	add_feature("t(-1#0)#lu(1)=".$tags[$i-1]."#".$tags[$i]."##__UNKNOWN__",0,"",$t) if $tagwindowwidth >= 1;
	add_feature("t(-2#-1#0)#lu(1)=".$tags[$i-2]."#".$tags[$i-1]."#".$tags[$i]."##__UNKNOWN__",0,"",$t) if $tagwindowwidth2 >= 2;
      }
    }
    add_feature("t(-1#0)#w=".$tags[$i-1]."#".$tags[$i]."##".$toks[$i],0,"",$t) if $tagwindowwidth2 >= 1;
    add_feature("t(-2#-1#0)#w=".$tags[$i-2]."#".$tags[$i-1]."#".$tags[$i]."##".$toks[$i],0,"",$t) if $tagwindowwidth2 >= 2;
    add_feature("t(-3#-2#-1#0)#w=".$tags[$i-3]."#".$tags[$i-2]."#".$tags[$i-1]."#".$tags[$i]."##".$toks[$i],0,"",$t) if $tagwindowwidth2 >= 3;
  }
}

sub add_contextual_ms_features {
  my ($i,$t) = @_;
  if ($tagwindowwidth >= 1) {
    add_feature("ms(-1)=".$ms[$i-1],0,"",$t) if $tagwindowwidth2 >= 1;
    add_feature("ms(-2#-1)=".$ms[$i-2]."#".$ms[$i-1],0,"",$t) if $tagwindowwidth2 >= 2;
    add_feature("ms(-3#-2#-1)=".$ms[$i-3]."#".$ms[$i-2]."#".$ms[$i-1],0,"",$t) if $tagwindowwidth2 >= 3;
    add_feature("ms(-4#-3#-2#-1)=".$ms[$i-4]."#".$ms[$i-3]."#".$ms[$i-2]."#".$ms[$i-1],0,"",$t) if $tagwindowwidth2 >= 4;
    add_feature("ms(-1)#w=".$ms[$i-1]."##".$toks[$i],0,"",$t) if $tagwindowwidth2 >= 1;
    add_feature("ms(-2#-1)#w=".$ms[$i-2]."#".$ms[$i-1]."##".$toks[$i],0,"",$t) if $tagwindowwidth2 >= 2;
    add_feature("ms(-3#-2#-1)#w=".$ms[$i-3]."#".$ms[$i-2]."#".$ms[$i-1]."##".$toks[$i],0,"",$t) if $tagwindowwidth2 >= 3;
    add_feature("ms(-4#-3#-2#-1)#w=".$ms[$i-4]."#".$ms[$i-3]."#".$ms[$i-2]."#".$ms[$i-1]."##".$toks[$i],0,"",$t) if $tagwindowwidth2 >= 4;
  }
}


sub add_special_features {
  my ($i,$t) = @_;
  for my $k (reverse ($n-1)..($i-1)) {
    if ($tags[$k] =~ /^v/i) {
      #      add_feature("lastv=".$toks[$k],0,"",$t);
      #      add_feature("lastvdist=".($i-$k),0,"",$t);
      last;
    }
    if ($tags[$k] =~ /^npp/i) {
      #      add_feature("lastnppdist=".($i-$k),0,"",$t);
      last;
    }
    if ($tags[$k] =~ /^det/i) {
#      add_feature("lastdet=".$toks[$k],0,"",$t) if $i-$k < 4;
      last;
    }
  }
}


sub add_feature {
  my ($f, $cache_type, $key, $t) = @_;
  my $fid;
  if ($filereadmode == 2) { # train
    return unless $minfeatocc == 1 || $feat2occ{$f} >= $minfeatocc;
    $feat2featid{$f} = ++$maxfeatid unless defined($feat2featid{$f});
  }
  if ($filereadmode == 1) { # pretrain
    $feat2occ{$f}++;
  } elsif ($filereadmode == 2) { # train
    $fid = $feat2featid{$f};
    $curfeats{$fid}++ if $minfeatocc == 1 || $feat2occ{$f} >= $minfeatocc;
  } elsif ($filereadmode == 3 || $filereadmode == 4 || $filereadmode == 5) {
    $fid = $feat2featid{$f};
    return unless $fid;
    if ($t eq "") {
      update_score($fid);
    } else {
      update_msscore($fid,$t);
    }
  } else {die $filereadmode}
  if ($filereadmode >= 2) {
    if ($cache_type == 1 && $token2tfeatid_cache_size < $token2tfeatid_max_cache_size) {
      push @{$token2tfeatid_cache{$key}}, $fid;
    } elsif ($cache_type == 2 && $token2ctfeatid_cache_size < $token2ctfeatid_max_cache_size) {
      push @{$token2ctfeatid_cache{$key}}, $fid;
    }
  }
}




sub read_model {
  my $modelname = shift || die;
  print STDERR "  Reading model $modelname...";
  die "ERROR: file '$modeldir/$modelname.vwarm' not found" unless -r "$modeldir/$modelname.vwarm";
  local $/ = "\n";
  open MODEL, "<$modeldir/$modelname.vwarm" || die $!;
  binmode MODEL, ":utf8";
  my $factor = 1;
  @model = ();
  %msmodel = ();
  $factor = 2**(1+int(log((scalar keys %tag2tagid)-1)/log(2))) if scalar keys %tag2tagid > 1;
  while (<MODEL>) {
    chomp;
    if (/^(\d*):(-?\d(?:\.\d+)?(?:e-\d+)?)$/) {
      $model[int($1/$factor)][$1 % $factor] = $2;
    }
  }
  close MODEL;
  if (-r "$modeldir/$modelname.vwamsrm") {
    open MODEL, "<$modeldir/$modelname.vwamsrm" || die $!;
    binmode MODEL, ":utf8";
    my $factor = 1;
    $factor = 2**(1+int(log((scalar keys %ms2msid)-1)/log(2))) if scalar keys %ms2msid > 2;
    while (<MODEL>) {
      chomp;
      if (/^(\d*):(-?\d(?:\.\d+)?(?:e-\d+)?)$/) {
	$msmodel{""}[int($1/$factor)]{$1 % $factor} = $2 unless $2 == 0;
      }
    }
    close MODEL;
  } else {
    for my $vwamsrm (<$modeldir/$modelname.*.vwamsrm>) {
      my $t = $vwamsrm;
      $t =~ s/^.*\.(.+)\.vwamsrm$/\1/ || die $t;
      open MODEL, "<$vwamsrm" || die $!;
      binmode MODEL, ":utf8";
      my $factor = 1;
      $factor = 2**(1+int(log((scalar keys %ms2msid)-1)/log(2))) if scalar keys %ms2msid > 2;
      while (<MODEL>) {
	chomp;
	if (/^(\d*):(-?\d(?:\.\d+)?(?:e-\d+)?)$/) {
	  $msmodel{$t}[int($1/$factor)]{$1 % $factor} = $2 unless $2 == 0;
	}
      }
      close MODEL;      
    }
  }
  print STDERR "done\n";
}

sub process_file {
  $filereadmode = shift;
  my $file = shift;
  my $format = shift;
  my $modelname = shift || "";
  my $datahandle;
  my $store_words;
  if ($filereadmode == 1 || $filereadmode == 2) { # pretrain || train
    $store_words = 1 unless scalar %training_corpus_words > 0;
  } elsif ($filereadmode == 3 || $filereadmode == 4) { # test || tag
    $maxtesttagid = $maxtagid;
  }
  if ($filereadmode == 2) {
    open TRAININGDATA, ">$modeldir/$modelname.trainingdata.brown";
    binmode TRAININGDATA, ":utf8";
    if ($ms_mode == 2) {
      open $mstd{""}, ">$modeldir/$modelname.mstrainingdata.brown";
      binmode $mstd{""}, ":utf8";
    } elsif ($ms_mode == 1) {
      for (keys %tag2tagid) {
	open $mstd{$_}, ">$modeldir/$modelname.mstrainingdata.$_.brown";
	binmode $mstd{$_}, ":utf8";
      }
    }
  }
  open DATA, "<$file" || $!;
  binmode DATA, ":utf8";
  local $/;
  if ($format eq "conll") {
    $/ = "\n\n";
  } else {
    $/ = "\n";
  }
  my $outline;
  my $sentid = 0;
  while (<DATA>) {
    $sentid++;
    $outline = "";
    chomp;
    next if /^DEV$/;
    die "ERROR line $sentid: sequence '__#__' is forbidden" if /__#__/;
    @toks = ();
    @rtoks = ();
    @tags = ();
    @ms = ();
    @col0 = ();
    @col2 = ();
    @col4 = ();
    @col5 = ();
    @col6ff = ();
    @spaces = ();
    @origtoks = ();
    @cmpnd_line = ();
    for (1..$n-1) {
      push @toks, "__#__";
      push @tags, "__#__";
      push @ms, "__#__";
      push @rtoks, "";
      push @col0, "";
      push @col2, "";
      push @col4, "";
      push @col5, "";
      push @col6ff, "";
      push @spaces, "";
      push @origtoks, "";
      push @cmpnd_line, "";
    }
    if ($format eq "brown") {
      s/^ +//;
      s/ +$//;
      if ($filereadmode <= 3) { # pretrain || train || test
	s/ +/ /;
	for (split / /, $_) {
	  /^(.+)\/([^\/#]+)(?:(#([^\/]*)))?$/ || die "ERROR in data (mode=$filereadmode) sentence $sentid: incorrect word/token pair '$_'";
	  push @rtoks, $1;
	  push @toks, $1;
	  if ($ms_mode) {
	    push @tags, $2;
	    push @ms, extract_relevant_subms($4);
	  } else {
	    push @tags, $2.$3;
	  }
	  $training_corpus_words{$1}++ if $store_words;
	}
      } else { # (4) tag
	my $bool = 1;
	if ($has_comments) {
	  for (split /(?<!})( +)/, $_) {
	    if ($bool) {
	      if ($has_comments) {
		if (s/^({[^{}]*}) //) {
		  push @origtoks, $1;
		} else {
		  push @origtoks, "";
		}
	      }
	      push @rtoks, $_;
	      push @toks, $_;
	    } else {
	      push @spaces, $_;
	    }
	    $bool = 1-$bool;
	  }
	} else {
	  for (split /( +)/, $_) {
	    if ($bool) {
	      push @rtoks, $_;
	      push @toks, $_;
	    } else {
	      push @spaces, $_;
	    }
	    $bool = 1-$bool;
	  }
	}
      }
    } elsif ($format eq "conll") {
      my @l;
      my $cmpnd_line;
      for (split /[\n\r]+/s, $_) {
	if (/^#/ && !/\t/) {
	  $cmpnd_line .= $_."\n" if $output_format eq "conll";
	} elsif (/^\d+-\d+\t/i) {
	  $cmpnd_line .= $_."\n";
	} elsif (!/\t/) {
	  die "STRANGE LINE= <$_>";
	} else {
	  @l = split /\t/, $_;
	  push @rtoks, $l[1];
	  push @toks, $l[1];
	  if ($is_xconll) {
	    push @tags, $l[4];
	  } else {
	    push @tags, $l[3];
	  }
	  $training_corpus_words{$l[1]}++ if $store_words;
	  push @col0, $l[0];
	  push @col2, $l[2];
	  push @col4, $l[4];
	  push @col5, $l[5];
	  push @col6ff, (join "\t", @l[6..$#l]);
	  push @ms, extract_relevant_subms($l[5]) if $ms_mode;
	  push @cmpnd_line, $cmpnd_line;
	  $cmpnd_line = "";
	}
      }
    } else {die}
    for (1..$n-1) {
      push @toks, "__#__";
      push @rtoks, "";
      push @tags, "__#__";
      push @ms, "__#__";
    }
    for (0..$#toks) {
      $toks[$_] =~ s/-LRB-/(/g;
      $toks[$_] =~ s/-RRB-/)/g;
      $toks[$_] =~ s/-LSB-/[/g;
      $toks[$_] =~ s/-RSB-/]/g;
      $toks[$_] =~ s/-LCB-/{/g;
      $toks[$_] =~ s/-RCB-/}/g;
      $toks[$_] =~ s/__HASH__/#/g;
    }
    @cursfeats = ();
    compute_sentence_level_features($_);
    for my $tid (0..$#toks) {
      next if $toks[$tid] eq "__#__";
      if ($filereadmode == 1) {
	$tag2tagid{$tags[$tid]} = ++$maxtagid unless defined $tag2tagid{$tags[$tid]};
      } elsif ($filereadmode == 2) { # train
	%curfeats = ();
	$tag2tagid{$tags[$tid]} = ++$maxtagid unless defined $tag2tagid{$tags[$tid]};
	print TRAININGDATA $tag2tagid{$tags[$tid]}; # train
	print TRAININGDATA " | ";
	if ($ms_mode) {
	  $ms2msid{$ms[$tid]} = ++$maxmsid unless defined $ms2msid{$ms[$tid]};
	  if ($ms_mode == 2) {$mstd = $mstd{""}} else {$mstd = $mstd{$tags[$tid]}}
	  print {$mstd} $ms2msid{$ms[$tid]}; # train
	  print {$mstd} " | ";
	}
      } elsif ($filereadmode == 3) { # test
	unless (defined $tag2tagid{$tags[$tid]} || defined $testtag2testtagid{$tags[$tid]}) {
	  $testtag2testtagid{$tags[$tid]} = ++$maxtesttagid;
	  $testtagid2testtag[$maxtesttagid] = $tags[$tid];
	}
	if ($ms_mode) {
	  unless (defined $ms2msid{$ms[$tid]} || defined $testms2testmsid{$ms[$tid]}) {
	    $testms2testmsid{$ms[$tid]} = ++$maxtestmsid;
	    $testmsid2testms[$maxtestmsid] = $ms[$tid];
	  }
	}
	print GOLD $tags[$tid];
	print GOLD "#".$ms[$tid] if $ms_mode;
	print GOLD "\t$rtoks[$tid]\n";
	@score = ();
	@msscore = () if $ms_mode;
      } elsif ($filereadmode == 4) { # tag
	@score = ();
	@msscore = () if $ms_mode;
      } else {die $filereadmode}
      add_feature("Constant",0,"","");
      for (@cursfeats) {
	add_feature($_,0,"","");
      }
      add_current_token_features($tid,"");
      add_contextual_token_features($tid,"");
      add_contextual_tag_features($tid,"");
      if ($lexiconfile ne "") {
	add_current_lexical_features($tid,"");
	add_contextual_lexical_features($tid,"");
      }
      add_contextual_mixed_features($tid,"");
      #add_special_features($tid,"");
      add_contextual_ms_features($tid,"");
      if ($filereadmode == 2) { # train
	print TRAININGDATA join (" ", sort keys %curfeats);
	print TRAININGDATA "\n";
      } elsif ($filereadmode >= 3) { # test || tag || conllretag
	$bestc = "";
	$maxscore = -10000000;
	for my $c (0..$maxtagid-1) {
	  if ($score[$c] > $maxscore) {
	    $maxscore = $score[$c];
	    $bestc = $c+1;
	  }
	}
	die if $bestc eq "";
	$tags[$tid] = $tagid2tag[$bestc];
      }
      if ($ms_mode) {
	add_feature("Constant",0,"",$tags[$tid]);
	add_current_token_features($tid,$tags[$tid]);
	add_contextual_token_features($tid,$tags[$tid]);
	add_contextual_tag_features($tid,$tags[$tid]);
	if ($lexiconfile ne "") {
	  add_current_lexical_features($tid,$tags[$tid]);
	  add_contextual_lexical_features($tid,$tags[$tid]);
	}
	add_contextual_mixed_features($tid,$tags[$tid]);
	#add_special_features($tid,$tags[$tid]);
	add_curtag_features($tid,$tags[$tid]);
	add_contextual_mixed_features2($tid,$tags[$tid]);
	add_contextual_ms_features($tid,$tags[$tid]);
	if ($filereadmode == 2) { # train
	  if ($ms_mode == 2) {$mstd = $mstd{""}} else {$mstd = $mstd{$tags[$tid]}}
	  print {$mstd} join (" ", sort keys %curfeats);
	  print {$mstd} "\n";
	} elsif ($filereadmode >= 3) { # test || tag || conllretag
	  $bestms = "";
	  $maxscore = -10000000;
	  for my $ms (0..$maxmsid-1) {
	    if ($msscore[$ms] > $maxscore) {
	      $maxscore = $msscore[$ms];
	      $bestms = $ms+1;
	    }
	  }
	  die if $bestms eq "";
	  $ms[$tid] = $msid2ms[$bestms];
	}
      }
      if ($filereadmode >= 3) { # test || tag || conllretag
	if ($filereadmode == 3) { # test
	  print PRED $tagid2tag[$bestc];
	  print PRED "#".$msid2ms[$bestms] if $ms_mode;
	  print PRED "\t".$maxscore."\n";
	} elsif ($filereadmode == 4) { # tag
	  if ($output_format eq "brown") {
	    if ($has_comments && $origtoks[$tid] ne "") {
	      $outline .= $origtoks[$tid]." ";
	    }
	    $outline .= $rtoks[$tid]."/".$tags[$tid];
	    $outline .= "#".$ms[$tid] if $ms_mode;
	    $outline .= ":".$maxscore if $printprobas;
	    if ($format eq "brown") {
	      $outline .= $spaces[$tid];
	    } elsif ($format eq "conll") {
	      $outline .= " ";
	      $outline .= " " unless $col6ff[$tid] =~ /spaceafter=no/i;
	    }
	  } elsif ($output_format eq "conll") { # conllretag
	    $outline .= $cmpnd_line[$tid] if $cmpnd_line[$tid] ne "";
	    $outline .= $col0[$tid]."\t".$rtoks[$tid].($printprobas ? ":".$maxscore : "")."\t".$col2[$tid]."\t".$tags[$tid]."\t".$col4[$tid]."\t".($ms_mode ? ($ms[$tid] || "_") : $col5[$tid])."\t".$col6ff[$tid]."\n";
	  }
	}
      }
    }
    print PRED "\n";
    print GOLD "\n";
    print "$outline\n" if $filereadmode >= 4; # tag || conllretag
  }
  close DATA;
  if ($filereadmode == 2) {
    close TRAININGDATA;
    if ($ms_mode == 2) {
      close $mstd{""};
    } elsif ($ms_mode == 1) {
      close $mstd{$_} for (keys %tag2tagid);
    }
  }
#  print STDERR ($cache_hits || "0")." cache hit(s)\n";
}

sub compute_sentence_level_features {
  my ($sent) = @_;
#  if (uc($sent) eq $sent) {
#    push @cursfeats, "Sau=1";
#  } else {
#    push @cursfeats, "Sau=0";
#  }
}

sub update_score {
  my ($f) = @_;
  if (defined($model[$f])) {
    for my $c (0..$maxtagid-1) {
      if (defined($model[$f][$c])) {
	$score[$c] += $model[$f][$c];
      }
    }
  }
}

sub update_msscore {
  my ($f,$t) = @_;
  if ($ms_mode == 2) {
    if (defined($msmodel{""}[$f])) {
      for my $ms (0..$maxmsid-1) {
	if (defined($msmodel{""}[$f]{$ms})) {
	  $msscore[$ms] += $msmodel{""}[$f]{$ms};
	}
      }
    }
  } elsif ($ms_mode == 1) {
    if (defined($msmodel{$t}[$f])) {
      for my $ms (0..$maxmsid-1) {
	if (defined($msmodel{$t}[$f]{$ms})) {
	  $msscore[$ms] += $msmodel{$t}[$f]{$ms};
	}
      }
    }
  }
}



sub extract_relevant_subms {
  my $fullms = shift;
  if ($ms_feature_list ne "") {
    my $new_ms = "";
    for (sort {$a cmp $b} split /\|/, $fullms) {
      if (/^$ms_feature_list_re=/) {
	$new_ms .= "|" if $new_ms ne "";
	$new_ms .= $_;
      }
    }
    return $new_ms;
  } elsif ($ms_feature_excl_list ne "") {
    my $new_ms = "";
    for (sort {$a cmp $b} split /\|/, $fullms) {
      if (!/^$ms_feature_excl_list_re=/) {
	$new_ms .= "|" if $new_ms ne "";
	$new_ms .= $_;
      }
    }
    return $new_ms;
  }
  return $fullms;
}

sub max {
  my ($a,$b) = @_;
  return $a if $a > $b;
  return $b;
}
