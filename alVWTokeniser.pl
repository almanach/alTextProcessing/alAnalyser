#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

#use alLanguageDetection;
use alRawTextCleaning;
use alURL;
use alEmail;
use alTokenisation;
use alCPunct;
use alNormalisation;
use alSmiley;

$|=1;

my $passnb=4;
my $lang = "en";
my $ell = 1;
my $split_sentences = 0;

my $maxsufflength=5;
my $maxpreflength=4;
my $maxcontextualsufflength=5;
my $maxcontextualpreflength=2;
my $tokenwindowwidth=2;
my $tokenwindowwidth2=2;
my $tagwindowwidth=4;
my $tagwindowwidth2=2;

my $no_sentence_splitting = 0;

my $array_size = 100;

my $output_format = ""; # default value
my $feats;
my (@toks, @rtoks, @tags, @ms, @col0, @col2, @col4, @col5, @col6ff, @spaces, @origtoks, @cmpnd_line);
my (%feat2featid,@featid2feat,$maxfeatid);
my @model;
my %feat2occ;
my (%curfeats,@score);
my (%token2tfeatid_cache, $token2tfeatid_cache_size, %token2ctfeatid_cache, $token2ctfeatid_cache_size, $cache_hits);
my $token2tfeatid_max_cache_size = 10000;
my $token2ctfeatid_max_cache_size = 100000;

my $force_delete_existing_model = 0;

my $modeldir;
my $modelname;
my $trainfile;
my $testfile;

my $minfeatocc = 1;

my $filereadmode;

my $command = $0." ".join(" ",@ARGV);

#MODES:
#1=pretrain
#2=train
#3=test
#4=tag

my $n = max(max($tokenwindowwidth,$tokenwindowwidth2),max($tagwindowwidth,$tagwindowwidth2));

my $maj = qr/[A-ZA-ZΑ-ΩÀÁÂÃÄÅÆÇÈÉÊËÌÍÒÓÔÖØÚÛÜÝĄĆČĎĘĚĹĽŁŃŇŒŔŘŚŠŤŮŹŻΆΈΉΊΌΎΏἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍῬὙὛὝὟὨὩὪὫὬὭὮὯΪΫЁАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЬЭЮЯ]/o;

while (1) {
  $_ = shift;
  if (/^$/) {last}
  elsif (/^--passnb$/) {$passnb = shift || die "Option '--passnb' must be followed by the number of passes"; die "'$passnb' is not a valid number of passes" unless $passnb =~ /^\d+$/}
  elsif (/^--test$/) {$testfile = shift || die "Option '--test' must be followed by a test file in Brown format"}
  elsif (/^--train$/) {$trainfile = shift || die "Option '--train' must be followed by a training file in Brown format"}
  elsif (/^(-m|--model)$/) {$modeldir = shift || die "Option '-m' must be followed by a model folder"}
  elsif (/^(-of|--output_format)$/) {$output_format = shift || die "Option '-of' or '--output_format' must be followed by a format name ('tok' or 'conll')"}
  elsif (/^--?mfo$/) {$minfeatocc = shift || die "Option '--mfo' must be followed by the minimum number of occurrences a feature must have to be used"; die "'$minfeatocc' is not a valid number of occurrences" unless $minfeatocc =~ /^\d+$/}
  elsif (/^--force_delete_existing_model$/) {$force_delete_existing_model = 1}
  elsif (/^-l$/) {$lang = shift || die "Option '-l' must be followed by a language code"}
  elsif (/^-e$/) {$ell = shift || die "Option '-e' must be followed by a valid entity normalisation level (1=default, 2, 3)"}
  elsif (/^-no_s$/) {$no_sentence_splitting = 1}
  else {die "Option '$_' unknown"}
}
$output_format = "tok" if $output_format eq "";

die "ERROR: invalid model folder name '$modeldir'" if $modeldir eq "" || $modeldir !~ /^[A-Za-z0-9\-_\.\/]+$/;
$modelname = $modeldir;
$modelname =~ s/^.*\///;
die "ERROR: invalid model folder name '$modeldir'" if $modelname eq "";

die "ERROR: option '--force_delete_existing_model' can only be used in train mode (option '--train')" if $trainfile eq "" && $force_delete_existing_model;

if ($trainfile) {
  die "ERROR: training file '$trainfile' not found" unless -r $trainfile;
}
if ($testfile) {
  die "ERROR: testing file '$testfile' not found" unless -r $testfile;
}

if ($trainfile) {
  if (-d $modeldir) {
    if ($force_delete_existing_model) {
      `rm -f $modeldir/*`;
      `rmdir $modeldir`;
    } else {
      print STDERR "ERROR: folder '$modeldir' already exists\n";
      exit;
    }
  }
  `mkdir $modeldir`;
} else {
  unless (-d $modeldir) {
    die "ERROR: model folder '$modeldir' does not exist";
  }
}

if (!$trainfile) {
  print STDERR "  Reading features...";
  my $line = 0;
  local $/;
  $/ = "\n";
  open FEATS, "<$modeldir/$modelname.vwtaf" || die $!;
  binmode FEATS, ":utf8";
  while (<FEATS>) {
    chomp;
    /^(\d+)\t(.+)$/ || die "ERROR in feature file line $line: incorrect line '$_'";
    $feat2featid{$2} = $1;
    $featid2feat[$1] = $2;
    $maxfeatid = $1 if $maxfeatid < $1;
  }
  close FEATS;
  print STDERR "done\n";
  open TCOMMAND, "<$modeldir/$modelname.train_command" || die $!;
  binmode TCOMMAND, ":utf8";
  while (<TCOMMAND>) {
    chomp;
    next unless /^OVERALL COMMAND/;
    $lang = $1 if / -l ([^ ]+)/;
    $ell = $1 if / -e ([^ ]+)/;
  }
  close TCOMMAND;
}


if ($lang =~ /^(ja|zh)(_|$)/) {
  $maxsufflength=0;
  $maxpreflength=0;
  $maxcontextualsufflength=0;
  $maxcontextualpreflength=0;
  $tokenwindowwidth=4;
  $tokenwindowwidth2=4;
  $tagwindowwidth=4;
  $tagwindowwidth2=4;
}


my ($maxscore,$bestc,$bestms);

train($trainfile,$modelname) if $trainfile;
read_model($modelname) if !$trainfile || $testfile;
test($testfile,$modelname) if $testfile;
evaluate($modelname) if $testfile;
process_file(4,"-") if !$trainfile && !$testfile; #tag

sub train {
  my ($trainfile, $modelname) = @_;
  open COMMAND, ">$modeldir/$modelname.train_command";
  binmode COMMAND, ":utf8";
  print COMMAND "OVERALL COMMAND: $command\n";
  print STDERR "OVERALL COMMAND: $command\n";
  print STDERR "TRAINING FILE: $trainfile\n";
  if ($minfeatocc > 1) {
    print STDERR "  Pre-analysing training data...";
    process_file(1,$trainfile);		#pretrain
    print STDERR "done\n";
  }
  print STDERR "  Generating training data...";
  process_file(2,$trainfile,$modelname);		#train

  open FEATS, ">$modeldir/$modelname.vwtaf" || die $!;
  binmode FEATS, ":utf8";
  for (sort {$feat2featid{$a} <=> $feat2featid{$b}} keys %feat2featid) {
    print FEATS "$feat2featid{$_}\t$_\n";
    $featid2feat[$feat2featid{$_}] = $_;
  }
  close FEATS;

  print STDERR "done\n";
  print STDERR "  Training using vowpal wabbit\n";
  my $maxtagid = 3; # 1 = no boundary; 2 = token boundary; 3 = sentence boundary
  my $vwcommand = "vw --noconstant --cache_file $modeldir/$modelname.cache_train -k --oaa $maxtagid -b ".(1+int(log((1+$maxtagid)*(1+$maxfeatid))/log(2)))." --passes $passnb --readable_model $modeldir/$modelname.vwtarm $modeldir/$modelname.trainingdata.brown";

  print COMMAND "Vowpal Wabbit command: $vwcommand\n";
  print STDERR "Vowpal Wabbit command: $vwcommand\n";
  `$vwcommand`;
  close COMMAND;
}

sub test {
  my ($testfile, $modelname) = @_;
  print STDERR "  Tagging test data '$testfile'...";
  open PRED, ">$modeldir/$modelname.testingdata.pred";
  binmode PRED, ":utf8";
  open GOLD, ">$modeldir/$modelname.testingdata.gold";
  binmode GOLD, ":utf8";
  process_file(3,$testfile,"$modeldir/$modelname");# test
  print STDERR "done\n";
  close PRED;
  close GOLD;
}

sub evaluate {
  my ($modelname) = @_;
  my ($total, $correct);
  my ($pred, $gold, $tok);
  my (%conf_mat);
  open PRED, "<$modeldir/$modelname.testingdata.pred" || die $!;
  open GOLD, "<$modeldir/$modelname.testingdata.gold" || die $!;
  binmode PRED, ":utf8";
  binmode GOLD, ":utf8";
  open DIFF, ">$modeldir/$modelname.testingdata.diff";
  binmode DIFF, ":utf8";
  local $/;
  $/ = "\n";
  while (<PRED>) {
    chomp($pred = $_);
    chomp($gold = <GOLD> || die);
    if ($gold eq "") {
      next if $pred eq "";
      die;
    }
    $gold =~ s/^([^\t]+)\t(.*)$/\1/ || die "|$gold|\n";
    $tok = $2;
    $pred =~ s/\t.*//;
    $total++;
    $correct++ if $pred eq $gold;
    print DIFF "$tok\t$gold\t$pred\n" if $pred ne $gold;
    $conf_mat{$pred}{$gold}++;
  }
  close PRED;
  close GOLD;
  close DIFF;
  open LOG, ">$modeldir/$modelname.eval_log";
  binmode LOG, ":utf8";
  print LOG "\t".$_ for (1..3);
  print LOG "\n";
  for $pred (sort {$a cmp $b} keys %conf_mat) {
    print LOG $pred;
    print LOG "\t".$conf_mat{$pred}{$_} for (1..3);
    print LOG "\n";
  }
  print LOG "  Overall accuracy: $correct/$total = ".(int(100000*$correct/$total+0.4999)/1000)."%\n";
  print STDERR "  Overall accuracy: $correct/$total = ".(int(100000*$correct/$total+0.4999)/1000)."%\n";
  close LOG;
  return ($correct, $total);
}

sub add_current_token_features {
  my ($i) = @_;
  my $w = $toks[$i % $array_size];
  my $length = length($w);
  if ($filereadmode >= 2 && defined($token2tfeatid_cache{$w})) {
    $cache_hits++;
    for (@{$token2tfeatid_cache{$w}}) {
      if ($filereadmode == 2) { # train
	$curfeats{$_}++;
      } else { # test || tag
	update_score($_);
      }
    }
  } else {
    add_feature("w=$w",1,$w);
    for my $j (1..$maxpreflength) {
      add_feature("p$j=".substr($w,0,$j),1,$w) if $length >= $j;
    }
    for my $j (1..$maxsufflength) {
      add_feature("s$j=".substr($w,-$j),1,$w) if $length >= $j;
    }
    add_feature("nb=".($w =~ /[0-9]/ ? "1" : "0"),1,$w);
    add_feature("an=".($w =~ /^[0-9\.,]+$/ ? "1" : "0"),1,$w);
    add_feature("hy=".($w =~ /-/ ? "1" : "0"),1,$w);
    add_feature("uc=".($w =~ /$maj/o ? "1" : "0"),1,$w);
    add_feature("au=".($w =~ /^$maj+$/o ? "1" : "0"),1,$w);
    add_feature("sspunct=".($w =~ /^[·\.\?\!।。؟…]$/ ? "1" : "0"),1,$w);
    add_feature("wspunct=".($w =~ /^[:;·۔]$/ ? "1" : "0"),1,$w);
  }
}


sub add_contextual_token_features {
  my ($i) = @_;
  for my $k (-$tokenwindowwidth..$tokenwindowwidth) {
    next if $k == 0;
    my $w = $toks[($i+$k) % $array_size];
    my $length = length($w);
    add_feature("w($k)=$w",0,"");
  }
  if ($tokenwindowwidth > 0) {
    add_feature("w(-1#0#1)=".$toks[($i-1)  % $array_size]."#".$toks[$i % $array_size]."#".$toks[($i+1) % $array_size],0,"");
    add_feature("w(0#1)=".$toks[$i % $array_size]."#".$toks[($i+1) % $array_size],0,"");
    add_feature("w(-1#0)=".$toks[($i-1) % $array_size]."#".$toks[$i % $array_size],0,"");
    add_feature("w(-1#1)=".$toks[($i-1) % $array_size]."#".$toks[($i+1) % $array_size],0,"");
    if ($tokenwindowwidth > 1) {
      add_feature("w(-2#-1#0)=".$toks[($i-2) % $array_size]."#".$toks[($i-1) % $array_size]."#".$toks[$i % $array_size],0,"");
      add_feature("w(0#1#2)=".$toks[$i % $array_size]."#".$toks[($i+1) % $array_size]."#".$toks[($i+2) % $array_size],0,"");
      if ($tokenwindowwidth > 2) {
	add_feature("w(-3#-2#-1#0)=".$toks[($i-3) % $array_size]."#".$toks[($i-2) % $array_size]."#".$toks[($i-1) % $array_size]."#".$toks[$i % $array_size],0,"");
	add_feature("w(0#1#2#3)=".$toks[$i % $array_size]."#".$toks[($i+1) % $array_size]."#".$toks[($i+2) % $array_size]."#".$toks[($i+3) % $array_size],0,"");
	if ($tokenwindowwidth > 3) {
	  add_feature("w(-4#-3#-2#-1#0)=".$toks[($i-4) % $array_size]."#".$toks[($i-3) % $array_size]."#".$toks[($i-2) % $array_size]."#".$toks[($i-1) % $array_size]."#".$toks[$i % $array_size],0,"");
	  add_feature("w(0#1#2#3#4)=".$toks[$i % $array_size]."#".$toks[($i+1) % $array_size]."#".$toks[($i+2) % $array_size]."#".$toks[($i+3) % $array_size]."#".$toks[($i+4) % $array_size],0,"");
      }
      }
    }
  }
  for my $k (-$tokenwindowwidth2..$tokenwindowwidth2) {
    next if $k == 0;
    my $w = $toks[($i+$k) % $array_size];
    my $length = length($w);
    my $key = "$w($k)";
    unless ($w eq "__#__") {
      if ($filereadmode >= 2 && defined($token2ctfeatid_cache{$key})) {
	$cache_hits++;
	for (@{$token2ctfeatid_cache{$key}}) {
	  if ($filereadmode == 2) { # train
	    $curfeats{$_}++;
	  } else {		# test || tag
	    update_score($_);
	  }
	}
      } else {
	$token2ctfeatid_cache_size++;
	for my $j (1..$maxcontextualpreflength) {
	  add_feature("p$j($k)=".substr($w,0,$j),2,$key) if $length >= $j;
	}
	for my $j (1..$maxcontextualsufflength) {
	  add_feature("s$j($k)=".substr($w,-$j),2,$key) if $length >= $j;
	}
	add_feature("nb($k)=".($w =~ /[0-9]/ ? "1" : "0"),2,$key);
	add_feature("an($k)=".($w =~ /^[0-9\.,]+$/ ? "1" : "0"),2,$key);
	add_feature("hy($k)=".($w =~ /-/ ? "1" : "0"),2,$key);
	add_feature("uc($k)=".($w =~ /$maj/o ? "1" : "0"),2,$key);
	add_feature("au($k)=".($w =~ /^$maj+$/o ? "1" : "0"),2,$key);
      }
    }
  }
}

sub add_contextual_tag_features {
  my ($i) = @_;
  for my $k (-$tagwindowwidth..-1) {
    next if $k == 0;
    add_feature("t($k)=".$tags[($i+$k) % $array_size],0,"");
  }
  add_feature("t(-2#-1)=".$tags[($i-2) % $array_size]."#".$tags[($i-1) % $array_size],0,"") if $tagwindowwidth2 >= 2;
  add_feature("t(-3#-2#-1)=".$tags[($i-3) % $array_size]."#".$tags[($i-2) % $array_size]."#".$tags[($i-1) % $array_size],0,"") if $tagwindowwidth2 >= 3;
  add_feature("t(-4#-3#-2#-1)=".$tags[($i-4) % $array_size]."#".$tags[($i-3) % $array_size]."#".$tags[($i-2) % $array_size]."#".$tags[($i-1) % $array_size],0,"") if $tagwindowwidth2 >= 4;
}


sub add_feature {
  my ($f, $cache_type, $key) = @_;
#  print STDERR "add_feature($f,$cache_type,$key);\n";
  my $fid;
  if ($filereadmode == 2) { # train
    return unless $minfeatocc == 1 || $feat2occ{$f} >= $minfeatocc;
    $feat2featid{$f} = ++$maxfeatid unless defined($feat2featid{$f});
  }
  if ($filereadmode == 1) { # pretrain
    $feat2occ{$f}++;
  } elsif ($filereadmode == 2) { # train
    $fid = $feat2featid{$f};
    $curfeats{$fid}++ if $minfeatocc == 1 || $feat2occ{$f} >= $minfeatocc;
  } elsif ($filereadmode == 3 || $filereadmode == 4 || $filereadmode == 5) {
    $fid = $feat2featid{$f};
    return unless $fid;
    update_score($fid);
  } else {die $filereadmode}
  if ($filereadmode >= 2) {
    if ($cache_type == 1 && $token2tfeatid_cache_size < $token2tfeatid_max_cache_size) {
      push @{$token2tfeatid_cache{$key}}, $fid;
    } elsif ($cache_type == 2 && $token2ctfeatid_cache_size < $token2ctfeatid_max_cache_size) {
      push @{$token2ctfeatid_cache{$key}}, $fid;
    }
  }
}




sub read_model {
  my $modelname = shift || die;
  print STDERR "  Reading model $modelname...";
  die "ERROR: file '$modeldir/$modelname.vwtarm' not found" unless -r "$modeldir/$modelname.vwtarm";
  local $/ = "\n";
  open MODEL, "<$modeldir/$modelname.vwtarm" || die $!;
  binmode MODEL, ":utf8";
  my $factor = 1;
  $factor = 2**(1+int(log(3-1)/log(2)));
  while (<MODEL>) {
    chomp;
    if (/^(\d*):(-?\d(?:\.\d+)?(?:e-\d+)?)$/) {
      $model[int($1/$factor)][$1 % $factor] = $2;
    }
  }
  close MODEL;
  print STDERR "done\n";
}

sub process_file {
  $filereadmode = shift;
  my $file = shift;
  my $modelname = shift || "";
  my $datahandle;
  if ($filereadmode == 2) {
    open TRAININGDATA, ">$modeldir/$modelname.trainingdata.brown";
    binmode TRAININGDATA, ":utf8";
  }
  open DATA, "<$file" || $!;
  binmode DATA, ":utf8";
  my $outline;
  my $sentid = 0;
  @toks = (); # no initialisation between lines
  @rtoks = (); # no initialisation between lines
  @tags = (); # no initialisation between lines
  @spaces = (); # no initialisation between lines
  my $curtid = 0;
  my $workingtid = 0;
  my $index = 0;
  for (1..$n) {
    $toks[$index] = "__#__";
    $rtoks[$index] = "__#__";
    $tags[$index] = "__#__";
    $spaces[$index] = -1;
    $curtid++;
    $index = $curtid % $array_size;
  }
  while (<DATA>) {
    $sentid++;
    chomp;
    next if /^DEV$/;
    die "ERROR line $sentid: sequence '__#__' is forbidden" if /__#__/;
    s/^ +//;
    s/ +$//;
    $_ = tokenise($_);
    die "|$_|" if /    +/;
    s/\\([^  ])/$1/g;
    s/^ +//;
    s/ +$//;
    if ($filereadmode <= 3) { # pretrain || train || test
      # in this case, the input was already (gold-)tokenised and we (automatically) retokenised it
      # so we have one more space than in a normal tokenised file
      # because the input comes from a CoNLL-U file, we can also have non-breaking spaces, which mean space-inside-a-token
      # gold token boundary = 1 or 2 spaces (depending on the existence of a space in the raw text), hence now 2 or 3 spaces
      # gold non-token-boundary = 0 spaces, which can have been kept as such or replaced by a single space by the automatic tokeniser: we must learn to recognise such cases as non-token boundaries
      my $bool = 1;
      for (split /( +| )/, $_) {
	if ($bool) {
	  $rtoks[$index] = $_;
	  $toks[$index] = normalise_token($_);
	} else {
	  if ($_ eq " ") {$tags[$index] = 1}
	  elsif ($_ eq " ") {$tags[$index] = 1}
	  elsif ($_ eq "  ") {$tags[$index] = 2}
	  elsif ($_ eq "   ") {$tags[$index] = 2}
	  else {die "|$_|"}
	  if ($_ eq " ") {$spaces[$index] = 1}
	  elsif ($_ eq " ") {$spaces[$index] = 0}
	  elsif ($_ eq "  ") {$spaces[$index] = 0}
	  elsif ($_ eq "   ") {$spaces[$index] = 1}
	  else {die}
	  process_token($workingtid);
	  $curtid++;
	  $workingtid++;
	  $index = $curtid % $array_size;
	}
	$bool = 1-$bool;
      }
      die if $bool;
      $tags[$index] = 3;
      process_token($workingtid);
      $curtid++;
      $workingtid++;
      $index = $curtid % $array_size;
    } else { # (4) tag
      my $bool = 1;
      for (split /( +)/, $_) {
	if ($bool) {
	  $rtoks[$index] = $_;
	  $toks[$index] = normalise_token($_);
	} else {
	  if ($_ eq " ") {$spaces[$index] = 0}
	  elsif ($_ eq "  ") {$spaces[$index] = 1}
	  else {die}
	  process_token($workingtid);
	  $curtid++;
	  $workingtid++;
	  $index = $curtid % $array_size;
	}
	$bool = 1-$bool;
      }
      if ($bool == 0) { # standard line (not empty line)
	$spaces[$index] = 1;
	process_token($workingtid);
	$curtid++;
	$workingtid++;
	$index = $curtid % $array_size;
      }
      if ($bool #empty line, paragraph change => sentence boundary
	  || $no_sentence_splitting) { # every line ends with a sentence boundary
	$tags[($curtid-1) % $array_size] = -3;
      }
    }
  }
  for (1..$n) {
    $toks[$index] = "__#__";
    $rtoks[$index] = "";
    $tags[$index] = "__#__";
    $spaces[$index] = "";
    process_token($workingtid);
    $curtid++;
    $workingtid++;
    $index = $curtid % $array_size;
  }
  print PRED "\n";
  print GOLD "\n";
  $outline .= "\n" if $filereadmode >= 4 && $outline !~ /\n$/gs && $outline ne "";
  print "$outline" if $filereadmode >= 4; # tag

  close DATA;
  if ($filereadmode == 2) {
    close TRAININGDATA;
  }

  sub normalise_token {
    my $curtok = shift;
    $curtok =~ s/-LRB-/(/g;
    $curtok =~ s/-RRB-/)/g;
    $curtok =~ s/-LSB-/[/g;
    $curtok =~ s/-RSB-/]/g;
    $curtok =~ s/-LCB-/{/g;
    $curtok =~ s/-RCB-/}/g;
    $curtok =~ s/__HASH__/#/g;
    return $curtok;
  }
  
  sub process_token {
    my $tid = shift;
    return if $toks[$tid % $array_size] eq "__#__";
    if ($filereadmode == 1) { # pretrain
    } elsif ($filereadmode == 2) { # train
      %curfeats = ();
      print TRAININGDATA $tags[$tid % $array_size]; # train
      print TRAININGDATA " | ";
    } elsif ($filereadmode == 3) { # test
      print GOLD $tags[$tid % $array_size];
      print GOLD "\t".$rtoks[$tid % $array_size]."\n";
      @score = ();
    } elsif ($filereadmode == 4) { # tag
      @score = ();
    } else {die $filereadmode}
    add_feature("Constant",0,"");
    add_feature("SP=".$spaces[$tid % $array_size],0,"");
    add_current_token_features($tid,"");
    add_contextual_token_features($tid,"");
    add_contextual_tag_features($tid,"");
    if ($filereadmode == 2) { # train
      print TRAININGDATA join (" ", sort keys %curfeats);
      print TRAININGDATA "\n";
    } elsif ($filereadmode >= 3) { # test || tag
      if ($tags[$tid % $array_size] < 0) {
	$tags[$tid % $array_size] = -$tags[$tid % $array_size];
      } else {
	$bestc = "";
	$maxscore = -10000000;
	for my $c (0..3-1) {
	  if ($score[$c] > $maxscore) {
	    $maxscore = $score[$c];
	    $bestc = $c+1;
	  }
	}
	die if $bestc eq "";
	$bestc = 2 if $no_sentence_splitting && $bestc == 3;
	$tags[$tid % $array_size] = $bestc;
      }
    }
    if ($filereadmode >= 3) { # test || tag
      if ($filereadmode == 3) { # test
	print PRED $bestc;
	print PRED "\t".$maxscore."\n";
      } elsif ($filereadmode == 4) { # tag
	if ($output_format eq "tok") {
	  $outline .= $rtoks[$tid % $array_size];
	  if ($tags[$tid % $array_size] == 1) {
	    if ($spaces[$tid % $array_size]) {
	      $outline .= " ";
	    }
	  } elsif ($tags[$tid % $array_size] == 2) {
	    $outline .= " ";
	    if ($spaces[$tid % $array_size]) {
	      $outline .= " ";
	    }
	  } elsif ($tags[$tid % $array_size] == 3) {
	    $outline .= "\n";
	    print $outline;
	    $outline = "";
	  }
	} elsif ($output_format eq "conll") {
	  die "TODO";
	}
      }
    }
  }
}

sub update_score {
  my ($f) = @_;
  if (defined($model[$f])) {
    for my $c (0..3-1) {
      if (defined($model[$f][$c])) {
	$score[$c] += $model[$f][$c];
      }
    }
  }
}


sub tokenise {
  my $s = shift;
  $s = altok_escape_metacharacters($s);
  $s = altok_escape_xml($s);
  $s = altok_tokenise($s, $lang);
  $s = altok_normalise($s, $lang);
  $s = altok_URL($s, $lang);
  $s = altok_smiley($s);
  $s = altok_remove_embedded_contents($s);
  $s = altok_detokenise_multiple_punctuations($s);
  $s = altok_remove_embedded_contents($s);
  $s = altok_retokenise_lexical_punctuations($s, $lang);
  $s = altok_remove_embedded_contents($s);
  $s = altok_detokenise_entity_content($s, 1);
  $s = altok_remove_embedded_contents($s);
  if ($ell == 2) {
    $s = altok_partly_remove_entities($s, 1);
  } elsif ($ell == 3) {
    $s = altok_remove_entities($s, 1);
  }
  $s = altok_restore_original_tokens($s, 1);
  return $s;
}

sub max {
  my ($a,$b) = @_;
  return $a if $a > $b;
  return $b;
}
