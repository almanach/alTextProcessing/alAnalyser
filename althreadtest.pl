#!/usr/bin/perl

use threads;
use Thread::Queue;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use strict;

my $queue = Thread::Queue->new();
my $queue2 = Thread::Queue->new();
my $thr = threads->create(sub {
			    while ($_=$queue->dequeue()) {
			      $queue2->enqueue($_);
			    }
			    $queue2->enqueue(undef);
			  });
my $thr2 = threads->create(sub {
			    while ($_=$queue2->dequeue()) {
			      print uc($_)."\n";
			    }
			  });

while (<>) {
  chomp;
  $queue->enqueue($_);
}
$queue->enqueue(undef);
$thr->join();
$thr2->join();

